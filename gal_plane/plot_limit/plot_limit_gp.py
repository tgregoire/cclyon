#!/usr/local/bin/python

# UNCOMMENT THE LINES BELOW IF YOU WANT XKCD STYLE
#import matplotlib
# matplotlib.use('QT4Agg')
import matplotlib.pyplot as plt
from matplotlib import rc, rcParams
# plt.xkcd()
import numpy as np
import healpy as hp
import pyfits
import os
import datetime
from scipy import optimize
from scipy import interpolate
from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.interpolate import interp1d

start = datetime.datetime.now()

# activate latex text rendering
rc('text', usetex=True)
# Change all fonts to 'Computer Modern'
rc('font', **{'family': 'serif', 'serif': ['Computer Modern Roman']})
rc('xtick', labelsize=18)
rc('ytick', labelsize=18)
rcParams['legend.numpoints'] = 1

GeV_erg = 0.00160217657

# ------------------------  EDIT HERE -----------------------------------------------------
windows_l = [0., 40.]  # Galactic longitudes of each window; latitude is from -5 to 5
windows_b = [0., 3.]
correction = 1.  # 0.3
# -----------------------------------------------------------------------------------------


#model_dir      =       '/Users/marino/Neutrino-astronomia/KRA-gamma/output/'
#model_names    =      ['settings_ferriere_HESS_KRAgamma_Ferriere_NOspike_hardening_Ferriere_Ferriere_healpix.fits.gz','settings_ferriere_HESS_KRA_Ferriere_NOspike_hardening_Ferriere_Ferriere_healpix.fits.gz']


model_dir = '/sps/km3net/users/tgregoir/gal_plane/models/'
model_names = ['settings_neutrino_KRA_hardening_5e6_neutrino_COGalprop_HIGalprop_healpix.fits.gz', 'settings_neutrino_KRAgamma_hardening_5e6_neutrino_COGalprop_HIGalprop_healpix.fits.gz', 'settings_neutrino_KRA_hardening_5e7_neutrino_COGalprop_HIGalprop_healpix.fits.gz', 'settings_neutrino_KRAgamma_hardening_5e7_neutrino_COGalprop_HIGalprop_healpix.fits.gz']

linestyles  = ['-', '-']
colors_pi0  = ['green', 'darkGreen']
colors_IC   = ['blue', 'blue']
colors_brems = ['red', 'red']
colors      = ['black', 'grey']
print len(model_names)
print len(linestyles)
print len(colors)

datadirFermi = model_dir

datadirIceCube  = model_dir
IceCubefile = datadirIceCube + "ICECUBE_Ridge_from_plot.txt"
ANTARES_UL_file = datadirIceCube + "ANTARES_UL.txt"

plotdir      = '/sps/km3net/users/tgregoir/gal_plane/plot_limit/'
#correction = 0.75


#####################################################
def pixnumrectangle_symmetric(nside, lowb_North, highb_North, lowb_South, highb_South, lowl, highl):

    # moving to radiants
    lowb_North = lowb_North / 180. * np.pi
    lowb_South = lowb_South / 180. * np.pi
    lowl = lowl / 180. * np.pi
    highb_North = highb_North / 180. * np.pi
    highb_South = highb_South / 180. * np.pi
    highl = highl / 180. * np.pi
    npix = 12 * nside**2
    listpix = np.arange(npix)
    theta, phi = hp.pixelfunc.pix2ang(nside, listpix)
    b = np.pi / 2. - theta
    l = phi
    mask = []
    for i in np.arange(npix):
        if(l[i] > np.pi):
            l[i] -= 2. * np.pi
        if((((b[i] >= lowb_North) and (b[i] <= highb_North)) or ((b[i] >= lowb_South) and (b[i] <= highb_South))) and ((l[i] >= lowl) and (l[i] <= highl) or (l[i] >= -highl) and (l[i] <= -lowl))):
            mask.append(1)
        else:
            mask.append(0)
    return mask
#####################################################

#####################################################


def pixel_circle(nside_, npix_, lowr, highr):

    # moving to radiants
    lowr  = lowr / 180. * np.pi
    highr = highr / 180. * np.pi
    listpix = np.arange(npix_)
    theta, phi = hp.pixelfunc.pix2ang(nside_, listpix)
    b = np.pi / 2. - theta
    l = phi
    mask = []
    for i in np.arange(npix_):
        if(l[i] > np.pi):
            l[i] -= 2. * np.pi
        radius = np.sqrt(l[i]**2. + b[i]**2.)
        # if(b[i] >= -2./180.*np.pi and b[i] <= 2./180.*np.pi):
        #    radius = 0.
        if ((radius >= lowr) and (radius < highr)):
            mask.append(1)
        else:
            mask.append(0)
    return mask
#####################################################

#####################################################


def find_map_process(filename_, process, npix_, dimE):
    hdulist = pyfits.open(filename_)
    print "*** we are looking for: ", process, "; reading map..."
    map_ = np.zeros((npix_, dimE))
    for indexProcess in range(1, len(hdulist)):
        current_header = hdulist[indexProcess].header
        current_process_name = current_header['PROCESS']
        print "current HDU contains: ", current_process_name
        if (current_process_name == process):
            current_hdu = hdulist[indexProcess].data
            #      map_ = current_hdu
            for i in range(0, npix_):
                for ie in range(0, dimE):
                    map_[i][ie] = current_hdu[ie][i]
    print "ok!"
    hdulist.close
    return map_
#####################################################

#####################################################


def find_map_process_alfredo_format(filename, process, npix, dimE):
    hdulist = pyfits.open(filename)
    print "*** looking into: ", filename
    print "*** we are looking for: ", process, "; reading map..."
    map_ = np.zeros((npix, dimE))
    print "(dimE, npix) = ", dimE, npix
    for indexProcess in range(1, len(hdulist)):
        current_header = hdulist[indexProcess].header
        current_process_name = current_header['PROCESS']
        print "current HDU contains: ", current_process_name
        if (current_process_name == process):
            current_hdu = hdulist[indexProcess].data
            print "current HDU shape = ", current_hdu.shape
            #      map_ = current_hdu
            # print ie
            for ie in range(0, dimE):
                for i in range(0, npix):
                    map_[i][ie] = current_hdu[ie][i]
    print "ok!"
    hdulist.close
    return map_
#####################################################


# ------------------------------------------------------------------------------------------
print "Initializing plot..."
# ------------------------------------------------------------------------------------------


font = {'family': 'fantasy',
        'color': 'Black',
        'weight': 'bold',
        'size': 22,
        }
font2 = {'family': 'fantasy',
         'weight': 'bold',
         'size': 17.5,
         }
fig = plt.figure(figsize=(9., 7.6))  # create a figure object
ax = fig.add_subplot(1, 1, 1)  # create an axes object in the figure
for axis in ['top', 'bottom', 'left', 'right']:
    ax.spines[axis].set_linewidth(1.5)
ax.minorticks_on()
ax.tick_params('both', length=10, width=1.4, which='major')
ax.tick_params('both', length=5, width=1.2, which='minor')
plt.xscale('log')
plt.yscale('log')
#plt.title('Neutrino and Gamma-ray vs ANTARES Upper limits $|l|<40^{\circ}$ $|b|<3^{\circ}$',family='fantasy',color='Black',size=22.5,y=1.02)
# plt.axis([.0010,50.,1.e-7,1.e-3],interpolation='none')
# plt.xlabel(r'$E\,[TeV\,]$',fontsize=20)
#plt.ylabel(r'$E^2 \,d\Phi / dE_{\gamma}  /d{\Omega} \, {\rm [GeV cm^{-2} s^{-1} sr^{-1}]} $ ',fontsize=20)
plt.axis([1.e-2, 1.e4, 1.e-8, 2.e-4], interpolation='none')
plt.xlabel(r'$E\,[TeV\,]$', fontsize=20)
plt.ylabel(r'$E^2 d\Phi / dE d\Omega \, {\rm [GeV cm^{-2} s^{-1} sr^{-1}]} $ ', fontsize=20)
xbar = (0.01, 0.1, 1., 10., 100., 1000., 10000.)
plt.xticks(xbar, (r'$10^{-2}$', r'$10^{-1}$', r'$10^0$', r'$10^1$', r'$10^2$', r'$10^{3}$', r'$10^4$'), family='fantasy', color='Black', size=20)
ybar = (1.e-8, 1.e-7, 1.e-6, 1.e-5, 1.e-4)
plt.yticks(ybar, (r'$10^{-8}$', r'$10^{-7}$', r'$10^{-6}$', r'$10^{-5}$', r'$10^{-4}$'), family='fantasy', color='Black', size=20)

print
print "Fermi PS   file"
print

PS_file = "/sps/km3net/users/tgregoir/gal_plane/models/MTgll_psc_v14_heal_NSIDE1024test.fits"


hdulistCounts = pyfits.open(PS_file)
# print
# print hdulistCounts[1].header
# print
#
hdulistCounts.info()
countData = hdulistCounts[1].data
# print countData
#
nside_data  = hdulistCounts[1].header['NSIDE']
print "nside = ", nside_data
npix_data   = countData.field(0).size
print "npix = ", npix_data
Pix_Solid_Angle_data = 4. * np.pi / npix_data
dimE_data = 31  # 31 CHECK! better to read this from FITS...
Emin_data = 3.e2 / 1000.  # in MeV ;CHECK! better to read this from FITS... /1000. now in GeV
Emax_data = 3.e5 / 1000.  # in MeV ;CHECK! better to read this from FITS... /1000. now in GeV
E_factor_data = pow((Emax_data / Emin_data), 1. / (dimE_data - 1))
map_ps = np.zeros((npix_data, dimE_data))
for iE in xrange(0, dimE_data - 1):
    map_ps[:, iE] = countData.field(iE)
# print ps_flux


current_spectrum_PS = np.zeros(dimE_data - 1)
bmin_south = -windows_b[1]
bmax_south = -windows_b[0]
bmin_north = windows_b[0]
bmax_north = windows_b[1]
print "Creating DATA sky window from l = ", windows_l[0], " to l = ", windows_l[1], " and  from b = ", bmin_north, " to b = ", bmax_north
current_mask_data = pixnumrectangle_symmetric(nside_data, bmin_north, bmax_north, bmin_south, bmax_south, windows_l[0], windows_l[1])

E_geo_mean_PS = np.zeros(dimE_data - 1)
E_vec_data_PS = np.zeros(dimE_data)

for ien in xrange(0, dimE_data):
    E_vec_data_PS[ien] = Emin_data * pow(E_factor_data, ien)
for ien in xrange(0, dimE_data - 1):
    E_geo_mean_PS[ien] = np.sqrt(E_vec_data_PS[ien] * E_vec_data_PS[ien + 1])


for ip in xrange(0, dimE_data - 1):
    npix = 0
    for ipix in np.arange(npix_data):
        if (current_mask_data[ipix] == 1):
            npix += 1
            current_spectrum_PS[ip] += (map_ps[ipix, ip])  # / 1000.  #  cm^-2 s^-1 sr^-1 --> TeV cm^-2 s^-1 sr^-1

    current_spectrum_PS[ip] /= npix  # cm^-2 s^-1 sr^-1 GeV-1
# print E_geo_mean_PS[ip],' ',current_spectrum_PS[ip]
    # Put in GeV cm-2 sr-1 sr-1
    current_spectrum_PS[ip] = current_spectrum_PS[ip] * E_geo_mean_PS[ip] * E_geo_mean_PS[ip]
    #Put in TeV
    E_geo_mean_PS[ip] = E_geo_mean_PS[ip] / 1000.
    current_spectrum_PS[ip] = current_spectrum_PS[ip] / 1000.


# CREATE INTERPOLATING FUNCTION (in case I want to extrapolate outside the range of integration)

log_energy = np.log10(E_geo_mean_PS)
log_dnde = np.log10(current_spectrum_PS)
log_interp = InterpolatedUnivariateSpline(log_energy, log_dnde)
spectrumPS = lambda e: np.nan_to_num(10**(log_interp(np.log10(e))))


# ------------------------------------------------------------------------------------------
print "Reading Fermi data..."
# ------------------------------------------------------------------------------------------


counts_file   = datadirFermi  + "lat_clean_frontback_healpix.fits"
exposure_file = datadirFermi  + "MTexposure_clean.fits"
#ps_counts_file = np.loadtxt(datadirFermi+"Smoothed_PS_Counts_Map.dat")

print
print "Fermi PASS 8 count file"
print
hdulistCounts = pyfits.open(counts_file)
# print
# print hdulistCounts[1].header
# print
#
hdulistCounts.info()
countData = hdulistCounts[1].data
# print countData
#
nside_data  = hdulistCounts[1].header['NSIDE']
print "nside = ", nside_data
npix_data   = countData.field(0).size
print "npix = ", npix_data
Pix_Solid_Angle_data = 4. * np.pi / npix_data
dimE_data = 31  # 31 CHECK! better to read this from FITS...
Emin_data = 3.e2 / 1000.  # in MeV ;CHECK! better to read this from FITS... /1000. now in GeVqsatt
Emax_data = 3.e5 / 1000.  # in MeV ;CHECK! better to read this from FITS... /1000. now in GeV
E_factor_data = pow((Emax_data / Emin_data), 1. / (dimE_data - 1))

import datetime
counts_file = np.zeros((npix_data, dimE_data))
for iE in xrange(0, dimE_data - 1):
    counts_file[:, iE] = countData.field(iE)
end = datetime.datetime.now()
print counts_file

print
print "Fermi PASS 8 exposure file"
hdulistExp = pyfits.open(exposure_file)
hdulistExp.info()
exposureData = hdulistExp[1].data
print
exposure_file = np.zeros((npix_data, dimE_data))
for iE in xrange(0, dimE_data - 1):
    exposure_file[:, iE] = exposureData.field(iE)
print counts_file

E_vec_data = np.zeros(dimE_data)
E_geo_mean = np.zeros(dimE_data - 1)
E_vec_data_TeV = np.zeros(dimE_data)
E_geo_mean_TeV = np.zeros(dimE_data - 1)
for ien in xrange(0, dimE_data):
    E_vec_data[ien] = Emin_data * pow(E_factor_data, ien)
    E_vec_data_TeV[ien] = Emin_data * pow(E_factor_data, ien) / 1000.
for ien in xrange(0, dimE_data - 1):
    E_geo_mean[ien] = np.sqrt(E_vec_data[ien] * E_vec_data[ien + 1])
    E_geo_mean_TeV[ien] = np.sqrt(E_vec_data[ien] * E_vec_data[ien + 1]) / 1000.
bmin_south = -windows_b[1]
bmax_south = -windows_b[0]
bmin_north = windows_b[0]
bmax_north = windows_b[1]
counts_spectrum     = np.zeros(dimE_data - 1)
exposure_spectrum   = np.zeros(dimE_data - 1)
spectrum_data       = np.zeros(dimE_data - 1)
spectrum_data_error = np.zeros(dimE_data - 1)
print "Creating DATA sky window from l = ", windows_l[0], " to l = ", windows_l[1], " and  from b = ", bmin_north, " to b = ", bmax_north
current_mask_data = pixnumrectangle_symmetric(nside_data, bmin_north, bmax_north, bmin_south, bmax_south, windows_l[0], windows_l[1])
#
for ie in range(dimE_data - 1):
    print "--> energy bin number = ", ie
    # bin_width = (E_vec_data[ie+1] - E_vec_data[ie])*GeV_erg #erg
    bin_width = (E_vec_data[ie + 1] - E_vec_data[ie])
    bin_width_TeV = (E_vec_data[ie + 1] - E_vec_data[ie]) / 1000.
    # photon_energy2_erg = (np.sqrt(E_vec_data[ie+1]*E_vec_data[ie])*GeV_erg)**2. #erg^2
    counter = 0
    for ipixel in range(npix_data):
        if(current_mask_data[ipixel] == 1):
            counts = counts_file[ipixel, ie]
            if (counts < 0.):
                print "warning: negative count found"
                counts = 0.
            counter += 1
            counts_spectrum[ie] += counts
            #current_sp = photon_energy2_erg * counts_file[ipixel,ie] / ( exposure_file[ipixel,ie] * bin_width * Pix_Solid_Angle_data )
            current_sp = counts / (exposure_file[ipixel, ie] * bin_width_TeV * Pix_Solid_Angle_data)
            # print counts, exposure_file[ipixel,ie], bin_width_TeV, Pix_Solid_Angle_data, current_sp
            spectrum_data[ie] += current_sp
    if (counter != 0):
        spectrum_data[ie] = spectrum_data[ie] / counter
        spectrum_data_error[ie]  = np.sqrt(1. / (counts_spectrum[ie])) * spectrum_data[ie]
    print "number of pixel in the window: ", counter
    print "number of counts in the window: ", counts_spectrum[ie]
    print "relative error: ", np.sqrt(1. / (counts_spectrum[ie]))
    print "Egeomean TeV", E_geo_mean_TeV[ie]
    print "E^2 flux: GeV cm-2 s-1 sr-1 ", spectrum_data[ie] * (E_geo_mean_TeV[ie])**2. * 1000.
    print "flux error: GeV cm-2 s-1 sr-1", spectrum_data_error[ie] * (E_geo_mean_TeV[ie])**2. * 1000.

#
print "Fermi-LAT data"
for iE in xrange(dimE_data - 1):
    print E_geo_mean_TeV[iE], "TeV\t", spectrum_data[iE], "\t", spectrum_data_error[iE]
#fermi, = plt.plot(E_geo_mean_TeV, E_geo_mean_TeV*E_geo_mean_TeV*spectrum_data, 'ro', color='red', ms=9)
#plt.errorbar(E_geo_mean_TeV, E_geo_mean_TeV*E_geo_mean_TeV*spectrum_data, yerr=(spectrum_data_error*E_geo_mean_TeV*E_geo_mean_TeV), fmt=None, ecolor='red')


# print "Fermi-LAT data subtracted from PS"
# MT subtract PS
spectrum_data_subPS  = np.zeros(dimE_data - 1)
newspec = np.zeros(dimE_data - 1)
for iE in xrange(dimE_data - 1):
    spectrum_data_subPS[iE] = E_geo_mean_TeV[iE] * E_geo_mean_TeV[iE] * spectrum_data[iE] - spectrumPS(E_geo_mean_TeV[iE])
    print E_geo_mean_TeV[iE], " ", E_geo_mean_TeV[iE] * E_geo_mean_TeV[iE] * spectrum_data[iE], spectrum_data_subPS[iE]

# Error does not change!!!
newspec = spectrum_data_subPS * 1000.
fermi, = plt.plot(E_geo_mean_TeV, spectrum_data_subPS * 1000., 'ro', color='red', ms=6)
#plt.errorbar(E_geo_mean_TeV, spectrum_data_subPS, yerr=(spectrum_data_error*E_geo_mean_TeV*E_geo_mean_TeV), fmt=None, ecolor='red')
plt.errorbar(E_geo_mean_TeV, spectrum_data_subPS * 1000., yerr=(1000. * spectrum_data_error * E_geo_mean_TeV * E_geo_mean_TeV), fmt=None, ecolor='red')

# print "Fermi-LAT data subtracted from PS"
# MT subtract PS
#spectrum_data_subPS       = np.zeros(dimE_data-1)
# for iE in xrange(dimE_data-1):
#    spectrum_data_subPS[iE]=E_geo_mean[iE]*E_geo_mean[iE]*spectrum_data[iE]- spectrumPS(E_geo_mean[iE])
#    print E_geo_mean[iE]," ",E_geo_mean[iE]*E_geo_mean[iE]*spectrum_data[iE], spectrum_data_subPS[iE]


#fermi, = plt.plot(E_geo_mean, spectrum_data_subPS, 'ro', color='red', ms=9)
#plt.errorbar(E_geo_mean, spectrum_data_subPS, yerr=(spectrum_data_error*E_geo_mean*E_geo_mean), fmt=None, ecolor='red')

# FERMI data fit
log10E = np.log10(E_geo_mean)  # GeV
#log10E = np.log10( E_geo_mean_TeV )
#current_spectrum_total = newspec
current_spectrum_total = spectrum_data_subPS * 100
#current_spectrum_total = (newspec*E_geo_mean_TeV*E_geo_mean_TeV)
# current_spectrum_total = ( newspec*E_geo_mean_TeV*E_geo_mean_TeV) # erg cm^-2 s^-1 sr^-1
#  current_spectrum_total /= GeV_erg  # GeV cm^-2 s^-1 sr^-1
current_spectrum_total /= ((E_geo_mean)**2.)
print current_spectrum_total.shape
print E_geo_mean.shape
logSpectrum = np.log10(current_spectrum_total)
fitfunc = lambda p, x: p[0] + p[1] * x
errfunc = lambda p, x, y: (y - fitfunc(p, x))
pinit = [1.e-1, 1.e-4]
out = optimize.leastsq(errfunc, pinit, args=(log10E, logSpectrum), full_output=1)
pfinal = out[0]
covar = out[1]
print pfinal
print covar
norm = 10.0**pfinal[0]
slope = pfinal[1]
print "norm = ", norm, " GeV^-1 cm^2 s^-1 sr^-1"
print "slope = ", slope
current_spectrum_total_fitted = norm * E_geo_mean**slope  # GeV^-1 cm^-2 s^-1 sr^-1
print current_spectrum_total_fitted
#spec_total_fitted, = plt.plot(E_geo_mean_TeV, (current_spectrum_total_fitted)*(E_geo_mean_TeV**2.), ls='--' , color="grey", lw=2.8)
#spec_total_fitted, = plt.plot(E_geo_mean, (current_spectrum_total_fitted), ls='--' , color="grey", lw=2.8)


#spec_total_fitted, = plt.plot(E_geo_mean, (current_spectrum_total_fitted)*(E_geo_mean**2.), ls='--' , color="grey", lw=2.8)


# ------------------------------------------------------------------------------------------
print "Reading and integrating models..."
# ------------------------------------------------------------------------------------------
print

for i in xrange(len(model_names)):

    filename = model_dir + model_names[i]

    print "++++++++++++++++++++++++++++"
    print "Now reading ", filename
    print "this is model ", i + 1, " out of ", len(model_names)
    print "++++++++++++++++++++++++++++"

    # pi0, brems -----------------------------------------------------------------------------

    print "Reading hadronic FITS header..."
    hdulist = pyfits.open(filename)
    n_ext = len(hdulist)

    first_hdu = hdulist[0]
    first_hdu_header = first_hdu.header
    nside = first_hdu_header['nside']
    npix_total = 12 * nside ** 2
    Pix_Solid_Angle = 4. * np.pi / npix_total
    dimE = first_hdu_header['nE']
    Emin = first_hdu_header['Emin']
    Emax = first_hdu_header['Emax']
    E_factor = first_hdu_header['Efactor']
    print "nside, npix, dimE, Emin, Emax, E_factor"
    print nside
    print npix_total
    print dimE
    print Emin
    print Emax
    print E_factor

    E_vec = np.zeros(dimE)
    E_vec_TeV = np.zeros(dimE)
    for ie in xrange(0, dimE):
        E_vec[ie] = Emin * pow(E_factor, ie)
        E_vec_TeV[ie] = E_vec[ie] / 1000.

    hdulist.close

    map_nu = np.zeros((npix_total, dimE))
    map_nu[:, :] = find_map_process(filename, 'nu', npix_total, dimE)
    map_nubar = np.zeros((npix_total, dimE))
    map_nubar[:, :] = find_map_process(filename, 'nubar', npix_total, dimE)

    print "nside, dimE, Emin, Emax, E_factor"
    print nside
    print dimE
    print Emin
    print Emax
    print E_factor
    # print E_vec

    bmin_south = -windows_b[1]
    bmax_south = -windows_b[0]
    bmin_north = windows_b[0]
    bmax_north = windows_b[1]

    current_mask = pixnumrectangle_symmetric(nside, bmin_north, bmax_north, bmin_south, bmax_south, windows_l[0],
                                             windows_l[1])
    current_spectrum_nu = np.zeros(dimE)
    current_spectrum_nubar = np.zeros(dimE)
    current_spectrum_total = np.zeros(dimE)
    spectrum_BF = np.zeros(dimE)

    print "Integraging neutrinos... "

    for ip in xrange(0, dimE):

        if (ip % 10 == 0):
            print "--> energy bin number = ", ip, "E = ", E_vec[ip]  # / 1000., "GeV"
        npix = 0
        #
        for ipix in np.arange(npix_total):
            if (current_mask[ipix] == 1):
                npix += 1
                current_spectrum_nu[ip] += correction * (
                    map_nu[ipix, ip])  # / 1000.  # GeV cm^-2 s^-1 sr^-1 --> TeV cm^-2 s^-1 sr^-1
                current_spectrum_nubar[ip] += (
                    map_nubar[ipix, ip])  # / 1000.  # GeV cm^-2 s^-1 sr^-1 --> TeV cm^-2 s^-1 sr^-1
        #
        if (ip % 10 == 0):
            print "npix = ", npix, " out of ", npix_total

        current_spectrum_nu[ip] /= npix  # TeV cm^-2 s^-1 sr^-1
        current_spectrum_nubar[ip] /= npix  # TeV cm^-2 s^-1 sr^-1

# MT from GeV cm-2 s-1 sr-1 to TeV cm-2 s-1 sr-1
#        current_spectrum_pi0[ip] = current_spectrum_pi0[ip]/1000.
#        current_spectrum_IC[ip] = current_spectrum_IC[ip]/1000.
#        current_spectrum_brems[ip] = current_spectrum_brems[ip]/1000.

    for ie in xrange(dimE):
        current_spectrum_total[ie] = current_spectrum_nu[ie] + current_spectrum_nubar[ie]

    print "Plotting..."

    print "spectrum of nu emission"
    for ip in xrange(0, dimE):
        print E_vec[ip], "GeV\t", current_spectrum_nu[ip], "GeV cm^-1 s^-1"

        E_vec_TeV = E_vec / 1000.

    # Best Fit  data SED Fermi + HESS

    #N = 8.96 * 10.**(-6.)
    # for ip in xrange(0, dimE):
    #    spectrum_BF[ip] = N * ((E_vec_TeV[ip]*1.)**(-0.49))

#    spec_total, = plt.plot(E_vec_TeV, current_spectrum_total, ls=linestyles[i], color=colors[i], lw=2.8)

    if (i == 0):
        spec_total1, = plt.plot(E_vec_TeV, current_spectrum_total, ls=":", color="red", lw=2.7)

#    if (i == 0):
#        first_legend = plt.legend([spec_total, spec_pi0, spec_IC, spec_brems],
#                                  [r'{KRA$_{\gamma} Ferriere$ [total]}', r'{KRA$_{\gamma} Ferriere$ [$\pi^0$]}', r'{KRA$_{\gamma}$ [IC]}',
#                                   r'{{KRA$_{\gamma}$ [brems]}}'], loc='upper right')
    if (i == 1):
        spec_total2, = plt.plot(E_vec_TeV, current_spectrum_total, ls=":", color="blue", lw=2.7)
        # spec_UL, = plt.plot(E_vec_TeV, current_spectrum_total*1.29, ls="-",color="black", lw=2.7)

        # mask_Erange_ANT = (np.array(E_vec_TeV) > 0.407) * (np.array(E_vec_TeV) < 246)
        # E_vec_TeV_Erange_ANT = [x for i, x in enumerate(E_vec_TeV) if mask_Erange_ANT[i]]
        # current_spectrum_total_Erange_ANT = [x * 1.08 for i, x in enumerate(current_spectrum_total) if mask_Erange_ANT[i]]
        # spec_UL3, = plt.plot(E_vec_TeV_Erange_ANT, current_spectrum_total_Erange_ANT, ls=":", color="black", lw=2.7)

    if (i == 2):
        spec_total3, = plt.plot(E_vec_TeV, current_spectrum_total, ls="--", color="red", lw=2.7)

    if (i == 3):
        spec_total4, = plt.plot(E_vec_TeV, current_spectrum_total, ls="--", color="blue", lw=2.7)

        # mask_Erange_IC = (np.array(E_vec_TeV) > 1.06) * (np.array(E_vec_TeV) < 520)
        # E_vec_TeV_Erange_IC = [x for i, x in enumerate(E_vec_TeV) if mask_Erange_IC[i]]
        # current_spectrum_total_Erange_IC = [x * 1.2 for i, x in enumerate(current_spectrum_total) if mask_Erange_IC[i]]
        # spec_UL3, = plt.plot(E_vec_TeV_Erange_IC, current_spectrum_total_Erange_IC, ls="-", color="darkorange", lw=2.7)

        mask_Erange_ANT = (np.array(E_vec_TeV) > 0.407) * (np.array(E_vec_TeV) < 246)
        E_vec_TeV_Erange_ANT = [x for i, x in enumerate(E_vec_TeV) if mask_Erange_ANT[i]]
        current_spectrum_total_Erange_ANT = [x * 1.16 for i, x in enumerate(current_spectrum_total) if mask_Erange_ANT[i]]
        spec_UL2, = plt.plot(E_vec_TeV_Erange_ANT, current_spectrum_total_Erange_ANT, ls="-", color="black", lw=2.7)


#        second_legend = plt.legend([spec_total],
#                                   [r'{KRA [total]}'], loc='lower right')


#ax = plt.gca().add_artist(first_legend)
#ax = plt.gca().add_artist(second_legend)


#E_geo_mean_TeV = E_geo_mean/1000.
#fermi_data, = plt.plot(E_geo_mean_TeV, spectrum_data,'ro', color='magenta',ms=3)


with open(IceCubefile) as f:
    print "Loading selected halos"
    lines = (line for line in f if not line.startswith('#'))
    data  = np.genfromtxt(lines, names=True, dtype=None)

list0 = data['E']  # (TeV)
list1 = data['Flux']  # (TeV-1cm-2s-1sr^-1)
list2 = data['leftErr']
list3 = data['rightErr']
list4 = data['lowErr']
list5 = data['upErr']


E_vec_IceCube_GeV = np.array(list0) * 1000.  # GeV
E_vec_IceCube_TeV = np.array(list0)  # TeV
# E(TeV)        flux(TeV-1cm-2s-1sr^-1)        lowErr     upErr

#Flux_IceCube      = np.array(list1) * 1000
Flux_IceCube      = np.array(list1)
#Flux_IceCube[:]   *= E_vec_hess_TeV[:]**2.

errLeftIceCube = np.array(list2)
errRightIceCube = np.array(list3)
errLowIceCube      = np.array(list4)  # * 1000
#errLowIceCube[:]  *= E_vec_IceCube_TeV[:]**2.
errUpIceCube       = np.array(list5)  # * 1000
#errUpIceCube[:]   *= E_vec_IceCube_TeV[:]**2.


print "IceCube data"
for iE in xrange(len(E_vec_IceCube_TeV)):
    print E_vec_IceCube_TeV[iE], "TeV\t", Flux_IceCube[iE], "\t", errUpIceCube[iE], "\t", errLowIceCube[iE]
plt.title(r'$|l| < 40^\circ$ $|b| < 3^\circ$', fontsize=30, y=1.02)
IceCube, = plt.plot(E_vec_IceCube_TeV, Flux_IceCube, '^', color='black', ms=9)
plt.errorbar(E_vec_IceCube_TeV, Flux_IceCube, xerr=(E_vec_IceCube_TeV - errLeftIceCube), yerr=(Flux_IceCube - errUpIceCube, errLowIceCube - Flux_IceCube), fmt=None, ecolor='black')

# ------------------------------------------------------------------------------------------
print "Reading ANTARES Upper limits"
# ------------------------------------------------------------------------------------------

with open(ANTARES_UL_file) as f:
    print "Loading selected fit"
    lines = (line for line in f if not line.startswith('#'))
    data  = np.genfromtxt(lines, names=True, dtype=None)

list0 = data['E']  # (GeV)
list1 = data['Flux']  # (GeV-1cm-2s-1sr^-1)


E_vec_ANTARES_UL_TeV = np.array(list0)  # TeV
Flux_ANTARES_UL_GeV = np.array(list1)
#Flux_hess[:]   *= E_vec_hess_TeV[:]**2.


print "Fit data"
for iE in xrange(len(E_vec_ANTARES_UL_TeV)):
    print E_vec_ANTARES_UL_TeV[iE], "TeV\t", Flux_ANTARES_UL_GeV[iE], "GeVcm-2s-1sr-1"
ANTARES_UL_TeV, = plt.plot(E_vec_ANTARES_UL_TeV, Flux_ANTARES_UL_GeV, 'r-', color='green', lw=2.7)

#IceCube, = plt.plot(E_vec_IceCube_TeV, Flux_IceCube*E_vec_IceCube_TeV*E_vec_IceCube_TeV, '^', color='black', ms=9)
# plt.errorbar(E_vec_IceCube_TeV, Flux_IceCube*E_vec_IceCube_TeV*E_vec_IceCube_GeV, xerr=(errLeftIceCube+np.abs(errRightIceCube)), yerr=(errUpIceCube+np.abs(errLowIceCube)#)/2.*E_vec_IceCube_GeV*E_vec_IceCube_GeV , fmt=None, ecolor='black')

# first_legend = plt.legend([spec_total1, spec_total3, spec_total2, spec_total4, spec_UL2, spec_UL3, ANTARES_UL_TeV, IceCube, fermi], [r'{Conventional Model cutoff 5 PeV}', r'{Conventional Model cutoff 50 PeV}', r'{Reference Model cutoff 5 PeV}', r'{Reference Model cutoff 50 PeV}', r'{ANTARES UL 50 PeV}', r'{ANTARES UL 5 PeV}', r'{ANTARES UL binned}', r'{IceCube 4y HESE data, $\nu$}', r'{Fermi-LAT PASS8 data, $\gamma$}'], loc='upper right')
first_legend = plt.legend([spec_total1, spec_total3, spec_total2, spec_total4, spec_UL2, ANTARES_UL_TeV, IceCube, fermi], [r'{Conventional Model cutoff 5 PeV}', r'{Conventional Model cutoff 50 PeV}', r'{Reference Model cutoff 5 PeV}', r'{Reference Model cutoff 50 PeV}', r'{ANTARES UL 50 PeV}', r'{ANTARES UL binned}', r'{IceCube 4y HESE data, $\nu$}', r'{Fermi-LAT PASS8 data, $\gamma$}'], loc='upper right')
ax = plt.gca().add_artist(first_legend)

#third_legend = plt.legend([hess,fermi],[r'{HESS 2006}',r'Fermi-LAT P8-CLEAN 3FGL sub.'],loc='lower left')
#ax = plt.gca().add_artist(third_legend)


plt.savefig(plotdir + 'ANTARES_UL_test.pdf',
            format='pdf',
            # bbox_inches='tight',
            dpi=300)

# plt.show()
plt.close()

end = datetime.datetime.now()
elapsed = end - start
print "elapsed", elapsed.seconds, "seconds"
