# Useful functions: #
**spline_to_histo()** dans *statistic_tests.py*
docopt.py is used to make a parser
**get_y_value(curve, X_value)** dans *Chi2_distribs_decl.py* get the Y-value corresponding to a X-value no matter if curve is an histo or a spline
**compute_chi2(histo1, histo2)** dans *statistical_tests.py*
**angular_separation(ra1, dec1, ra2, dec2)** dans *select_eventsMC.py*
**get_2D_cumulative** dans *select_eventsMC.py*
*copy_from_hpss.py* uses docopt, main and documentation. Use it as a template to start a new code !
