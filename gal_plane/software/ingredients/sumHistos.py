#! /usr/bin/env python

"""Fill docstring here"""

import os
import datetime
import glob
import array
import sys
import numpy as np
from math import pi, log, cos, sin, asin, acos
from ROOT import TFile, TH2D, TH3F, TH3D, TH1D, TGraph, TSpline3, TCanvas
# from antares_db_utils import *

def stretch_histo(histo):
    # should I always stretch of 2 pi
    n_bins = histo.GetNbinsX()
    titles = histo.GetTitle()+";"+histo.GetXaxis().GetTitle()+";"+histo.GetYaxis().GetTitle()
    low_edge = histo.GetBinLowEdge(1)
    up_edge = histo.GetBinLowEdge(n_bins) + histo.GetBinWidth(n_bins)

    long_histo = TH1D(histo.GetName()+ "_cyclic", titles, n_bins*2, low_edge - pi, up_edge + pi)

    for long_binX in range(1, n_bins*2+1):
        binX = long_binX + n_bins/2 if long_binX < n_bins/2 else long_binX - 3*n_bins/2 if long_binX > 3*n_bins/2 else long_binX - n_bins/2
        long_histo.SetBinContent(long_binX, histo.GetBinContent(binX))
        long_histo.SetBinError(long_binX, histo.GetBinError(binX))
    
    return long_histo

def shorten_spline(spline):
    npoints = spline.GetNp()
    npoints2 = npoints/2

    # Coord
    x = np.zeros(1)
    y = np.zeros(1)
    # Coeff
    b = np.zeros(1)
    c = np.zeros(1)
    d = np.zeros(1)

    Points = {"x":list(), "y":list(), "b":list(), "c":list(), "d":list()}
    for point in range(npoints):
        spline.GetCoeff(point, x, y, b, c, d)

        if point >= npoints/4 and point < 3*npoints/4:
            Points["x"].append(x[0])
            Points["y"].append(y[0])
            Points["b"].append(b[0])
            Points["c"].append(c[0])
            Points["d"].append(d[0])

    spline2 = TSpline3(spline.GetName() + "2", np.array(Points["x"]), np.array(Points["y"]), npoints2)
    spline2.SetName(spline.GetName())

    for point in range(npoints2):
        spline2.SetPointCoeff(point, Points["b"][point], Points["c"][point], Points["d"][point])

    return spline2

def histo_to_spline(histo, n_points_spline, cyclic=False):
    """Take a 1D histogram and return a Spline of it with a number of points close to n_points_spline"""
    if cyclic:
        histo = stretch_histo(histo)
        if n_points_spline % 2 == 0:
            n_points_spline += 1
        n_points_spline = 2 * n_points_spline

    n_bins = histo.GetNbinsX()
    if cyclic:
        for i in range(10):
            n_rebin_p = int(pi/((n_points_spline + 4 * i)/4. * histo.GetBinWidth(1)))
            n_rebin_m = int(pi/((n_points_spline - 4 * i)/4. * histo.GetBinWidth(1)))
            if n_bins % n_rebin_p == 0:
                n_rebin = n_rebin_p
                break
            elif n_bins % n_rebin_m == 0:
                n_rebin = n_rebin_m
                break
    else:
        for i in range(10):
            if n_bins % (n_points_spline + i) == 0:
                n_rebin = n_bins / (n_points_spline + i)
                break
            elif n_bins % (n_points_spline - i) == 0:
                n_rebin = n_bins / (n_points_spline - i)
                break

    histo_rebinned = histo.Rebin(n_rebin, "histo_rebinned")
    histo_rebinned.Scale(1. / n_rebin)

    graph = TGraph(histo_rebinned.GetNbinsX() + 2)
    # print 'making spline with', histo_rebinned.GetNbinsX(), '+ 2 points'

    bins_in_edge_point = max(n_rebin/2 , 3)
    low_edge_point_Y = 0
    up_edge_point_Y = 0

    for n in range(1, bins_in_edge_point):
        low_edge_point_Y += histo.GetBinContent(n)
        up_edge_point_Y += histo.GetBinContent(n_bins - n - 1)
    if cyclic:
        low_edge_point_Y = up_edge_point_Y = (low_edge_point_Y + up_edge_point_Y) / 2.
    low_edge_point_Y /= bins_in_edge_point
    up_edge_point_Y /= bins_in_edge_point

    graph.SetPoint(0, histo.GetBinLowEdge(1), low_edge_point_Y)
    for thisbin in range(1, histo_rebinned.GetNbinsX() + 1):
        graph.SetPoint(thisbin, histo_rebinned.GetBinCenter(thisbin), histo_rebinned.GetBinContent(thisbin))
    last_bin = histo.GetNbinsX()
    graph.SetPoint(histo_rebinned.GetNbinsX() + 1, histo.GetBinLowEdge(last_bin) + histo.GetBinWidth(last_bin), up_edge_point_Y)
    last_bin_rebinned = histo_rebinned.GetNbinsX()

    spline = TSpline3(histo.GetName() + "_spline", graph)
    spline.SetName(histo.GetName() + "_spline_" + str(n_points_spline/2 if cyclic else n_points_spline) + "pts")

    if cyclic:
        spline = shorten_spline(spline)
    return spline

def main():
    start = datetime.datetime.now()
    n_empty_files = 0

    date = ""
    if "date" in sys.argv:
        for i in range(len(sys.argv)):
            if sys.argv[i] == "date":
                date = "_" + sys.argv[i + 1]

    # Sum histos
    filesMC = glob.glob(
        "/sps/km3net/users/tgregoir/gal_plane/ingredient_files/MC_tim" + date + "/*.root")
    filesData = glob.glob(
        "/sps/km3net/users/tgregoir/gal_plane/ingredient_files/Data_tim" + date + "/*.root")

    if "test" in sys.argv:
        filesMC = glob.glob(
            "/sps/km3net/users/tgregoir/gal_plane/ingredient_files/MC_tim" + date + "/*0.root")
        filesData = glob.glob(
            "/sps/km3net/users/tgregoir/gal_plane/ingredient_files/Data_tim" + date + "/*0.root")


    if "Data" in sys.argv:
        outfile = TFile.Open(
            "/sps/km3net/users/tgregoir/gal_plane/ingredient_files/Sum_ing_files_Data" + date + ".root", "recreate")
    elif "MC" in sys.argv:
        outfile = TFile.Open(
            "/sps/km3net/users/tgregoir/gal_plane/ingredient_files/Sum_ing_files_MC" + date + ".root", "recreate")
    else:
        outfile = TFile.Open(
            "/sps/km3net/users/tgregoir/gal_plane/ingredient_files/Sum_ing_files" + date + "_test24juillet_bis.root", "recreate")


    nside_ = 128 * 2
    decl_arr = array.array('f')
    ra_arr = array.array('f')
    zen_arr = array.array('f')
    zen_arr_opt = array.array('f')
    az_arr = array.array('f')
    nhits_arr = array.array('f')
    ener_arr = array.array('f')
    true_ener_arr = array.array('f')
    nhits_arr2 = array.array('f')
    delta_ra = 2. * pi / (nside_)
    decl_arr_aitoff = array.array('f')
    ra_arr_aitoff = array.array('f')
    delta_decl_aitoff = 180. / (nside_)
    delta_ra_aitoff = 360. / (nside_)


    for i in range(nside_ + 1):
        if i % 2 == 0:
            decl_arr .append(asin(-1 + i * 1. / (nside_ / 2)))
        if i % 4 == 0:
            zen_arr .append(acos(1 - i * 1. / (nside_ / 2)))
        ra_arr .append(-pi + i * delta_ra)
        az_arr .append(i * delta_ra)

    for i in range(nside_ + 1):
        if i % 2 == 0:
            decl_arr_aitoff .append(-90 + i * delta_decl_aitoff)
        ra_arr_aitoff .append(-180 + i * delta_ra_aitoff)

    for i in range(nside_ + 1):
        if i % 4 == 0 and i > 0.42 * nside_:
            zen_arr_opt .append(acos(1 - i * 1. / (nside_ / 2)))

    for i in range(0, 201):
        nhits_arr .append(4 * i)
    for i in range(0, 101):
        nhits_arr2 .append(8 * i)


    for i in range(0, 71):
        ener_arr .append(7. * i / 70.)

    for i in range(0, 91):
        true_ener_arr .append(9. * i / 90.)


    ##########  MC  ##########
    tot_morpho_reco_sh = TH2D("morpho_reco_sh", "Distribution of the reconstructed events for showers;ra;decl;N_{evts}", len(
        ra_arr) - 1, ra_arr, len(decl_arr) - 1, decl_arr)
    tot_morpho_reco_tr = TH2D("morpho_reco_tr", "Distribution of the reconstructed events for tracks;ra;decl;N_{evts}", len(
        ra_arr) - 1, ra_arr, len(decl_arr) - 1, decl_arr)
    tot_morpho_reco_sh_aitoff = TH2D("morpho_reco_sh_aitoff", "Distribution of the reconstructed events for showers;ra;decl;N_{evts}/bin", len(
        ra_arr_aitoff) - 1, ra_arr_aitoff, len(decl_arr_aitoff) - 1, decl_arr_aitoff)
    tot_morpho_reco_tr_aitoff = TH2D("morpho_reco_tr_aitoff", "Distribution of the reconstructed events for tracks;ra;decl;N_{evts}/bin", len(
        ra_arr_aitoff) - 1, ra_arr_aitoff, len(decl_arr_aitoff) - 1, decl_arr_aitoff)

    tot_sh_spectr3d = TH3F("sh_spectrum3d", "Spectrum of the number of hits vs ra vs decl for showers;ra;decl;N_{hits}", len(
        ra_arr) - 1, ra_arr, len(decl_arr) - 1, decl_arr, len(nhits_arr) - 1, nhits_arr)
    tot_sh_spectr = TH1D(
        "sh_spectrum", "Spectrum of the number of hits for showers;N_{hits}", 200, 0, 800)
    tot_tr_spectr3d = TH3F("tr_spectrum3d", "Spectrum of the number of hits vs ra vs decl for tracks;ra;decl;N_{hits}", len(
        ra_arr) - 1, ra_arr, len(decl_arr) - 1, decl_arr, len(nhits_arr) - 1, nhits_arr)
    tot_tr_spectr = TH1D(
        "tr_spectrum", "Spectrum of the number of hits for tracks;N_{hits}", 200, 0, 800)

    tot_tr_Etrue_Nhits_dec_KRA = TH3D("tr_Etrue_Nhits_dec_KRA", "KRA#gamma spectrum of the true energy vs the number of hits for tracks;log_{10}(E_{true} [GeV]);N_{hits};reco decl", len(
        ener_arr) - 1, ener_arr, len(nhits_arr) - 1, nhits_arr, len(decl_arr) - 1, decl_arr)

    tot_sh_spectr3d_KRA_bis = TH3F("sh_spectrum3d_KRA_bis", "KRA#gamma spectrum of the number of hits vs ra vs decl for showers;reco ra;reco decl;N_{hits}", len(
        ra_arr) - 1, ra_arr, len(decl_arr) - 1, decl_arr, len(nhits_arr) - 1, nhits_arr)
    # tot_sh_spectr3d_zen_KRA   = TH3F("sh_spectrum3d_zen_KRA", "KRA#gamma
    # spectrum of the number of hits vs azimuth vs zenith for showers;reco
    # az;reco zen;N_{hits}", len(az_arr)-1, az_arr, len(zen_arr)-1, zen_arr,
    # len(nhits_arr)-1, nhits_arr)#50, 0, 2000)
    tot_sh_spectr_KRA = TH1D(
        "sh_spectrum_KRA", "KRA#gamma spectrum of the number of hits for showers;N_{hits}", 200, 0, 800)

    tot_sh_spectr3d_e_KRA_bis = TH3F("sh_spectrum3d_e_KRA_bis", "KRA#gamma spectrum of the reconstructed energy vs ra vs decl for showers;reco ra;reco decl;log_{10}(E_{reco} [GeV]);Number of events", len(
        ra_arr) - 1, ra_arr, len(decl_arr) - 1, decl_arr, len(ener_arr) - 1, ener_arr)  # 50, 0, 2000)
    # tot_sh_spectr3d_e_zen_KRA = TH3F("sh_spectrum3d_e_zen_KRA", "KRA#gamma
    # spectrum of the reconstructed energy vs azimuth vs zenith for
    # showers;reco az;reco zen;log_{10}(E_{reco} [GeV]);Number of events",
    # len(az_arr)-1, az_arr, len(zen_arr)-1, zen_arr, len(ener_arr)-1,
    # ener_arr)#50, 0, 2000)
    tot_sh_spectr_e_KRA = TH1D(
        "sh_spectrum_e_KRA", "KRA#gamma spectrum of the reconstructed energy for showers;log_{10}(E_{reco} [GeV])", 70, 0, 7)

    tot_tr_spectr3d_KRA_bis = TH3F("tr_spectrum3d_KRA_bis", "KRA#gamma spectrum of the number of hits vs ra vs decl for tracks;reco ra;reco decl;N_{hits}", len(
        ra_arr) - 1, ra_arr, len(decl_arr) - 1, decl_arr, len(nhits_arr) - 1, nhits_arr)
    # tot_tr_spectr3d_zen_KRA   = TH3F("tr_spectrum3d_zen_KRA", "KRA#gamma
    # spectrum of the number of hits vs azimuth vs zenith for showers;reco
    # az;reco zen;N_{hits}", len(az_arr)-1, az_arr, len(zen_arr)-1, zen_arr,
    # len(nhits_arr)-1, nhits_arr)#50, 0, 2000)
    tot_tr_spectr_KRA = TH1D(
        "tr_spectrum_KRA", "KRA#gamma spectrum of the number of hits for tracks;N_{hits}", 200, 0, 800)

    tot_tr_spectr3d_e_KRA_bis = TH3F("tr_spectrum3d_e_KRA_bis", "KRA#gamma spectrum of the reconstructed energy vs ra vs decl for tracks;reco ra;reco decl;log_{10}(E_{reco} [GeV]);Number of events", len(
        ra_arr) - 1, ra_arr, len(decl_arr) - 1, decl_arr, len(ener_arr) - 1, ener_arr)  # 50, 0, 2000)
    # tot_tr_spectr3d_e_zen_KRA = TH3F("tr_spectrum3d_e_zen_KRA", "KRA#gamma
    # spectrum of the reconstructed energy vs azimuth vs zenith for
    # tracks;reco az;reco zen;log_{10}(E_{reco} [GeV]);Number of events",
    # len(az_arr)-1, az_arr, len(zen_arr)-1, zen_arr, len(ener_arr)-1,
    # ener_arr)#50, 0, 2000)
    tot_tr_spectr_e_KRA = TH1D(
        "tr_spectrum_e_KRA", "KRA#gamma spectrum of the reconstructed energy for tracks;log_{10}(E_{reco} [GeV])", 70, 0, 7)

    tot_acc_sh = TH1D(
        "acc_sh", "Acceptance vs sin(decl);sin(true decl);Nsh", 50, -1, 1)
    tot_acc_shbis = TH1D(
        "acc_shbis", "Acceptance vs sin(decl);sin(decl);Nsh", 50, -1, 1)
    tot_acc_tr = TH1D(
        "acc_tr", "Acceptance vs sin(decl);sin(true decl);Ntr", 50, -1, 1)
    tot_acc_trbis = TH1D(
        "acc_trbis", "Acceptance vs sin(decl);sin(decl);Ntr", 50, -1, 1)

    # tot_acc_sh_zen            = TH1D("acc_sh_zen", "Acceptance vs cos(zen);cos(true zenith);Nsh", 50, -1, 1)
    # tot_acc_tr_zen            = TH1D("acc_tr_zen", "Acceptance vs cos(zen);cos(true zenith);Ntr", 50, -1, 1)

    tot_acc_sh_KRA = TH1D(
        "acc_sh_KRA", "Shower acceptance in function of the declination;sin(true decl);Nsh", 50, -1, 1)
    tot_acc_tr_KRA = TH1D(
        "acc_tr_KRA", "Track acceptance in function of the declination;sin(true decl);Ntr", 50, -1, 1)
    tot_acc_sh_KRA_bis = TH1D(
        "acc_sh_KRA_bis", "Distribution of the showers in function of the reconstructed declination;sin(reco decl);Nsh", 50, -1, 1)
    tot_acc_tr_KRA_bis = TH1D(
        "acc_tr_KRA_bis", "Distribution of the tracks in function of the reconstructed declination;sin(reco decl);Ntr", 50, -1, 1)
    # tot_acc_sh_zen_KRA        = TH1D("acc_sh_zen_KRA", "Shower acceptance in function of zenith;cos(true zen);Nsh", 50, -1, 1)
    # tot_acc_tr_zen_KRA        = TH1D("acc_tr_zen_KRA", "Track acceptance in function of zenith;cos(true zen);Ntr", 50, -1, 1)

    # 1 = CC nue a, 2 = NC nue a, 3 = CC anue a, 4 = NC anue a, 5 = CC numu a,
    # 6 = NC numu a, 7 = CC anumu a, 8 = NC anumu a, 10 = CC nue b, 11 = NC
    # nue b, 12 = CC anue b, 13 = NC anue b, 14 = CC numu b, 15 = NC numu b,
    # 16 = CC anumu b, 17 = NC anumu b, 19 = mupage
    tot_histo_run_durMC = TH1D(
        "histo_run_durMC", "a: CCnue, NCnue, CCanue, NCanue, CCnumu, NCnumu, CCanumu, NCanumu, b: idem, mupage;;run duration", 20, 0.5, 20.5)
    tot_histo_run_numberMC = TH1D(
        "histo_run_numberMC", "histo_run_numberMC;run number;run duration [s]", 90001, -0.5, 90000.5)
    # tot_hist_N_evts           = TH1D("hist_N_evts", "hist_N_evts", 10, 0.5,
    # 10.5) # 1 = atm_nu_e_CC, 3 = atm nu_X_NC+nu_mu_CC, 5 = astro nu_e_CC, 7
    # = astro nu_X_NC+nu_mu_CC, 9 = muons
    # 5, -0.5, 4.5)
    tot_NevtsMC = TH1D(
        "NevtsMC", "0=xrdcp failure, N KRA#gamma events 1=tr, 3=sh, N#phy_{0} events 6=tr, 8=sh", 9, -0.5, 8.5)
    tot_check_NevtsMC = TH1D(
        "check_NevtsMC", "atm, 1:numuNC, 2:nueCC, 3:numuCC, phy0, 5:numuNC, 6:nueCC, 7:numuCC, 9:mupage", 10, 0.5, 10.5)
    # tot_NevtsComp             = TH1D("NevtsComparison","N#phy_{0} events Jav
    # sel not me: 1=tr, 3=sh, I sel not Jav: 6=tr, 8=sh", 9, -0.5, 8.5)

    tot_sh_spectr_atm = TH1D(
        "sh_spectr_atmmc", "Atmospheric spectrum of the number of hits;N_{hits};N_{evts}", 250, 0, 1000)
    tot_sh_spectr_e_atm = TH1D(
        "sh_spectr_e_atmmc", "Atmospheric spectrum of the reconstructed energy;log_{10}(E_{reco} [GeV])", 70, 0, 7)
    tot_sh_spectr_e_nuatm = TH1D(
        "sh_spectr_e_nuatmmc", "Atmospheric spectrum of the reconstructed energy without mupage;log_{10}(E_{reco} [GeV])", 70, 0, 7)
    tot_sh_spectr_e_muatm = TH1D(
        "sh_spectr_e_muatmmc", "Atmospheric spectrum of the reconstructed energy with only mupage events;log_{10}(E_{reco} [GeV])", 70, 0, 7)
    tot_sh_spectr_e_muatm .Sumw2()

    tot_sh_spectr2d_e_atm_zen_initial = TH2D("sh_spectr2d_e_atmmc_zen_initial", "Atmospheric spectrum of the reconstructed energy vs zen for Showers;log_{10}(E_{reco} [GeV]);zenith [rad]", len(
        ener_arr) - 1, ener_arr, len(zen_arr) - 1, zen_arr)
    tot_sh_spectr2d_e_atm_zen_final = TH2D("sh_spectr2d_e_atmmc_zen_final", "Atmospheric spectrum of the reconstructed energy vs zen for nu Showers;log_{10}(E_{reco} [GeV]);zenith [rad]", len(
        ener_arr) - 1, ener_arr, len(zen_arr) - 1, zen_arr)
    tot_sh_spectr2d_e_atm_zen_final_opt = TH2D("sh_spectr2d_e_atmmc_zen_final_opt", "Atmospheric spectrum of the reconstructed energy vs zen for nu Showers;log_{10}(E_{reco} [GeV]);zenith [rad]", len(
        ener_arr) - 1, ener_arr, len(zen_arr_opt) - 1, zen_arr_opt)
    tot_sh_spectr2d_e_muatm_zen = TH2D("sh_spectr2d_e_muatmmc_zen", "Atmospheric spectrum of the reconstructed energy vs zen for mupage Showers;log_{10}(E_{reco} [GeV]);zenith [rad]", len(
        ener_arr) - 1, ener_arr, len(zen_arr) - 1, zen_arr)
    tot_sh_spectr2d_e_muatm_zen_bef_cuts = TH2D("sh_spectr2d_e_muatmmc_zen_bef_cuts", "Atmospheric spectrum of the reconstructed energy vs zen for mupage Showers;log_{10}(E_{reco} [GeV]);zenith [rad]", len(
        ener_arr) - 1, ener_arr, len(zen_arr) - 1, zen_arr)
    tot_sh_spectr2d_e_muatm_zen_bef_cuts_final = TH2D(
        "sh_spectr2d_e_muatmmc_zen_bef_cuts_final", "Atmospheric spectrum of the reconstructed energy vs zen for mupage Showers;log_{10}(E_{reco} [GeV]);zenith [rad]", len(ener_arr) - 1, ener_arr, len(zen_arr) - 1, zen_arr)

    tot_tr_spectr_atm = TH1D(
        "tr_spectr_atmmc", "Atmospheric spectrum of the number of hits;N_{hits};N_{evts}", 310, 0, 992)
    tot_tr_spectr2d_atm = TH2D("tr_spectr2d_atmmc", "Atmospheric spectrum of the number of hits;N_{hits};Declination [rad];N_{evts}", len(
        nhits_arr) - 1, nhits_arr, len(decl_arr) - 1, decl_arr)
    tot_tr_spectr2d_atm_zen = TH2D("tr_spectr2d_atmmc_zen", "Atmospheric spectrum of the number of hits;N_{hits};Zenith [rad];N_{evts}", len(
        nhits_arr2) - 1, nhits_arr2, len(zen_arr) - 1, zen_arr)
    tot_tr_spectr_e_atm = TH1D(
        "tr_spectr_e_atmmc", "Atmospheric spectrum of the reconstructed energy;log_{10}(E_{reco} [GeV])", 70, 0, 7)
    tot_tr_spectr2d_e_atm_zen_initial = TH2D("tr_spectr2d_e_atmmc_zen_initial", "Atmospheric spectrum of the reconstructed energy vs zen for Tracks;log_{10}(E_{reco} [GeV]);zenith [rad]", len(
        ener_arr) - 1, ener_arr, len(zen_arr) - 1, zen_arr)
    tot_tr_spectr2d_e_atm_zen_final = TH2D("tr_spectr2d_e_atmmc_zen_final", "Atmospheric spectrum of the reconstructed energy vs zen for Tracks;log_{10}(E_{reco} [GeV]);zenith [rad]", len(
        ener_arr) - 1, ener_arr, len(zen_arr) - 1, zen_arr)
    tot_tr_spectr2d_e_atm_zen_final_opt = TH2D("tr_spectr2d_e_atmmc_zen_final_opt", "Atmospheric spectrum of the reconstructed energy vs zen for Tracks;log_{10}(E_{reco} [GeV]);zenith [rad]", len(
        ener_arr) - 1, ener_arr, len(zen_arr_opt) - 1, zen_arr_opt)
    tot_tr_spectr2d_e_muatm_zen = TH2D("tr_spectr2d_e_muatm_zen", "Atmospheric spectrum of the reconstructed energy vs zen for Tracks;log_{10}(E_{reco} [GeV]);zenith [rad]", len(
        ener_arr) - 1, ener_arr, len(zen_arr) - 1, zen_arr)
    tot_tr_spectr2d_e_muatm_zen_bef_cuts = TH2D("tr_spectr2d_e_muatmmc_zen_bef_cuts", "Atmospheric spectrum of the reconstructed energy vs zen for mupage Tracks;log_{10}(E_{reco} [GeV]);zenith [rad]", len(
        ener_arr) - 1, ener_arr, len(zen_arr) - 1, zen_arr)
    tot_tr_spectr2d_e_muatm_zen_bef_cuts_final = TH2D(
        "tr_spectr2d_e_muatmmc_zen_bef_cuts_final", "Atmospheric spectrum of the reconstructed energy vs zen for mupage Tracks;log_{10}(E_{reco} [GeV]);zenith [rad]", len(ener_arr) - 1, ener_arr, len(zen_arr) - 1, zen_arr)

    sindec_bins = 360
    tot_sh_sindec_atmMC = TH1D(
        "sh_sindec_atmMC", "Shower Background vs sin(decl) from MC;sin(decl);dN/dsin(dec)", sindec_bins, -1, 0.8)
    tot_tr_sindec_atmMC = TH1D(
        "tr_sindec_atmMC", "Track Background vs sin(decl) from MC;sin(decl);dN/dsin(dec)", sindec_bins, -1, 0.8)

    tot_sh_ra_atmMC = TH1D(
        "sh_ra_atmMC", "Shower Background vs ra from MC;ra [rad];N_{evts}", 300, -pi, pi)
    tot_tr_ra_atmMC = TH1D(
        "tr_ra_atmMC", "Track Background vs ra from MC;ra [rad];N_{evts}", 300, -pi, pi)

    coszen_bins = 220
    tot_sh_coszen_atmMC = TH1D(
        "sh_coszen_atmMC", "Shower Background vs cos(zen) from MC;cos(zen);dN/dcos(zen)", coszen_bins, -1, 0.1)
    tot_tr_coszen_atmMC = TH1D(
        "tr_coszen_atmMC", "Track Background vs cos(zen) from MC;cos(zen);dN/dcos(zen)", coszen_bins, -1, 0.1)

    tot_sh_az_atmMC = TH1D("sh_az_atmMC", "Shower Background vs azimuth from MC;azimuth (rad);N_{evts}", 300, 0, 2. * pi)
    # tot_sh_az_atmMC.Sumw2()
    # tot_sh_az_atmMC_nu = TH1D("sh_az_atmMC_nu", "Shower Background vs azimuth from MC;azimuth (rad);N_{evts}", 300, 0, 2. * pi)
    # tot_sh_az_atmMC_nu.Sumw2()
    # tot_sh_az_atmMC_mu = TH1D("sh_az_atmMC_mu", "Shower Background vs azimuth from MC;azimuth (rad);N_{evts}", 300, 0, 2. * pi)
    # tot_sh_az_atmMC_mu.Sumw2()

    tot_tr_az_atmMC = TH1D("tr_az_atmMC", "Track Background vs azimuth from MC;azimuth (rad);N_{evts}", 300, 0, 2. * pi)

    tot_PSF_sh_KRA = TH1D(
        "PSF_sh_KRA", "Distribution of the angular error for showers;log_{10}(angular error (/deg));dP/d#Omega", 35, -1., 2.5)  # 60, -3, 3)
    tot_PSF_tr_KRA = TH1D(
        "PSF_tr_KRA", "Distribution of the angular error for tracks;log_{10}(angular error (/deg));dP/d#Omega", 42, -2.5, 2.5)  # 60, -3, 3)
    tot_PSF_sh = TH1D(
        "PSF_sh", "Distribution of the angular error for showers;log_{10}(angular error (/deg));dP/d#Omega", 35, -1., 2.5)  # 60, -3, 3)
    tot_PSF_tr = TH1D(
        "PSF_tr", "Distribution of the angular error for tracks;log_{10}(angular error (/deg));dP/d#Omega", 42, -2.5, 2.5)  # 60, -3, 3)

    # 1 = atm_nu_mu_CC, 3 = atm nu_X_NC+nu_e_CC, 5 = astro nu_mu_CC, 7 = astro
    # nu_X_NC+nu_e_CC, 9 = muons
    tot_check_Nevts = TH1D(
        "check_Nevts", "atm, 1:numuNC, 2:nueCC, 3:numuCC, phy0, 5:numuNC, 6:nueCC, 7:numuCC, 9:mupage", 10, 0.5, 10.5)

    tot_hist_cuts_tr_KRA = TH1D(
        "hist_cuts_tr_KRA", "Signal events after each cuts for Tracks;cuts;N_{evts}", 15, 0.5, 15.5)
    tot_hist_cuts_tr_nuatm = TH1D(
        "hist_cuts_tr_nuatm", "nu atm events after each cuts for Tracks;cuts;N_{evts}", 15, 0.5, 15.5)
    tot_hist_cuts_tr_mupage = TH1D(
        "hist_cuts_tr_mupage", "Mupage events after each cuts for Tracks;cuts;N_{evts}", 15, 0.5, 15.5)

    tot_hist_cuts_sh_KRA = TH1D(
        "hist_cuts_sh_KRA", "Signal events after each cuts for Showers;cuts;N_{evts}", 19, 0.5, 19.5)
    tot_hist_cuts_sh_nuatm = TH1D(
        "hist_cuts_sh_nuatm", "nu atm events after each cuts for Showers;cuts;N_{evts}", 15, 0.5, 15.5)
    tot_hist_cuts_sh_mupage = TH1D(
        "hist_cuts_sh_mupage", "Mupage events after each cuts for Showers;cuts;N_{evts}", 15, 0.5, 15.5)

    # tot_hist_cuts_lik_sh_KRA    = TH1D("hist_cuts_lik_sh_KRA", "Signal events after each cuts for Showers;cuts;N_{evts}", 12, -2.5, 57.5)
    # tot_hist_cuts_lik_sh_nuatm  = TH1D("hist_cuts_lik_sh_nuatm", "nu atm events after each cuts for Showers;cuts;N_{evts}", 12, -2.5, 57.5)
    # tot_hist_cuts_lik_sh_mupage = TH1D("hist_cuts_lik_sh_mupage", "Mupage events after each cuts for Showers;cuts;N_{evts}", 12, -2.5, 57.5)

    # tot_hist_cuts_lik_sh_KRA2    = TH1D("hist_cuts_lik_sh_KRA2", "Signal events after each cuts for Showers;cuts;N_{evts}", 12, -2.5, 57.5)
    # tot_hist_cuts_lik_sh_nuatm2  = TH1D("hist_cuts_lik_sh_nuatm2", "nu atm events after each cuts for Showers;cuts;N_{evts}", 12, -2.5, 57.5)
    # tot_hist_cuts_lik_sh_mupage2 = TH1D("hist_cuts_lik_sh_mupage2", "Mupage events after each cuts for Showers;cuts;N_{evts}", 12, -2.5, 57.5)

    tot_hist_channels_tr = TH1D(
        "hist_channels_tr", "Nevts for each channel after Track cuts", 15, 0.5, 15.5)
    tot_hist_channels_sh = TH1D(
        "hist_channels_sh", "Nevts for each channel after Shower cuts", 15, 0.5, 15.5)

    tot_MC_tr_trueEnergy = TH1D(
        "MC_tr_trueEnergy", "MC_tr_trueEnergy", len(true_ener_arr) - 1, true_ener_arr)
    tot_MC_sh_trueEnergy = TH1D(
        "MC_sh_trueEnergy", "MC_sh_trueEnergy", len(true_ener_arr) - 1, true_ener_arr)

    # tot_sh_spectr_nueNC     = TH1D("sh_spectrum_nueNC", "E-2 spectrum for nue NC;log_{10}(True energy)", 70, 0, 7)
    # tot_sh_spectr_numuNC    = TH1D("sh_spectrum_numuNC", "E-2 spectrum for numu NC;log_{10}(True energy)", 70, 0, 7)
    # tot_sh_spectr_nutauNC   = TH1D("sh_spectrum_nutauNC", "E-2 spectrum for nutau NC;log_{10}(True energy)", 70, 0, 7)

    # tot_beta_distrib_tr_atm      = TH1D("beta_tr_atm", "#beta tr distrib", 40, -6, -2)
    # tot_beta_distrib_tr_cosm     = TH1D("beta_tr_cosm", "#beta tr distrib",
    # 40, -6, -2)

    # number of events in the gal ridge
    tot_N_evts_gal_ridge = TH1D(
        "N_evts_gal_ridge", "1=sig sh, 3=sig tr, 6=bg sh, 8=bg tr", 8, 0.5, 8.5)


    tau = False
    if "tau" in sys.argv:
        tau = True
        tot_acc_sh_tauCC_had = TH1D(
            "acc_sh_tauCC_had", "Acceptance;sin(true decl);Nsh", 50, -1, 1)
        tot_acc_sh_tauCC_e = TH1D(
            "acc_sh_tauCC_e", "Acceptance;sin(true decl);Nsh", 50, -1, 1)
        tot_acc_sh_tauCC_mu = TH1D(
            "acc_sh_tauCC_mu", "Acceptance;sin(true decl);Nsh", 50, -1, 1)
        tot_acc_tr_tauCC_mu = TH1D(
            "acc_tr_tauCC_mu", "Acceptance;sin(true decl);Nsh", 50, -1, 1)


    ##########  Data  ##########

    tot_sh_spectrum_bgr = TH1D(
        "sh_spectrum_bgr", "Shower spectrum of the number of hits for Background;N_{hits}", 250, 0, 1000)
    tot_sh_spectrum_e_bgr = TH1D(
        "sh_spectrum_e_bgr", "Shower spectrum of the reconstructed energy for Background;log_{10}(E_{reco} [GeV])", 70, 0, 7)
    tot_tr_spectrum_bgr = TH1D(
        "tr_spectrum_bgr", "Track spectrum of the number of hits for Background;N_{hits}", 250, 0, 800)
    tot_tr_spectrum_e_bgr = TH1D(
        "tr_spectrum_e_bgr", "Track spectrum of the reconstructed energy for Background;log_{10}(E_{reco} [GeV])", 70, 0, 7)
    tot_sh_bgr_vs_sindec = TH1D(
        "sh_bgr_vs_sindec", "Shower Background vs sin(decl);sin(decl);# of events", 90, -1, 0.8)
    tot_tr_bgr_vs_sindec = TH1D(
        "tr_bgr_vs_sindec", "Track Background vs sin(decl);sin(decl);# of events", 180, -1, 0.8)
    tot_sh_bgr_vs_ra = TH1D(
        "sh_bgr_vs_ra", "Shower Background vs ra;ra;# of events", 300, -pi, pi)
    tot_tr_bgr_vs_ra = TH1D(
        "tr_bgr_vs_ra", "Track Background vs ra;ra;# of events", 300, -pi, pi)
    tot_sh_bgr_vs_coszen = TH1D(
        "sh_bgr_vs_coszen", "Shower Background vs cos(zen);cos(zen);# of events", 48, -1, 0.1)
    tot_tr_bgr_vs_coszen = TH1D(
        "tr_bgr_vs_coszen", "Track Background vs cos(zen);cos(zen);# of events", 96, -1, 0.1)
    tot_sh_bgr_vs_az = TH1D(
        "sh_bgr_vs_az", "Shower Background vs az;az;# of events", 300, 0, 2 * pi)
    tot_tr_bgr_vs_az = TH1D(
        "tr_bgr_vs_az", "Track Background vs az;az;# of events", 300, 0, 2 * pi)

    sh_bgr_vs_sindec_rebinned_stretched = TH1D(
        "sh_bgr_vs_sindec_rebinned_stretched", "Shower Background vs sin(decl);sin(decl);# of events", 10, -1.09, 0.89)
    tr_bgr_vs_sindec_rebinned_stretched = TH1D(
        "tr_bgr_vs_sindec_rebinned_stretched", "Track Background vs sin(decl);sin(decl);# of events", 20, -1.045, 0.845)
    sh_bgr_vs_coszen_rebinned_stretched = TH1D(
        "sh_bgr_vs_coszen_rebinned_stretched", "Shower Background vs cos(zen);cos(zen);# of events", 10, -1.055, 0.155)
    tr_bgr_vs_coszen_rebinned_stretched = TH1D(
        "tr_bgr_vs_coszen_rebinned_stretched", "Track Background vs cos(zen);cos(zen);# of events", 20, -1.0275, 0.1275)

    sh_bgr_vs_sindec_rebinned_stretched_bis = TH1D(
        "sh_bgr_vs_sindec_rebinned_stretched_bis", "Shower Background vs sin(decl);sin(decl);# of events", 5, -1.18, 0.98)
    tr_bgr_vs_sindec_rebinned_stretched_bis = TH1D(
        "tr_bgr_vs_sindec_rebinned_stretched_bis", "Track Background vs sin(decl);sin(decl);# of events", 10, -1.09, 0.89)
    sh_bgr_vs_coszen_rebinned_stretched_bis = TH1D(
        "sh_bgr_vs_coszen_rebinned_stretched_bis", "Shower Background vs cos(zen);cos(zen);# of events", 5, -1.11, 0.21)
    tr_bgr_vs_coszen_rebinned_stretched_bis = TH1D(
        "tr_bgr_vs_coszen_rebinned_stretched_bis", "Track Background vs cos(zen);cos(zen);# of events", 10, -1.055, 0.155)

    tot_histo_run_durData = TH1D(
        "histo_run_durData", "tot_histo_run_durData;;total run duration", 10, 0, 10)
    tot_histo_run_numberData = TH1D(
        "histo_run_numberData", "tot_histo_run_numberData", 90001, -0.5, 90000.5)
    tot_NevtsData = TH1D(
        "NevtsData", "1 =tr, 3 =sh, 0=xrdcp failure", 5, -0.5, 4.5)

    tot_hist_begin_runMC = TH1D(
        "hist_begin_runMC", "starting date of the run;[days]", 33000, 0, 3300)
    tot_hist_end_runMC = TH1D(
        "hist_end_runMC", "ending date of the run;[days]", 33000, 0, 3300)

    tot_hist_begin_runData = TH1D(
        "hist_begin_runData", "starting date of the run;[days]", 33000, 0, 3300)
    tot_hist_end_runData = TH1D(
        "hist_end_runData", "ending date of the run;[days]", 33000, 0, 3300)


    tot_MC_sh_cos_zen_cut = TH1D(
        "MC_sh_cos_zen_cut", "MC_sh_cos_zen_cut", 22, -1, 0.1)
    tot_MC_sh_M_est = TH1D("MC_sh_M_est", "MC_sh_M_est", 100, 0, 1000)
    tot_MC_sh_beta = TH1D("MC_sh_beta", "MC_sh_beta", 26, 0, 26)
    tot_MC_sh_RDF = TH1D("MC_sh_RDF", "MC_sh_RDF", 14, 0.3, 1)
    tot_MC_sh_lik_muVeto = TH1D(
        "MC_sh_lik_muVeto", "MC_sh_lik_muVeto", 36, 40, 400)

    tot_MC_tr_cos_zen_cut = TH1D(
        "MC_tr_cos_zen_cut", "MC_tr_cos_zen_cut", 22, -1, 0.1)
    tot_MC_tr_lambda = TH1D("MC_tr_lambda", "MC_tr_lambda", 23, -5.15, -3.2)
    tot_MC_tr_beta = TH1D("MC_tr_beta", "MC_tr_beta", 10, 0, 1)

    tot_Data_sh_cos_zen_cut = TH1D(
        "Data_sh_cos_zen_cut", "Data_sh_cos_zen_cut", 22, -1, 0.1)
    tot_Data_sh_M_est = TH1D("Data_sh_M_est", "Data_sh_M_est", 100, 0, 1000)
    tot_Data_sh_beta = TH1D("Data_sh_beta", "Data_sh_beta", 26, 0, 26)
    tot_Data_sh_RDF = TH1D("Data_sh_RDF", "Data_sh_RDF", 14, 0.3, 1)
    tot_Data_sh_lik_muVeto = TH1D(
        "Data_sh_lik_muVeto", "Data_sh_lik_muVeto", 36, 40, 400)

    tot_Data_tr_cos_zen_cut = TH1D(
        "Data_tr_cos_zen_cut", "Data_tr_cos_zen_cut", 22, -1, 0.1)
    tot_Data_tr_lambda = TH1D("Data_tr_lambda", "Data_tr_lambda", 23, -5.15, -3.2)
    tot_Data_tr_beta = TH1D("Data_tr_beta", "Data_tr_beta", 10, 0, 1)

    # tot_MC_sh_cos_zen_cut      = TH1D("MC_sh_cos_zen_cut","MC_sh_cos_zen_cut", 40, -1, 1)
    # tot_MC_sh_M_est        = TH1D("MC_sh_M_est","MC_sh_M_est", 50, 0, 5000)
    # tot_MC_sh_beta         = TH1D("MC_sh_beta","MC_sh_beta", 35, 0, 35)
    # tot_MC_sh_RDF          = TH1D("MC_sh_RDF","MC_sh_RDF", 20, 0, 1)
    # tot_MC_sh_lik_muVeto   = TH1D("MC_sh_lik_muVeto","MC_sh_lik_muVeto", 50 ,-100 , 400)
    #
    # tot_MC_tr_cos_zen_cut      = TH1D("MC_tr_cos_zen_cut","MC_tr_cos_zen_cut", 40, -1, 1)
    # tot_MC_tr_lambda       = TH1D("MC_tr_lambda","MC_tr_lambda", 200, -10, 10)
    # tot_MC_tr_beta         = TH1D("MC_tr_beta","MC_tr_beta", 50, 0, 5)
    #
    # tot_Data_sh_cos_zen_cut    = TH1D("Data_sh_cos_zen_cut","Data_sh_cos_zen_cut", 40, -1, 1)
    # tot_Data_sh_M_est      = TH1D("Data_sh_M_est","Data_sh_M_est", 50, 0, 5000)
    # tot_Data_sh_beta       = TH1D("Data_sh_beta","Data_sh_beta", 35, 0, 35)
    # tot_Data_sh_RDF        = TH1D("Data_sh_RDF","Data_sh_RDF", 20, 0, 1)
    # tot_Data_sh_lik_muVeto = TH1D("Data_sh_lik_muVeto","Data_sh_lik_muVeto", 50 ,-100 , 400)
    #
    # tot_Data_tr_cos_zen_cut    = TH1D("Data_tr_cos_zen_cut","Data_tr_cos_zen_cut", 40, -1, 1)
    # tot_Data_tr_lambda     = TH1D("Data_tr_lambda","Data_tr_lambda", 200, -10, 10)
    # tot_Data_tr_beta       = TH1D("Data_tr_beta","Data_tr_beta", 50, 0, 5)

    tot_MC_tr_angRes = TH1D("MC_tr_angRes", "MC_tr_angRes", 500, 0, 0.5)
    tot_MC_sh_angRes = TH1D("MC_sh_angRes", "MC_sh_angRes", 500, 0, 0.6)


    test = 0
    no_old_plots = True
    empty_files = list()
    ######### MC ##########
    if "Data" not in sys.argv:
        for infile in filesMC:
            tfile = TFile.Open(infile)

            if tfile.GetNkeys() == 0:
                print infile, "is empty !!!"
                n_empty_files += 1
                empty_files .append(infile)
                continue
            else:
                print infile

            # beta_distrib_tr_cosm  = tfile.Get("beta_tr_cosm")
            # tot_beta_distrib_tr_cosm.Add(beta_distrib_tr_cosm)
            #
            # beta_distrib_tr_atm  = tfile.Get("beta_tr_atm")
            # tot_beta_distrib_tr_atm.Add(beta_distrib_tr_atm)

            # NevtsComparison      = tfile.Get("NevtsComparison")
            # tot_NevtsComp.Add(NevtsComparison)

            # check_Nevts = tfile.Get("check_Nevts")
            # tot_check_Nevts.Add(check_Nevts)

            # check_NevtsMC               = tfile.Get("check_Nevts")
            # tot_check_NevtsMC.Add(check_NevtsMC)

            if tfile.Get("acc_sh"):
                no_old_plots = False
                ##### Phy_0 #####
                sh_spectrum = tfile.Get("sh_spectrum")
                tot_sh_spectr.Add(sh_spectrum)

                sh_spectrum3d = tfile.Get("sh_spectrum3d")
                tot_sh_spectr3d.Add(sh_spectrum3d)

                tr_spectrum = tfile.Get("tr_spectrum")
                tot_tr_spectr.Add(tr_spectrum)

                tr_spectrum3d = tfile.Get("tr_spectrum3d")
                tot_tr_spectr3d.Add(tr_spectrum3d)

                acc_sh = tfile.Get("acc_sh")
                tot_acc_sh.Add(acc_sh)

                acc_shbis = tfile.Get("acc_shbis")
                tot_acc_shbis.Add(acc_shbis)

                acc_tr = tfile.Get("acc_tr")
                tot_acc_tr.Add(acc_tr)

                acc_trbis = tfile.Get("acc_trbis")
                tot_acc_trbis.Add(acc_trbis)

                # acc_sh_zen          = tfile.Get("acc_sh_zen")
                # tot_acc_sh_zen.Add(acc_sh_zen)

                # acc_tr_zen          = tfile.Get("acc_tr_zen")
                # tot_acc_tr_zen.Add(acc_tr_zen)

                PSF_sh = tfile.Get("PSF_sh")
                tot_PSF_sh.Add(PSF_sh)

                PSF_tr = tfile.Get("PSF_tr")
                tot_PSF_tr.Add(PSF_tr)

                sh_spectrum3d_KRA_bis = tfile.Get("sh_spectrum3d_KRA_bis")
                tot_sh_spectr3d_KRA_bis.Add(sh_spectrum3d_KRA_bis)

                sh_spectrum_KRA = tfile.Get("sh_spectrum_KRA")
                tot_sh_spectr_KRA.Add(sh_spectrum_KRA)

                # sh_spectrum3d_zen_KRA = tfile.Get("sh_spectrum3d_zen_KRA")
                # tot_sh_spectr3d_zen_KRA.Add(sh_spectrum3d_zen_KRA)

                # sh_spectrum3d_KRA   = tfile.Get("sh_spectrum3d_KRA")
                # tot_sh_spectr3d_KRA.Add(sh_spectrum3d_KRA)

                # tr_spectrum3d_zen_KRA = tfile.Get("tr_spectrum3d_zen_KRA")
                # tot_tr_spectr3d_zen_KRA.Add(tr_spectrum3d_zen_KRA)

                # tr_spectrum3d_KRA   = tfile.Get("tr_spectrum3d_KRA")
                # tot_tr_spectr3d_KRA.Add(tr_spectrum3d_KRA)

                acc_sh_KRA_bis = tfile.Get("acc_sh_KRA_bis")
                tot_acc_sh_KRA_bis.Add(acc_sh_KRA_bis)

                acc_tr_KRA_bis = tfile.Get("acc_tr_KRA_bis")
                tot_acc_tr_KRA_bis.Add(acc_tr_KRA_bis)

                PSF_sh_KRA = tfile.Get("PSF_sh_KRA")
                tot_PSF_sh_KRA.Add(PSF_sh_KRA)

                PSF_tr_KRA = tfile.Get("PSF_tr_KRA")
                tot_PSF_tr_KRA.Add(PSF_tr_KRA)

                tr_Etrue_Nhits_dec_KRA = tfile.Get("tr_Etrue_Nhits_dec_KRA")
                tot_tr_Etrue_Nhits_dec_KRA.Add(tr_Etrue_Nhits_dec_KRA)

                hist_begin_runMC = tfile.Get("hist_begin_runMC")
                tot_hist_begin_runMC.Add(hist_begin_runMC)

                hist_end_runMC = tfile.Get("hist_end_runMC")
                tot_hist_end_runMC.Add(hist_end_runMC)

            ##### KRA #####
            # MC_tr_angRes = tfile.Get("MC_tr_angRes")
            # tot_MC_tr_angRes.Add(MC_tr_angRes)
            #
            # MC_sh_angRes = tfile.Get("MC_sh_angRes")
            # tot_MC_sh_angRes.Add(MC_sh_angRes)
            #
            # N_evts_gal_ridge = tfile.Get("N_evts_gal_ridge")
            # tot_N_evts_gal_ridge.Add(N_evts_gal_ridge)

            hist_channels_tr = tfile.Get("hist_channels_tr")
            tot_hist_channels_tr.Add(hist_channels_tr)

            hist_channels_sh = tfile.Get("hist_channels_sh")
            tot_hist_channels_sh.Add(hist_channels_sh)

            # MC_tr_trueEnergy = tfile.Get("MC_tr_trueEnergy")
            # tot_MC_tr_trueEnergy.Add(MC_tr_trueEnergy)
            #
            # MC_sh_trueEnergy = tfile.Get("MC_sh_trueEnergy")
            # tot_MC_sh_trueEnergy.Add(MC_sh_trueEnergy)

            hist_cuts_tr_KRA = tfile.Get("hist_cuts_tr_KRA")
            tot_hist_cuts_tr_KRA.Add(hist_cuts_tr_KRA)

            hist_cuts_tr_nuatm = tfile.Get("hist_cuts_tr_nuatm")
            tot_hist_cuts_tr_nuatm.Add(hist_cuts_tr_nuatm)

            hist_cuts_tr_mupage = tfile.Get("hist_cuts_tr_mupage")
            tot_hist_cuts_tr_mupage.Add(hist_cuts_tr_mupage)

            hist_cuts_sh_KRA = tfile.Get("hist_cuts_sh_KRA")
            tot_hist_cuts_sh_KRA.Add(hist_cuts_sh_KRA)

            hist_cuts_sh_nuatm = tfile.Get("hist_cuts_sh_nuatm")
            tot_hist_cuts_sh_nuatm.Add(hist_cuts_sh_nuatm)

            hist_cuts_sh_mupage = tfile.Get("hist_cuts_sh_mupage")
            tot_hist_cuts_sh_mupage.Add(hist_cuts_sh_mupage)

            # hist_cuts_lik_sh_KRA    = tfile.Get("hist_cuts_lik_sh_KRA")
            # tot_hist_cuts_lik_sh_KRA.Add(hist_cuts_lik_sh_KRA)

            # hist_cuts_lik_sh_nuatm    = tfile.Get("hist_cuts_lik_sh_nuatm")
            # tot_hist_cuts_lik_sh_nuatm.Add(hist_cuts_lik_sh_nuatm)

            # hist_cuts_lik_sh_mupage    = tfile.Get("hist_cuts_lik_sh_mupage")
            # tot_hist_cuts_lik_sh_mupage.Add(hist_cuts_lik_sh_mupage)

            # hist_cuts_lik_sh_KRA2    = tfile.Get("hist_cuts_lik_sh_KRA2")
            # tot_hist_cuts_lik_sh_KRA2.Add(hist_cuts_lik_sh_KRA2)

            # hist_cuts_lik_sh_nuatm2    = tfile.Get("hist_cuts_lik_sh_nuatm2")
            # tot_hist_cuts_lik_sh_nuatm2.Add(hist_cuts_lik_sh_nuatm2)

            # hist_cuts_lik_sh_mupage2    = tfile.Get("hist_cuts_lik_sh_mupage2")
            # tot_hist_cuts_lik_sh_mupage2.Add(hist_cuts_lik_sh_mupage2)

            tr_spectrum_KRA = tfile.Get("tr_spectrum_KRA")
            tot_tr_spectr_KRA.Add(tr_spectrum_KRA)

            tr_spectrum3d_KRA_bis = tfile.Get("tr_spectrum3d_KRA_bis")
            tot_tr_spectr3d_KRA_bis.Add(tr_spectrum3d_KRA_bis)

            morpho_reco_sh = tfile.Get("morpho_reco_sh")
            tot_morpho_reco_sh.Add(morpho_reco_sh)

            morpho_reco_tr = tfile.Get("morpho_reco_tr")
            tot_morpho_reco_tr.Add(morpho_reco_tr)

            # morpho_reco_sh_aitoff = tfile.Get("morpho_reco_sh_aitoff")
            # tot_morpho_reco_sh_aitoff.Add(morpho_reco_sh_aitoff)
            #
            # morpho_reco_tr_aitoff = tfile.Get("morpho_reco_tr_aitoff")
            # tot_morpho_reco_tr_aitoff.Add(morpho_reco_tr_aitoff)

            NevtsMC = tfile.Get("NevtsMC")
            tot_NevtsMC.Add(NevtsMC)

            sh_sindec_atmMC = tfile.Get("sh_sindec_atmMC")
            tot_sh_sindec_atmMC.Add(sh_sindec_atmMC)

            tr_sindec_atmMC = tfile.Get("tr_sindec_atmMC")
            tot_tr_sindec_atmMC.Add(tr_sindec_atmMC)

            sh_ra_atmMC = tfile.Get("sh_ra_atmMC")
            tot_sh_ra_atmMC.Add(sh_ra_atmMC)

            tr_ra_atmMC = tfile.Get("tr_ra_atmMC")
            tot_tr_ra_atmMC.Add(tr_ra_atmMC)

            sh_coszen_atmMC = tfile.Get("sh_coszen_atmMC")
            tot_sh_coszen_atmMC.Add(sh_coszen_atmMC)

            tr_coszen_atmMC = tfile.Get("tr_coszen_atmMC")
            tot_tr_coszen_atmMC.Add(tr_coszen_atmMC)

            sh_az_atmMC = tfile.Get("sh_az_atmMC")
            tot_sh_az_atmMC.Add(sh_az_atmMC)

            # sh_az_atmMC_nu = tfile.Get("sh_az_atmMC_nu")
            # tot_sh_az_atmMC_nu.Add(sh_az_atmMC_nu)
            # 
            # sh_az_atmMC_mu = tfile.Get("sh_az_atmMC_mu")
            # tot_sh_az_atmMC_mu.Add(sh_az_atmMC_mu)

            tr_az_atmMC = tfile.Get("tr_az_atmMC")
            tot_tr_az_atmMC.Add(tr_az_atmMC)

            sh_spectrum3d_e_KRA_bis = tfile.Get("sh_spectrum3d_e_KRA_bis")
            tot_sh_spectr3d_e_KRA_bis.Add(sh_spectrum3d_e_KRA_bis)

            sh_spectrum_e_KRA = tfile.Get("sh_spectrum_e_KRA")
            tot_sh_spectr_e_KRA.Add(sh_spectrum_e_KRA)

            # sh_spectrum3d_e_zen_KRA = tfile.Get("sh_spectrum3d_e_zen_KRA")
            # tot_sh_spectr3d_e_zen_KRA.Add(sh_spectrum3d_e_zen_KRA)

            # tr_spectrum3d_e_zen_KRA = tfile.Get("tr_spectrum3d_e_zen_KRA")
            # tot_tr_spectr3d_e_zen_KRA.Add(tr_spectrum3d_e_zen_KRA)

            acc_sh_KRA = tfile.Get("acc_sh_KRA")
            tot_acc_sh_KRA.Add(acc_sh_KRA)

            acc_tr_KRA = tfile.Get("acc_tr_KRA")
            tot_acc_tr_KRA.Add(acc_tr_KRA)

            tr_spectrum3d_e_KRA_bis = tfile.Get("tr_spectrum3d_e_KRA_bis")
            tot_tr_spectr3d_e_KRA_bis.Add(tr_spectrum3d_e_KRA_bis)

            tr_spectrum_e_KRA = tfile.Get("tr_spectrum_e_KRA")
            tot_tr_spectr_e_KRA.Add(tr_spectrum_e_KRA)

            tr_spectr_e_atmmc = tfile.Get("tr_spectr_e_atmmc")
            tot_tr_spectr_e_atm.Add(tr_spectr_e_atmmc)

            tr_spectr2d_e_atm_zen_initial = tfile.Get("tr_spectr2d_e_atmmc_zen")
            tot_tr_spectr2d_e_atm_zen_initial.Add(tr_spectr2d_e_atm_zen_initial)

            # acc_sh_zen_KRA        = tfile.Get("acc_sh_zen_KRA")
            # tot_acc_sh_zen_KRA.Add(acc_sh_zen_KRA)
            #
            # acc_tr_zen_KRA        = tfile.Get("acc_tr_zen_KRA")
            # tot_acc_tr_zen_KRA.Add(acc_tr_zen_KRA)

            histo_run_durMC = tfile.Get("histo_run_durMC")
            tot_histo_run_durMC.Add(histo_run_durMC)

            histo_run_numberMC = tfile.Get("histo_run_numberMC")
            for bin_ in range(1, histo_run_numberMC .GetXaxis().GetNbins() + 1):
                if tot_histo_run_numberMC .GetBinContent(bin_) == 0:
                    tot_histo_run_numberMC .SetBinContent(
                        bin_, histo_run_numberMC .GetBinContent(bin_))

            sh_spectr_atmmc = tfile.Get("sh_spectr_atmmc")
            tot_sh_spectr_atm.Add(sh_spectr_atmmc)

            sh_spectr_e_atmmc = tfile.Get("sh_spectr_e_atmmc")
            tot_sh_spectr_e_atm.Add(sh_spectr_e_atmmc)

            sh_spectr_e_nuatmmc = tfile.Get("sh_spectr_e_nuatmmc")
            tot_sh_spectr_e_nuatm.Add(sh_spectr_e_nuatmmc)

            sh_spectr_e_muatmmc = tfile.Get("sh_spectr_e_muatmmc")
            tot_sh_spectr_e_muatm.Add(sh_spectr_e_muatmmc)

            sh_spectr2d_e_atmmc_zen = tfile.Get("sh_spectr2d_e_atmmc_zen")
            tot_sh_spectr2d_e_atm_zen_initial.Add(sh_spectr2d_e_atmmc_zen)

            sh_spectr2d_e_nuatmmc_zen = tfile.Get("sh_spectr2d_e_nuatmmc_zen")
            tot_sh_spectr2d_e_atm_zen_final.Add(sh_spectr2d_e_nuatmmc_zen)

            sh_spectr2d_e_muatmmc_zen = tfile.Get("sh_spectr2d_e_muatmmc_zen")
            tot_sh_spectr2d_e_muatm_zen.Add(sh_spectr2d_e_muatmmc_zen)

            sh_spectr2d_e_muatmmc_zen_bef_cuts = tfile.Get(
                "sh_spectr2d_e_muatmmc_zen_bef_cuts")
            tot_sh_spectr2d_e_muatm_zen_bef_cuts.Add(
                sh_spectr2d_e_muatmmc_zen_bef_cuts)

            tr_spectr_atmmc = tfile.Get("tr_spectr_atmmc")
            tot_tr_spectr_atm.Add(tr_spectr_atmmc)

            tr_spectr2d_atmmc = tfile.Get("tr_spectr2d_atmmc")
            tot_tr_spectr2d_atm.Add(tr_spectr2d_atmmc)

            tr_spectr2d_atmmc_zen = tfile.Get("tr_spectr2d_atmmc_zen")
            tot_tr_spectr2d_atm_zen.Add(tr_spectr2d_atmmc_zen)

            tr_spectr2d_e_muatmmc_zen_bef_cuts = tfile.Get(
                "tr_spectr2d_e_muatmmc_zen_bef_cuts")
            tot_tr_spectr2d_e_muatm_zen_bef_cuts.Add(
                tr_spectr2d_e_muatmmc_zen_bef_cuts)

            tr_spectr2d_e_muatm_zen = tfile.Get("tr_spectr2d_e_atmmc_zen_mup")
            tot_tr_spectr2d_e_muatm_zen.Add(tr_spectr2d_e_muatm_zen)

            # Data/MC comparison
            MC_sh_cos_zen_cut = tfile.Get("MC_sh_cos_zen_cut")
            tot_MC_sh_cos_zen_cut.Add(MC_sh_cos_zen_cut)

            MC_sh_M_est = tfile.Get("MC_sh_M_est")
            tot_MC_sh_M_est.Add(MC_sh_M_est)

            MC_sh_beta = tfile.Get("MC_sh_beta")
            tot_MC_sh_beta.Add(MC_sh_beta)

            MC_sh_RDF = tfile.Get("MC_sh_RDF")
            tot_MC_sh_RDF.Add(MC_sh_RDF)

            MC_sh_lik_muVeto = tfile.Get("MC_sh_lik_muVeto")
            tot_MC_sh_lik_muVeto.Add(MC_sh_lik_muVeto)

            MC_tr_cos_zen_cut = tfile.Get("MC_tr_cos_zen_cut")
            tot_MC_tr_cos_zen_cut.Add(MC_tr_cos_zen_cut)

            MC_tr_lambda = tfile.Get("MC_tr_lambda")
            tot_MC_tr_lambda.Add(MC_tr_lambda)

            MC_tr_beta = tfile.Get("MC_tr_beta")
            tot_MC_tr_beta.Add(MC_tr_beta)

            if "tau" in sys.argv:
                acc_sh_tauCC_had = tfile.Get("acc_sh_tauCC_had")
                tot_acc_sh_tauCC_had.Add(acc_sh_tauCC_had)

                acc_sh_tauCC_e = tfile.Get("acc_sh_tauCC_e")
                tot_acc_sh_tauCC_e.Add(acc_sh_tauCC_e)

                acc_sh_tauCC_mu = tfile.Get("acc_sh_tauCC_mu")
                tot_acc_sh_tauCC_mu.Add(acc_sh_tauCC_mu)

                acc_tr_tauCC_mu = tfile.Get("acc_tr_tauCC_mu")
                tot_acc_tr_tauCC_mu.Add(acc_tr_tauCC_mu)

                # sh_spectrum_nueNC   = tfile.Get("sh_spectrum_nueNC")
                # tot_sh_spectr_nueNC.Add(sh_spectrum_nueNC)
                #
                # sh_spectrum_numuNC  = tfile.Get("sh_spectrum_numuNC")
                # tot_sh_spectr_numuNC.Add(sh_spectrum_numuNC)
                #
                # sh_spectrum_nutauNC = tfile.Get("sh_spectrum_nutauNC")
                # tot_sh_spectr_nutauNC.Add(sh_spectrum_nutauNC)


    ######### Data #########
    if "MC" not in sys.argv and "tau" not in sys.argv:
        #
        # tfile = TFile.Open("/sps/km3net/users/tgregoir/gal_plane/software/ingredients/test_ing_files_data.root")
        # tr_bgr_vs_sindec   = tfile.Get("tr_bgr_vs_sindec")
        # tot_tr_bgr_vs_sindec.Add(tr_bgr_vs_sindec)
        #
        # tr_bgr_vs_sindec_rebinned     = tot_tr_bgr_vs_sindec.Rebin(10, "tr_bgr_vs_sindec_rebinned")
        # tr_bgr_vs_sindec_spline       = TSpline3(tr_bgr_vs_sindec_rebinned)
        #
        # tr_bgr_vs_sindec_spline.Draw()

        for infile in filesData:
            tfile = TFile.Open(infile)

            if tfile.GetNkeys() == 0:
                print infile, "is empty !!!"
                n_empty_files += 1
                empty_files .append(infile)
                continue
            else:
                print infile

            if not no_old_plots:
                hist_begin_runData = tfile.Get("hist_begin_runData")
                tot_hist_begin_runData.Add(hist_begin_runData)

                hist_end_runData = tfile.Get("hist_end_runData")
                tot_hist_end_runData.Add(hist_end_runData)

            NevtsData = tfile.Get("NevtsData")
            tot_NevtsData.Add(NevtsData)

            sh_spectrum_bgr = tfile.Get("sh_spectrum_bgr")
            tot_sh_spectrum_bgr.Add(sh_spectrum_bgr)

            tr_spectrum_bgr = tfile.Get("tr_spectrum_bgr")
            tot_tr_spectrum_bgr.Add(tr_spectrum_bgr)

            sh_spectrum_e_bgr = tfile.Get("sh_spectrum_e_bgr")
            tot_sh_spectrum_e_bgr.Add(sh_spectrum_e_bgr)

            tr_spectrum_e_bgr = tfile.Get("tr_spectrum_e_bgr")
            tot_tr_spectrum_e_bgr.Add(tr_spectrum_e_bgr)

            sh_bgr_vs_sindec = tfile.Get("sh_bgr_vs_sindec")
            tot_sh_bgr_vs_sindec.Add(sh_bgr_vs_sindec)

            tr_bgr_vs_sindec = tfile.Get("tr_bgr_vs_sindec")
            tot_tr_bgr_vs_sindec.Add(tr_bgr_vs_sindec)

            sh_bgr_vs_ra = tfile.Get("sh_bgr_vs_ra")
            tot_sh_bgr_vs_ra.Add(sh_bgr_vs_ra)

            tr_bgr_vs_ra = tfile.Get("tr_bgr_vs_ra")
            tot_tr_bgr_vs_ra.Add(tr_bgr_vs_ra)

            sh_bgr_vs_coszen = tfile.Get("sh_bgr_vs_coszen")
            tot_sh_bgr_vs_coszen.Add(sh_bgr_vs_coszen)

            tr_bgr_vs_coszen = tfile.Get("tr_bgr_vs_coszen")
            tot_tr_bgr_vs_coszen.Add(tr_bgr_vs_coszen)

            sh_bgr_vs_az = tfile.Get("sh_bgr_vs_az")
            tot_sh_bgr_vs_az.Add(sh_bgr_vs_az)

            tr_bgr_vs_az = tfile.Get("tr_bgr_vs_az")
            tot_tr_bgr_vs_az.Add(tr_bgr_vs_az)

            # Data/MC comparison
            Data_sh_cos_zen_cut = tfile.Get("Data_sh_cos_zen_cut")
            tot_Data_sh_cos_zen_cut.Add(Data_sh_cos_zen_cut)

            Data_sh_M_est = tfile.Get("Data_sh_M_est")
            tot_Data_sh_M_est.Add(Data_sh_M_est)

            Data_sh_beta = tfile.Get("Data_sh_beta")
            tot_Data_sh_beta.Add(Data_sh_beta)

            Data_sh_RDF = tfile.Get("Data_sh_RDF")
            tot_Data_sh_RDF.Add(Data_sh_RDF)

            Data_sh_lik_muVeto = tfile.Get("Data_sh_lik_muVeto")
            tot_Data_sh_lik_muVeto.Add(Data_sh_lik_muVeto)

            Data_tr_cos_zen_cut = tfile.Get("Data_tr_cos_zen_cut")
            tot_Data_tr_cos_zen_cut.Add(Data_tr_cos_zen_cut)

            Data_tr_lambda = tfile.Get("Data_tr_lambda")
            tot_Data_tr_lambda.Add(Data_tr_lambda)

            Data_tr_beta = tfile.Get("Data_tr_beta")
            tot_Data_tr_beta.Add(Data_tr_beta)

            # histo_run_durData    = tfile.Get("histo_run_durData")
            # tot_histo_run_durData.Add(histo_run_durData)

            histo_run_numberData = tfile.Get("histo_run_numberData")

            for bin_ in range(1, histo_run_numberData.GetXaxis().GetNbins() + 1):
                if tot_histo_run_numberData.GetBinContent(bin_) == 0:
                    tot_histo_run_numberData.SetBinContent(
                        bin_, histo_run_numberData.GetBinContent(bin_))

    # ########## Useless, I think ########## because the spline goes below zero
    #     sh_spectrum_bgr_rebinned      = tot_sh_spectrum_bgr.Rebin(10, "sh_spectrum_bgr_rebinned")
    #     sh_spectrum_bgr_spline        = TSpline3(sh_spectrum_bgr_rebinned)
    #     tr_spectrum_bgr_rebinned      = tot_tr_spectrum_bgr.Rebin(10, "tr_spectrum_bgr_rebinned")
    #     tr_spectrum_bgr_spline        = TSpline3(tr_spectrum_bgr_rebinned)
    #
    #     sh_spectrum_bgr_rebinned_bis  = tot_sh_spectrum_bgr.Rebin(20, "sh_spectrum_bgr_rebinned_bis")
    #     sh_spectrum_bgr_spline_bis    = TSpline3(sh_spectrum_bgr_rebinned_bis)
    #     tr_spectrum_bgr_rebinned_bis  = tot_tr_spectrum_bgr.Rebin(20, "tr_spectrum_bgr_rebinned_bis")
    #     tr_spectrum_bgr_spline_bis    = TSpline3(tr_spectrum_bgr_rebinned_bis)


    ########## Stretching and rebinning histograms ##########
    # The stretching is made to have a spline with the same range than the
    # histogram

        # for i in range(1, tot_sh_bgr_vs_sindec .GetXaxis().GetNbins()+1):
        #     sh_bgr_vs_sindec_rebinned_stretched .Fill(sh_bgr_vs_sindec_rebinned_stretched .GetBinCenter((i-1)/9+1), tot_sh_bgr_vs_sindec .GetBinContent(i))
        # sh_bgr_vs_sindec_rebinned_stretched .Scale(1./9.)
        # for i in range(1, tot_sh_bgr_vs_sindec .GetXaxis().GetNbins()+1):
        #     sh_bgr_vs_sindec_rebinned_stretched_bis .Fill(sh_bgr_vs_sindec_rebinned_stretched_bis .GetBinCenter((i-1)/18+1), tot_sh_bgr_vs_sindec .GetBinContent(i))
        # sh_bgr_vs_sindec_rebinned_stretched_bis .Scale(1./18.)
        #
        # for i in range(1, tot_tr_bgr_vs_sindec .GetXaxis().GetNbins()+1):
        #     tr_bgr_vs_sindec_rebinned_stretched .Fill(tr_bgr_vs_sindec_rebinned_stretched .GetBinCenter((i-1)/9+1), tot_tr_bgr_vs_sindec .GetBinContent(i))
        # tr_bgr_vs_sindec_rebinned_stretched .Scale(1./9.)
        # for i in range(1, tot_tr_bgr_vs_sindec .GetXaxis().GetNbins()+1):
        #     tr_bgr_vs_sindec_rebinned_stretched_bis .Fill(tr_bgr_vs_sindec_rebinned_stretched_bis .GetBinCenter((i-1)/18+1), tot_tr_bgr_vs_sindec .GetBinContent(i))
        # tr_bgr_vs_sindec_rebinned_stretched_bis .Scale(1./18.)
        #
        #
        # for i in range(1, tot_sh_bgr_vs_coszen .GetXaxis().GetNbins()+1):
        #     sh_bgr_vs_coszen_rebinned_stretched .Fill(sh_bgr_vs_coszen_rebinned_stretched .GetBinCenter((i-1)/6+1), tot_sh_bgr_vs_coszen .GetBinContent(i))
        # sh_bgr_vs_coszen_rebinned_stretched .Scale(1./6.)
        # for i in range(1, tot_sh_bgr_vs_coszen .GetXaxis().GetNbins()+1):
        #     sh_bgr_vs_coszen_rebinned_stretched_bis .Fill(sh_bgr_vs_coszen_rebinned_stretched_bis .GetBinCenter((i-1)/12+1), tot_sh_bgr_vs_coszen .GetBinContent(i))
        # sh_bgr_vs_coszen_rebinned_stretched_bis .Scale(1./12.)
        #
        # for i in range(1, tot_tr_bgr_vs_coszen .GetXaxis().GetNbins()+1):
        #     tr_bgr_vs_coszen_rebinned_stretched .Fill(tr_bgr_vs_coszen_rebinned_stretched .GetBinCenter((i-1)/6+1), tot_tr_bgr_vs_coszen .GetBinContent(i))
        # tr_bgr_vs_coszen_rebinned_stretched .Scale(1./6.)
        # for i in range(1, tot_tr_bgr_vs_coszen .GetXaxis().GetNbins()+1):
        #     tr_bgr_vs_coszen_rebinned_stretched_bis .Fill(tr_bgr_vs_coszen_rebinned_stretched_bis .GetBinCenter((i-1)/12+1), tot_tr_bgr_vs_coszen .GetBinContent(i))
        # tr_bgr_vs_coszen_rebinned_stretched_bis .Scale(1./12.)
        #
        #
        # sh_bgr_vs_sindec_spline       = TSpline3(sh_bgr_vs_sindec_rebinned_stretched)
        # tr_bgr_vs_sindec_spline       = TSpline3(tr_bgr_vs_sindec_rebinned_stretched)
        # sh_bgr_vs_sindec_spline_bis   = TSpline3(sh_bgr_vs_sindec_rebinned_stretched_bis)
        # tr_bgr_vs_sindec_spline_bis   = TSpline3(tr_bgr_vs_sindec_rebinned_stretched_bis)
        # sh_bgr_vs_coszen_spline       = TSpline3(sh_bgr_vs_coszen_rebinned_stretched)
        # tr_bgr_vs_coszen_spline       = TSpline3(tr_bgr_vs_coszen_rebinned_stretched)
        # sh_bgr_vs_coszen_spline_bis   = TSpline3(sh_bgr_vs_coszen_rebinned_stretched_bis)
        # tr_bgr_vs_coszen_spline_bis   = TSpline3(tr_bgr_vs_coszen_rebinned_stretched_bis)

        # Splines with more points !
        # sh_bgr_vs_sindec_rebinned = tot_sh_bgr_vs_sindec.Rebin(3, "sh_bgr_vs_sindec_rebinned")
        # sh_bgr_vs_sindec_rebinned .Scale(1. / 3.)
        # sh_bgr_vs_sindec_graph = TGraph(32)
        # sh_bgr_vs_sindec_graph .SetPoint(0, -1, (tot_sh_bgr_vs_sindec .GetBinContent(1) + tot_sh_bgr_vs_sindec .GetBinContent(2)) / 2.)
        # for thisbin in range(1, sh_bgr_vs_sindec_rebinned  .GetNbinsX() + 1):
        #     sh_bgr_vs_sindec_graph .SetPoint(thisbin, sh_bgr_vs_sindec_rebinned .GetBinCenter(thisbin), sh_bgr_vs_sindec_rebinned .GetBinContent(thisbin))
        # sh_bgr_vs_sindec_graph .SetPoint(31, 0.8, (tot_sh_bgr_vs_sindec .GetBinContent(90) + tot_sh_bgr_vs_sindec .GetBinContent(90 - 1)) / 2.)
        #
        # sh_bgr_vs_sindec_rebinned_bis = tot_sh_bgr_vs_sindec.Rebin(15, "sh_bgr_vs_sindec_rebinned_bis")
        # sh_bgr_vs_sindec_rebinned_bis .Scale(1. / 15.)
        # sh_bgr_vs_sindec_graph_bis = TGraph(8)
        # sh_bgr_vs_sindec_graph_bis .SetPoint(0, -1, (tot_sh_bgr_vs_sindec .GetBinContent(1) + tot_sh_bgr_vs_sindec .GetBinContent(2)) / 2.)
        # for thisbin in range(1, sh_bgr_vs_sindec_rebinned_bis .GetNbinsX() + 1):
        #     sh_bgr_vs_sindec_graph_bis .SetPoint(thisbin, sh_bgr_vs_sindec_rebinned_bis .GetBinCenter(thisbin), sh_bgr_vs_sindec_rebinned_bis .GetBinContent(thisbin))
        # sh_bgr_vs_sindec_graph_bis .SetPoint(7, 0.8, (tot_sh_bgr_vs_sindec .GetBinContent(90) + tot_sh_bgr_vs_sindec .GetBinContent(90 - 1)) / 2.)
        #
        # tr_bgr_vs_sindec_rebinned = tot_tr_bgr_vs_sindec.Rebin(6, "tr_bgr_vs_sindec_rebinned")
        # tr_bgr_vs_sindec_rebinned .Scale(1. / 6.)
        # tr_bgr_vs_sindec_graph = TGraph(32)
        # tr_bgr_vs_sindec_graph .SetPoint(0, -1, (tot_tr_bgr_vs_sindec .GetBinContent(1) + tot_tr_bgr_vs_sindec .GetBinContent(2)) / 2.)
        # for thisbin in range(1, tr_bgr_vs_sindec_rebinned .GetNbinsX() + 1):
        #     tr_bgr_vs_sindec_graph .SetPoint(thisbin, tr_bgr_vs_sindec_rebinned .GetBinCenter(thisbin), tr_bgr_vs_sindec_rebinned .GetBinContent(thisbin))
        # tr_bgr_vs_sindec_graph .SetPoint(31, 0.8, (tot_tr_bgr_vs_sindec .GetBinContent(90 * 2) + tot_tr_bgr_vs_sindec .GetBinContent(90 * 2 - 1)) / 2.)
        #
        # tr_bgr_vs_sindec_rebinned_bis = tot_tr_bgr_vs_sindec.Rebin(20, "tr_bgr_vs_sindec_rebinned_bis")
        # tr_bgr_vs_sindec_rebinned_bis .Scale(1. / 20.)
        # tr_bgr_vs_sindec_graph_bis = TGraph(11)
        # tr_bgr_vs_sindec_graph_bis .SetPoint(0, -1, (tot_tr_bgr_vs_sindec .GetBinContent(1) + tot_tr_bgr_vs_sindec .GetBinContent(2)) / 2.)
        # for thisbin in range(1, tr_bgr_vs_sindec_rebinned_bis .GetNbinsX() + 1):
        #     tr_bgr_vs_sindec_graph_bis .SetPoint(thisbin, tr_bgr_vs_sindec_rebinned_bis .GetBinCenter(thisbin), tr_bgr_vs_sindec_rebinned_bis .GetBinContent(thisbin))
        # tr_bgr_vs_sindec_graph_bis .SetPoint(10, 0.8, (tot_tr_bgr_vs_sindec .GetBinContent(90 * 2) + tot_tr_bgr_vs_sindec .GetBinContent(90 * 2 - 1)) / 2.)
        #
        # sh_bgr_vs_coszen_rebinned = tot_sh_bgr_vs_coszen.Rebin(2, "sh_bgr_vs_coszen_rebinned")
        # sh_bgr_vs_coszen_rebinned .Scale(1. / 2.)
        # sh_bgr_vs_coszen_graph = TGraph(26)
        # sh_bgr_vs_coszen_graph .SetPoint(0, -1, (tot_sh_bgr_vs_coszen .GetBinContent(1) + sh_bgr_vs_coszen .GetBinContent(2)) / 2.)
        # for thisbin in range(1, sh_bgr_vs_coszen_rebinned .GetNbinsX() + 1):
        #     sh_bgr_vs_coszen_graph .SetPoint(thisbin, sh_bgr_vs_coszen_rebinned .GetBinCenter(thisbin), sh_bgr_vs_coszen_rebinned .GetBinContent(thisbin))
        # sh_bgr_vs_coszen_graph .SetPoint(25, 0.1, (tot_sh_bgr_vs_coszen .GetBinContent(48) + tot_sh_bgr_vs_coszen .GetBinContent(48 - 1)) / 2.)
        #
        # sh_bgr_vs_coszen_rebinned_bis = tot_sh_bgr_vs_coszen.Rebin(12, "sh_bgr_vs_coszen_rebinned_bis")
        # sh_bgr_vs_coszen_rebinned_bis .Scale(1. / 12.)
        # sh_bgr_vs_coszen_graph_bis = TGraph(6)
        # sh_bgr_vs_coszen_graph_bis .SetPoint(0, -1, (tot_sh_bgr_vs_coszen .GetBinContent(1) + sh_bgr_vs_coszen .GetBinContent(2)) / 2.)
        # for thisbin in range(1, sh_bgr_vs_coszen_rebinned_bis .GetNbinsX() + 1):
        #     sh_bgr_vs_coszen_graph_bis .SetPoint(thisbin, sh_bgr_vs_coszen_rebinned_bis .GetBinCenter(thisbin), sh_bgr_vs_coszen_rebinned_bis .GetBinContent(thisbin))
        # sh_bgr_vs_coszen_graph_bis .SetPoint(5, 0.1, (tot_sh_bgr_vs_coszen .GetBinContent(48) + tot_sh_bgr_vs_coszen .GetBinContent(48 - 1)) / 2.)
        #
        # tr_bgr_vs_coszen_rebinned = tot_tr_bgr_vs_coszen.Rebin(4, "tr_bgr_vs_coszen_rebinned")
        # tr_bgr_vs_coszen_rebinned .Scale(1. / 4.)
        # tr_bgr_vs_coszen_graph = TGraph(26)
        # tr_bgr_vs_coszen_graph .SetPoint(0, -1, (tot_tr_bgr_vs_coszen .GetBinContent(1) + tot_tr_bgr_vs_coszen .GetBinContent(2)) / 2.)
        # for thisbin in range(1, tr_bgr_vs_coszen_rebinned .GetNbinsX() + 1):
        #     tr_bgr_vs_coszen_graph .SetPoint(thisbin, tr_bgr_vs_coszen_rebinned .GetBinCenter(thisbin), tr_bgr_vs_coszen_rebinned .GetBinContent(thisbin))
        # tr_bgr_vs_coszen_graph .SetPoint(25, 0.1, (tot_tr_bgr_vs_coszen .GetBinContent(48 * 2) + tot_tr_bgr_vs_coszen .GetBinContent(48 * 2 - 1)) / 2.)
        #
        # tr_bgr_vs_coszen_rebinned_bis = tot_tr_bgr_vs_coszen.Rebin(16, "tr_bgr_vs_coszen_rebinned_bis")
        # tr_bgr_vs_coszen_rebinned_bis .Scale(1. / 16.)
        # tr_bgr_vs_coszen_graph_bis = TGraph(8)
        # tr_bgr_vs_coszen_graph_bis .SetPoint(0, -1, (tot_tr_bgr_vs_coszen .GetBinContent(1) + tot_tr_bgr_vs_coszen .GetBinContent(2)) / 2.)
        # for thisbin in range(1, tr_bgr_vs_coszen_rebinned_bis .GetNbinsX() + 1):
        #     tr_bgr_vs_coszen_graph_bis .SetPoint(thisbin, tr_bgr_vs_coszen_rebinned_bis .GetBinCenter(thisbin), tr_bgr_vs_coszen_rebinned_bis .GetBinContent(thisbin))
        # tr_bgr_vs_coszen_graph_bis .SetPoint(7, 0.1, (tot_tr_bgr_vs_coszen .GetBinContent(48 * 2) + tot_tr_bgr_vs_coszen .GetBinContent(48 * 2 - 1)) / 2.)
        #
        # tot_sh_bgr_vs_az .Scale(tot_sh_bgr_vs_az .GetNbinsX() / (2. * pi))
        # sh_bgr_vs_az_rebinned = tot_sh_bgr_vs_az.Rebin(10, "sh_bgr_vs_az_rebinned")
        # sh_bgr_vs_az_rebinned .Scale(1. / 10.)
        # sh_bgr_vs_az_graph = TGraph(32)
        # sh_bgr_vs_az_graph .SetPoint(0, 0, (tot_sh_bgr_vs_az .GetBinContent(1) + tot_sh_bgr_vs_az .GetBinContent(2) + tot_sh_bgr_vs_az .GetBinContent(3)) / 3.)
        # for thisbin in range(1, sh_bgr_vs_az_rebinned .GetNbinsX() + 1):
        #     sh_bgr_vs_az_graph .SetPoint(thisbin, sh_bgr_vs_az_rebinned .GetBinCenter(thisbin), sh_bgr_vs_az_rebinned .GetBinContent(thisbin))
        # sh_bgr_vs_az_graph .SetPoint(31, 2 * pi, (tot_sh_bgr_vs_az .GetBinContent(300) + tot_sh_bgr_vs_az .GetBinContent(300 - 1) + tot_sh_bgr_vs_az .GetBinContent(300 - 2)) / 3.)
        #
        # sh_bgr_vs_az_rebinned_bis = tot_sh_bgr_vs_az.Rebin(30, "sh_bgr_vs_az_rebinned_bis")
        # sh_bgr_vs_az_rebinned_bis .Scale(1. / 30.)
        # sh_bgr_vs_az_graph_bis = TGraph(12)
        # sh_bgr_vs_az_graph_bis .SetPoint(0, 0, (tot_sh_bgr_vs_az .GetBinContent(1) + tot_sh_bgr_vs_az .GetBinContent(2) + tot_sh_bgr_vs_az .GetBinContent(3)) / 3.)
        # for thisbin in range(1, sh_bgr_vs_az_rebinned_bis .GetNbinsX() + 1):
        #     sh_bgr_vs_az_graph_bis .SetPoint(thisbin, sh_bgr_vs_az_rebinned_bis .GetBinCenter(thisbin), sh_bgr_vs_az_rebinned_bis .GetBinContent(thisbin))
        # sh_bgr_vs_az_graph_bis .SetPoint(11, 2 * pi, (tot_sh_bgr_vs_az .GetBinContent(300) + tot_sh_bgr_vs_az .GetBinContent(300 - 1) + tot_sh_bgr_vs_az .GetBinContent(300 - 2)) / 3.)
        #
        # tot_tr_bgr_vs_az .Scale(tot_tr_bgr_vs_az .GetNbinsX() / (2. * pi))
        # tr_bgr_vs_az_rebinned = tot_tr_bgr_vs_az.Rebin(10, "tr_bgr_vs_az_rebinned")
        # tr_bgr_vs_az_rebinned .Scale(1. / 10.)
        # tr_bgr_vs_az_graph = TGraph(32)
        # tr_bgr_vs_az_graph .SetPoint(0, 0, (tot_tr_bgr_vs_az .GetBinContent(1) + tot_tr_bgr_vs_az .GetBinContent(2) + tot_tr_bgr_vs_az .GetBinContent(3)) / 3.)
        # for thisbin in range(1, tr_bgr_vs_az_rebinned .GetNbinsX() + 1):
        #     tr_bgr_vs_az_graph .SetPoint(thisbin, tr_bgr_vs_az_rebinned .GetBinCenter(thisbin), tr_bgr_vs_az_rebinned .GetBinContent(thisbin))
        # tr_bgr_vs_az_graph .SetPoint(31, 2 * pi, (tot_tr_bgr_vs_az .GetBinContent(300) + tot_tr_bgr_vs_az .GetBinContent(300 - 1) + tot_tr_bgr_vs_az .GetBinContent(300 - 2)) / 3.)
        #
        # tr_bgr_vs_az_rebinned_bis = tot_tr_bgr_vs_az.Rebin(30, "tr_bgr_vs_az_rebinned_bis")
        # tr_bgr_vs_az_rebinned_bis .Scale(1. / 30.)
        # tr_bgr_vs_az_graph_bis = TGraph(12)
        # tr_bgr_vs_az_graph_bis .SetPoint(0, 0, (tot_tr_bgr_vs_az .GetBinContent(1) + tot_tr_bgr_vs_az .GetBinContent(2) + tot_tr_bgr_vs_az .GetBinContent(3)) / 3.)
        # for thisbin in range(1, tr_bgr_vs_az_rebinned_bis .GetNbinsX() + 1):
        #     tr_bgr_vs_az_graph_bis .SetPoint(thisbin, tr_bgr_vs_az_rebinned_bis .GetBinCenter(thisbin), tr_bgr_vs_az_rebinned_bis .GetBinContent(thisbin))
        # tr_bgr_vs_az_graph_bis .SetPoint(11, 2 * pi, (tot_tr_bgr_vs_az .GetBinContent(300) + tot_tr_bgr_vs_az .GetBinContent(300 - 1) + tot_tr_bgr_vs_az .GetBinContent(300 - 2)) / 3.)

        # sh_bgr_vs_sindec_rebinned = tot_sh_bgr_vs_sindec.Rebin(
        #     10, "sh_bgr_vs_sindec_rebinned")
        # sh_bgr_vs_sindec_rebinned .Scale(1. / 10.)
        # sh_bgr_vs_sindec_graph = TGraph(11)
        # sh_bgr_vs_sindec_graph .SetPoint(
        #     0, -1, (tot_sh_bgr_vs_sindec .GetBinContent(1) + tot_sh_bgr_vs_sindec .GetBinContent(2)) / 2.)
        # for thisbin in range(1, sh_bgr_vs_sindec_rebinned  .GetNbinsX() + 1):
        #     sh_bgr_vs_sindec_graph .SetPoint(thisbin, sh_bgr_vs_sindec_rebinned .GetBinCenter(
        #         thisbin), sh_bgr_vs_sindec_rebinned .GetBinContent(thisbin))
        # sh_bgr_vs_sindec_graph .SetPoint(10, 0.8, (tot_sh_bgr_vs_sindec .GetBinContent(
        #     90) + tot_sh_bgr_vs_sindec .GetBinContent(90 - 1)) / 2.)
        # 
        # sh_bgr_vs_sindec_rebinned_bis = tot_sh_bgr_vs_sindec.Rebin(
        #     15, "sh_bgr_vs_sindec_rebinned_bis")
        # sh_bgr_vs_sindec_rebinned_bis .Scale(1. / 15.)
        # sh_bgr_vs_sindec_graph_bis = TGraph(8)
        # sh_bgr_vs_sindec_graph_bis .SetPoint(
        #     0, -1, (tot_sh_bgr_vs_sindec .GetBinContent(1) + tot_sh_bgr_vs_sindec .GetBinContent(2)) / 2.)
        # for thisbin in range(1, sh_bgr_vs_sindec_rebinned_bis .GetNbinsX() + 1):
        #     sh_bgr_vs_sindec_graph_bis .SetPoint(thisbin, sh_bgr_vs_sindec_rebinned_bis .GetBinCenter(
        #         thisbin), sh_bgr_vs_sindec_rebinned_bis .GetBinContent(thisbin))
        # sh_bgr_vs_sindec_graph_bis .SetPoint(7, 0.8, (tot_sh_bgr_vs_sindec .GetBinContent(
        #     90) + tot_sh_bgr_vs_sindec .GetBinContent(90 - 1)) / 2.)

        sh_bgr_vs_sindec_spline_bis2 = histo_to_spline( tot_sh_bgr_vs_sindec, 5)
        sh_bgr_vs_sindec_spline_bis2.SetName("sh_bgr_vs_sindec_rebinned_bis")

        sh_bgr_vs_sindec_spline2 = histo_to_spline( tot_sh_bgr_vs_sindec, 20 )
        sh_bgr_vs_sindec_spline2.SetName("sh_bgr_vs_sindec_rebinned")


        # tr_bgr_vs_sindec_rebinned = tot_tr_bgr_vs_sindec.Rebin(
        #     10, "tr_bgr_vs_sindec_rebinned")
        # tr_bgr_vs_sindec_rebinned .Scale(1. / 10.)
        # tr_bgr_vs_sindec_graph = TGraph(20)
        # tr_bgr_vs_sindec_graph .SetPoint(
        #     0, -1, (tot_tr_bgr_vs_sindec .GetBinContent(1) + tot_tr_bgr_vs_sindec .GetBinContent(2)) / 2.)
        # for thisbin in range(1, tr_bgr_vs_sindec_rebinned .GetNbinsX() + 1):
        #     tr_bgr_vs_sindec_graph .SetPoint(thisbin, tr_bgr_vs_sindec_rebinned .GetBinCenter(
        #         thisbin), tr_bgr_vs_sindec_rebinned .GetBinContent(thisbin))
        # tr_bgr_vs_sindec_graph .SetPoint(19, 0.8, (tot_tr_bgr_vs_sindec .GetBinContent(
        #     90 * 2) + tot_tr_bgr_vs_sindec .GetBinContent(90 * 2 - 1)) / 2.)
        # 
        # tr_bgr_vs_sindec_rebinned_bis = tot_tr_bgr_vs_sindec.Rebin(
        #     20, "tr_bgr_vs_sindec_rebinned_bis")
        # tr_bgr_vs_sindec_rebinned_bis .Scale(1. / 20.)
        # tr_bgr_vs_sindec_graph_bis = TGraph(11)
        # tr_bgr_vs_sindec_graph_bis .SetPoint(
        #     0, -1, (tot_tr_bgr_vs_sindec .GetBinContent(1) + tot_tr_bgr_vs_sindec .GetBinContent(2)) / 2.)
        # for thisbin in range(1, tr_bgr_vs_sindec_rebinned_bis .GetNbinsX() + 1):
        #     tr_bgr_vs_sindec_graph_bis .SetPoint(thisbin, tr_bgr_vs_sindec_rebinned_bis .GetBinCenter(
        #         thisbin), tr_bgr_vs_sindec_rebinned_bis .GetBinContent(thisbin))
        # tr_bgr_vs_sindec_graph_bis .SetPoint(10, 0.8, (tot_tr_bgr_vs_sindec .GetBinContent(
        #     90 * 2) + tot_tr_bgr_vs_sindec .GetBinContent(90 * 2 - 1)) / 2.)

        tr_bgr_vs_sindec_spline_bis2 = histo_to_spline( tot_tr_bgr_vs_sindec, 5)
        tr_bgr_vs_sindec_spline_bis2.SetName("tr_bgr_vs_sindec_rebinned_bis")

        tr_bgr_vs_sindec_spline2 = histo_to_spline( tot_tr_bgr_vs_sindec, 15 )
        tr_bgr_vs_sindec_spline2.SetName("tr_bgr_vs_sindec_rebinned")



        # sh_bgr_vs_coszen_rebinned = tot_sh_bgr_vs_coszen.Rebin(
        #     4, "sh_bgr_vs_coszen_rebinned")
        # sh_bgr_vs_coszen_rebinned .Scale(1. / 4.)
        # sh_bgr_vs_coszen_graph = TGraph(14)
        # sh_bgr_vs_coszen_graph .SetPoint(
        #     0, -1, (tot_sh_bgr_vs_coszen .GetBinContent(1) + sh_bgr_vs_coszen .GetBinContent(2)) / 2.)
        # for thisbin in range(1, sh_bgr_vs_coszen_rebinned .GetNbinsX() + 1):
        #     sh_bgr_vs_coszen_graph .SetPoint(thisbin, sh_bgr_vs_coszen_rebinned .GetBinCenter(
        #         thisbin), sh_bgr_vs_coszen_rebinned .GetBinContent(thisbin))
        # sh_bgr_vs_coszen_graph .SetPoint(13, 0.1, (tot_sh_bgr_vs_coszen .GetBinContent(
        #     48) + tot_sh_bgr_vs_coszen .GetBinContent(48 - 1)) / 2.)
        # 
        # sh_bgr_vs_coszen_rebinned_bis = tot_sh_bgr_vs_coszen.Rebin(
        #     12, "sh_bgr_vs_coszen_rebinned_bis")
        # sh_bgr_vs_coszen_rebinned_bis .Scale(1. / 12.)
        # sh_bgr_vs_coszen_graph_bis = TGraph(6)
        # sh_bgr_vs_coszen_graph_bis .SetPoint(
        #     0, -1, (tot_sh_bgr_vs_coszen .GetBinContent(1) + sh_bgr_vs_coszen .GetBinContent(2)) / 2.)
        # for thisbin in range(1, sh_bgr_vs_coszen_rebinned_bis .GetNbinsX() + 1):
        #     sh_bgr_vs_coszen_graph_bis .SetPoint(thisbin, sh_bgr_vs_coszen_rebinned_bis .GetBinCenter(
        #         thisbin), sh_bgr_vs_coszen_rebinned_bis .GetBinContent(thisbin))
        # sh_bgr_vs_coszen_graph_bis .SetPoint(5, 0.1, (tot_sh_bgr_vs_coszen .GetBinContent(
        #     48) + tot_sh_bgr_vs_coszen .GetBinContent(48 - 1)) / 2.)

        sh_bgr_vs_coszen_spline_bis2 = histo_to_spline( tot_sh_bgr_vs_coszen, 5 )
        sh_bgr_vs_coszen_spline_bis2.SetName("sh_bgr_vs_coszen_rebinned_bis")

        sh_bgr_vs_coszen_spline2 = histo_to_spline( tot_sh_bgr_vs_coszen, 15 )
        sh_bgr_vs_coszen_spline2.SetName("sh_bgr_vs_coszen_rebinned")


        # tr_bgr_vs_coszen_rebinned = tot_tr_bgr_vs_coszen.Rebin(
        #     8, "tr_bgr_vs_coszen_rebinned")
        # tr_bgr_vs_coszen_rebinned .Scale(1. / 8.)
        # tr_bgr_vs_coszen_graph = TGraph(14)
        # tr_bgr_vs_coszen_graph .SetPoint(
        #     0, -1, (tot_tr_bgr_vs_coszen .GetBinContent(1) + tot_tr_bgr_vs_coszen .GetBinContent(2)) / 2.)
        # for thisbin in range(1, tr_bgr_vs_coszen_rebinned .GetNbinsX() + 1):
        #     tr_bgr_vs_coszen_graph .SetPoint(thisbin, tr_bgr_vs_coszen_rebinned .GetBinCenter(
        #         thisbin), tr_bgr_vs_coszen_rebinned .GetBinContent(thisbin))
        # tr_bgr_vs_coszen_graph .SetPoint(13, 0.1, (tot_tr_bgr_vs_coszen .GetBinContent(
        #     48 * 2) + tot_tr_bgr_vs_coszen .GetBinContent(48 * 2 - 1)) / 2.)
        # 
        # tr_bgr_vs_coszen_rebinned_bis = tot_tr_bgr_vs_coszen.Rebin(
        #     16, "tr_bgr_vs_coszen_rebinned_bis")
        # tr_bgr_vs_coszen_rebinned_bis .Scale(1. / 16.)
        # tr_bgr_vs_coszen_graph_bis = TGraph(8)
        # tr_bgr_vs_coszen_graph_bis .SetPoint(
        #     0, -1, (tot_tr_bgr_vs_coszen .GetBinContent(1) + tot_tr_bgr_vs_coszen .GetBinContent(2)) / 2.)
        # for thisbin in range(1, tr_bgr_vs_coszen_rebinned_bis .GetNbinsX() + 1):
        #     tr_bgr_vs_coszen_graph_bis .SetPoint(thisbin, tr_bgr_vs_coszen_rebinned_bis .GetBinCenter(
        #         thisbin), tr_bgr_vs_coszen_rebinned_bis .GetBinContent(thisbin))
        # tr_bgr_vs_coszen_graph_bis .SetPoint(7, 0.1, (tot_tr_bgr_vs_coszen .GetBinContent(
        #     48 * 2) + tot_tr_bgr_vs_coszen .GetBinContent(48 * 2 - 1)) / 2.)

        tr_bgr_vs_coszen_spline_bis2 = histo_to_spline( tot_tr_bgr_vs_coszen, 5)
        tr_bgr_vs_coszen_spline_bis2.SetName("tr_bgr_vs_coszen_rebinned_bis")

        tr_bgr_vs_coszen_spline2 = histo_to_spline( tot_tr_bgr_vs_coszen, 10 )
        tr_bgr_vs_coszen_spline2.SetName("tr_bgr_vs_coszen_rebinned")

        # tot_sh_bgr_vs_az .Scale(tot_sh_bgr_vs_az .GetNbinsX() / (2. * pi))
        # sh_bgr_vs_az_rebinned = tot_sh_bgr_vs_az.Rebin(15, "sh_bgr_vs_az_rebinned")
        # sh_bgr_vs_az_rebinned .Scale(1. / 15.)
        # sh_bgr_vs_az_graph = TGraph(22)
        # sh_bgr_vs_az_graph .SetPoint(0, 0, (tot_sh_bgr_vs_az .GetBinContent(
        #     1) + tot_sh_bgr_vs_az .GetBinContent(2) + tot_sh_bgr_vs_az .GetBinContent(3)) / 3.)
        # for thisbin in range(1, sh_bgr_vs_az_rebinned .GetNbinsX() + 1):
        #     sh_bgr_vs_az_graph .SetPoint(thisbin, sh_bgr_vs_az_rebinned .GetBinCenter(
        #         thisbin), sh_bgr_vs_az_rebinned .GetBinContent(thisbin))
        # sh_bgr_vs_az_graph .SetPoint(21, 2 * pi, (tot_sh_bgr_vs_az .GetBinContent(
        #     300) + tot_sh_bgr_vs_az .GetBinContent(300 - 1) + tot_sh_bgr_vs_az .GetBinContent(300 - 2)) / 3.)
        # 
        # sh_bgr_vs_az_rebinned_bis = tot_sh_bgr_vs_az.Rebin(
        #     30, "sh_bgr_vs_az_rebinned_bis")
        # sh_bgr_vs_az_rebinned_bis .Scale(1. / 30.)
        # sh_bgr_vs_az_graph_bis = TGraph(12)
        # sh_bgr_vs_az_graph_bis .SetPoint(0, 0, (tot_sh_bgr_vs_az .GetBinContent(
        #     1) + tot_sh_bgr_vs_az .GetBinContent(2) + tot_sh_bgr_vs_az .GetBinContent(3)) / 3.)
        # for thisbin in range(1, sh_bgr_vs_az_rebinned_bis .GetNbinsX() + 1):
        #     sh_bgr_vs_az_graph_bis .SetPoint(thisbin, sh_bgr_vs_az_rebinned_bis .GetBinCenter(
        #         thisbin), sh_bgr_vs_az_rebinned_bis .GetBinContent(thisbin))
        # sh_bgr_vs_az_graph_bis .SetPoint(11, 2 * pi, (tot_sh_bgr_vs_az .GetBinContent(
        #     300) + tot_sh_bgr_vs_az .GetBinContent(300 - 1) + tot_sh_bgr_vs_az .GetBinContent(300 - 2)) / 3.)

        tot_sh_bgr_vs_az .Scale(tot_sh_bgr_vs_az .GetNbinsX() / (2. * pi))

        sh_bgr_vs_az_spline_bis2 = histo_to_spline( tot_sh_bgr_vs_az, 5, cyclic=True )
        sh_bgr_vs_az_spline_bis2.SetName("sh_bgr_vs_az_rebinned_bis")

        sh_bgr_vs_az_spline2 = histo_to_spline( tot_sh_bgr_vs_az, 15, cyclic=True )
        sh_bgr_vs_az_spline2.SetName("sh_bgr_vs_az_rebinned")

        # tot_tr_bgr_vs_az .Scale(tot_tr_bgr_vs_az .GetNbinsX() / (2. * pi))
        # tr_bgr_vs_az_rebinned = tot_tr_bgr_vs_az.Rebin(15, "tr_bgr_vs_az_rebinned")
        # tr_bgr_vs_az_rebinned .Scale(1. / 15.)
        # tr_bgr_vs_az_graph = TGraph(22)
        # tr_bgr_vs_az_graph .SetPoint(0, 0, (tot_tr_bgr_vs_az .GetBinContent(
        #     1) + tot_tr_bgr_vs_az .GetBinContent(2) + tot_tr_bgr_vs_az .GetBinContent(3)) / 3.)
        # for thisbin in range(1, tr_bgr_vs_az_rebinned .GetNbinsX() + 1):
        #     tr_bgr_vs_az_graph .SetPoint(thisbin, tr_bgr_vs_az_rebinned .GetBinCenter(
        #         thisbin), tr_bgr_vs_az_rebinned .GetBinContent(thisbin))
        # tr_bgr_vs_az_graph .SetPoint(21, 2 * pi, (tot_tr_bgr_vs_az .GetBinContent(
        #     300) + tot_tr_bgr_vs_az .GetBinContent(300 - 1) + tot_tr_bgr_vs_az .GetBinContent(300 - 2)) / 3.)
        # 
        # tr_bgr_vs_az_rebinned_bis = tot_tr_bgr_vs_az.Rebin(
        #     30, "tr_bgr_vs_az_rebinned_bis")
        # tr_bgr_vs_az_rebinned_bis .Scale(1. / 30.)
        # tr_bgr_vs_az_graph_bis = TGraph(12)
        # tr_bgr_vs_az_graph_bis .SetPoint(0, 0, (tot_tr_bgr_vs_az .GetBinContent(
        #     1) + tot_tr_bgr_vs_az .GetBinContent(2) + tot_tr_bgr_vs_az .GetBinContent(3)) / 3.)
        # for thisbin in range(1, tr_bgr_vs_az_rebinned_bis .GetNbinsX() + 1):
        #     tr_bgr_vs_az_graph_bis .SetPoint(thisbin, tr_bgr_vs_az_rebinned_bis .GetBinCenter(
        #         thisbin), tr_bgr_vs_az_rebinned_bis .GetBinContent(thisbin))
        # tr_bgr_vs_az_graph_bis .SetPoint(11, 2 * pi, (tot_tr_bgr_vs_az .GetBinContent(
        #     300) + tot_tr_bgr_vs_az .GetBinContent(300 - 1) + tot_tr_bgr_vs_az .GetBinContent(300 - 2)) / 3.)

        tot_tr_bgr_vs_az .Scale(tot_tr_bgr_vs_az .GetNbinsX() / (2. * pi))

        tr_bgr_vs_az_spline_bis2 = histo_to_spline( tot_tr_bgr_vs_az, 5, cyclic=True )
        tr_bgr_vs_az_spline_bis2.SetName("tr_bgr_vs_az_rebinned_bis")

        tr_bgr_vs_az_spline2 = histo_to_spline( tot_tr_bgr_vs_az, 15, cyclic=True )
        tr_bgr_vs_az_spline2.SetName("tr_bgr_vs_az_rebinned")


        azimuth_array = np.array([0., 2*pi])
        Y_value = np.array([208/(2*pi), 208/(2*pi)])
        sh_bgr_graph_az_flat = TGraph(2, azimuth_array, Y_value)
        sh_bgr_spline_az_flat = TSpline3("sh_bgr_spline_az_flat", sh_bgr_graph_az_flat) # not needed for tracks
        sh_bgr_spline_az_flat .SetName("sh_bgr_spline_az_flat")

        # sh_bgr_vs_sindec_spline = TSpline3(
        #     "sh_bgr_vs_sindec_rebinned", sh_bgr_vs_sindec_graph)
        # sh_bgr_vs_sindec_spline     .SetName("sh_bgr_vs_sindec_rebinned")
        # tr_bgr_vs_sindec_spline = TSpline3(
        #     "tr_bgr_vs_sindec_rebinned", tr_bgr_vs_sindec_graph)
        # tr_bgr_vs_sindec_spline     .SetName("tr_bgr_vs_sindec_rebinned")
        # sh_bgr_vs_sindec_spline_bis = TSpline3(
        #     "sh_bgr_vs_sindec_rebinned_bis", sh_bgr_vs_sindec_graph_bis)
        # sh_bgr_vs_sindec_spline_bis .SetName("sh_bgr_vs_sindec_rebinned_bis")
        # tr_bgr_vs_sindec_spline_bis = TSpline3(
        #     "tr_bgr_vs_sindec_rebinned_bis", tr_bgr_vs_sindec_graph_bis)
        # tr_bgr_vs_sindec_spline_bis .SetName("tr_bgr_vs_sindec_rebinned_bis")
        # 
        # sh_bgr_vs_coszen_spline = TSpline3(
        #     "sh_bgr_vs_coszen_rebinned", sh_bgr_vs_coszen_graph)
        # sh_bgr_vs_coszen_spline     .SetName("sh_bgr_vs_coszen_rebinned")
        # tr_bgr_vs_coszen_spline = TSpline3(
        #     "tr_bgr_vs_coszen_rebinned", tr_bgr_vs_coszen_graph)
        # tr_bgr_vs_coszen_spline     .SetName("tr_bgr_vs_coszen_rebinned")
        # sh_bgr_vs_coszen_spline_bis = TSpline3(
        #     "sh_bgr_vs_coszen_rebinned_bis", sh_bgr_vs_coszen_graph_bis)
        # sh_bgr_vs_coszen_spline_bis .SetName("sh_bgr_vs_coszen_rebinned_bis")
        # tr_bgr_vs_coszen_spline_bis = TSpline3(
        #     "tr_bgr_vs_coszen_rebinned_bis", tr_bgr_vs_coszen_graph_bis)
        # tr_bgr_vs_coszen_spline_bis .SetName("tr_bgr_vs_coszen_rebinned_bis")
        # 
        # sh_bgr_vs_az_spline = TSpline3("sh_bgr_vs_az_rebinned", sh_bgr_vs_az_graph)
        # sh_bgr_vs_az_spline     .SetName("sh_bgr_vs_az_rebinned")
        # tr_bgr_vs_az_spline = TSpline3("tr_bgr_vs_az_rebinned", tr_bgr_vs_az_graph)
        # tr_bgr_vs_az_spline     .SetName("tr_bgr_vs_az_rebinned")
        # sh_bgr_vs_az_spline_bis = TSpline3(
        #     "sh_bgr_vs_az_rebinned_bis", sh_bgr_vs_az_graph_bis)
        # sh_bgr_vs_az_spline_bis .SetName("sh_bgr_vs_az_rebinned_bis")
        # tr_bgr_vs_az_spline_bis = TSpline3(
        #     "tr_bgr_vs_az_rebinned_bis", tr_bgr_vs_az_graph_bis)
        # tr_bgr_vs_az_spline_bis .SetName("tr_bgr_vs_az_rebinned_bis")


        ########## Build the PSF and make a spline from it ##########
    lowlogang_tr = -1.3
    lowlogang_sh = -0.3

    if not tau and "Data" not in sys.argv:
        if not no_old_plots:
            ##### Phy0 #####
            for x in range(1, tot_PSF_tr .GetNbinsX() + 1):
                con = tot_PSF_tr .GetBinContent(x)
                xxx = 10 ** tot_PSF_tr .GetBinCenter(x) * pi / 180.
                s = sin(xxx) * log(10) * xxx
                tot_PSF_tr .SetBinContent(x, con / s)
            bins = [b for b in range(1, tot_PSF_tr .GetNbinsX(
            ) + 1) if tot_PSF_tr .GetBinCenter(b) < lowlogang_tr]
            sum_ = sum([tot_PSF_tr .GetBinContent(b) for b in bins])
            tot_PSF_tr .Scale(1. / (sum_ / len(bins)))

            grph_PSF_tr = TGraph(tot_PSF_tr .GetNbinsX())
            grph_PSF_tr .SetPoint(0, -2.5, 1)
            firstBins = False
            for thisbin in reversed(range(1, tot_PSF_tr .GetNbinsX() + 1)):
                if tot_PSF_tr .GetBinContent(thisbin) > 1 or (tot_PSF_tr .GetBinContent(thisbin) < tot_PSF_tr .GetBinContent(thisbin + 1) and tot_PSF_sh .GetBinContent(thisbin) > 0.8) or firstBins:
                    grph_PSF_tr .SetPoint(
                        thisbin, tot_PSF_tr .GetBinCenter(thisbin), 1)
                    firstBins = True
                else:
                    grph_PSF_tr .SetPoint(thisbin, tot_PSF_tr .GetBinCenter(
                        thisbin), tot_PSF_tr .GetBinContent(thisbin))
            PSF_spline_tr = TSpline3("PSF_spline_tr", grph_PSF_tr)
            PSF_spline_tr .SetName("PSF_spline_tr")

            for x in range(1, tot_PSF_sh .GetNbinsX() + 1):
                con = tot_PSF_sh .GetBinContent(x)
                xxx = 10 ** tot_PSF_sh .GetBinCenter(x) * pi / 180.
                s = sin(xxx) * log(10) * xxx
                tot_PSF_sh .SetBinContent(x, con / s)
            bins = [b for b in range(1, tot_PSF_sh .GetNbinsX(
            ) + 1) if tot_PSF_sh .GetBinCenter(b) < lowlogang_sh]
            sum_ = sum([tot_PSF_sh .GetBinContent(b) for b in bins])
            tot_PSF_sh .Scale(1. / (sum_ / len(bins)))

            grph_PSF_sh = TGraph(tot_PSF_sh .GetNbinsX())
            grph_PSF_sh .SetPoint(0, -1, 1)
            firstBins = False
            for thisbin in reversed(range(1, tot_PSF_sh .GetNbinsX() + 1)):
                if tot_PSF_sh .GetBinContent(thisbin) > 1 or (tot_PSF_sh .GetBinContent(thisbin) < tot_PSF_sh .GetBinContent(thisbin + 1) and tot_PSF_sh .GetBinContent(thisbin) > 0.8) or firstBins:
                    grph_PSF_sh .SetPoint(
                        thisbin, tot_PSF_sh .GetBinCenter(thisbin), 1)
                    firstBins = True
                else:
                    grph_PSF_sh .SetPoint(thisbin, tot_PSF_sh .GetBinCenter(
                        thisbin), tot_PSF_sh .GetBinContent(thisbin))
            PSF_spline_sh = TSpline3("PSF_spline_sh", grph_PSF_sh)
            PSF_spline_sh .SetName("PSF_spline_sh")

            ##### KRA #####
            for x in range(1, tot_PSF_tr_KRA .GetNbinsX() + 1):
                con = tot_PSF_tr_KRA .GetBinContent(x)
                xxx = 10 ** tot_PSF_tr_KRA .GetBinCenter(x) * pi / 180.
                s = sin(xxx) * log(10) * xxx
                tot_PSF_tr_KRA .SetBinContent(x, con / s)
            bins = [b for b in range(1, tot_PSF_tr_KRA .GetNbinsX(
            ) + 1) if tot_PSF_tr_KRA .GetBinCenter(b) < lowlogang_tr]
            sum_ = sum([tot_PSF_tr_KRA .GetBinContent(b) for b in bins])
            tot_PSF_tr_KRA .Scale(1. / (sum_ / len(bins)))

            grph_PSF_tr_KRA = TGraph(tot_PSF_tr_KRA .GetNbinsX())
            grph_PSF_tr_KRA .SetPoint(0, -2.5, 1)
            firstBins = False
            for thisbin in reversed(range(1, tot_PSF_tr_KRA .GetNbinsX() + 1)):
                if tot_PSF_tr_KRA .GetBinContent(thisbin) > 1 or (tot_PSF_tr_KRA .GetBinContent(thisbin) < tot_PSF_tr_KRA .GetBinContent(thisbin + 1) and tot_PSF_sh .GetBinContent(thisbin) > 0.8) or firstBins:
                    grph_PSF_tr_KRA .SetPoint(
                        thisbin, tot_PSF_tr_KRA .GetBinCenter(thisbin), 1)
                    firstBins = True
                else:
                    grph_PSF_tr_KRA .SetPoint(thisbin, tot_PSF_tr_KRA .GetBinCenter(
                        thisbin), tot_PSF_tr_KRA .GetBinContent(thisbin))
            PSF_spline_tr_KRA = TSpline3("PSF_spline_tr_KRA", grph_PSF_tr_KRA)
            PSF_spline_tr_KRA .SetName("PSF_spline_tr_KRA")

            for x in range(1, tot_PSF_sh_KRA .GetNbinsX() + 1):
                con = tot_PSF_sh_KRA .GetBinContent(x)
                xxx = 10 ** tot_PSF_sh_KRA .GetBinCenter(x) * pi / 180.
                s = sin(xxx) * log(10) * xxx
                tot_PSF_sh_KRA .SetBinContent(x, con / s)
            bins = [b for b in range(1, tot_PSF_sh_KRA .GetNbinsX(
            ) + 1) if tot_PSF_sh_KRA .GetBinCenter(b) < lowlogang_sh]
            sum_ = sum([tot_PSF_sh_KRA .GetBinContent(b) for b in bins])
            tot_PSF_sh_KRA .Scale(1. / (sum_ / len(bins)))

            grph_PSF_sh_KRA = TGraph(tot_PSF_sh_KRA .GetNbinsX())
            grph_PSF_sh_KRA .SetPoint(0, -1, 1)
            firstBins = False
            for thisbin in reversed(range(1, tot_PSF_sh_KRA .GetNbinsX() + 1)):
                if tot_PSF_sh_KRA .GetBinContent(thisbin) > 1 or (tot_PSF_sh_KRA .GetBinContent(thisbin) < tot_PSF_sh_KRA .GetBinContent(thisbin + 1) and tot_PSF_sh_KRA .GetBinContent(thisbin) > 0.8) or firstBins:
                    grph_PSF_sh_KRA .SetPoint(
                        thisbin, tot_PSF_sh_KRA .GetBinCenter(thisbin), 1)
                    firstBins = True
                else:
                    grph_PSF_sh_KRA .SetPoint(thisbin, tot_PSF_sh_KRA .GetBinCenter(
                        thisbin), tot_PSF_sh_KRA .GetBinContent(thisbin))
            PSF_spline_sh_KRA = TSpline3("PSF_spline_sh_KRA", grph_PSF_sh_KRA)
            PSF_spline_sh_KRA .SetName("PSF_spline_sh_KRA")

        else:
            cheat_file = TFile.Open(
                "/sps/km3net/users/tgregoir/gal_plane/ingredient_files/cheat_file.root")

            PSF_spline_sh_KRA = cheat_file .Get("PSF_spline_sh_KRA")
            PSF_spline_tr_KRA = cheat_file .Get("PSF_spline_tr_KRA")

        tot_morpho_reco_sh .Scale(1. / tot_morpho_reco_sh .Integral())
        tot_morpho_reco_tr .Scale(1. / tot_morpho_reco_tr .Integral())

        Nmupage = tot_sh_spectr_e_muatm .Integral()

        ProjX = tot_sh_spectr2d_e_muatm_zen_bef_cuts .ProjectionX()
        ProjY = tot_sh_spectr2d_e_muatm_zen_bef_cuts .ProjectionY()

        rebin = 2
        # ProjX .Rebin(rebin)
        ProjX .Smooth()
        # ProjY .Rebin(rebin)
        ProjY .Smooth()

        for binY in range(1, tot_sh_spectr2d_e_muatm_zen_bef_cuts .GetNbinsY() + 1):
            if cos(tot_sh_spectr2d_e_muatm_zen_bef_cuts_final .GetYaxis().GetBinCenter(binY + 1)) < 0.1:
                for binX in range(1, tot_sh_spectr2d_e_muatm_zen_bef_cuts .GetNbinsX() + 1):
                    tot_sh_spectr2d_e_muatm_zen_bef_cuts_final .SetBinContent(
                        binX, binY, ProjX .GetBinContent(binX) * ProjY .GetBinContent(binY))
                    # tot_sh_spectr2d_e_muatm_zen_bef_cuts_final .SetBinContent(binX, binY, ProjX .GetBinContent((binX+1)/2) * ProjY .GetBinContent((binY+1)/2))

        tot_sh_spectr2d_e_muatm_zen_bef_cuts_final .Scale(
            Nmupage / tot_sh_spectr2d_e_muatm_zen_bef_cuts_final .Integral())
        tot_sh_spectr2d_e_atm_zen_final .Add(
            tot_sh_spectr2d_e_muatm_zen_bef_cuts_final)

        for binY in range(1, tot_sh_spectr2d_e_atm_zen_final_opt .GetNbinsY() + 1):
            for binX in range(1, tot_sh_spectr2d_e_atm_zen_final_opt .GetNbinsX() + 1):
                tot_sh_spectr2d_e_atm_zen_final_opt .SetBinContent(binX, binY, tot_sh_spectr2d_e_atm_zen_final .GetBinContent(
                    binX, tot_sh_spectr2d_e_atm_zen_final .GetYaxis().FindBin(tot_sh_spectr2d_e_atm_zen_final_opt .GetYaxis().GetBinCenter(binY))))

        tot_tr_spectr2d_e_atm_zen_final .Add(tot_tr_spectr2d_e_atm_zen_initial)
        tot_tr_spectr2d_e_atm_zen_final .Add(tot_tr_spectr2d_e_muatm_zen, -1)
        tot_tr_spectr2d_e_muatm_zen_bef_cuts_final .Add(
            tot_tr_spectr2d_e_muatm_zen_bef_cuts)
        tot_tr_spectr2d_e_muatm_zen_bef_cuts_final .Smooth()
        for binY in range(1, tot_tr_spectr2d_e_muatm_zen_bef_cuts_final .GetNbinsY() + 1):
            for binX in range(1, tot_tr_spectr2d_e_muatm_zen_bef_cuts_final .GetNbinsX() + 1):
                if cos(tot_tr_spectr2d_e_muatm_zen_bef_cuts_final .GetYaxis().GetBinCenter(binY + 2)) > 0.1:
                    tot_tr_spectr2d_e_muatm_zen_bef_cuts_final .SetBinContent(
                        binX, binY, 0.)

        tot_tr_spectr2d_e_muatm_zen_bef_cuts_final .Scale(tot_tr_spectr2d_e_muatm_zen .Integral(
        ) / tot_tr_spectr2d_e_muatm_zen_bef_cuts_final .Integral())
        tot_tr_spectr2d_e_atm_zen_final .Add(
            tot_tr_spectr2d_e_muatm_zen_bef_cuts_final)

        for binY in range(1, tot_tr_spectr2d_e_atm_zen_final_opt .GetNbinsY() + 1):
            for binX in range(1, tot_tr_spectr2d_e_atm_zen_final_opt .GetNbinsX() + 1):
                tot_tr_spectr2d_e_atm_zen_final_opt .SetBinContent(binX, binY, tot_tr_spectr2d_e_atm_zen_final .GetBinContent(
                    binX, tot_tr_spectr2d_e_atm_zen_final .GetYaxis().FindBin(tot_tr_spectr2d_e_atm_zen_final_opt .GetYaxis().GetBinCenter(binY))))

        # for binX in range(1, tot_sh_spectr2d_e_muatm_zen_bef_cuts .GetNbinsX()+1):
        #     for binY in range(1, tot_sh_spectr2d_e_muatm_zen_bef_cuts .GetNbinsY()+1):
        #         tot_sh_spectr2d_e_muatm_zen_bef_cuts_final .SetBinContent(binX, binY, ProjX .GetBinContent((binX+1)/2) * ProjY .GetBinContent((binY+1)/2))


    tot_histo_run_durData .Fill(1, tot_histo_run_numberData.Integral())
    tot_histo_run_durMC .Fill(1, tot_histo_run_numberMC.Integral())

    # for Xbin in range(tot_sh_spectr3d_e_KRA_bis .GetNbinsX()):
    #     for Ybin in range(tot_sh_spectr3d_e_KRA_bis .GetNbinsY()):
    #         binContent0 = 0.
    #         binContent1 = 0.
    #         for Zbin in range(tot_sh_spectr3d_e_KRA_bis .GetNbinsZ()):
    #             if tot_sh_spectr3d_e_KRA_bis .GetZaxis().GetBinCenter(Zbin) < 3.7:
    #                 binContent0 += tot_sh_spectr3d_e_KRA_bis .GetBinContent(Xbin, Ybin, Zbin)
    #             elif tot_sh_spectr3d_e_KRA_bis .GetZaxis().GetBinCenter(Zbin) > 5.3:
    #                 binContent1 += tot_sh_spectr3d_e_KRA_bis .GetBinContent(Xbin, Ybin, Zbin)
    #         tot_morpho_reco_sh0 .SetBinContent(Xbin, Ybin, binContent0)
    #         tot_morpho_reco_sh1 .SetBinContent(Xbin, Ybin, binContent1)
    #
    # for Xbin in range(tot_tr_spectr3d_KRA_bis .GetNbinsX()):
    #     for Ybin in range(tot_tr_spectr3d_KRA_bis .GetNbinsY()):
    #         binContent0 = 0.
    #         binContent1 = 0.
    #         for Zbin in range(tot_tr_spectr3d_KRA_bis .GetNbinsZ()):
    #             if tot_tr_spectr3d_KRA_bis .GetZaxis().GetBinCenter(Zbin) < 50.:
    #                 binContent0 += tot_tr_spectr3d_KRA_bis .GetBinContent(Xbin, Ybin, Zbin)
    #             elif tot_tr_spectr3d_KRA_bis .GetZaxis().GetBinCenter(Zbin) > 70.:
    #                 binContent1 += tot_tr_spectr3d_KRA_bis .GetBinContent(Xbin, Ybin, Zbin)
    #         tot_morpho_reco_tr0 .SetBinContent(Xbin, Ybin, binContent0)
    #         tot_morpho_reco_tr1 .SetBinContent(Xbin, Ybin, binContent1)


    print "\ntot_histo_run_durData", tot_histo_run_durData.Integral(), "tot_histo_run_durMC", tot_histo_run_durMC.Integral(), "\n"

    sh_integ = tot_sh_bgr_vs_sindec .GetEntries()
    tr_integ = tot_tr_bgr_vs_sindec .GetEntries()


    outfile.cd()


    if tau:
        tot_acc_sh_tauCC_had .Write()
        tot_acc_sh_tauCC_e   .Write()
        tot_acc_sh_tauCC_mu  .Write()
        tot_acc_tr_tauCC_mu  .Write()
        outfile.Close()

        if n_empty_files > 0:
            print "Error:", n_empty_files, "empty files !!  /!\\ /!\\"
            print "                                    "
            print "              AAA                   "
            print "             A   A                  "
            print "            A  l  A                 "
            print "           A   l   A                "
            print "          A    l    A               "
            print "         A     o     A              "
            print "           AAAAAAAAA                "
            print "                                    "

            run_jobs = raw_input(
                "\n\nRun jobs for empty files?\nType n to not run the jobs or any key to run it.")
            if run_jobs != "n" and run_jobs != "N" and run_jobs != "no" and run_jobs != "No":
                for empty_file in empty_files:
                    Data_MC = empty_file .split(
                        "ingredient_files/")[1].split("_")[0]
                    fileNum = empty_file .split("ing_files_")[1].split(".")[0]
                    print "\n./Submit_sel.py " + Data_MC + " " + fileNum + " date " + date[1:]
                    os.system("./Submit_sel.py " + Data_MC +
                              " " + fileNum + " date " + date[1:])
        else:
            print "No empty files"
        exit()

    if "Data" not in sys.argv:

        if not no_old_plots:
            ##### Phy0 #####
            tot_sh_spectr               .Write()
            tot_sh_spectr3d             .Write()
            tot_tr_spectr               .Write()
            tot_tr_spectr3d             .Write()

            tot_acc_sh                  .Write()
            tot_acc_shbis               .Write()
            tot_acc_tr                  .Write()
            tot_acc_trbis               .Write()

            # tot_acc_sh_zen            .Write()
            # tot_acc_tr_zen            .Write()

            tot_PSF_tr                  .Write()
            tot_PSF_sh                  .Write()
            PSF_spline_tr               .Write()
            PSF_spline_sh               .Write()

            tot_sh_spectr_KRA           .Write()
            # tot_sh_spectr3d_zen_KRA   .Write()
            # tot_sh_spectr3d_KRA       .Write()
            tot_sh_spectr3d_KRA_bis     .Write()

            # tot_tr_spectr3d_e_zen_KRA .Write()
            # tot_tr_spectr3d_e_KRA     .Write()

            # tot_tr_spectr3d_zen_KRA   .Write()
            # tot_tr_spectr3d_KRA       .Write()

            tot_tr_Etrue_Nhits_dec_KRA  .Write()

            tot_hist_begin_runMC .Write()
            tot_hist_end_runMC   .Write()

        # tot_MC_tr_angRes  .Write()
        # tot_MC_sh_angRes  .Write()
        #
        # tot_N_evts_gal_ridge  .Write()

        tot_MC_sh_cos_zen_cut    .Write()
        tot_MC_sh_M_est      .Write()
        tot_MC_sh_beta       .Write()
        tot_MC_sh_RDF        .Write()
        tot_MC_sh_lik_muVeto .Write()

        tot_MC_tr_cos_zen_cut    .Write()
        tot_MC_tr_lambda     .Write()
        tot_MC_tr_beta       .Write()

        PSF_spline_tr_KRA           .Write()
        PSF_spline_sh_KRA           .Write()
        ##### KRA #####
        tot_hist_channels_tr        .Write()
        tot_hist_channels_sh        .Write()

        # tot_MC_tr_trueEnergy        .Write()
        # tot_MC_sh_trueEnergy        .Write()

        tot_hist_cuts_tr_KRA        .Write()
        tot_hist_cuts_tr_nuatm      .Write()
        tot_hist_cuts_tr_mupage     .Write()

        tot_hist_cuts_sh_KRA        .Write()
        tot_hist_cuts_sh_nuatm      .Write()
        tot_hist_cuts_sh_mupage     .Write()

        # tot_hist_cuts_lik_sh_KRA    .Write()
        # tot_hist_cuts_lik_sh_nuatm  .Write()
        # tot_hist_cuts_lik_sh_mupage .Write()

        # tot_hist_cuts_lik_sh_KRA2    .Write()
        # tot_hist_cuts_lik_sh_nuatm2  .Write()
        # tot_hist_cuts_lik_sh_mupage2 .Write()

        tot_check_Nevts             .Write()

        tot_morpho_reco_sh          .Write()
        tot_morpho_reco_tr          .Write()

        # tot_morpho_reco_sh_aitoff   .Write()
        # tot_morpho_reco_tr_aitoff   .Write()

        # tot_morpho_reco_sh0          .Write()
        # tot_morpho_reco_sh1          .Write()
        # tot_morpho_reco_tr0          .Write()
        # tot_morpho_reco_tr1          .Write()

        # tot_beta_distrib_tr_cosm  .Write()
        # tot_beta_distrib_tr_atm   .Write()
        tot_NevtsMC                 .Write()
        tot_check_NevtsMC           .Write()
        # tot_NevtsComp             .Write()

        tot_sh_spectr_e_KRA         .Write()
        # tot_sh_spectr3d_e_zen_KRA .Write()
        # tot_sh_spectr3d_e_KRA     .Write()
        tot_sh_spectr3d_e_KRA_bis   .Write()

        tot_acc_sh_KRA              .Write()
        tot_acc_tr_KRA              .Write()
        tot_acc_sh_KRA_bis          .Write()
        tot_acc_tr_KRA_bis          .Write()
        # tot_acc_sh_zen_KRA        .Write()
        # tot_acc_tr_zen_KRA        .Write()

        tot_histo_run_durMC         .Write()
        tot_histo_run_numberMC      .Write()
        # tot_hist_N_evts           .Write()

        tot_tr_spectr3d_KRA_bis     .Write()
        tot_tr_spectr_KRA           .Write()

        tot_tr_spectr_atm           .Write()
        tot_tr_spectr2d_atm         .Write()
        tot_tr_spectr2d_atm_zen     .Write()

        tot_tr_spectr_e_KRA                        .Write()
        tot_tr_spectr3d_e_KRA_bis                  .Write()
        tot_tr_spectr_e_atm                        .Write()
        tot_tr_spectr2d_e_atm_zen_final            .Write()
        tot_tr_spectr2d_e_atm_zen_initial          .Write()
        tot_tr_spectr2d_e_muatm_zen_bef_cuts       .Write()
        tot_tr_spectr2d_e_muatm_zen                .Write()
        tot_tr_spectr2d_e_muatm_zen_bef_cuts_final .Write()
        tot_tr_spectr2d_e_atm_zen_final_opt        .Write()

        tot_sh_spectr_atm                          .Write()
        tot_sh_spectr_e_atm                        .Write()
        tot_sh_spectr_e_nuatm                      .Write()
        tot_sh_spectr_e_muatm                      .Write()
        tot_sh_spectr2d_e_atm_zen_initial          .Write()
        tot_sh_spectr2d_e_atm_zen_final            .Write()
        tot_sh_spectr2d_e_muatm_zen                .Write()
        tot_sh_spectr2d_e_muatm_zen_bef_cuts       .Write()
        tot_sh_spectr2d_e_muatm_zen_bef_cuts_final .Write()
        tot_sh_spectr2d_e_atm_zen_final_opt        .Write()

        tot_sh_sindec_atmMC         .Write()
        tot_tr_sindec_atmMC         .Write()

        tot_sh_ra_atmMC             .Write()
        tot_tr_ra_atmMC             .Write()

        tot_sh_coszen_atmMC         .Write()
        tot_tr_coszen_atmMC         .Write()

        tot_sh_az_atmMC             .Write()
        # tot_sh_az_atmMC_nu             .Write()
        # tot_sh_az_atmMC_mu             .Write()
        tot_tr_az_atmMC             .Write()

        # tot_PSF_tr_KRA            .Write()
        # PSF_tr_KRA_init           .Write()
        # PSF_tr_KRA_cumul          .Write()
        # PSF_spline_tr_KRA         .Write()
        # tot_PSF_sh_KRA_rebinned   .Write()
        # PSF_spline_sh_KRA         .Write()

    tot_sh_spectrum_bgr        .Write()
    tot_tr_spectrum_bgr        .Write()
    tot_sh_spectrum_e_bgr      .Write()
    tot_tr_spectrum_e_bgr      .Write()

    tot_sh_bgr_vs_sindec       .Write()
    tot_tr_bgr_vs_sindec       .Write()

    tot_sh_bgr_vs_ra           .Write()
    tot_tr_bgr_vs_ra           .Write()

    tot_sh_bgr_vs_coszen       .Write()
    tot_tr_bgr_vs_coszen       .Write()

    tot_sh_bgr_vs_az           .Write()
    tot_tr_bgr_vs_az           .Write()

    tot_histo_run_durData      .Write()
    tot_histo_run_numberData   .Write()

    # if "tau" in sys.argv:
    #     tot_sh_spectr_nueNC   .Write()
    #     tot_sh_spectr_numuNC  .Write()
    #     tot_sh_spectr_nutauNC .Write()

    if "MC" not in sys.argv:
        # sh_bgr_vs_sindec_graph      .Write()
        # sh_bgr_vs_sindec_graph_bis  .Write()
        # tr_bgr_vs_sindec_graph      .Write()
        # tr_bgr_vs_sindec_graph_bis  .Write()
        # sh_bgr_vs_coszen_graph      .Write()
        # sh_bgr_vs_coszen_graph_bis  .Write()
        # tr_bgr_vs_coszen_graph      .Write()
        # tr_bgr_vs_coszen_graph_bis  .Write()

        if not no_old_plots:
            tot_hist_begin_runData .Write()
            tot_hist_end_runData   .Write()

        tot_Data_sh_cos_zen_cut    .Write()
        tot_Data_sh_M_est      .Write()
        tot_Data_sh_beta       .Write()
        tot_Data_sh_RDF        .Write()
        tot_Data_sh_lik_muVeto .Write()

        tot_Data_tr_cos_zen_cut    .Write()
        tot_Data_tr_lambda     .Write()
        tot_Data_tr_beta       .Write()

        tot_NevtsData               .Write()

        tr_bgr_vs_sindec_spline2     .Write()
        tr_bgr_vs_sindec_spline_bis2 .Write()
        sh_bgr_vs_sindec_spline2     .Write()
        sh_bgr_vs_sindec_spline_bis2 .Write()

        tr_bgr_vs_coszen_spline2     .Write()
        tr_bgr_vs_coszen_spline_bis2 .Write()
        sh_bgr_vs_coszen_spline2     .Write()
        sh_bgr_vs_coszen_spline_bis2 .Write()

        tr_bgr_vs_az_spline2     .Write()
        tr_bgr_vs_az_spline_bis2 .Write()
        sh_bgr_vs_az_spline2     .Write()
        sh_bgr_vs_az_spline_bis2 .Write()

        # sh_spectrum_bgr_spline      .Write()
        # tr_spectrum_bgr_spline      .Write()
        # sh_spectrum_bgr_spline_bis  .Write()
        # tr_spectrum_bgr_spline_bis  .Write()

        sh_bgr_spline_az_flat      .Write()


    #### TO plot th PSF ####
    # c = TCanvas("c", "c", 1200, 500)
    # c .Divide(2,1)
    # c .cd(1)
    # tot_PSF_tr_KRA .Draw()
    # PSF_spline_tr_KRA .Draw("same")
    #
    # c .cd(2)
    # tot_PSF_sh_KRA .Draw()
    # PSF_spline_sh_KRA .Draw("same")

    if "MC" not in sys.argv:
        print "For", date

        print "\nnote snbg =", str(sh_integ)
        print "note tnbg =", str(tr_integ)
        print "\nAnd change input file line 420 of run_PEx"

        # print "\n\nThe KRAgamma flux produce", tot_NevtsMC .GetBinContent(2),
        # "tracks and", tot_NevtsMC .GetBinContent(4), "showers"
        print "\n\nThe KRAgamma flux produce a total of ", tot_NevtsMC .GetBinContent(2) + tot_NevtsMC .GetBinContent(4), "events"

        if "Data" not in sys.argv:
            print "\nTheir is around", Nmupage, "mupage selected as shower-like events"


    # if not "Data" in sys.argv:
    #
    #     Sum Nb of events of MC files
    #     Nevts_filesMC = glob.glob("/sps/km3net/users/tgregoir/gal_plane/ingredient_files/Nevts_MC/Nevt"+date+"_*.txt")
    #     outfile = open("/sps/km3net/users/tgregoir/gal_plane/ingredient_files/Nevt"+date+".txt", "w")
    #
    #     Nsh = 0
    #     Ntr = 0
    #
    #     for infile in Nevts_filesMC:
    #         txtfile = open(infile, "r")
    #         lines = txtfile.readlines()
    #
    #         if len(lines) == 0:
    #             print infile, "is empty !!"
    #             continue
    #
    #         print infile
    #
    #         Nsh += float(lines[1])
    #         Ntr += float(lines[4])
    #         #print "tr", float(lines[4])
    #         #print "sh", float(lines[1])
    #         os.system("rm -f "+infile)
    #     os.system("rmdir /sps/km3net/users/tgregoir/gal_plane/ingredient_files/Nevts_MC"+date)
    #
    #
    #     outfile.write("Nsh =\n"+str(Nsh)+"\nNtr =\n"+str(Ntr)+"")
    #     print "For MC :"
    #     print "Nsh = "+str(Nsh)+"\nNtr = "+str(Ntr)+"\n"

    if n_empty_files > 0:
        print "Error:", n_empty_files, "empty files !!  /!\\ /!\\"
        print "                                    "
        print "              AAA                   "
        print "             A   A                  "
        print "            A  l  A                 "
        print "           A   l   A                "
        print "          A    l    A               "
        print "         A     o     A              "
        print "           AAAAAAAAA                "
        print "                                    "

        run_jobs = raw_input(
            "\n\nRun jobs for empty files?\nType n to not run the jobs or any key to run it.\n/!\\/!\\ Change the cuts in select_events_Data/MC before launching the jobs !!!!!\n")
        if run_jobs != "n" and run_jobs != "N" and run_jobs != "no" and run_jobs != "No":
            for empty_file in empty_files:
                Data_MC = empty_file .split("ingredient_files/")[1].split("_")[0]
                fileNum = empty_file .split("ing_files_")[1].split(".")[0]
                print "\n./Submit_sel.py " + Data_MC + " " + fileNum + " date " + date[1:]
                os.system("./Submit_sel.py " + Data_MC +
                          " " + fileNum + " date " + date[1:])
    else:
        print "No empty files"


    # raw_input("Press Enter to continue...")
    outfile.Close()

    end = datetime.datetime.now()
    elapsed = end - start

    print "\nelapsed", elapsed.seconds, "seconds"

if __name__ == "__main__":

    main()
