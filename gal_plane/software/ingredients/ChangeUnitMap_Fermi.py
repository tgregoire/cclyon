#! /usr/bin/env python

import os
from ROOT import *
from math import *
import datetime
import glob
import math
import ctypes
import array
import sys
from antares_db_utils import *

gStyle.SetOptStat(0)

aitoff = False
print "aitoff option is", aitoff
mapFile = TFile("/sps/km3net/users/tgregoir/gal_plane/ingredient_files/Fermi_model_32728_bins_15mars.root")

map_ = mapFile.Get("mapFermi")

nside_    = 128 * 2
decl_arr  = array.array('f')
ra_arr    = array.array('f')
delta_ra  = 2. * pi / (nside_)

for i in range(nside_ + 1):
    if i % 2 == 0:
        decl_arr .append(asin(-1 + i * 1. / (nside_ / 2)))
    ra_arr .append(-pi + i * delta_ra)

decl_aitoff = array.array('f')
ra_aitoff   = array.array('f')
delta_decl_aitoff = 180. / (nside_)
delta_ra_aitoff = 360. / (nside_)
for i in range(nside_ + 1):
    if i % 2 == 0:
        decl_aitoff .append(-90 + i * delta_decl_aitoff)
    ra_aitoff .append(-180 + i * delta_ra_aitoff)

if not aitoff:
    map_2D_fluence = TH2F("mapFermi_2D_fluence", "Neutrino fluence predicted by the Fermi benchmark model;Right Ascension [rad];Declination [rad];E d#Phi/d#Omega [GeV cm^{-2} s^{-1} sr^{-1}]", len(ra_arr) - 1, ra_arr, len(decl_arr) - 1, decl_arr)
    map_2D = TH2F("mapFermi_2D", "Neutrino flux predicted by the Fermi benchmark model;Right Ascension [rad];Declination [rad];d#Phi/d#Omega [cm^{-2} s^{-1} sr^{-1}]", len(ra_arr) - 1, ra_arr, len(decl_arr) - 1, decl_arr)
else:
    map_2D_fluence = TH2F("mapFermi_2D_fluence_aitoff", "Neutrino fluence predicted by the Fermi benchmark model;Right Ascension [rad];Declination [rad];E d#Phi/d#Omega [GeV cm^{-2} s^{-1} sr^{-1}]", len(ra_aitoff) - 1, ra_aitoff, len(decl_aitoff) - 1, decl_aitoff)
    map_2D = TH2F("mapFermi_2D_aitoff", "Neutrino flux predicted by the Fermi benchmark model;Right Ascension [rad];Declination [rad];d#Phi/d#Omega [cm^{-2} s^{-1} sr^{-1}]", len(ra_aitoff) - 1, ra_aitoff, len(decl_aitoff) - 1, decl_aitoff)

for binX in range(1, map_ .GetNbinsX() + 1):
    for binY in range(1, map_ .GetNbinsY() + 1):
        Sum1 = 0
        Sum2 = 0
        for binZ in range(1, map_ .GetNbinsZ() + 1):
            bin = map_ .GetBin(binX, binY, binZ)
            if aitoff:
                bin = map_ .GetBin(binX, map_ .GetNbinsY() + 1 - binY, binZ) #To change the X axis without changing it because of aitoff
            Sum1 += map_ .GetBinContent(bin) / (exp(map_ .GetZaxis().GetBinCenter(binZ) * log(10)) * 1000) * (exp(map_ .GetZaxis().GetBinUpEdge(binZ) * log(10)) * 1000 - exp(map_ .GetZaxis().GetBinLowEdge(binZ) * log(10)) * 1000)
            Sum2 += map_ .GetBinContent(bin) / (exp(map_ .GetZaxis().GetBinCenter(binZ) * log(10)) * 1000)**2 * (exp(map_ .GetZaxis().GetBinUpEdge(binZ) * log(10)) * 1000 - exp(map_ .GetZaxis().GetBinLowEdge(binZ) * log(10)) * 1000)
            # print binX, binZ, "E =", exp(map_ .GetZaxis().GetBinCenter(binZ)*log(10))*1000, map_ .GetZaxis().GetBinCenter(binZ), "Width", exp(map_ .GetZaxis().GetBinUpEdge(binZ)*log(10))*1000 - exp(map_ .GetZaxis().GetBinLowEdge(binZ)*log(10))*1000, "content  ", map_ .GetBinContent(bin)
        binXY = map_2D .GetBin(binY, binX)
        map_2D_fluence .SetBinContent(binXY, Sum1)
        map_2D .SetBinContent(binXY, Sum2)


outfile = TFile.Open("/sps/km3net/users/tgregoir/gal_plane/ingredient_files/Map_CorrectUnits_Fermi_16mar.root", "recreate")

map_2D_fluence .Write()
map_2D .Write()

outfile.Close()
