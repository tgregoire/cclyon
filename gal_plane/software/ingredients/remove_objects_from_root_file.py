#!/usr/bin/env python
"""
Copy a selection of the TObjects of a ROOT file in a new file. It is equivalent to duplicate the file removing the TObjects you don't want.
"""

import argparse
# # from termcolor import colored
# # pylint: disable=E0611, W0401
# from numpy import linspace
from ROOT import TFile



def main(filename):
    """
    Do the thing
    """

    infile = TFile(filename)

    # example of file_string
    """
  KEY: TH1D	MC_sh_cos_zen_cut;1	MC_sh_cos_zen_cut
  KEY: TH1D	MC_sh_M_est;1	MC_sh_M_est
  KEY: TH1D	MC_sh_beta;1	MC_sh_beta
  KEY: TH1D	MC_sh_RDF;1	MC_sh_RDF
  KEY: TH1D	MC_sh_lik_muVeto;1	MC_sh_lik_muVeto
  KEY: TSpline3	sh_bgr_vs_coszen_rebinned_bis;1	sh_bgr_vs_coszen_rebinned_bis
  KEY: TSpline3	tr_bgr_vs_coszen_rebinned_bis;1	tr_bgr_vs_coszen_rebinned_bis
  KEY: TSpline3	sh_bgr_vs_az_rebinned;1	sh_bgr_vs_az_rebinned
  KEY: TSpline3	tr_bgr_vs_az_rebinned;1	tr_bgr_vs_az_rebinned
  KEY: TSpline3	sh_bgr_vs_az_rebinned_bis;1	sh_bgr_vs_az_rebinned_bis
  KEY: TSpline3	tr_bgr_vs_az_rebinned_bis;1	tr_bgr_vs_az_rebinned_bis
    """

    # Copy paste in file_string the content of the .ls you want to keep
    file_string = """
  KEY: TH1D	MC_sh_cos_zen_cut;1	MC_sh_cos_zen_cut
  KEY: TH1D	MC_sh_M_est;1	MC_sh_M_est
  KEY: TH1D	MC_sh_beta;1	MC_sh_beta
  KEY: TH1D	MC_sh_RDF;1	MC_sh_RDF
  KEY: TH1D	MC_sh_lik_muVeto;1	MC_sh_lik_muVeto
  KEY: TH1D	MC_tr_cos_zen_cut;1	MC_tr_cos_zen_cut
  KEY: TH1D	MC_tr_lambda;1	MC_tr_lambda
  KEY: TH1D	MC_tr_beta;1	MC_tr_beta
  KEY: TSpline3	PSF_spline_tr_KRA;1	PSF_spline_tr_KRA
  KEY: TSpline3	PSF_spline_sh_KRA;1	PSF_spline_sh_KRA
  KEY: TH1D	hist_channels_tr;1	Nevts for each channel after Track cuts
  KEY: TH1D	hist_channels_sh;1	Nevts for each channel after Shower cuts
  KEY: TH1D	MC_tr_trueEnergy;1	MC_tr_trueEnergy
  KEY: TH1D	MC_sh_trueEnergy;1	MC_sh_trueEnergy
  KEY: TH1D	hist_cuts_tr_KRA;1	Signal events after each cuts for Tracks
  KEY: TH1D	hist_cuts_tr_nuatm;1	nu atm events after each cuts for Tracks
  KEY: TH1D	hist_cuts_tr_mupage;1	Mupage events after each cuts for Tracks
  KEY: TH1D	hist_cuts_sh_KRA;1	Signal events after each cuts for Showers
  KEY: TH1D	hist_cuts_sh_nuatm;1	nu atm events after each cuts for Showers
  KEY: TH1D	hist_cuts_sh_mupage;1	Mupage events after each cuts for Showers
  KEY: TH1D	check_Nevts;1	atm, 1:numuNC, 2:nueCC, 3:numuCC, phy0, 5:numuNC, 6:nueCC, 7:numuCC, 9:mupage
  KEY: TH2D	morpho_reco_sh;1	Distribution of the reconstructed events for showers
  KEY: TH2D	morpho_reco_tr;1	Distribution of the reconstructed events for tracks
  KEY: TH2D	morpho_reco_sh_aitoff;1	Distribution of the reconstructed events for showers
  KEY: TH2D	morpho_reco_tr_aitoff;1	Distribution of the reconstructed events for tracks
  KEY: TH1D	NevtsMC;1	0=xrdcp failure, N KRA#gamma events 1=tr, 3=sh, N#phy_{0} events 6=tr, 8=sh
  KEY: TH1D	check_NevtsMC;1	atm, 1:numuNC, 2:nueCC, 3:numuCC, phy0, 5:numuNC, 6:nueCC, 7:numuCC, 9:mupage
  KEY: TH1D	sh_spectrum_e_KRA;1	KRA#gamma spectrum of the reconstructed energy for showers
  KEY: TH3F	sh_spectrum3d_e_KRA_bis;1	KRA#gamma spectrum of the reconstructed energy vs ra vs decl for showers
  KEY: TH1D	acc_sh_KRA;1	Shower acceptance in function of the declination
  KEY: TH1D	acc_tr_KRA;1	Track acceptance in function of the declination
  KEY: TH1D	acc_sh_KRA_bis;1	Distribution of the showers in function of the reconstructed declination
  KEY: TH1D	acc_tr_KRA_bis;1	Distribution of the tracks in function of the reconstructed declination
  KEY: TH1D	histo_run_durMC;1	a: CCnue, NCnue, CCanue, NCanue, CCnumu, NCnumu, CCanumu, NCanumu, b: idem, mupage
  KEY: TH1D	histo_run_numberMC;1	histo_run_numberMC
  KEY: TH3F	tr_spectrum3d_KRA_bis;1	KRA#gamma spectrum of the number of hits vs ra vs decl for tracks
  KEY: TH1D	tr_spectrum_KRA;1	KRA#gamma spectrum of the number of hits for tracks
  KEY: TH1D	tr_spectr_atmmc;1	Atmospheric spectrum of the number of hits
  KEY: TH2D	tr_spectr2d_atmmc;1	Atmospheric spectrum of the number of hits
  KEY: TH2D	tr_spectr2d_atmmc_zen;1	Atmospheric spectrum of the number of hits
  KEY: TH1D	tr_spectrum_e_KRA;1	KRA#gamma spectrum of the reconstructed energy for tracks
  KEY: TH3F	tr_spectrum3d_e_KRA_bis;1	KRA#gamma spectrum of the reconstructed energy vs ra vs decl for tracks
  KEY: TH1D	tr_spectr_e_atmmc;1	Atmospheric spectrum of the reconstructed energy
  KEY: TH2D	tr_spectr2d_e_atmmc_zen_final;1	Atmospheric spectrum of the reconstructed energy vs zen for Tracks
  KEY: TH2D	tr_spectr2d_e_atmmc_zen_initial;1	Atmospheric spectrum of the reconstructed energy vs zen for Tracks
  KEY: TH2D	tr_spectr2d_e_muatmmc_zen_bef_cuts;1	Atmospheric spectrum of the reconstructed energy vs zen for mupage Tracks
  KEY: TH2D	tr_spectr2d_e_muatm_zen;1	Atmospheric spectrum of the reconstructed energy vs zen for Tracks
  KEY: TH2D	tr_spectr2d_e_muatmmc_zen_bef_cuts_final;1	Atmospheric spectrum of the reconstructed energy vs zen for mupage Tracks
  KEY: TH2D	tr_spectr2d_e_atmmc_zen_final_opt;1	Atmospheric spectrum of the reconstructed energy vs zen for Tracks
  KEY: TH1D	sh_spectr_atmmc;1	Atmospheric spectrum of the number of hits
  KEY: TH1D	sh_spectr_e_atmmc;1	Atmospheric spectrum of the reconstructed energy
  KEY: TH1D	sh_spectr_e_nuatmmc;1	Atmospheric spectrum of the reconstructed energy without mupage
  KEY: TH1D	sh_spectr_e_muatmmc;1	Atmospheric spectrum of the reconstructed energy with only mupage events
  KEY: TH2D	sh_spectr2d_e_atmmc_zen_initial;1	Atmospheric spectrum of the reconstructed energy vs zen for Showers
  KEY: TH2D	sh_spectr2d_e_atmmc_zen_final;1	Atmospheric spectrum of the reconstructed energy vs zen for nu Showers
  KEY: TH2D	sh_spectr2d_e_muatmmc_zen;1	Atmospheric spectrum of the reconstructed energy vs zen for mupage Showers
  KEY: TH2D	sh_spectr2d_e_muatmmc_zen_bef_cuts;1	Atmospheric spectrum of the reconstructed energy vs zen for mupage Showers
  KEY: TH2D	sh_spectr2d_e_muatmmc_zen_bef_cuts_final;1	Atmospheric spectrum of the reconstructed energy vs zen for mupage Showers
  KEY: TH2D	sh_spectr2d_e_atmmc_zen_final_opt;1	Atmospheric spectrum of the reconstructed energy vs zen for nu Showers
  KEY: TH1D	sh_sindec_atmMC;1	Shower Background vs sin(decl) from MC
  KEY: TH1D	tr_sindec_atmMC;1	Track Background vs sin(decl) from MC
  KEY: TH1D	sh_ra_atmMC;1	Shower Background vs ra from MC
  KEY: TH1D	tr_ra_atmMC;1	Track Background vs ra from MC
  KEY: TH1D	sh_coszen_atmMC;1	Shower Background vs cos(zen) from MC
  KEY: TH1D	tr_coszen_atmMC;1	Track Background vs cos(zen) from MC
  KEY: TH1D	sh_az_atmMC;1	Shower Background vs azimuth from MC
  KEY: TH1D	tr_az_atmMC;1	Track Background vs azimuth from MC
  KEY: TH1D	sh_spectrum_bgr;1	Shower spectrum of the number of hits for Background
  KEY: TH1D	tr_spectrum_bgr;1	Track spectrum of the number of hits for Background
  KEY: TH1D	sh_spectrum_e_bgr;1	Shower spectrum of the reconstructed energy for Background
  KEY: TH1D	tr_spectrum_e_bgr;1	Track spectrum of the reconstructed energy for Background
  KEY: TH1D	sh_bgr_vs_sindec;1	Shower Background vs sin(decl)
  KEY: TH1D	tr_bgr_vs_sindec;1	Track Background vs sin(decl)
  KEY: TH1D	sh_bgr_vs_ra;1	Shower Background vs ra
  KEY: TH1D	tr_bgr_vs_ra;1	Track Background vs ra
  KEY: TH1D	sh_bgr_vs_coszen;1	Shower Background vs cos(zen)
  KEY: TH1D	tr_bgr_vs_coszen;1	Track Background vs cos(zen)
  KEY: TH1D	sh_bgr_vs_az;1	Shower Background vs az
  KEY: TH1D	tr_bgr_vs_az;1	Track Background vs az
  KEY: TH1D	histo_run_durData;1	tot_histo_run_durData
  KEY: TH1D	histo_run_numberData;1	tot_histo_run_numberData
  KEY: TH1D	Data_sh_cos_zen_cut;1	Data_sh_cos_zen_cut
  KEY: TH1D	Data_sh_M_est;1	Data_sh_M_est
  KEY: TH1D	Data_sh_beta;1	Data_sh_beta
  KEY: TH1D	Data_sh_RDF;1	Data_sh_RDF
  KEY: TH1D	Data_sh_lik_muVeto;1	Data_sh_lik_muVeto
  KEY: TH1D	Data_tr_cos_zen_cut;1	Data_tr_cos_zen_cut
  KEY: TH1D	Data_tr_lambda;1	Data_tr_lambda
  KEY: TH1D	Data_tr_beta;1	Data_tr_beta
  KEY: TH1D	NevtsData;1	1 =tr, 3 =sh, 0=xrdcp failure
  KEY: TSpline3	sh_bgr_vs_sindec_rebinned;1	sh_bgr_vs_sindec_rebinned
  KEY: TSpline3	tr_bgr_vs_sindec_rebinned;1	tr_bgr_vs_sindec_rebinned
  KEY: TSpline3	sh_bgr_vs_sindec_rebinned_bis;1	sh_bgr_vs_sindec_rebinned_bis
  KEY: TSpline3	tr_bgr_vs_sindec_rebinned_bis;1	tr_bgr_vs_sindec_rebinned_bis
  KEY: TSpline3	sh_bgr_vs_coszen_rebinned;1	sh_bgr_vs_coszen_rebinned
  KEY: TSpline3	tr_bgr_vs_coszen_rebinned;1	tr_bgr_vs_coszen_rebinned
  KEY: TSpline3	sh_bgr_vs_coszen_rebinned_bis;1	sh_bgr_vs_coszen_rebinned_bis
  KEY: TSpline3	tr_bgr_vs_coszen_rebinned_bis;1	tr_bgr_vs_coszen_rebinned_bis
  KEY: TSpline3	sh_bgr_vs_az_rebinned;1	sh_bgr_vs_az_rebinned
  KEY: TSpline3	tr_bgr_vs_az_rebinned;1	tr_bgr_vs_az_rebinned
  KEY: TSpline3	sh_bgr_vs_az_rebinned_bis;1	sh_bgr_vs_az_rebinned_bis
  KEY: TSpline3	tr_bgr_vs_az_rebinned_bis;1	tr_bgr_vs_az_rebinned_bis
    """

    TObjectsNames = [string.split("\t")[1][:-2] for string in file_string.split("\n") if string.find("KEY") != -1]
    print TObjectsNames

    outfile = TFile("temporary_file.root", "update")
    for TObjects_name in TObjectsNames:
        TObject = infile.Get(TObjects_name)
        TObject.Write()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__,)

    parser.add_argument(
        "file",
        nargs=1,
        default='',
        type=str,
        help="Give the directory/file.root to which we remove useless TObjects")

    if parser.parse_args().file[0][-5:] != ".root":
        raise ValueError("File with wrong extension, ROOT file should be given")

    main(parser.parse_args().file[0])
