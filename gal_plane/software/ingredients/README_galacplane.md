**Don't forget the "make clean; make" when changes are done**

To get the ingredients for the pseudo-experiments
`./Submit_sel.py no_old_plots date option_date`

add "MC" or "Data" if you want only the MC or Data file
no_old_plots == allows to get only the usefull histograms
option_date  == the suffix of the output file, it can be the date or anything else

After this is finished, do
`./sumHistos.py date option_date`

If you want to use a flat azimuth, use demo_pdf_bg_decl.py to write the spline of the flat azimuth and of the dec
    corresponding to a flat az.
To do so set the name of the file l.115 and run it
`./demo_pdf_bg_decl.py`

and you will have the file in `/sps/km3net/users/tgregoir/gal_plane/ingredient_files`

then you have to run `submit_PseudoExperiment.py` in `/sps/km3net/users/tgregoir/gal_plane/software/PseudoExperiments`


If you are at the unblinded stage:
use Submit_sel_ntupleData.py to produce a ntuple with your selected data
instead of Submit_sel.py
then merge (using hadd)
then use submit_PseudoExperiment.py dry galacplaneData
    (date not needed, infile can be changed in cpex.cc add_data and the model used in
    ing_file2)
