#!/usr/bin/env python
"""
Compute the Chi2 between the histo of declination obtained from flat azimuth and the splines of
dec from data and the histo of data themself.
The idea is to understand if the distrib from flat az is compatible with the data and splines.
"""

import argparse
from termcolor import colored
# pylint: disable=E0611, W0401
from numpy import linspace
from ROOT import TFile, TSpline3, TH1D, kYellow, kRed, kGreen, kMagenta

def get_y_value(curve, X_value):
    """
    Get the Y_value corresponding to an X_value for spline or TH1D
    """
    if isinstance(curve, type(TSpline3())):
        Y_value = curve.Eval(X_value)
    elif isinstance(curve, type(TH1D())):
        Y_value = curve.GetBinContent(curve.FindBin(X_value))
    else:
        raise ValueError("Not correct type. Should be TH1D or TSpline3")

    return Y_value

# XXX This chi2 is not correct, see statistical_tests.py
def compute_chi2(curve_th, curve_exp):
    """
    Compute the Chi2 between the 'theoretical' distribution and the 'experimental results'.
    It takes as argument a TH1D/TSpline3 of the theoretical distribution, and a TH1D/TSpline3 of the experimental results.
    It returns the Chi2 and the number of points used.
    """
    spline_th = True
    if isinstance(curve_th, type(TH1D())):
        spline_th = False

    chi2 = 0
    npoints = 90
    if spline_th:
        Range = linspace(-1., 0.8, npoints)
        npoints = len(Range)
    else:
        npoints = curve_th.GetNbinsX()
        Range = [curve_th.GetBinCenter(binX) for binX in range(1, npoints+1)]

    for X_th in Range:
        Y_th = get_y_value(curve_th, X_th)
        Y_exp = get_y_value(curve_exp, X_th)

        chi2 += (Y_th-Y_exp)**2/Y_th

    chi2 /= npoints
    print '{0:.3f}'.format(chi2), "using", '{0:.3f}'.format(npoints), "points"

    return chi2, npoints


def main(isRemote=False):
    """
    Get the curves and print the Chi2.
    """

    data_folder = pex_folder = "/home/timothee/code/CC_files_localy/"
    if isRemote:
        data_folder = "/sps/km3net/users/tgregoir/gal_plane/ingredient_files/"
        pex_folder = "/sps/km3net/users/tgregoir/gal_plane/software/PseudoExperiments/"

    data_file = TFile(data_folder+"Sum_ing_files_TheFinal_map3_withFlatAz.root")
    spline_data = data_file.Get("sh_bgr_vs_sindec_rebinned")
    spline_data_bis = data_file.Get("sh_bgr_vs_sindec_rebinned_bis")
    histo_data = data_file.Get("sh_bgr_vs_sindec")

    pex_file = TFile(pex_folder+"dry_CPEx_0_galacticPlane_E_14_flat.root")
    histo_from_inj = pex_file.Get("sh_bgr_vs_sindec")

    histo_data.SetFillColor(kYellow)
    histo_data.Draw("same")
    spline_data.SetLineWidth(3)
    spline_data.SetLineColor(kRed)
    spline_data.Draw("same")
    spline_data_bis.SetLineWidth(3)
    spline_data_bis.SetLineColor(kGreen)
    spline_data_bis.Draw("same")

    histo_from_inj.Scale(histo_data.Integral()/histo_from_inj.Integral())
    histo_from_inj.SetLineWidth(3)
    histo_from_inj.Draw("same")

    # Create an histo with only 3 bins
    histo_basic = TH1D()
    histo_data.Copy(histo_basic)
    histo_basic.RebinX(30)
    histo_basic.Scale(1./30.)

    histo_basic.SetFillColor(0)
    histo_basic.SetLineColor(kMagenta)
    histo_basic.SetLineWidth(3)
    # histo_basic.Draw("same")

    # Duplicate with more bins to get the Chi2 with more points.
    histo_basic_90bins = TH1D()
    histo_data.Copy(histo_basic_90bins)
    for binX in range(1, histo_basic_90bins.GetNbinsX() + 1):
        X_value = histo_basic_90bins.GetBinCenter(binX)
        Y_value = histo_basic.GetBinContent(histo_basic.FindBin(X_value))
        histo_basic_90bins.SetBinContent(binX, Y_value)


    print "Chi2 for", colored('inj', 'blue'), "vs", colored('histo_data', 'yellow'),
    compute_chi2(histo_from_inj, histo_data)
    print "Chi2 for", colored('spline_data', 'red'), "vs", colored('histo_data', 'yellow'),
    compute_chi2(spline_data, histo_data)
    print "Chi2 for", colored('spline_data_bis', 'green'), "vs", colored('histo_data', 'yellow'),
    compute_chi2(spline_data_bis, histo_data)
    print "Chi2 for", colored('histo_basic_90bins', 'magenta'), "vs", colored('histo_data', 'yellow'),
    compute_chi2(histo_basic_90bins, histo_data)

    raw_input()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__,)

    parser.add_argument(
        '--remote',
        '-r',
        default=False,
        action="store_true",
        help='Set to run in on the CCin2p3.'
        ' Leave unset to run localy.')

    main(parser.parse_args().remote)
