#! /usr/bin/env python

import os
from ROOT import *
from math import *
import datetime
import glob
import ctypes
import array
import sys
from antares_db_utils import *

start = datetime.datetime.now()

#load astro library for astronomical conversions
gSystem.Load("/sps/km3net/users/tgregoir/astro/lib/libastro.so")#galata/opt/astro/lib/libastro.so")
antares = Astro.AntaresDetector()
local = Astro.LocalCoords()
eq    = Astro.EqCoords()

# Load AntDst library
gSystem.Load(os.path.expandvars("$ANTDSTROOT/lib/libAntDST.so"))
#Load selection library which content GetNontime, GetChargeRatio, GetCoincidentHits and GetLikelihood
gSystem.Load("/afs/in2p3.fr/home/t/tgregoir/sps/gal_plane/software/ingredients/selection.so")

# Strategies.h, these are not loaded with the library
eBBFit                      = 1; eAart             = 2; eScanFit          = 3; eAAFit          = 4;
eBBFitMest                  = 5; eSeaPDF           = 6; eBBFitBrightPoint = 7; eOSFFit         = 8;
eKrakeFit                   = 9; eGridFit          = 10; eShowerQFit      = 11; eShowerDusjFit = 12;
eShowerTantraPositionPreFit = 13; eShowerTantraFit = 14; eShowerAaFit     = 15; eNew           = 16;

# TriggerTypes.h
e3D_SCAN          = 6; eT3           = 14; eTQ = 16;

# ParticleType.h
eElectron         = 11; eAntiElectron         = ctypes.c_ulong(-11).value;
eElectronNeutrino = 12; eAntiElectronNeutrino = ctypes.c_ulong(-12).value;
eMuon             = 13; eAntiMuon             = ctypes.c_ulong(-13).value;
eMuonNeutrino     = 14; eAntiMuonNeutrino     = ctypes.c_ulong(-14).value;
eTau              = 15; eAntiTau              = ctypes.c_ulong(-15).value;
eTauNeutrino      = 16; eAntiTauNeutrino      = ctypes.c_ulong(-16).value;
eBrightPoint      = 999

# Hit Type
eTriggered = 1

# EnergyRecoStrategy.h
eUnknownEnergyStrategy = 0; edEdX_CEA           = 1; eANN_ECAP       = 2; eML_NIKHEF = 3;
eR_Bologna             = 4; eTantraShowerEnergy = 5; eAaShowerEnergy = 6;
eDusjShowerEnergy      = 7; eNewEnergyReco      = 8; eMonteCarlo     = 99;


# # Put the good run IDs in runIDs
# RunID_file = open("/sps/km3net/users/tgregoir/gal_plane/RunIDs.txt", "r")
# runIDs     = set()
# lines      = RunID_file.readlines()
# for i in lines:
#   thisline = i.split("\n")
#   runIDs.add(thisline[0])

# Put the sparking run IDs in SparkingRuns
SparkingRuns_file = open("/sps/km3net/users/tgregoir/files/SparkingRuns.log", "r")
SparkingRuns      = set()
lines             = SparkingRuns_file .readlines()
for i in lines:
    thisline = i.split("\n")
    SparkingRuns .add(thisline[0])

ScanRuns_file = open("/sps/km3net/users/tgregoir/files/gal_plane/javiersRunLists/SCAN_all_sorted.dat", "r")
ScanRuns      = {}
lines         = ScanRuns_file .readlines()
for i in lines:
    if i == "\n": continue
    thisline = i.split("\n")
    ScanRuns[int(thisline[0][0:5])] = thisline[0][7:9]

QualityBasic_file = open("/sps/km3net/users/tgregoir/files/gal_plane/javiersRunLists/QualityBasic_all_sorted.dat", "r")
QualityBasic      = {}
lines             = QualityBasic_file .readlines()
for i in lines:
    if i == "\n": continue
    thisline = i.split("\n")
    QualityBasic[int(thisline[0][0:5])] = int(thisline[0][7:9])




pdf_show = TFile.Open("/sps/km3net/users/tgregoir/gal_plane/software/ingredients/pdf_murej/outfile2_sig.root")
pdf_mupa = TFile.Open("/sps/km3net/users/tgregoir/gal_plane/software/ingredients/pdf_murej/outfile2_mu.root")
pdf3show = pdf_show.Get( "h43dsig" )
pdf3mup  = pdf_mupa.Get( "h43dmu"  )
pdf3show .Scale( 1./pdf3show.Integral() )
pdf3mup  .Scale( 1./pdf3mup.Integral() )

pi = TMath.Pi()


nbins_bgr_sindec = 90
nbins_bgr_coszen = 48
tr_nbins_bgr_sindec = nbins_bgr_sindec*2
tr_nbins_bgr_coszen = nbins_bgr_coszen*2
# nside_    = 128*2
# decl_arr  = array.array('f')
# ra_arr    = array.array('f')
# nhits_arr = array.array('f')
# delta_ra  = 2.*pi/(nside_)
#
# for i in range(nside_+1):
#   if i%2 == 0:
#       decl_arr.append(asin(-1 + i*1./(nside_/2)))
#   ra_arr.append(-pi+i*delta_ra)
#
# for i in range(1, 200):
#     nhits_arr.append(4*i)

# spectr3d       = TH3F("spectrum3d", "Spectrum of the number of hits vs ra vs decl;ra;decl;Nhits;Number of events", len(ra_arr)-1, ra_arr, len(decl_arr)-1, decl_arr, len(nhits_arr)-1, nhits_arr)#50, 0, 2000)
sh_spectr_bgr    = TH1D("sh_spectrum_bgr", "Spectrum of the number of hits;Nhits", 250, 0, 1000)
sh_spectr_e_bgr  = TH1D("sh_spectrum_e_bgr", "Spectrum of reconstructed energy;log10(E_{reco} [GeV])", 70, 0, 7)
tr_spectr_bgr    = TH1D("tr_spectrum_bgr", "Spectrum of the number of hits;Nhits", 250, 0, 800) #200, 0, 1000)
tr_spectr_e_bgr  = TH1D("tr_spectrum_e_bgr", "Spectrum of reconstructed energy;log10(E_{reco} [GeV])", 70, 0, 7)
sh_bgr_vs_sindec = TH1D("sh_bgr_vs_sindec", "Shower Background vs sin(decl);sin(decl);# of events", nbins_bgr_sindec, -1, 0.8)
tr_bgr_vs_sindec = TH1D("tr_bgr_vs_sindec", "Track Background vs sin(decl);sin(decl);# of events", tr_nbins_bgr_sindec, -1, 0.8)
sh_bgr_vs_ra     = TH1D("sh_bgr_vs_ra", "Shower Background vs ra;ra;# of events", 300, -pi, pi)
tr_bgr_vs_ra     = TH1D("tr_bgr_vs_ra", "Track Background vs ra;ra;# of events", 300, -pi, pi)
sh_bgr_vs_coszen = TH1D("sh_bgr_vs_coszen", "Shower Background vs cos(zen);cos(zen);# of events", nbins_bgr_coszen, -1, 0.1)
tr_bgr_vs_coszen = TH1D("tr_bgr_vs_coszen", "Track Background vs cos(zen);cos(zen);# of events", tr_nbins_bgr_coszen, -1, 0.1)
sh_bgr_vs_az     = TH1D("sh_bgr_vs_az", "Shower Background vs az;az;# of events", 300, 0, 2*pi)
tr_bgr_vs_az     = TH1D("tr_bgr_vs_az", "Track Background vs az;az;# of events", 300, 0, 2*pi)
# test             = TH2D("test", "titre", 200, 7900, 8600, 200, 2200, 2600)
histo_run_dur    = TH1D("histo_run_durData", "titre", 10, 0, 10)
histo_run_number = TH1D("histo_run_numberData", "titre", 90001, -0.5, 90000.5)
Nevts_hist       = TH1D("NevtsData","1=tr, 3=sh", 5, -0.5, 4.5)

hist_begin_runData = TH1D("hist_begin_runData", "starting date of the run;[days]", 33000, 0, 3300)
hist_end_runData   = TH1D("hist_end_runData", "ending date of the run;[days]", 33000, 0, 3300)

# Data_sh_cos_zen_cut = TH1D("Data_sh_cos_zen_cut","Data_sh_cos_zen_cut", 40, -1, 1)
# Data_sh_M_est       = TH1D("Data_sh_M_est","Data_sh_M_est", 50, 0, 5000)
# Data_sh_beta        = TH1D("Data_sh_beta","Data_sh_beta", 35, 0, 35)
# Data_sh_RDF         = TH1D("Data_sh_RDF","Data_sh_RDF", 20, 0, 1)
# Data_sh_lik_muVeto  = TH1D("Data_sh_lik_muVeto","Data_sh_lik_muVeto", 50 ,-100 , 400)
#
# Data_tr_cos_zen_cut = TH1D("Data_tr_cos_zen_cut","Data_tr_cos_zen_cut", 40, -1, 1)
# Data_tr_lambda      = TH1D("Data_tr_lambda","Data_tr_lambda", 200, -10, 10)
# Data_tr_beta        = TH1D("Data_tr_beta","Data_tr_beta", 50, 0, 5)

Data_sh_cos_zen_cut = TH1D("Data_sh_cos_zen_cut","Data_sh_cos_zen_cut", 22, -1, 0.1)
Data_sh_M_est       = TH1D("Data_sh_M_est","Data_sh_M_est", 100, 0, 1000)
Data_sh_beta        = TH1D("Data_sh_beta","Data_sh_beta", 26, 0, 26)
Data_sh_RDF         = TH1D("Data_sh_RDF","Data_sh_RDF", 14, 0.3, 1)
Data_sh_lik_muVeto  = TH1D("Data_sh_lik_muVeto","Data_sh_lik_muVeto", 36 ,40 , 400)

Data_tr_cos_zen_cut = TH1D("Data_tr_cos_zen_cut","Data_tr_cos_zen_cut", 22, -1, 0.1)
Data_tr_lambda      = TH1D("Data_tr_lambda","Data_tr_lambda", 23, -5.15, -3.2)
Data_tr_beta        = TH1D("Data_tr_beta","Data_tr_beta", 10, 0, 1)

# evt_nt = TNtuple("evt_nt","evt_nt","evt:isShower:N_hits:Lambda")

n_files      = 0
c_water      = 0.217288148
one_year     = 31557600 # seconds/year

cp_failure   = 0
sh_sum_Nevts = 0
tr_sum_Nevts = 0
evt          = 0
official     = 1
sps          = 0
job          = False

if "sps" in sys.argv:
    sps = 1

if "official" in sys.argv:
    official = 1

n_files_per_job = 5
if sps: n_files_per_job = 3
if official: n_files_per_job = 2200

# If their is a number as an argument of the script, it is considered as a job
range_files = 0
n_jobs = 100

for nb in range(n_jobs):
    if str(nb) in sys.argv and range_files == 0:
        job = True
        range_files = nb
        break

date = ""
if "date" in sys.argv:
    for i in range(len(sys.argv)):
        if sys.argv[i] == "date":
            date = "_"+sys.argv[i+1]
    # print "mkdir /sps/km3net/users/tgregoir/gal_plane/ingredient_files/Data_tim"+date # XXX dois-je le remettre ?
    # os.system("mkdir /sps/km3net/users/tgregoir/gal_plane/ingredient_files/Data_tim"+date)

# CurrentFiles = set()
# for prod in range(2, 4):
#     for year in range(2008, 2016): #TODO : put like before 2016): or more 2017
#         for month in range(01, 13):
#             if month <10:
#                 os.system("rfdir /hpss/in2p3.fr/group/antares/SeaTray/prod_2016-0"+str(prod)+"/Line12/sea/"+str(year)+"/0"+str(month)+"/ | awk -F ' ' '{print $9}' > /sps/km3net/users/tgregoir/files/gal_plane/CurrentFiles.txt")
#             else:
#                 os.system("rfdir /hpss/in2p3.fr/group/antares/SeaTray/prod_2016-0"+str(prod)+"/Line12/sea/"+str(year)+"/"+str(month)+"/ | awk -F ' ' '{print $9}' > /sps/km3net/users/tgregoir/files/gal_plane/CurrentFiles.txt")
#
#             rootFiles = open("/sps/km3net/users/tgregoir/files/gal_plane/CurrentFiles.txt", "r")
#             lines = rootFiles.readlines()
#
#             for i in lines:
#                 thisline = i.split("\n")
#                 if thisline[0][0:11] == "MicroAntDST":
#                     print "/hpss/in2p3.fr/group/antares/SeaTray/prod_2016-0"+str(prod)+"/Line12/sea/"+str(year)+"/0"+str(month)+"/"+thisline[0]
#                     CurrentFiles.add("/hpss/in2p3.fr/group/antares/SeaTray/prod_2016-0"+str(prod)+"/Line12/sea/"+str(year)+"/0"+str(month)+"/"+thisline[0])
# # print CurrentFiles
#
# FilesPath = open("/sps/km3net/users/tgregoir/gal_plane/software/ingredients/FilesPath.txt", "w")
#
# for current_file in CurrentFiles:
#     FilesPath.write("root://ccxroot.in2p3.fr:1999/"+current_file+"\n")


# os.system("rm -f /sps/km3net/users/tgregoir/gal_plane/ingredient_files/Data_tim/Current_data_file.root")

# FilesPath = open("/sps/km3net/users/tgregoir/gal_plane/software/ingredients/FilesPath.txt", "r")
# lines = FilesPath.readlines()
# for i in lines:
#   current_file = i.split("\n")
#   if current_file[0][98:109] != "MicroAntDST": continue
#
#   if job: #current_file[0][98:] != "MicroAntDST_trunk_Antares_074492.16-02-00.50000.50000.Prod2016-02.root":
#       print "xrdcp "+current_file[0]+" /sps/km3net/users/tgregoir/gal_plane/ingredient_files/Data_tim/Current_data_file.root"
#       os.system("xrdcp "+current_file[0]+" /sps/km3net/users/tgregoir/gal_plane/ingredient_files/Data_tim/Current_data_file.root")
#       infile = "/sps/km3net/users/tgregoir/gal_plane/ingredient_files/Data_tim/Current_data_file.root"
#   else:
#       infile = "/sps/km3net/users/tgregoir/gal_plane/ingredient_files/Data_tim/MicroAntDST_trunk_Antares_074492.16-02-00.50000.50000.Prod2016-02.root"

if not job:
    # outfile    = TFile.Open("/sps/km3net/users/tgregoir/gal_plane/software/ingredients/test_ing_files_data.root","recreate")
    os.system("mkdir /sps/km3net/users/tgregoir/gal_plane/ingredient_files/Data_tim"+date)
    os.system("rm -f /sps/km3net/users/tgregoir/gal_plane/ingredient_files/Data_tim"+date+"/*")
    outfile    = TFile.Open("/sps/km3net/users/tgregoir/gal_plane/ingredient_files/Data_tim"+date+"/Data_ing_files"+date+".root","recreate")

else:
    outfile    = TFile.Open("/sps/km3net/users/tgregoir/gal_plane/ingredient_files/Data_tim"+date+"/Data_ing_files_"+str(range_files)+".root","recreate")

if sps:
    lines        = glob.glob("/afs/in2p3.fr/home/t/tinom/sps/datafiles/antares/data/shower_reco_v4/finalSel/Antares_0*.root")

# outfile = TFile.Open("/sps/km3net/users/tgregoir/gal_plane/ingredient_files/test_ing_files_data.root","recreate")
# files = glob.glob("/sps/km3net/users/tgregoir/files/recofiles/oneRun/reco_*mupage.root")
# files = glob.glob("/afs/in2p3.fr/home/t/tinom/sps/datafiles/antares/mc/rbr_v3/shower_reco_v4/skim/MC_05000*.skim.root") #*_v4/skim/*tino-reco.skim.root") # MC_052409_numu_CC_b_tino-reco.PS.10.skim.root
# files = glob.glob("/sps/km3net/users/antprod/mc/rbr_v3/AntDST/*700*.root")
# files = glob.glob("/sps/km3net/users/antprod/data/pre2013/AntDST/MicroAntDSTv1r4p1_Antares_*.root")


elif not official: # if Tino's files
    DataFiles_tino = open("/sps/km3net/users/tgregoir/files/gal_plane/DataFiles_tino.txt", "r")
    lines = DataFiles_tino.readlines()

else: # if official
    # DataFiles_official = open("/sps/km3net/users/tgregoir/files/gal_plane/DataFiles_official.txt", "r")
    # lines = DataFiles_official.readlines()
    lines        = glob.glob("/sps/km3net/users/antprod/data/Reprocessing_2016_05/AntDST/Antares_*0k/Antares_*.root")
    # lines        = ["/sps/km3net/users/antprod/data/Reprocessing_2016_05/AntDST/Antares_70k/Antares_073699.16-02-00.0.50000.CAL.RECO.RE1.trunk_rev15395.i3.gz.antdst.root", "/sps/km3net/users/antprod/data/Reprocessing_2016_05/AntDST/Antares_70k/Antares_073699.16-02-00.50000.50000.CAL.RECO.RE1.trunk_rev15395.i3.gz.antdst.root"]
    # lines        = ["/sps/km3net/users/antprod/data/Reprocessing_2016_05/AntDST/Antares_60k/Antares_068518.16-02-00.0.50000.CAL.RECO.RE1.trunk_rev15395.i3.gz.antdst.root", "/sps/km3net/users/antprod/data/Reprocessing_2016_05/AntDST/Antares_60k/Antares_068518.16-02-00.100000.50000.CAL.RECO.RE1.trunk_rev15395.i3.gz.antdst.root", "/sps/km3net/users/antprod/data/Reprocessing_2016_05/AntDST/Antares_60k/Antares_068518.16-02-00.50000.50000.CAL.RECO.RE1.trunk_rev15395.i3.gz.antdst.root"]
    # lines        = ["/sps/km3net/users/antprod/data/Reprocessing_2016_05/AntDST/Antares_60k/Antares_068714.14-02-00.75000.75000.CAL.RECO.RE1.trunk_rev15395.i3.gz.antdst.root"]

##########  Take the file from hpss and untar it  ##########
for thisline in lines: # for each file in the txt
    n_files += 1

    if not official and not sps:
        if n_files <= range_files * n_files_per_job or n_files > range_files * n_files_per_job + n_files_per_job or n_files > len(lines):
        # if n_files <= range_files * 5 or n_files > range_files * 5 + 2:
            continue

        check = 1
        print "n_files =", n_files
        if job:
            current_file = thisline.split("\n")
            print current_file[0]

            for i in range(5):
                print "xrdcp root://ccxroot.in2p3.fr:1999//hpss/in2p3.fr/group/antares/user/tinom/data/"+current_file[0]+" ."
                check = os.system("xrdcp root://ccxroot.in2p3.fr:1999//hpss/in2p3.fr/group/antares/user/tinom/data/"+current_file[0]+" .")
                if check == 0: break
                if i == 5 and check != 0:
                    print "Error : xrdcp doesn't work !"
                    cp_failure += 1
            os.system("tar xvzf "+current_file[0]+" -C ./")
            files = glob.glob("./*.root")
        else:
            files = glob.glob("/sps/km3net/users/tgregoir/files/gal_plane/Data/*.root")
            if n_files > 1: continue

    # if not official:
    #     if n_files <= range_files * 5 or n_files > range_files * 5 + 5 or n_files > len(lines):
    #     # if n_files <= range_files * 5 or n_files > range_files * 5 + 2:
    #         continue
    #
    #     check = 1
    #     print "n_files =", n_files
    #     if job:
    #         current_file = thisline.split("\n")
    #         print current_file[0]
    #         os.chdir("/")
    #         os.system("mkdir /sps/km3net/users/tgregoir/files/gal_plane/temporaryData"+current_file[0][8:14]+"/")
    #         os.system("rm -f /sps/km3net/users/tgregoir/files/gal_plane/temporaryData"+current_file[0][8:14]+"/*")
    #         os.chdir("/sps/km3net/users/tgregoir/files/gal_plane/temporaryData"+current_file[0][8:14]+"/")
    #
    #         for i in range(5):
    #             print "xrdcp root://ccxroot.in2p3.fr:1999//hpss/in2p3.fr/group/antares/user/tinom/data/"+current_file[0]+" /sps/km3net/users/tgregoir/files/gal_plane/temporaryData"+current_file[0][8:14]+"/"+current_file[0]
    #             check = os.system("xrdcp root://ccxroot.in2p3.fr:1999//hpss/in2p3.fr/group/antares/user/tinom/data/"+current_file[0]+" /sps/km3net/users/tgregoir/files/gal_plane/temporaryData"+current_file[0][8:14]+"/"+current_file[0])
    #             if check == 0: break
    #             if i == 5 and check != 0: print "Error : xrdcp doesn't work !"
    #         os.chdir("/sps/km3net/users/tgregoir/files/gal_plane/temporaryData"+current_file[0][8:14]+"/")
    #         os.system("tar xvzf /sps/km3net/users/tgregoir/files/gal_plane/temporaryData"+current_file[0][8:14]+"/"+current_file[0]+" -C /sps/km3net/users/tgregoir/files/gal_plane/temporaryData"+current_file[0][8:14]+"/")
    #         os.system("rm -f /sps/km3net/users/tgregoir/files/gal_plane/temporaryData"+current_file[0][8:14]+"/"+current_file[0])
    #         files = glob.glob("/sps/km3net/users/tgregoir/files/gal_plane/temporaryData"+current_file[0][8:14]+"/*.root")
    #     else:
    #         # files = glob.glob("/sps/km3net/users/tgregoir/files/gal_plane/lala/MicroAntDST*.root")
    #         files = glob.glob("/sps/km3net/users/tgregoir/files/gal_plane/Data/*.root")
    #         # files = glob.glob("/sps/km3net/users/baret/data/MicroAntDSTv1r4p2_Antares_*.root") # From Bruny
    #         if n_files > 1: continue

    elif not sps: # if official
        if n_files <= range_files * n_files_per_job or n_files > range_files * n_files_per_job + n_files_per_job or n_files > len(lines):
            continue
        files = [thisline]

    elif sps:
        # if n_files <= range_files * n_files_per_job or n_files > range_files * n_files_per_job + n_files_per_job or n_files > len(lines):
        #     continue
        files = [thisline]

    #########  Read each of the last copied files  #########
    for infile in files:
        if os.path.exists(infile):### if file exists, loop in it to look for good events.

            if not TFile.Open(infile):
                print "Cannot open file,", infile, "continue to next file"
                continue

            dst          = AntDST()
            dstfile      = AntDSTFile(infile)
            dstfile .SetBuffers(dst)
            tfile        = TFile.Open(infile)
            antData      = tfile.Get("AntData")
            data_quality = AntDataQuality()

            if antData == None:
                print "antData == None in file", infile, "continue to next file"
                continue

            i=0
            Starting = True
            if dstfile.HasDataQuality() or sps:
            #   while dstfile.ReadEvent(i)==AntDSTFile.eSuccess and ((official and (dstfile.GetDataQuality(dst.GetRunId(), data_quality)>= 1 and data_quality.GetScanFlag() != 1)) \
            #   or (not official and (QualityBasic .get(dst.GetRunId(), 1) == 1 or (QualityBasic[int(dst.GetRunId())] >= 1 and ScanRuns[int(dst.GetRunId())] != "1")))): # == 0: if I put == 0 I have 1% less events
            #   while dstfile.ReadEvent(i)==AntDSTFile.eSuccess and (QualityBasic .get(dst.GetRunId(), 1) == 1 or (QualityBasic[int(dst.GetRunId())] >= 1 and (ScanRuns[int(dst.GetRunId())] == "0" or ScanRuns[int(dst.GetRunId())] == "10"))): # == 0: if I put == 0 I have 1% less events
              while dstfile.ReadEvent(i)==AntDSTFile.eSuccess:
                dstfile .GetDataQuality(dst.GetRunId(), data_quality)
                if data_quality .GetQualityBasic() < 1:
                    break
                if (official and data_quality.GetScanFlag() != 1) or (not official and (QualityBasic .get(dst.GetRunId(), 1) == 1 or (QualityBasic[int(dst.GetRunId())] >= 1 and ScanRuns[int(dst.GetRunId())] != "1"))): # == 0: if I put == 0 I have 1% less events
                    # print data_quality .GetQualityBasic()

                    if Starting or sps: # Do this only once per file
                        Starting = False
                        ##########  Check if it is a sparking run  ##########
                        run_number = dst.GetRunId()
                        if str(run_number) in SparkingRuns:
                            print "run", run_number, "is not selected; sparking"
                            if sps:
                                i+=1
                                continue
                            else: break

                        # if run_number > 68689:
                        #     break

                        # run_dur = data_quality.GetRunDuration()

                        run_dur = data_quality .GetFrameTime() * data_quality .GetNSlices() / 1000.
                        if run_number < 31051: run_dur *= 0.8
                        # print data_quality .GetFrameTime(), data_quality .GetNSlices(), run_dur
                        if histo_run_number .GetBinContent(histo_run_number .FindBin(run_number)) == 0:
                            histo_run_number .Fill(run_number, run_dur)
                            histo_run_dur    .Fill(1, run_dur)

                        hist_begin_runData .Fill(dst .GetMJD() - 54101)
                        hist_end_runData .Fill(dst .GetMJD() - 54101 + (run_dur/3600/24))


                    if not ((dst.HasStrategy(eShowerTantraFit) and dst.GetStrategy(eShowerTantraFit).HasRecParticle(eBrightPoint)) or (dst.HasStrategy(eAAFit) and dst.GetStrategy(eAAFit).HasRecParticle(eMuon))):
                        i+=1
                        print "Missing TANTRA and AaFit"
                        continue

                    # if dst.HasStrategy(eShowerTantraFit): print "lala"

                    # info = get_run_infos(curs, run_number)
                    # run_dur = 10**-3*info['FrameTime']*info['NSlices'] #duree du run en secondes
                    # print run_dur

                    evt                += 1
                    N_hits             = 0
                    # Nontime            = 0
                    # lik                = 0
                    AaFit_sel          = False # selected as muon by AaTrack
                    TANTRA_sel         = False
                    # time               = dst.GetMJD() # time in Modified Julian Day unit

                    ##########  If reco by Aafit  ##########
                    if dst.HasStrategy(eAAFit) and dst.GetStrategy(eAAFit).HasRecParticle(eMuon):
                        reco_part    = dst.GetStrategy(eAAFit).GetRecParticle(eMuon)
                        beta         = reco_part.GetAngularError()
                        lambda_      = dst.GetStrategy(eAAFit).GetRecQuality()
                        # cos_zenith = cos( reco_part.GetZenith() )
                        cos_theta_AA = cos( reco_part.GetTheta() )
                        N_hits       = reco_part.GetNUsedHits()
                        rec_zen      = reco_part.GetZenith()

                        energy    = reco_part.GetEnergy(edEdX_CEA)
                        if energy < 0.1: energy = 0.1

                        # energy    = reco_part.GetAntEnergy(edEdX_CEA).GetEnergy()
                        # energyANN = reco_part.GetAntEnergy(eANN_ECAP).GetEnergy()
                        # print "energy is", energy, energyANN

                        # print lambda_, reco_part.GetZenith()*TMath.RadToDeg(), cos_zenith, beta*TMath.RadToDeg()
                        #########  Track selection  ##########
                        if (eT3 in dst.GetTriggerType() or e3D_SCAN in dst.GetTriggerType()):
                            # Data_tr_cos_zen_cut .Fill(cos(rec_zen))
                            if cos_theta_AA > -0.1:
                                # Data_tr_lambda    .Fill(lambda_)
                                if lambda_ > -5.15:
                                    # Data_tr_beta      .Fill(beta*TMath.RadToDeg())
                                    if beta*TMath.RadToDeg() < 1. and beta > 0:

                                        AaFit_sel = True

                                        rec_az        = reco_part.GetAzimuth()
                                        rec_ra        = reco_part.GetRa()
                                        rec_dec       = reco_part.GetDec()
                                        # zenith_err  = dst.GetStrategy(eAAFit).GetRecParticle(eMuon).GetZenithError()
                                        # azimuth_err = dst.GetStrategy(eAAFit).GetRecParticle(eMuon).GetAzimuthError()
                                        # beta_AA     = dst.GetStrategy(eAAFit).GetRecParticle(eMuon).GetAngularError()
                                        # Lambda      = dst.GetStrategy(eAAFit).GetRecQuality()

                                        # local.SetZenAzRad(rec_zen, azimuth)
                                        # t       = Astro.Time(time)
                                        # eq      = antares.LocalToEquatorialJ2000(local,t)
                                        # rec_dec = eq.GetDecRad()
                                        # rec_ra  = eq.GetRaRad()
                                        if rec_ra > pi: rec_ra -=2*pi

                                        # N_hits = gRandom.Poisson(100)
                                        tr_bgr_vs_sindec .Fill(sin(rec_dec), tr_nbins_bgr_sindec*(2/1.8)/2) # This weight is to have a number of event/unit of sin(dec)
                                        tr_bgr_vs_ra     .Fill(rec_ra)
                                        tr_bgr_vs_coszen .Fill(cos(rec_zen), tr_nbins_bgr_coszen*(2/1.1)/2) # This weight is to have a number of event/unit of cos(zen)
                                        tr_bgr_vs_az     .Fill(rec_az)
                                        tr_spectr_bgr    .Fill(N_hits)#, tr_spectr_bgr .GetNbinsX())
                                        tr_spectr_e_bgr  .Fill(log10(energy))#, tr_spectr_bgr .GetNbinsX())
                                        tr_sum_Nevts += 1

                                        Data_tr_cos_zen_cut .Fill(cos(rec_zen))
                                        Data_tr_lambda    .Fill(lambda_)
                                        Data_tr_beta      .Fill(beta*TMath.RadToDeg())


                    ##########  If reco by TANTRA  #########
                    if (not AaFit_sel) and dst.HasStrategy(eShowerTantraFit) and dst.GetStrategy(eShowerTantraFit).HasRecParticle(eBrightPoint):
                        reco_part  = dst.GetStrategy(eShowerTantraFit).GetRecParticle(eBrightPoint)
                        beta       = reco_part.GetAngularError()
                        if beta < 0.:
                            i+=1
                            continue
                        N_hits     = reco_part.GetNUsedHits()
                        # zenith     = reco_part.GetZenith()
                        cos_theta  = cos( reco_part.GetTheta() )
                        rec_vertex = reco_part.GetPosition()# - TVector3(8220.05, 2385.13, -2206.47)
                        # qual_par   = dst.GetStrategy(eShowerTantraFit).GetAdditionalParameters().at(1)
                        M_Est      = dst.GetStrategy(eShowerTantraFit).GetAdditionalParameters().at(2)
                        rec_zen    = reco_part.GetZenith()

                        GridFit_Ratio = -100 #The GridFit ratio is the ratio between the sum of the compatible hits of all up-going and all down-going test directions
                        if dst.HasStrategy(eGridFit):
                            GridFit_Ratio = dst.GetStrategy(eGridFit).GetAdditionalParameters().at(0)

                        # print rec_vertex[0], rec_vertex[1], rec_vertex[2], M_Est, "beta", beta, GridFit_Ratio, N_hits, "cos_zen", cos(zenith)
                        # test.Fill(rec_vertex[0], rec_vertex[1])
                        # print rec_vertex[0], rec_vertex[1], rec_vertex[2]

                        # for i in range(40):  # Only energy strategy 6 gives someting but always the same...
                        #     energy = -1
                        #     energy = reco_part.GetAntEnergy(i).GetEnergy()
                        #     if energy != -1: print i, energy

                        energy    = reco_part.GetEnergy(eTantraShowerEnergy)
                        if energy < 0.1: energy = 0.1
                        # energyANN = reco_part.GetAntEnergy(eANN_ECAP).GetEnergy()

                        if dst.HasStrategy(eShowerDusjFit):
                            if dst.GetStrategy(eShowerDusjFit).HasRecParticle(eBrightPoint):
                                L_dusj = dst.GetStrategy(eShowerDusjFit).GetRecQuality()
                            elif dst.GetStrategy(eShowerDusjFit).HasRecParticle(eMuon):
                                L_dusj = dst.GetStrategy(eShowerDusjFit).GetRecQuality()
                            else:
                                L_dusj = -999999

                        beta_cut = 10.
                        if official or tau:
                            beta_cut = 26.
                        ##########  Shower selection  ##########
                        if ((eT3 in dst.GetTriggerType() or e3D_SCAN in dst.GetTriggerType())
                                and sqrt((rec_vertex[0] - 8220.05)**2 + (rec_vertex[1] - 2385.13)**2) < 300.
                                and abs(rec_vertex[2] + 2206.47) < 250.):
                            # Data_sh_cos_zen_cut  .Fill(cos(rec_zen))
                            if cos_theta > -0.1:
                                # Data_sh_M_est      .Fill(M_Est)
                                if M_Est < 1000.:
                                    # Data_sh_beta       .Fill(beta)
                                    if beta < beta_cut:
                                        # Data_sh_RDF        .Fill(L_dusj)
                                        # and (((GridFit_Ratio/1.3)**3 + (float(N_hits)/150.)**3) > 1. or tau)
                                        if L_dusj > 0.3:
                                # and cos_theta_AA > 0.1):

                                            Nontime = GetNontime(dst.GetHitsVector(), reco_part)
                                            # q_ratio = GetChargeRatio(dst.GetUsedHits(eShowerTantraFit), reco_part)
                                            q_ratio = GetChargeRatio(dst.GetHitsVector(), reco_part)
                                            lik     = GetLikelihood(dst.GetHitsVector(), reco_part, Nontime, pdf3show, pdf3mup)
                                            # Data_sh_lik_muVeto .Fill(lik)

                                            if lik > 40.:


                                                TANTRA_sel = True

                                                rec_az          = reco_part.GetAzimuth()
                                                rec_ra          = reco_part.GetRa()
                                                rec_dec         = reco_part.GetDec()
                                                # beta_Shower   = dst.GetStrategy(eShowerTantraFit).GetRecParticle(eBrightPoint).GetAngularError()
                                                # rec_vertex    = dst.GetStrategy(eShowerTantraFit).GetRecParticle(eBrightPoint).GetPosition()
                                                # rec_direction = dst.GetStrategy(eShowerTantraFit).GetRecParticle(eBrightPoint).GetAxis()
                                                # rec_energy    = dst.GetStrategy(eShowerTantraFit).GetRecParticle(eBrightPoint).GetEnergy(eTinoShowerEnergy)

                                                # local.SetZenAzRad(zenith, azimuth)
                                                # t       = Astro.Time(time)
                                                # eq      = antares.LocalToEquatorialJ2000(local,t)
                                                # rec_dec = eq.GetDecRad()
                                                # rec_ra  = eq.GetRaRad()
                                                if rec_ra > pi: rec_ra -=2*pi

                                                # N_hits = gRandom.Poisson(150)
                                                sh_bgr_vs_sindec .Fill(sin(rec_dec), nbins_bgr_sindec*(2/1.8)/2) # This weight is to have a number of event/unit of sin(dec)
                                                sh_bgr_vs_ra     .Fill(rec_ra)
                                                sh_bgr_vs_coszen .Fill(cos(rec_zen), nbins_bgr_coszen*(2/1.1)/2) # This weight is to have a number of event/unit of cos(zen)
                                                sh_bgr_vs_az     .Fill(rec_az)
                                                sh_spectr_bgr    .Fill(N_hits)#, sh_spectr_bgr.GetNbinsX())
                                                sh_spectr_e_bgr  .Fill(log10(energy))
                                                sh_sum_Nevts += 1

                                                Data_sh_cos_zen_cut  .Fill(cos(rec_zen))
                                                Data_sh_M_est      .Fill(M_Est)
                                                Data_sh_beta       .Fill(beta)
                                                Data_sh_RDF        .Fill(L_dusj)
                                                Data_sh_lik_muVeto .Fill(lik)



                    # if not AaFit_sel and not TANTRA_sel:
                    #     lala +=1
                    #     print lala, "not sel!! \n \n"
                i+=1
              print i, "evts read\tsh_Nevt_phy0", sh_sum_Nevts, "\ttr_Nevt_phy0", tr_sum_Nevts, "\t", n_files, "\n"

            tfile.Close()
            del dstfile
            del dst

        #   os.system("rm -f /sps/km3net/users/tgregoir/gal_plane/ingredient_files/Data_tim/Current_data_file.root")

    ########## Remove the last copied files  ##########
    if job and not sps and not official:
            os.system("rm -f ./*.root")
            os.system("rm -f ./*.tar.gz")
            # print "Checking that I removed everything:\nls"
            # os.system("ls")


print "sh_Nevt_phy0:", sh_sum_Nevts, "tr_Nevt_phy0:",  tr_sum_Nevts, "files:", n_files

Nevts_hist .Fill(1, tr_sum_Nevts)
Nevts_hist .Fill(3, sh_sum_Nevts)
Nevts_hist .Fill(0, cp_failure)


# sh_bgr_vs_sindec_rebinned_bis = sh_bgr_vs_sindec .Rebin(nbins_bgr_sindec/5, "sh_bgr_vs_sindec_rebinned_bis")
# tr_bgr_vs_sindec_rebinned_bis = tr_bgr_vs_sindec .Rebin(nbins_bgr_sindec/5, "tr_bgr_vs_sindec_rebinned_bis")
#
# sh_bgr_vs_sindec_rebinned_bis .Scale(1./(nbins_bgr_sindec/5.))
# tr_bgr_vs_sindec_rebinned_bis .Scale(1./(nbins_bgr_sindec/5.))
#
# sh_bgr_vs_sindec_spline_bis = TSpline3(sh_bgr_vs_sindec_rebinned_bis)
# tr_bgr_vs_sindec_spline_bis = TSpline3(tr_bgr_vs_sindec_rebinned_bis)
#
# sh_bgr_vs_sindec_rebinned = sh_bgr_vs_sindec .Rebin(nbins_bgr_sindec/10, "sh_bgr_vs_sindec_rebinned")
# tr_bgr_vs_sindec_rebinned = tr_bgr_vs_sindec .Rebin(nbins_bgr_sindec/10, "tr_bgr_vs_sindec_rebinned")
#
# sh_bgr_vs_sindec_rebinned .Scale(1./(nbins_bgr_sindec/10.))
# tr_bgr_vs_sindec_rebinned .Scale(1./(nbins_bgr_sindec/10.))
#
# sh_bgr_vs_sindec_spline = TSpline3(sh_bgr_vs_sindec_rebinned)
# tr_bgr_vs_sindec_spline = TSpline3(tr_bgr_vs_sindec_rebinned)
#
#
#
#
# sh_bgr_vs_coszen_rebinned_bis = sh_bgr_vs_coszen .Rebin(nbins_bgr_coszen/5, "sh_bgr_vs_coszen_rebinned_bis")
# tr_bgr_vs_coszen_rebinned_bis = tr_bgr_vs_coszen .Rebin(nbins_bgr_coszen/5, "tr_bgr_vs_coszen_rebinned_bis")
#
# sh_bgr_vs_coszen_rebinned_bis .Scale(1./(nbins_bgr_coszen/5.))
# tr_bgr_vs_coszen_rebinned_bis .Scale(1./(nbins_bgr_coszen/5.))
#
# sh_bgr_vs_coszen_spline_bis = TSpline3(sh_bgr_vs_coszen_rebinned_bis)
# tr_bgr_vs_coszen_spline_bis = TSpline3(tr_bgr_vs_coszen_rebinned_bis)
#
# sh_bgr_vs_coszen_rebinned = sh_bgr_vs_coszen .Rebin(nbins_bgr_coszen/10, "sh_bgr_vs_coszen_rebinned")
# tr_bgr_vs_coszen_rebinned = tr_bgr_vs_coszen .Rebin(nbins_bgr_coszen/10, "tr_bgr_vs_coszen_rebinned")
#
# sh_bgr_vs_coszen_rebinned .Scale(1./(nbins_bgr_coszen/10.))
# tr_bgr_vs_coszen_rebinned .Scale(1./(nbins_bgr_coszen/10.))
#
# sh_bgr_vs_coszen_spline = TSpline3(sh_bgr_vs_coszen_rebinned)
# tr_bgr_vs_coszen_spline = TSpline3(tr_bgr_vs_coszen_rebinned)

# c = TCanvas()
# c.SetLogy()
# sh_spectr_bgr.Draw()
# tr_spectr_bgr.Draw()


# print outfile
outfile.cd()


Data_sh_cos_zen_cut  .Write()
Data_sh_M_est      .Write()
Data_sh_beta       .Write()
Data_sh_RDF        .Write()
Data_sh_lik_muVeto .Write()

Data_tr_cos_zen_cut  .Write()
Data_tr_lambda     .Write()
Data_tr_beta       .Write()

# test                      .Write()
Nevts_hist                  .Write()
histo_run_dur               .Write()
histo_run_number            .Write()

# evt_nt                    .Write()
# sh_bgr_vs_sindec_spline     .Write()
# tr_bgr_vs_sindec_spline     .Write()
#
# sh_bgr_vs_sindec_spline_bis .Write()
# tr_bgr_vs_sindec_spline_bis .Write()
#
# sh_bgr_vs_coszen_spline     .Write()
# tr_bgr_vs_coszen_spline     .Write()
#
# sh_bgr_vs_coszen_spline_bis .Write()
# tr_bgr_vs_coszen_spline_bis .Write()

sh_spectr_bgr               .Write()
sh_spectr_e_bgr             .Write()
tr_spectr_bgr               .Write()
tr_spectr_e_bgr             .Write()
# spectr3d                  .Write()

sh_bgr_vs_sindec            .Write()
tr_bgr_vs_sindec            .Write()

sh_bgr_vs_ra                .Write()
tr_bgr_vs_ra                .Write()

sh_bgr_vs_coszen            .Write()
tr_bgr_vs_coszen            .Write()

sh_bgr_vs_az                .Write()
tr_bgr_vs_az                .Write()

hist_end_runData .Write()
hist_begin_runData .Write()

outfile.Close()

end = datetime.datetime.now()
elapsed = end-start

print "elapsed ",elapsed.seconds," seconds"
