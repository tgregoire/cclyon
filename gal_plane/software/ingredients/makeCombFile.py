#!/usr/local/bin/python
"""
Make the txt file for combination from the root file.

Usage:
    makeCombFile.py INFILE TOPOLOGY [--model MODEL] [--ntrials NTRIALS]
    makeCombFile.py [-h]

Arguments:
  INFILE             Input file containing the ntuple.
  TOPOLOGY           Topology, sh or tr.

Options:
  -h, --help         show this help message and exit
  --model MODEL      Model, 2 or 3. Can be automatically taken from the name of the input file.
  --ntrials NTRIALS  Number of trials per injected flux.
"""

import sys
import os
import glob
from math import *
from os.path import expandvars
import numpy as np
from ROOT import *
import threading
ROOT.gSystem.Load("/afs/in2p3.fr/home/t/tgregoir/sps/gal_plane/software/bin/cpex.so")



def main(infile, n_expect, topology, model, ntrials):

    Ncurves_per_Phi = {0:[ntrials]+[0]*75} # Set first entry of dic for null flux
    for Phi in np.arange(0.25, 2.1, 0.25):

        # Get the number of curves needed from each number of events injected for a flux corresponding to tr/sh_expect events
        Gauss = [TMath .Gaus(n_inj, n_expect * Phi, 0.15 * n_expect * Phi) for n_inj in range(0, 76)]
        sumGauss = sum(Gauss)
        Gauss = [x / sumGauss for x in Gauss]
        Ncurves = [int(round(sum([TMath .Poisson(n_inj, n_evt_gauss)*ntrials*gauss for n_evt_gauss, gauss in enumerate(Gauss)]))) for n_inj in range(0, 76)]

        for i in range(abs(ntrials - sum(Ncurves))):  # The total number of curves must be ntrials
            Ncurves[gRandom .Poisson(n_expect * Phi)] += (ntrials - sum(Ncurves))/abs(ntrials - sum(Ncurves))

        Ncurves_per_Phi[Phi] = Ncurves

    prefile = {} # dictionnary in which we store and class everything before writing in file
    for Phi in np.arange(0, 2.1, 0.25):
        prefile[Phi] = []

    Nstop = [n_inj for n_inj in range(0, 76)] # Nstop permits to not loop over events when prefile is complete
    for n_inj in range(0, 76):
        for flux, Ncurves in Ncurves_per_Phi .iteritems():
            if Ncurves[n_inj] > 0:
                Nstop .remove(n_inj)
                break


    if topology == 'sh': # to use only the values of llik of the current topology in the vector that contains them
        range_llik = range(31)
    else:
       range_llik = range(31, 62)

    # Get the ntuple
    tfile = TFile .Open('/sps/km3net/users/tgregoir/gal_plane/'+infile)
    ntuplePex = tfile .Get('T')

    nentries = ntuplePex .GetEntries()
    for entry in range(nentries):
        ntuplePex .GetEntry(entry)

        branch = ntuplePex.Pex.showerclus[0]
        n_inj = int(branch.inj_snsig[0]) if topology == 'sh' else int(branch.inj_tnsig[0])

        if n_inj in Nstop:
            continue

        counter = 0
        for flux, Ncurves in Ncurves_per_Phi .iteritems():
            if Ncurves[n_inj] > 0:
                Ncurves[n_inj] += -1 # remove 1 to the number of curves needed

                line = '{:.2e}'.format(flux) + '\t' + ''.join('{:.2e}'.format(list(branch .llik)[i]) + '\t' for i in range_llik)
                prefile[flux].append(line)
                break

            else:
                counter += 1
                if counter == 13:
                    Nstop .append(n_inj)

        if len(Nstop) == 76:
            break

    model_str = 'KRAg5e'+str(4+int(model))
    topology_str = 'showers' if topology == 'sh' else 'muons'
    outfile = open('results_9yrANT'+topology_str+'_'+model_str+'_'+str(ntrials)+'trials.txt', 'w')
    print >> outfile, 0, 3, 31
    for flux in np.arange(0, 2.1, 0.25):
        for line in prefile[flux]:
            print >> outfile, line
        print flux, len(prefile[flux])



if __name__ == "__main__":

    import argparse
    parser = argparse .ArgumentParser(description=__doc__,)

    # Positional argument for the input file
    parser .add_argument(
      "infile",
      nargs="?",
      default='',
      type=str,
      help="Input file containing the ntuple.")

    # Topology
    parser .add_argument(
      "topology",
      nargs="?",
      default='sh',
      type=str,
      help="Topology, sh or tr.")

    # Model used
    parser .add_argument(
      '--model',
      default='3',
      help='Model used for the corresponding input file. Can be automatically taken from the name of the input file.')

    # Number of trials
    parser .add_argument(
      '--ntrials',
      default=1e4,
      type=int,
      help='Number of trials per injected flux.')

    args = parser .parse_args()

    model = ''
    if args.model:
        model = str(args.model)
    elif args .infile.find('map') != -1: # Get model from infile name
        model = str(args.infile[args.infile.find('map')+3])

    topology = args.topology

    if model == '2':
        n_expect = 2.28990 if topology == 'sh' else 9.33347
    if model == '3':
        n_expect = 2.78362 if topology == 'sh' else 10.9365

    print 'Model is map'+model, 'and topology is', topology

    if len(sys.argv) >= 2 and model != '':
    	main(args .infile, n_expect, topology, model, args .ntrials)
    else:
    	parser .print_help()
