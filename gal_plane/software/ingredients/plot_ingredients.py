#! /usr/bin/env python

import os
from ROOT import *
from math import *
import datetime
import glob
import sys


pi = TMath.Pi()

date = ""
if "date" in sys.argv:
    for i in range(len(sys.argv)):
        if sys.argv[i] == "date":
            date = "_" + sys.argv[i + 1]

if date == "":
    print "date option nedeed ! exiting..."
    exit()

infile = TFile.Open("/sps/km3net/users/tgregoir/gal_plane/ingredient_files/Sum_ing_files" + date + ".root")
infileKRA = TFile.Open("/sps/km3net/users/tgregoir/gal_plane/ingredient_files/GalPlane_24august.root")

print "possible arguments: 'KRA', 'morpho', '2DspectDec', '2DspectRa', '1Dspect', 'sindec', 'ra', 'bgr_spect', 'bgr_spect2D', 'coszen', 'az'"
print "with 'sh' or 'tr' or nothing to get both"

gStyle.SetTitleSize(0.02, "xyz")
gStyle.SetOptStat(0)

if "KRA" in sys.argv:
    c_KRA = TCanvas("c_KRA", "c_KRA", 1100, 650)
    c_KRA .SetRightMargin(0.15)
    c_KRA .SetLogy()

    galRidge2 = infileKRA .Get("galRidge2")
    galRidge2 .GetXaxis().SetTitle("E_{#nu} [TeV]")
    galRidge2 .GetYaxis().SetTitle(
        "E^{2}_{#nu} d#Phi/(dE_{#nu} d#Omega) [GeV cm^{-2} s^{-1} sr^{-1}]")
    galRidge2 .GetYaxis().SetTitleOffset(1.2)
    galRidge2 .GetYaxis().SetRangeUser(1e-15, 5e-5)
    galRidge2 .SetLineWidth(2)
    galRidge2 .SetLineColor(2)
    galRidge2 .SetLineStyle(10)

    galRidge2 .GetXaxis().SetTitleSize(0.05)
    galRidge2 .GetXaxis().SetTitleOffset(0.80)
    galRidge2 .GetYaxis().SetTitleSize(0.05)
    galRidge2 .GetYaxis().SetTitleOffset(0.80)

    galRidge2 .Draw()

    fullSky2 = infileKRA .Get("fullSky2")
    fullSky2 .SetLineWidth(2)
    fullSky2 .SetLineStyle(10)

    fullSky2 .Draw("same")

    galRidge3 = infileKRA .Get("galRidge3")
    galRidge3 .SetLineWidth(2)
    galRidge3 .SetLineColor(2)

    galRidge3 .Draw("same")

    fullSky3 = infileKRA .Get("fullSky3")
    fullSky3 .SetLineWidth(2)

    fullSky3 .Draw("same")

    leg_galRidge = TLegend(.10, .10, .55, .30)
    leg_galRidge .SetFillColor(kWhite)
    leg_galRidge .AddEntry(
        galRidge3, "KRA#gamma Galactic Ridge (cut 5e7 GeV)", "l")
    leg_galRidge .AddEntry(
        galRidge2, "KRA#gamma Galactic Ridge (cut 5e6 GeV)", "l")
    leg_galRidge .AddEntry(fullSky3, "KRA#gamma Full Sky (cut 5e7 GeV)", "l")
    leg_galRidge .AddEntry(fullSky2, "KRA#gamma Full Sky (cut 5e6 GeV)", "l")

    leg_galRidge .Draw()


if "morpho" in sys.argv:
    if "tr" not in sys.argv:
        c_morpho_reco_sh = TCanvas(
            "c_morpho_reco_sh", "c_morpho_reco_sh", 1100, 650)
        c_morpho_reco_sh .SetLogz()
        c_morpho_reco_sh .SetRightMargin(0.15)

        morpho_reco_sh = infile .Get("morpho_reco_sh")
        morpho_reco_sh .Scale(1. / (morpho_reco_sh .Integral() * 4 * pi))
        morpho_reco_sh .RebinX(2)
        morpho_reco_sh .RebinY(2)
        morpho_reco_sh .GetXaxis().SetTitle("Right Ascension [rad]")
        morpho_reco_sh .GetYaxis().SetTitle("Declination [rad]")
        morpho_reco_sh .GetZaxis().SetTitle("Probability [sr^{-1}]")
        # morpho_reco_sh .GetZaxis().SetRangeUser(6e-7, 1e-3)

        morpho_reco_sh .GetXaxis().SetTitleSize(0.05)
        morpho_reco_sh .GetXaxis().SetTitleOffset(0.80)
        morpho_reco_sh .GetYaxis().SetTitleSize(0.05)
        morpho_reco_sh .GetYaxis().SetTitleOffset(0.80)

        morpho_reco_sh .Draw("z")
        gPad.Update()
        palette = morpho_reco_sh .GetListOfFunctions().FindObject("palette")
        palette.SetTitleOffset(0.8)
        palette.SetTitleSize(0.05)
        morpho_reco_sh .Draw("colz")

    if "sh" not in sys.argv:
        c_morpho_reco_tr = TCanvas(
            "c_morpho_reco_tr", "c_morpho_reco_tr", 1100, 650)
        c_morpho_reco_tr .SetLogz()
        c_morpho_reco_tr .SetRightMargin(0.15)

        morpho_reco_tr = infile .Get("morpho_reco_tr")
        morpho_reco_tr .Scale(1. / (morpho_reco_tr .Integral() * 4 * pi))
        morpho_reco_tr .RebinX(2)
        morpho_reco_tr .RebinY(2)
        morpho_reco_tr .GetXaxis().SetTitle("Right Ascension [rad]")
        morpho_reco_tr .GetYaxis().SetTitle("Declination [rad]")
        morpho_reco_tr .GetZaxis().SetTitle("Probability [sr^{-1}]")
        # morpho_reco_tr .GetZaxis().SetRangeUser(6e-7, 1e-3)

        morpho_reco_tr .GetXaxis().SetTitleSize(0.05)
        morpho_reco_tr .GetXaxis().SetTitleOffset(0.80)
        morpho_reco_tr .GetYaxis().SetTitleSize(0.05)
        morpho_reco_tr .GetYaxis().SetTitleOffset(0.80)

        morpho_reco_tr .Draw("z")
        gPad.Update()
        palette = morpho_reco_tr .GetListOfFunctions().FindObject("palette")
        palette.SetTitleOffset(0.8)
        palette.SetTitleSize(0.05)
        morpho_reco_tr .Draw("colz")


if "2DspectRa" in sys.argv:
    if "tr" not in sys.argv:
        c_sh_spectrum_projZX = TCanvas(
            "c_sh_spectrum_projZX", "c_sh_spectrum_projZX", 1100, 650)
        c_sh_spectrum_projZX .SetLogz()
        c_sh_spectrum_projZX .SetRightMargin(0.15)

        sh_spectrum3d_e_KRA_bis = infile .Get("sh_spectrum3d_e_KRA_bis")
        sh_spectrum_projZX = sh_spectrum3d_e_KRA_bis .Project3D("zx")
        sh_spectrum_projZX .RebinX(2)
        sh_spectrum_projZX .GetYaxis().SetTitle("log_{10}(E_{TANTRA} [GeV])")
        sh_spectrum_projZX .GetYaxis().SetTitleOffset(1.2)
        sh_spectrum_projZX .GetXaxis().SetTitle("Right Ascension [rad]")
        sh_spectrum_projZX .GetZaxis().SetTitle("N_{evts}/bin")
        sh_spectrum_projZX .GetZaxis().SetRangeUser(1e-6, 1e-2)

        sh_spectrum_projZX .GetXaxis().SetTitleSize(0.05)
        sh_spectrum_projZX .GetXaxis().SetTitleOffset(0.80)
        sh_spectrum_projZX .GetYaxis().SetTitleSize(0.05)
        sh_spectrum_projZX .GetYaxis().SetTitleOffset(0.80)

        sh_spectrum_projZX  .Draw("z")
        gPad.Update()
        palette = sh_spectrum_projZX  .GetListOfFunctions().FindObject("palette")
        palette.SetTitleOffset(0.8)
        palette.SetTitleSize(0.05)

        sh_spectrum_projZX .Draw("colz")

    if "sh" not in sys.argv:
        c_tr_spectrum_projZX = TCanvas(
            "c_tr_spectrum_projZX", "c_tr_spectrum_projZX", 1100, 650)
        c_tr_spectrum_projZX .SetLogz()
        c_tr_spectrum_projZX .SetRightMargin(0.15)

        tr_spectrum3d_e_KRA_bis = infile .Get("tr_spectrum3d_e_KRA_bis")
        tr_spectrum_projZX = tr_spectrum3d_e_KRA_bis .Project3D("zx")
        tr_spectrum_projZX .RebinX(2)
        tr_spectrum_projZX .GetYaxis().SetTitle("log_{10}(E_{TANTRA} [GeV])")
        tr_spectrum_projZX .GetXaxis().SetTitle("Right Ascension [rad]")
        tr_spectrum_projZX .GetZaxis().SetTitle("N_{evts}/bin")
        tr_spectrum_projZX .GetZaxis().SetRangeUser(1e-7, 6e-2)

        tr_spectrum_projZX .GetXaxis().SetTitleSize(0.05)
        tr_spectrum_projZX .GetXaxis().SetTitleOffset(0.80)
        tr_spectrum_projZX .GetYaxis().SetTitleSize(0.05)
        tr_spectrum_projZX .GetYaxis().SetTitleOffset(0.80)

        tr_spectrum_projZX  .Draw("z")
        gPad.Update()
        palette = tr_spectrum_projZX  .GetListOfFunctions().FindObject("palette")
        palette.SetTitleOffset(0.8)
        palette.SetTitleSize(0.05)

        tr_spectrum_projZX .Draw("colz")


if "2DspectDec" in sys.argv:
    if "tr" not in sys.argv:
        c_sh_spectrum_projYZ = TCanvas(
            "c_sh_spectrum_projYZ", "c_sh_spectrum_projYZ", 1100, 650)
        c_sh_spectrum_projYZ .SetLogz()
        c_sh_spectrum_projYZ .SetRightMargin(0.15)

        sh_spectrum3d_e_KRA_bis = infile .Get("sh_spectrum3d_e_KRA_bis")
        sh_spectrum_projYZ = sh_spectrum3d_e_KRA_bis .Project3D("yz")
        sh_spectrum_projYZ .RebinY(2)
        sh_spectrum_projYZ .GetXaxis().SetTitle("log_{10}(E_{TANTRA} [GeV])")
        sh_spectrum_projYZ .GetXaxis().SetTitleOffset(1.2)
        sh_spectrum_projYZ .GetYaxis().SetTitle("Declination [rad]")
        sh_spectrum_projYZ .GetZaxis().SetTitle("N_{evts}/bin")
        sh_spectrum_projYZ .GetZaxis().SetRangeUser(1e-6, 1.5e-2)

        sh_spectrum_projYZ .GetXaxis().SetTitleSize(0.05)
        sh_spectrum_projYZ .GetXaxis().SetTitleOffset(0.80)
        sh_spectrum_projYZ .GetYaxis().SetTitleSize(0.05)
        sh_spectrum_projYZ .GetYaxis().SetTitleOffset(0.80)

        sh_spectrum_projYZ  .Draw("z")
        gPad.Update()
        palette = sh_spectrum_projYZ  .GetListOfFunctions().FindObject("palette")
        palette.SetTitleOffset(0.8)
        palette.SetTitleSize(0.05)

        sh_spectrum_projYZ .Draw("colz")

        # c_sh_spectrum_projYZ = TCanvas("c_sh_spectrum_projYZ", "c_sh_spectrum_projYZ", 1100, 650)#
        # c_sh_spectrum_projYZ .SetLogz()
        # c_sh_spectrum_projYZ .SetRightMargin(0.15)
        #
        # sh_spectrum3d_KRA_bis = infile .Get("sh_spectrum3d_KRA_bis")
        # sh_spectrum_projYZ = sh_spectrum3d_KRA_bis .Project3D("yz")
        # sh_spectrum_projYZ .GetXaxis().SetTitle("log_{10}(E_{TANTRA} [GeV])")
        # sh_spectrum_projYZ .GetYaxis().SetTitle("Declination [rad]")
        # sh_spectrum_projYZ .GetZaxis().SetTitle("N_{evts}/bin")
        #
        # sh_spectrum_projYZ .Draw("colz")

    if "sh" not in sys.argv:
        c_tr_spectrum_projYZ = TCanvas(
            "c_tr_spectrum_projYZ", "c_tr_spectrum_projYZ", 1100, 650)
        c_tr_spectrum_projYZ .SetLogz()
        c_tr_spectrum_projYZ .SetRightMargin(0.15)

        tr_spectrum3d_e_KRA_bis = infile .Get("tr_spectrum3d_e_KRA_bis")
        tr_spectrum_projYZ = tr_spectrum3d_e_KRA_bis .Project3D("yz")
        tr_spectrum_projYZ .RebinY(2)
        tr_spectrum_projYZ .GetXaxis().SetTitle("log_{10}(E_{TANTRA} [GeV])")
        tr_spectrum_projYZ .GetYaxis().SetTitle("Declination [rad]")
        tr_spectrum_projYZ .GetZaxis().SetTitle("N_{evts}/bin")
        tr_spectrum_projYZ .GetZaxis().SetRangeUser(1e-7, 4e-2)

        tr_spectrum_projYZ .GetXaxis().SetTitleSize(0.05)
        tr_spectrum_projYZ .GetXaxis().SetTitleOffset(0.80)
        tr_spectrum_projYZ .GetYaxis().SetTitleSize(0.05)
        tr_spectrum_projYZ .GetYaxis().SetTitleOffset(0.80)

        tr_spectrum_projYZ  .Draw("z")
        gPad.Update()
        palette = tr_spectrum_projYZ  .GetListOfFunctions().FindObject("palette")
        palette.SetTitleOffset(0.8)
        palette.SetTitleSize(0.05)

        tr_spectrum_projYZ .Draw("colz")


if "1Dspect" in sys.argv:
    if "sh" not in sys.argv:
        c_tr_spectrum_projZ = TCanvas(
            "c_tr_spectrum_projZ", "c_tr_spectrum_projZ", 1100, 650)
        c_tr_spectrum_projZ .SetLogy()
        c_tr_spectrum_projZ .SetRightMargin(0.15)

        tr_spectrum3d_e_KRA_bis = infile .Get("tr_spectrum3d_e_KRA_bis")
        tr_spectrum_projZ = tr_spectrum3d_e_KRA_bis .Project3D("z")
        tr_spectrum_projZ .GetXaxis().SetTitle("log_{10}(E_{TANTRA} [GeV])")
        tr_spectrum_projZ .GetYaxis().SetTitle("N_{evts}/bin")
        tr_spectrum_projZ .SetLineWidth(2)

        tr_spectrum_projZ .GetXaxis().SetTitleSize(0.05)
        tr_spectrum_projZ .GetXaxis().SetTitleOffset(0.80)
        tr_spectrum_projZ .GetYaxis().SetTitleSize(0.05)
        tr_spectrum_projZ .GetYaxis().SetTitleOffset(0.80)

        tr_spectrum_projZ .Draw()

    if "tr" not in sys.argv:
        c_sh_spectrum_projZ = TCanvas(
            "c_sh_spectrum_projZ", "c_sh_spectrum_projZ", 1100, 650)
        c_sh_spectrum_projZ .SetLogy()
        c_sh_spectrum_projZ .SetRightMargin(0.15)

        sh_spectrum3d_e_KRA_bis = infile .Get("sh_spectrum3d_e_KRA_bis")
        sh_spectrum_projZ = sh_spectrum3d_e_KRA_bis .Project3D("z")
        sh_spectrum_projZ .GetXaxis().SetTitle("log_{10}(E_{TANTRA} [GeV])")
        sh_spectrum_projZ .GetXaxis().SetTitleOffset(1.2)
        sh_spectrum_projZ .GetYaxis().SetTitle("N_{evts}/bin")
        sh_spectrum_projZ .SetLineWidth(2)

        sh_spectrum_projZ .GetXaxis().SetTitleSize(0.05)
        sh_spectrum_projZ .GetXaxis().SetTitleOffset(0.80)
        sh_spectrum_projZ .GetYaxis().SetTitleSize(0.05)
        sh_spectrum_projZ .GetYaxis().SetTitleOffset(0.80)

        sh_spectrum_projZ .Draw()

        # sh_spectrum_projZ = sh_spectrum3d_KRA_bis .Project3D("z")
        # sh_spectrum_projZ .SetLineColor(2)
        # sh_spectrum_projZ .SetLineWidth(2)
        #
        # sh_spectrum_projZ .Draw("same")
        #
        #
        # leg_spectrum_projZ = TLegend(.70,.75,.85,.90)
        # leg_spectrum_projZ .SetFillColor(kWhite)
        # leg_spectrum_projZ .AddEntry(tr_spectrum_projZ, "Track", "l")
        # leg_spectrum_projZ .AddEntry(sh_spectrum_projZ, "Shower", "l")
        #
        # leg_spectrum_projZ .Draw()


if "sindec" in sys.argv:
    if "tr" not in sys.argv:
        c_sh_sindec_atmMC = TCanvas(
            "c_sh_sindec_atmMC", "c_sh_sindec_atmMC", 1100, 650)
        c_sh_sindec_atmMC .SetRightMargin(0.15)

        sh_bgr_vs_sindec = infile .Get("sh_bgr_vs_sindec")
        sh_bgr_vs_sindec .GetXaxis().SetTitle("sin(decl)")
        sh_bgr_vs_sindec .GetYaxis().SetTitle("dN_{evts}/dsin(decl)")
        sh_bgr_vs_sindec .GetYaxis().SetTitleOffset(1.3)
        sh_bgr_vs_sindec .GetYaxis().SetRangeUser(0, 1200)
        sh_bgr_vs_sindec .SetFillColor(5)
        sh_bgr_vs_sindec .SetLineWidth(2)

        sh_bgr_vs_sindec .GetXaxis().SetTitleSize(0.05)
        sh_bgr_vs_sindec .GetXaxis().SetTitleOffset(0.80)
        sh_bgr_vs_sindec .GetYaxis().SetTitleSize(0.05)
        sh_bgr_vs_sindec .GetYaxis().SetTitleOffset(0.80)

        sh_bgr_vs_sindec .Draw()

        sh_sindec_atmMC = infile .Get("sh_sindec_atmMC")
        sh_sindec_atmMC .SetLineColor(1)

        sh_sindec_atmMC .Draw("same")

        sh_bgr_vs_sindec_rebinned = infile .Get("sh_bgr_vs_sindec_rebinned")
        sh_bgr_vs_sindec_rebinned .SetLineColor(2)
        sh_bgr_vs_sindec_rebinned .SetLineWidth(2)

        sh_bgr_vs_sindec_rebinned .Draw("same")

        leg_sh_sindec_atmMC = TLegend(.75, .75, .85, .90)
        leg_sh_sindec_atmMC .SetFillColor(kWhite)
        leg_sh_sindec_atmMC .AddEntry(sh_bgr_vs_sindec, "Data", "f")
        leg_sh_sindec_atmMC .AddEntry(sh_bgr_vs_sindec_rebinned, "Spline", "l")
        leg_sh_sindec_atmMC .AddEntry(sh_sindec_atmMC, "MC", "l")

        leg_sh_sindec_atmMC .Draw()

    if "sh" not in sys.argv:
        c_tr_sindec_atmMC = TCanvas(
            "c_tr_sindec_atmMC", "c_tr_sindec_atmMC", 1100, 650)
        c_tr_sindec_atmMC .SetRightMargin(0.15)

        tr_bgr_vs_sindec = infile .Get("tr_bgr_vs_sindec")
        tr_bgr_vs_sindec .GetXaxis().SetTitle("sin(decl)")
        tr_bgr_vs_sindec .GetYaxis().SetTitle("dN_{evts}/dsin(decl)")
        tr_bgr_vs_sindec .GetYaxis().SetTitleOffset(1.3)
        tr_bgr_vs_sindec .GetYaxis().SetRangeUser(0, 13000)
        tr_bgr_vs_sindec .SetFillColor(5)
        tr_bgr_vs_sindec .SetLineWidth(2)

        tr_bgr_vs_sindec .GetXaxis().SetTitleSize(0.05)
        tr_bgr_vs_sindec .GetXaxis().SetTitleOffset(0.80)
        tr_bgr_vs_sindec .GetYaxis().SetTitleSize(0.05)
        tr_bgr_vs_sindec .GetYaxis().SetTitleOffset(0.80)

        tr_bgr_vs_sindec .Draw()

        tr_sindec_atmMC = infile .Get("tr_sindec_atmMC")
        tr_sindec_atmMC .SetLineColor(1)

        tr_sindec_atmMC .Draw("same")

        tr_bgr_vs_sindec_rebinned = infile .Get("tr_bgr_vs_sindec_rebinned")
        tr_bgr_vs_sindec_rebinned .SetLineColor(2)
        tr_bgr_vs_sindec_rebinned .SetLineWidth(2)

        tr_bgr_vs_sindec_rebinned .Draw("same")

        leg_tr_sindec_atmMC = TLegend(.75, .75, .85, .90)
        leg_tr_sindec_atmMC .SetFillColor(kWhite)
        leg_tr_sindec_atmMC .AddEntry(tr_bgr_vs_sindec, "Data", "f")
        leg_tr_sindec_atmMC .AddEntry(tr_bgr_vs_sindec_rebinned, "Spline", "l")
        leg_tr_sindec_atmMC .AddEntry(tr_sindec_atmMC, "MC", "l")

        leg_tr_sindec_atmMC .Draw()


if "ra" in sys.argv:
    if "tr" not in sys.argv:

        c_sh_ra_atmMC = TCanvas("c_sh_ra_atmMC", "c_sh_ra_atmMC", 1100, 650)
        c_sh_ra_atmMC .SetRightMargin(0.15)

        sh_bgr_vs_ra = infile .Get("sh_bgr_vs_ra")
        sh_bgr_vs_ra .Scale(sh_bgr_vs_ra .GetNbinsX() / (2 * pi))
        sh_bgr_vs_ra .GetXaxis().SetTitle("right ascension [rad]")
        sh_bgr_vs_ra .GetYaxis().SetTitle("dN_{evts}/d(right ascension)")
        sh_bgr_vs_ra .GetYaxis().SetRangeUser(0, 380)
        sh_bgr_vs_ra .SetLineColor(2)

        sh_bgr_vs_ra .GetXaxis().SetTitleSize(0.05)
        sh_bgr_vs_ra .GetXaxis().SetTitleOffset(0.80)
        sh_bgr_vs_ra .GetYaxis().SetTitleSize(0.05)
        sh_bgr_vs_ra .GetYaxis().SetTitleOffset(0.80)

        sh_bgr_vs_ra .Draw()

        sh_ra_atmMC = infile .Get("sh_ra_atmMC")
        sh_ra_atmMC .SetLineWidth(2)

        sh_ra_atmMC .Draw("same")

        leg_sh_ra_atmMC = TLegend(.75, .75, .85, .90)
        leg_sh_ra_atmMC .SetFillColor(kWhite)
        leg_sh_ra_atmMC .AddEntry(sh_bgr_vs_ra, "Data", "l")
        leg_sh_ra_atmMC .AddEntry(sh_ra_atmMC, "MC", "l")

        leg_sh_ra_atmMC .Draw()

    if "sh" not in sys.argv:
        c_tr_ra_atmMC = TCanvas("c_tr_ra_atmMC", "c_tr_ra_atmMC", 1100, 650)
        c_tr_ra_atmMC .SetRightMargin(0.15)

        tr_bgr_vs_ra = infile .Get("tr_bgr_vs_ra")
        tr_bgr_vs_ra .Scale(tr_bgr_vs_ra .GetNbinsX() / (2 * pi))
        tr_bgr_vs_ra .GetXaxis().SetTitle("right ascension [rad]")
        tr_bgr_vs_ra .GetYaxis().SetTitle("dN_{evts}/d(right ascension)")
        tr_bgr_vs_ra .GetYaxis().SetTitleOffset(1.2)
        tr_bgr_vs_ra .GetYaxis().SetRangeUser(600, 3000)
        tr_bgr_vs_ra .SetLineColor(2)

        tr_bgr_vs_ra .GetXaxis().SetTitleSize(0.05)
        tr_bgr_vs_ra .GetXaxis().SetTitleOffset(0.80)
        tr_bgr_vs_ra .GetYaxis().SetTitleSize(0.05)
        tr_bgr_vs_ra .GetYaxis().SetTitleOffset(0.80)

        tr_bgr_vs_ra .Draw()

        tr_ra_atmMC = infile .Get("tr_ra_atmMC")
        tr_ra_atmMC .SetLineWidth(2)

        tr_ra_atmMC .Draw("same")

        leg_tr_ra_atmMC = TLegend(.75, .75, .85, .90)
        leg_tr_ra_atmMC .SetFillColor(kWhite)
        leg_tr_ra_atmMC .AddEntry(tr_bgr_vs_ra, "Data", "l")
        leg_tr_ra_atmMC .AddEntry(tr_ra_atmMC, "MC", "l")

        leg_tr_ra_atmMC .Draw()


if "bgr_spect" in sys.argv:
    if "tr" not in sys.argv:
        c_sh_spectrum_bgr = TCanvas(
            "c_sh_spectrum_bgr", "c_sh_spectrum_bgr", 1100, 650)
        c_sh_spectrum_bgr .SetLogy()
        c_sh_spectrum_bgr .SetRightMargin(0.15)

        sh_spectrum_bgr = infile .Get("sh_spectrum_e_bgr")
        sh_spectrum_bgr .GetXaxis().SetTitle("log_{10}(E_{TANTRA} [GeV])")
        sh_spectrum_bgr .GetYaxis().SetTitle("N_{evts}/bin")
        sh_spectrum_bgr .GetXaxis().SetRangeUser(0, 10)
        sh_spectrum_bgr .GetYaxis().SetRangeUser(1e-8, 100)
        sh_spectrum_bgr .SetLineColor(2)
        sh_spectrum_bgr .SetLineWidth(2)

        sh_spectrum_bgr .GetXaxis().SetTitleSize(0.05)
        sh_spectrum_bgr .GetXaxis().SetTitleOffset(0.80)
        sh_spectrum_bgr .GetYaxis().SetTitleSize(0.05)
        sh_spectrum_bgr .GetYaxis().SetTitleOffset(0.80)

        sh_spectrum_bgr .Draw()

        sh_spectr_atmmc = infile .Get("sh_spectr_e_atmmc")
        sh_spectr_atmmc .SetLineWidth(2)

        sh_spectr_atmmc .Draw("same")

        leg_sh_spectrum_bgr = TLegend(.75, .75, .85, .90)
        leg_sh_spectrum_bgr .SetFillColor(kWhite)
        leg_sh_spectrum_bgr .AddEntry(sh_spectrum_bgr, "Data", "l")
        leg_sh_spectrum_bgr .AddEntry(sh_spectr_atmmc, "MC", "l")

        leg_sh_spectrum_bgr .Draw()

    if "sh" not in sys.argv:
        c_tr_spectrum_bgr = TCanvas(
            "c_tr_spectrum_bgr", "c_tr_spectrum_bgr", 1100, 650)
        c_tr_spectrum_bgr .SetLogy()
        c_tr_spectrum_bgr .SetRightMargin(0.15)

        tr_spectrum_bgr = infile .Get("tr_spectrum_e_bgr")
        tr_spectrum_bgr .GetXaxis().SetTitle("log_{10}(E_{TANTRA} [GeV])")
        tr_spectrum_bgr .GetYaxis().SetTitle("N_{evts}/bin")
        # tr_spectrum_bgr .GetYaxis().SetTitleFontSize(0.4)
        tr_spectrum_bgr .GetXaxis().SetRangeUser(0, 800)
        tr_spectrum_bgr .GetYaxis().SetRangeUser(1e-12, 2e3)
        tr_spectrum_bgr .SetLineColor(2)
        tr_spectrum_bgr .SetLineWidth(2)

        tr_spectrum_bgr .GetXaxis().SetTitleSize(0.05)
        tr_spectrum_bgr .GetXaxis().SetTitleOffset(0.80)
        tr_spectrum_bgr .GetYaxis().SetTitleSize(0.05)
        tr_spectrum_bgr .GetYaxis().SetTitleOffset(0.80)

        tr_spectrum_bgr .Draw()

        tr_spectr_atmmc = infile .Get("tr_spectr_e_atmmc")
        tr_spectr_atmmc .SetLineWidth(2)

        tr_spectr_atmmc .Draw("same")

        leg_tr_spectrum_bgr = TLegend(.75, .75, .85, .90)
        leg_tr_spectrum_bgr .SetFillColor(kWhite)
        leg_tr_spectrum_bgr .AddEntry(tr_spectrum_bgr, "Data", "l")
        leg_tr_spectrum_bgr .AddEntry(tr_spectr_atmmc, "MC", "l")

        leg_tr_spectrum_bgr .Draw()


if "bgr_spect2D" in sys.argv:
    if "tr" not in sys.argv:
        c_sh_spectrum_bgr2D = TCanvas(
            "c_sh_spectrum_bgr2D", "c_sh_spectrum_bgr2D", 1100, 650)
        c_sh_spectrum_bgr2D .SetLogz()
        c_sh_spectrum_bgr2D .SetRightMargin(0.15)

        sh_spectrum_bgr2D = infile .Get("sh_spectr2d_e_atmmc_zen_final")
        sh_spectrum_bgr2D .GetXaxis().SetTitle("log_{10}(E_{TANTRA} [GeV])")
        sh_spectrum_bgr2D .GetYaxis().SetTitle("Zenith [rad]")
        sh_spectrum_bgr2D .GetZaxis().SetTitle("N_{evts}/bin")
        sh_spectrum_bgr2D .GetXaxis().SetRangeUser(0, 7)
        # sh_spectrum_bgr2D .GetZaxis().SetRangeUser(1e-3, 1.5)

        sh_spectrum_bgr2D .GetXaxis().SetTitleSize(0.05)
        sh_spectrum_bgr2D .GetXaxis().SetTitleOffset(0.80)
        sh_spectrum_bgr2D .GetYaxis().SetTitleSize(0.05)
        sh_spectrum_bgr2D .GetYaxis().SetTitleOffset(0.80)

        sh_spectrum_bgr2D  .Draw("z")
        gPad.Update()
        palette = sh_spectrum_bgr2D  .GetListOfFunctions().FindObject("palette")
        palette.SetTitleOffset(0.8)
        palette.SetTitleSize(0.05)

        sh_spectrum_bgr2D .Draw("colz")

        c_sh_spectrum_bgr2D_peaks = TCanvas(
            "c_sh_spectrum_bgr2D_peaks", "c_sh_spectrum_bgr2D_peaks", 1100, 650)
        c_sh_spectrum_bgr2D_peaks .SetLogz()
        c_sh_spectrum_bgr2D_peaks .SetRightMargin(0.15)

        sh_spectrum_bgr2D_peaks = infile .Get(
            "sh_spectr2d_e_atmmc_zen_initial")
        sh_spectrum_bgr2D_peaks .GetXaxis().SetTitle(
            "log_{10}(E_{TANTRA} [GeV])")
        sh_spectrum_bgr2D_peaks .GetYaxis().SetTitle("Zenith [rad]")
        sh_spectrum_bgr2D_peaks .GetZaxis().SetTitle("N_{evts}/bin")
        # sh_spectrum_bgr2D_peaks .GetXaxis().SetRangeUser(0, 7)
        sh_spectrum_bgr2D_peaks .GetZaxis().SetRangeUser(1e-3, 9)

        sh_spectrum_bgr2D_peaks .GetXaxis().SetTitleSize(0.05)
        sh_spectrum_bgr2D_peaks .GetXaxis().SetTitleOffset(0.80)
        sh_spectrum_bgr2D_peaks .GetYaxis().SetTitleSize(0.05)
        sh_spectrum_bgr2D_peaks .GetYaxis().SetTitleOffset(0.80)

        sh_spectrum_bgr2D_peaks  .Draw("z")
        gPad.Update()
        palette = sh_spectrum_bgr2D_peaks  .GetListOfFunctions().FindObject("palette")
        palette.SetTitleOffset(0.8)
        palette.SetTitleSize(0.05)

        sh_spectrum_bgr2D_peaks .Draw("colz")

        c_sh_spectrum_bgr2D_mupage = TCanvas(
            "c_sh_spectrum_bgr2D_mupage", "c_sh_spectrum_bgr2D_mupage", 1100, 650)
        c_sh_spectrum_bgr2D_mupage .SetLogz()
        c_sh_spectrum_bgr2D_mupage .SetRightMargin(0.15)

        sh_spectrum_bgr2D_mupage = infile .Get(
            "sh_spectr2d_e_muatmmc_zen_bef_cuts")
        sh_spectrum_bgr2D_mupage .GetXaxis().SetTitle(
            "log_{10}(E_{TANTRA} [GeV])")
        sh_spectrum_bgr2D_mupage .GetYaxis().SetTitle("Zenith [rad]")
        sh_spectrum_bgr2D_mupage .GetZaxis().SetTitle("N_{evts}/bin")
        sh_spectrum_bgr2D_mupage .GetXaxis().SetRangeUser(0, 7)
        # sh_spectrum_bgr2D_mupage .GetZaxis().SetRangeUser(1, 15)

        sh_spectrum_bgr2D_mupage .GetXaxis().SetTitleSize(0.05)
        sh_spectrum_bgr2D_mupage .GetXaxis().SetTitleOffset(0.80)
        sh_spectrum_bgr2D_mupage .GetYaxis().SetTitleSize(0.05)
        sh_spectrum_bgr2D_mupage .GetYaxis().SetTitleOffset(0.80)

        sh_spectrum_bgr2D_mupage  .Draw("z")
        gPad.Update()
        palette = sh_spectrum_bgr2D_mupage  .GetListOfFunctions().FindObject("palette")
        palette.SetTitleOffset(0.8)
        palette.SetTitleSize(0.05)

        sh_spectrum_bgr2D_mupage .Draw("colz")

        c_sh_spectrum_bgr2D_ProjX = TCanvas(
            "c_sh_spectrum_bgr2D_ProjX", "c_sh_spectrum_bgr2D_ProjX", 1100, 650)
        c_sh_spectrum_bgr2D_ProjX .SetLogz()
        c_sh_spectrum_bgr2D_ProjX .SetRightMargin(0.15)

        sh_spectrum_bgr2D_ProjX = sh_spectrum_bgr2D_mupage .ProjectionX()
        sh_spectrum_bgr2D_ProjX .Rebin(2)
        sh_spectrum_bgr2D_ProjX .GetXaxis().SetTitle(
            "log_{10}(E_{TANTRA} [GeV])")
        sh_spectrum_bgr2D_ProjX .GetYaxis().SetTitle("N_{evts}/bin")
        sh_spectrum_bgr2D_ProjX .GetXaxis().SetRangeUser(0, 7)
        sh_spectrum_bgr2D_ProjX .SetFillColor(2)
        sh_spectrum_bgr2D_ProjX .SetLineColor(0)

        sh_spectrum_bgr2D_ProjX .GetXaxis().SetTitleSize(0.05)
        sh_spectrum_bgr2D_ProjX .GetXaxis().SetTitleOffset(0.80)
        sh_spectrum_bgr2D_ProjX .GetYaxis().SetTitleSize(0.05)
        sh_spectrum_bgr2D_ProjX .GetYaxis().SetTitleOffset(0.80)

        sh_spectrum_bgr2D_ProjX .Draw()

        c_sh_spectrum_bgr2D_ProjY = TCanvas(
            "c_sh_spectrum_bgr2D_ProjY", "c_sh_spectrum_bgr2D_ProjY", 1100, 650)
        c_sh_spectrum_bgr2D_ProjY .SetLogz()
        c_sh_spectrum_bgr2D_ProjY .SetRightMargin(0.15)

        sh_spectrum_bgr2D_ProjY = sh_spectrum_bgr2D_mupage .ProjectionY()
        sh_spectrum_bgr2D_ProjY .Rebin(2)
        sh_spectrum_bgr2D_ProjY .GetXaxis().SetTitle("Zenith [rad]")
        sh_spectrum_bgr2D_ProjY .GetYaxis().SetTitle("N_{evts}/bin")
        # sh_spectrum_bgr2D_ProjY .GetYaxis().SetTitleFontSize(0.4)
        sh_spectrum_bgr2D_ProjY .SetFillColor(2)

        sh_spectrum_bgr2D_ProjY .GetXaxis().SetTitleSize(0.05)
        sh_spectrum_bgr2D_ProjY .GetXaxis().SetTitleOffset(0.80)
        sh_spectrum_bgr2D_ProjY .GetYaxis().SetTitleSize(0.05)
        sh_spectrum_bgr2D_ProjY .GetYaxis().SetTitleOffset(0.80)

        sh_spectrum_bgr2D_ProjY .Draw("hbar")

        c_sh_spectrum_bgr2D_mupage_final = TCanvas(
            "c_sh_spectrum_bgr2D_mupage_final", "c_sh_spectrum_bgr2D_mupage_final", 1100, 650)
        c_sh_spectrum_bgr2D_mupage_final .SetLogz()
        c_sh_spectrum_bgr2D_mupage_final .SetRightMargin(0.15)

        sh_spectrum_bgr2D_mupage_final = infile .Get(
            "sh_spectr2d_e_muatmmc_zen_bef_cuts_final")
        sh_spectrum_bgr2D_mupage_final .GetXaxis().SetTitle(
            "log_{10}(E_{TANTRA} [GeV])")
        sh_spectrum_bgr2D_mupage_final .GetYaxis().SetTitle("Zenith [rad]")
        sh_spectrum_bgr2D_mupage_final .GetZaxis().SetTitle("N_{evts}/bin")
        sh_spectrum_bgr2D_mupage_final .GetXaxis().SetRangeUser(0, 7)
        # sh_spectrum_bgr2D_mupage_final .GetZaxis().SetRangeUser(1e-3, 1.2)

        sh_spectrum_bgr2D_mupage_final .GetXaxis().SetTitleSize(0.05)
        sh_spectrum_bgr2D_mupage_final .GetXaxis().SetTitleOffset(0.80)
        sh_spectrum_bgr2D_mupage_final .GetYaxis().SetTitleSize(0.05)
        sh_spectrum_bgr2D_mupage_final .GetYaxis().SetTitleOffset(0.80)

        sh_spectrum_bgr2D_mupage_final  .Draw("z")
        gPad.Update()
        palette = sh_spectrum_bgr2D_mupage_final  .GetListOfFunctions().FindObject("palette")
        palette.SetTitleOffset(0.8)
        palette.SetTitleSize(0.05)

        sh_spectrum_bgr2D_mupage_final .Draw("colz")

    if "sh" not in sys.argv:
        c_tr_spectrum_bgr2D = TCanvas(
            "c_tr_spectrum_bgr2D", "c_tr_spectrum_bgr2D", 1100, 650)
        c_tr_spectrum_bgr2D .SetLogz()
        c_tr_spectrum_bgr2D .SetRightMargin(0.15)

        tr_spectrum_bgr2D = infile .Get("tr_spectr2d_e_atmmc_zen_final")
        tr_spectrum_bgr2D .GetXaxis().SetTitle("log_{10}(E_{TANTRA} [GeV])")
        tr_spectrum_bgr2D .GetYaxis().SetTitle("Zenith [rad]")
        tr_spectrum_bgr2D .GetZaxis().SetTitle("N_{evts}/bin")
        # tr_spectrum_bgr2D .GetXaxis().SetRangeUser(0, 800)
        # tr_spectrum_bgr2D .GetZaxis().SetRangeUser(1e-15, 2e2)

        tr_spectrum_bgr2D .GetXaxis().SetTitleSize(0.05)
        tr_spectrum_bgr2D .GetXaxis().SetTitleOffset(0.80)
        tr_spectrum_bgr2D .GetYaxis().SetTitleSize(0.05)
        tr_spectrum_bgr2D .GetYaxis().SetTitleOffset(0.80)

        tr_spectrum_bgr2D  .Draw("z")
        gPad.Update()
        palette = tr_spectrum_bgr2D  .GetListOfFunctions().FindObject("palette")
        palette.SetTitleOffset(0.8)
        palette.SetTitleSize(0.05)

        tr_spectrum_bgr2D .Draw("colz")

        c_tr_spectrum_bgr2D_peaks = TCanvas(
            "c_tr_spectrum_bgr2D_peaks", "c_tr_spectrum_bgr2D_peaks", 1100, 650)
        c_tr_spectrum_bgr2D_peaks .SetLogz()
        c_tr_spectrum_bgr2D_peaks .SetRightMargin(0.15)

        tr_spectrum_bgr2D_peaks = infile .Get(
            "tr_spectr2d_e_atmmc_zen_initial")
        tr_spectrum_bgr2D_peaks .GetXaxis().SetTitle(
            "log_{10}(E_{TANTRA} [GeV])")
        tr_spectrum_bgr2D_peaks .GetYaxis().SetTitle("Zenith [rad]")
        tr_spectrum_bgr2D_peaks .GetZaxis().SetTitle("N_{evts}/bin")
        # tr_spectrum_bgr2D_peaks .GetXaxis().SetRangeUser(0, 7)
        # tr_spectrum_bgr2D_peaks .GetZaxis().SetRangeUser(1e-2, 19)

        tr_spectrum_bgr2D_peaks .GetXaxis().SetTitleSize(0.05)
        tr_spectrum_bgr2D_peaks .GetXaxis().SetTitleOffset(0.80)
        tr_spectrum_bgr2D_peaks .GetYaxis().SetTitleSize(0.05)
        tr_spectrum_bgr2D_peaks .GetYaxis().SetTitleOffset(0.80)

        tr_spectrum_bgr2D_peaks  .Draw("z")
        gPad.Update()
        palette = tr_spectrum_bgr2D_peaks  .GetListOfFunctions().FindObject("palette")
        palette.SetTitleOffset(0.8)
        palette.SetTitleSize(0.05)

        tr_spectrum_bgr2D_peaks .Draw("colz")

        c_tr_spectrum_bgr2D_mupage = TCanvas(
            "c_tr_spectrum_bgr2D_mupage", "c_tr_spectrum_bgr2D_mupage", 1100, 650)
        c_tr_spectrum_bgr2D_mupage .SetLogz()
        c_tr_spectrum_bgr2D_mupage .SetRightMargin(0.15)

        tr_spectrum_bgr2D_mupage = infile .Get(
            "tr_spectr2d_e_muatmmc_zen_bef_cuts")
        tr_spectrum_bgr2D_mupage .GetXaxis().SetTitle(
            "log_{10}(E_{TANTRA} [GeV])")
        tr_spectrum_bgr2D_mupage .GetYaxis().SetTitle("Zenith [rad]")
        tr_spectrum_bgr2D_mupage .GetZaxis().SetTitle("N_{evts}/bin")
        # tr_spectrum_bgr2D_mupage .GetXaxis().SetRangeUser(0, 7)
        # tr_spectrum_bgr2D_mupage .GetZaxis().SetRangeUser(1, 15)

        tr_spectrum_bgr2D_mupage .GetXaxis().SetTitleSize(0.05)
        tr_spectrum_bgr2D_mupage .GetXaxis().SetTitleOffset(0.80)
        tr_spectrum_bgr2D_mupage .GetYaxis().SetTitleSize(0.05)
        tr_spectrum_bgr2D_mupage .GetYaxis().SetTitleOffset(0.80)

        tr_spectrum_bgr2D_mupage  .Draw("z")
        gPad.Update()
        palette = tr_spectrum_bgr2D_mupage  .GetListOfFunctions().FindObject("palette")
        palette.SetTitleOffset(0.8)
        palette.SetTitleSize(0.05)

        tr_spectrum_bgr2D_mupage .Draw("colz")

        c_tr_spectrum_bgr2D_mupage_final = TCanvas(
            "c_tr_spectrum_bgr2D_mupage_final", "c_tr_spectrum_bgr2D_mupage_final", 1100, 650)
        c_tr_spectrum_bgr2D_mupage_final .SetLogz()
        c_tr_spectrum_bgr2D_mupage_final .SetRightMargin(0.15)

        tr_spectrum_bgr2D_mupage_final = infile .Get(
            "tr_spectr2d_e_muatmmc_zen_bef_cuts_final")
        tr_spectrum_bgr2D_mupage_final .GetXaxis().SetTitle(
            "log_{10}(E_{TANTRA} [GeV])")
        tr_spectrum_bgr2D_mupage_final .GetYaxis().SetTitle("Zenith [rad]")
        tr_spectrum_bgr2D_mupage_final .GetZaxis().SetTitle("N_{evts}/bin")
        # tr_spectrum_bgr2D_mupage_final .GetXaxis().SetRangeUser(0, 7)
        # tr_spectrum_bgr2D_mupage_final .GetZaxis().SetRangeUser(1e-3, 1.2)

        tr_spectrum_bgr2D_mupage_final .GetXaxis().SetTitleSize(0.05)
        tr_spectrum_bgr2D_mupage_final .GetXaxis().SetTitleOffset(0.80)
        tr_spectrum_bgr2D_mupage_final .GetYaxis().SetTitleSize(0.05)
        tr_spectrum_bgr2D_mupage_final .GetYaxis().SetTitleOffset(0.80)

        tr_spectrum_bgr2D_mupage_final  .Draw("z")
        gPad.Update()
        palette = tr_spectrum_bgr2D_mupage_final  .GetListOfFunctions().FindObject("palette")
        palette.SetTitleOffset(0.8)
        palette.SetTitleSize(0.05)

        tr_spectrum_bgr2D_mupage_final .Draw("colz")


if "coszen" in sys.argv:
    if "tr" not in sys.argv:

        c_sh_coszen_atmMC = TCanvas(
            "c_sh_coszen_atmMC", "c_sh_coszen_atmMC", 1100, 650)
        c_sh_coszen_atmMC .SetRightMargin(0.15)

        sh_bgr_vs_coszen = infile .Get("sh_bgr_vs_coszen")
        sh_bgr_vs_coszen .GetXaxis().SetTitle("cos(zen)")
        sh_bgr_vs_coszen .GetYaxis().SetTitle("dN_{evts}/dcos(zen)")
        sh_bgr_vs_coszen .GetYaxis().SetTitleOffset(1.2)
        sh_bgr_vs_coszen .GetYaxis().SetRangeUser(0, 1200)
        sh_bgr_vs_coszen .SetFillColor(5)
        sh_bgr_vs_coszen .SetLineWidth(2)

        sh_bgr_vs_coszen .GetXaxis().SetTitleSize(0.05)
        sh_bgr_vs_coszen .GetXaxis().SetTitleOffset(0.80)
        sh_bgr_vs_coszen .GetYaxis().SetTitleSize(0.05)
        sh_bgr_vs_coszen .GetYaxis().SetTitleOffset(0.80)

        sh_bgr_vs_coszen .Draw()

        sh_coszen_atmMC = infile .Get("sh_coszen_atmMC")
        sh_coszen_atmMC .SetLineColor(1)

        sh_coszen_atmMC .Draw("same")

        sh_bgr_vs_coszen_rebinned = infile .Get("sh_bgr_vs_coszen_rebinned")
        sh_bgr_vs_coszen_rebinned .SetLineColor(2)
        sh_bgr_vs_coszen_rebinned .SetLineWidth(2)

        sh_bgr_vs_coszen_rebinned .Draw("same")

        leg_sh_coszen_atmMC = TLegend(.75, .75, .85, .90)
        leg_sh_coszen_atmMC .SetFillColor(kWhite)
        leg_sh_coszen_atmMC .AddEntry(sh_bgr_vs_coszen, "Data", "f")
        leg_sh_coszen_atmMC .AddEntry(sh_bgr_vs_coszen_rebinned, "Spline", "l")
        leg_sh_coszen_atmMC .AddEntry(sh_coszen_atmMC, "MC", "l")

        leg_sh_coszen_atmMC .Draw()

    if "sh" not in sys.argv:
        c_tr_coszen_atmMC = TCanvas(
            "c_tr_coszen_atmMC", "c_tr_coszen_atmMC", 1100, 650)
        c_tr_coszen_atmMC .SetRightMargin(0.15)

        tr_bgr_vs_coszen = infile .Get("tr_bgr_vs_coszen")
        tr_bgr_vs_coszen .GetXaxis().SetTitle("cos(zen)")
        tr_bgr_vs_coszen .GetYaxis().SetTitle("dN_{evts}/dcos(zen)")
        tr_bgr_vs_coszen .GetYaxis().SetTitleOffset(1.4)
        tr_bgr_vs_coszen .GetYaxis().SetRangeUser(4000, 14000)
        tr_bgr_vs_coszen .SetFillColor(5)
        tr_bgr_vs_coszen .SetLineWidth(2)

        tr_bgr_vs_coszen .GetXaxis().SetTitleSize(0.05)
        tr_bgr_vs_coszen .GetXaxis().SetTitleOffset(0.80)
        tr_bgr_vs_coszen .GetYaxis().SetTitleSize(0.05)
        tr_bgr_vs_coszen .GetYaxis().SetTitleOffset(0.80)

        tr_bgr_vs_coszen .Draw()

        tr_coszen_atmMC = infile .Get("tr_coszen_atmMC")
        tr_coszen_atmMC .SetLineColor(1)

        tr_coszen_atmMC .Draw("same")

        tr_bgr_vs_coszen_rebinned = infile .Get("tr_bgr_vs_coszen_rebinned")
        tr_bgr_vs_coszen_rebinned .SetLineColor(2)
        tr_bgr_vs_coszen_rebinned .SetLineWidth(2)

        tr_bgr_vs_coszen_rebinned .Draw("same")

        leg_tr_coszen_atmMC = TLegend(.75, .75, .85, .90)
        leg_tr_coszen_atmMC .SetFillColor(kWhite)
        leg_tr_coszen_atmMC .AddEntry(tr_bgr_vs_coszen, "Data", "f")
        leg_tr_coszen_atmMC .AddEntry(tr_bgr_vs_coszen_rebinned, "Spline", "l")
        leg_tr_coszen_atmMC .AddEntry(tr_coszen_atmMC, "MC", "l")

        leg_tr_coszen_atmMC .Draw()


if "az" in sys.argv:
    if "tr" not in sys.argv:

        c_sh_az_atmMC = TCanvas("c_sh_az_atmMC", "c_sh_az_atmMC", 1100, 650)
        c_sh_az_atmMC .SetRightMargin(0.15)

        sh_bgr_vs_az = infile .Get("sh_bgr_vs_az")
        sh_bgr_vs_az .Scale(sh_bgr_vs_az .GetNbinsX() / (2 * pi))
        sh_bgr_vs_az .GetXaxis().SetTitle("azimuth [rad]")
        sh_bgr_vs_az .GetYaxis().SetTitle("dN_{evts}/d(azimuth)")
        sh_bgr_vs_az .GetYaxis().SetRangeUser(0, 380)
        sh_bgr_vs_az .SetLineColor(2)

        sh_bgr_vs_az .GetXaxis().SetTitleSize(0.05)
        sh_bgr_vs_az .GetXaxis().SetTitleOffset(0.80)
        sh_bgr_vs_az .GetYaxis().SetTitleSize(0.05)
        sh_bgr_vs_az .GetYaxis().SetTitleOffset(0.80)

        sh_bgr_vs_az .Draw()

        sh_az_atmMC = infile .Get("sh_az_atmMC")
        sh_az_atmMC .SetLineWidth(2)

        sh_az_atmMC .Draw("same")

        leg_sh_az_atmMC = TLegend(.75, .75, .85, .90)
        leg_sh_az_atmMC .SetFillColor(kWhite)
        leg_sh_az_atmMC .AddEntry(sh_bgr_vs_az, "Data", "l")
        leg_sh_az_atmMC .AddEntry(sh_az_atmMC, "MC", "l")

        leg_sh_az_atmMC .Draw()

    if "sh" not in sys.argv:
        c_tr_az_atmMC = TCanvas("c_tr_az_atmMC", "c_tr_az_atmMC", 1100, 650)
        c_tr_az_atmMC .SetRightMargin(0.15)

        tr_bgr_vs_az = infile .Get("tr_bgr_vs_az")
        tr_bgr_vs_az .Scale(tr_bgr_vs_az .GetNbinsX() / (2 * pi))
        tr_bgr_vs_az .GetXaxis().SetTitle("azimuth [rad]")
        tr_bgr_vs_az .GetYaxis().SetTitle("dN_{evts}/d(azimuth)")
        tr_bgr_vs_az .GetYaxis().SetTitleOffset(1.2)
        tr_bgr_vs_az .GetYaxis().SetRangeUser(600, 3000)
        tr_bgr_vs_az .SetLineColor(2)

        tr_bgr_vs_az .GetXaxis().SetTitleSize(0.05)
        tr_bgr_vs_az .GetXaxis().SetTitleOffset(0.80)
        tr_bgr_vs_az .GetYaxis().SetTitleSize(0.05)
        tr_bgr_vs_az .GetYaxis().SetTitleOffset(0.80)

        tr_bgr_vs_az .Draw()

        tr_az_atmMC = infile .Get("tr_az_atmMC")
        tr_az_atmMC .SetLineWidth(2)

        tr_az_atmMC .Draw("same")

        leg_tr_az_atmMC = TLegend(.75, .75, .85, .90)
        leg_tr_az_atmMC .SetFillColor(kWhite)
        leg_tr_az_atmMC .AddEntry(tr_bgr_vs_az, "Data", "l")
        leg_tr_az_atmMC .AddEntry(tr_az_atmMC, "MC", "l")

        leg_tr_az_atmMC .Draw()


raw_input("Press Enter to quit...")
