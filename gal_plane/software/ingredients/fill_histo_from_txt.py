#!/usr/bin/env python
"""
Plot what is in the text file in an histogram.
"""

import argparse
import numpy as np
import matplotlib.pyplot as plt
from math import log



def main(filename):
    """
    Plot what is in the text file in an histogram.
    """

    Data = np.loadtxt(filename)

    fig = plt.figure()
    # plt.hist(Data)

    # Data_shower = np.array([log(data[0]) for data in Data if data[1] == 1])
    # Data_track = np.array([log(data[0]) for data in Data if data[1] == 0])
    # print np.mean(Data_shower), np.mean(Data_track)
    #
    # plt.hist(Data_shower)
    # # plt.title("")
    # # plt.xlabel("")
    # # plt.ylabel("")
    #
    # fig1 = plt.figure()
    # plt.hist(Data_track)

    plt.show()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__,)

    parser.add_argument(
        "file",
        nargs=1,
        default='',
        type=str,
        help="Give the directory/file.txt to which we take the data")

    main(parser.parse_args().file[0])
