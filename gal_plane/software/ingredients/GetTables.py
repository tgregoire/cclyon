#! /usr/bin/env python


from ROOT import *
import sys


date = ""
if "date" in sys.argv:
    for i in range(len(sys.argv)):
        if sys.argv[i] == "date":
            date = "_" + sys.argv[i + 1]

infile = TFile("/sps/km3net/users/tgregoir/gal_plane/ingredient_files/Sum_ing_files" + date + ".root")

All = False
if "all" in sys.argv:
    All = True


if "evtSel" in sys.argv or All:

    for eventMorph in ['track', 'shower']:
        print "\nTable: event selection", eventMorph + "s\n"
        hist_channels = infile.Get("hist_channels_" + eventMorph[0:2])

        channel = [0] * (hist_channels .GetNbinsX() + 2)
        for binX in range(hist_channels .GetNbinsX() + 2):
            binContent = hist_channels .GetBinContent(binX)
            if binX == 12:
                binContent = hist_channels .GetBinContent(4) * .09 / 1.09 + hist_channels .GetBinContent(7) * 3.74 / 4.74 + hist_channels .GetBinContent(10) * 0.12 / 1.12
            elif binX == 14:
                binContent = hist_channels .GetBinContent(1) + hist_channels .GetBinContent(4) + hist_channels .GetBinContent(7) + hist_channels .GetBinContent(10)
            elif binX == 15:
                binContent = hist_channels .GetBinContent(2) + hist_channels .GetBinContent(5) + hist_channels .GetBinContent(8) + hist_channels .GetBinContent(11) + hist_channels .GetBinContent(13)
            elif binContent == 0:
                continue
            elif binX == 1:
                binContent /= 1
            elif binX == 4:
                binContent /= 1.09
            elif binX == 7:
                binContent /= 4.74
            elif binX == 10:
                binContent /= 1.12

            for order in range(10):
                if binContent > 10**(-order):
                    if order == 0:
                        if binX == 14:
                            channel[binX] = "{0:.2f}".format(binContent)
                            break
                        channel[binX] = "{0:.1f}".format(binContent)
                        break
                    if order == 1:
                        channel[binX] = "{0:.2f}".format(binContent)
                        break
                    else:
                        channel[binX] = "{0:.1f}".format(binContent * (10**order)) + " \cdot 10^{-" + str(order) + "}"
                        break

        print " & KRA_{\gamma} & \\text{atmospheric} \\\\"
        print "\midrule"
        print "\\nu_\mu~\\text{NC} &", channel[1], "&", channel[2], "\\\\"
        print "\\nu_\mu~\\text{CC} &", channel[4], "&", channel[5], "\\\\"
        print "\\nu_e~\\text{NC} &", channel[7], "&", channel[8], "\\\\"
        print "\\nu_e~\\text{CC} &", channel[10], "&", channel[11], "\\\\"
        print "\\nu_\\tau~\\text{CC + NC} &", channel[12], "&  \\\\"
        print "\\mu_{atm} &  &", channel[13], "\\\\"
        print "\\midrule"
        print "\\text{total} &", channel[14], "&", channel[15]


if "efficiency" in sys.argv or "eff" in sys.argv or All:

    for eventMorph in ['track', 'shower']:
        print "\nTable: efficiency", eventMorph + "s\n"

        dic_eff = {}
        for eventType in ['mupage', 'nuatm', 'KRA']:
            hist_cut = infile.Get("hist_cuts_" + eventMorph[0:2] + "_" + eventType)
            efficiencies = [0] * (hist_cut .GetNbinsX())

            for binX in range(hist_cut .GetNbinsX()):
                binTrigg = 1
                if eventType == 'KRA' and eventMorph == 'track' and binX > 5:
                    binTrigg = 6
                if eventType == 'KRA' and eventMorph == 'shower' and binX > 9:
                    binTrigg = 10
                this_eff = hist_cut .GetBinContent(binX) / hist_cut .GetBinContent(binTrigg)
                if this_eff == 0:
                    continue

                for order in range(10):
                    if this_eff == 1:
                        efficiencies[binX] = "1"
                    elif this_eff > 10**(-order):
                        if order == 0:
                            if binX == 14:
                                efficiencies[binX] = "{0:.2f}".format(this_eff)
                                break
                            efficiencies[binX] = "{0:.1f}".format(this_eff)
                            break
                        if order == 1:
                            efficiencies[binX] = "{0:.2f}".format(this_eff)
                            break
                        else:
                            efficiencies[binX] = "{0:.1f}".format(this_eff * (10**order)) + " \cdot 10^{-" + str(order) + "}"
                            break
            dic_eff[eventType] = efficiencies

        if eventMorph == 'track':
            print "\\text{Criterion} & \\text{Condition} & \epsilon^{atm}_{\mu} & \epsilon^{atm}_{\\nu \\to \\text{any}} & \epsilon^{KRA}_{\\nu \\to \\text{any}} &  \epsilon^{KRA}_{\\nu \\to \mu} \\\\ "
            print "\midrule"
            print "\\text{triggered} & \\text{T3 or 3N} &", dic_eff['mupage'][1], "&", dic_eff['nuatm'][1], "&", dic_eff['KRA'][1], "&", dic_eff['KRA'][6], "\\\\ "
            print "\\text{up-going} & \\text{cos(zenith)} < 0.1 &", dic_eff['mupage'][2], "&", dic_eff['nuatm'][2], "&", dic_eff['KRA'][2], "&", dic_eff['KRA'][7], "\\\\ "
            print "\\text{Quality} & \Lambda_{tr} > -5.15 &", dic_eff['mupage'][3], "&", dic_eff['nuatm'][3], "&", dic_eff['KRA'][3], "&", dic_eff['KRA'][8], "\\\\ "
            print "\\text{Error Estimate} & \\beta_{tr} < 1^{\circ}  & ", dic_eff['mupage'][4], "&", dic_eff['nuatm'][4], "&", dic_eff['KRA'][4], "&", dic_eff['KRA'][9]

        if eventMorph == 'shower':
            print "\\text{Criterion} & \\text{Condition} & \epsilon^{atm}_{\mu} & \epsilon^{atm}_{\\nu \\to \\text{any}} & \epsilon^{KRA}_{\\nu \\to \\text{any}} & \epsilon^{KRA}_{\\nu \\to \\text{sh}} \\\\ "
            print "\midrule"
            print "\\text{triggered} & \\text{T3 or 3N} &", dic_eff['mupage'][1], "&", dic_eff['nuatm'][1], "&", dic_eff['KRA'][1], "&", dic_eff['KRA'][10], "\\\\ "
            print "\\text{Track Veto} & \\text{Not selected as a track} &", dic_eff['mupage'][2], "&", dic_eff['nuatm'][2], "&", dic_eff['KRA'][2], "&", dic_eff['KRA'][11], "\\\\ "
            print "\\text{Containment} & \\rho_{sh} < 300 \\text{m}, |z|<250\\text{m}  &", dic_eff['mupage'][3], "&", dic_eff['nuatm'][3], "&", dic_eff['KRA'][3], "&", dic_eff['KRA'][12], "\\\\ "
            print "\\text{up-going} & \\text{cos(zenith)} < 0.1 &", dic_eff['mupage'][4], "&", dic_eff['nuatm'][4], "&", dic_eff['KRA'][4], "&", dic_eff['KRA'][13], "\\\\ "
            print "\\text{M-Estimator} & M_{est} < 1000 &", dic_eff['mupage'][5], "&", dic_eff['nuatm'][5], "&", dic_eff['KRA'][5], "&", dic_eff['KRA'][14], "\\\\ "
            print "\\text{Error Estimate} & \\beta_{sh} < 26  &", dic_eff['mupage'][6], "&", dic_eff['nuatm'][6], "&", dic_eff['KRA'][6], "&", dic_eff['KRA'][15], "\\\\ "
            print "\\text{RDF from Dusj} & \mathcal{L}_{dusj} > 0.3 &", dic_eff['mupage'][7], "&", dic_eff['nuatm'][7], "&", dic_eff['KRA'][7], "&", dic_eff['KRA'][16], "\\\\ "
            print "\\text{Muon Veto} & \mathcal{L}_{\mu} \\text{veto} > 40  &", dic_eff['mupage'][8], "&", dic_eff['nuatm'][8], "&", dic_eff['KRA'][8], "&", dic_eff['KRA'][17]


if "livetime" in sys.argv or All:
    print "\nTable: livetime\n"
    histo_run_numberData = infile.Get("histo_run_numberData")
    histo_run_numberMC = infile.Get("histo_run_numberMC")

    end_of_year = [0, 31050, 38233, 45538, 54252, 61906, 68690, 74352, 79221, 83077]

    print "NrunsMC", "\t", "livetimeMC", "\t", "NrunsData", "\t", "livetimeData"

    livetimeDataTot = 0
    NrunsDataTot = 0
    livetimeMCTot = 0
    NrunsMCTot = 0
    for i in range(0, 83077):
        if histo_run_numberData .GetBinContent(i) != 0:
            livetimeDataTot = livetimeDataTot + histo_run_numberData .GetBinContent(i)  # 1
            NrunsDataTot += 1
        if histo_run_numberMC .GetBinContent(i) != 0:
            livetimeMCTot = livetimeMCTot + histo_run_numberMC .GetBinContent(i)  # 1
            NrunsMCTot += 1

    for year_index in range(len(end_of_year) - 1):

        livetimeData = 0
        NrunsData = 0
        livetimeMC = 0
        NrunsMC = 0
        for i in range(end_of_year[year_index], end_of_year[year_index + 1]):
            if histo_run_numberData .GetBinContent(i) != 0:
                livetimeData = livetimeData + histo_run_numberData .GetBinContent(i)  # 1
                NrunsData += 1
            if histo_run_numberMC .GetBinContent(i) != 0:
                livetimeMC = livetimeMC + histo_run_numberMC .GetBinContent(i)  # 1
                NrunsMC += 1
        print 2007 + year_index, "&", NrunsMC, "&", "{0:.2f}".format(livetimeMC / 3600 / 24), "&", "{0:.2f}".format(livetimeMCTot / livetimeMC), "&", NrunsData, "&", "{0:.2f}".format(livetimeData / 3600 / 24), "&", "{0:.2f}".format(livetimeDataTot / livetimeData), "&", "{0:.3f}".format(livetimeData / livetimeMC), "\\\\"

    print "\\midrule"
    print "\\text{Total}", "&", NrunsMCTot, "&", "{0:.2f}".format(livetimeMCTot / 3600 / 24), "&", "100", "&", NrunsDataTot, "&", "{0:.2f}".format(livetimeDataTot / 3600 / 24), "&", "100", "&", "{0:.3f}".format(livetimeDataTot / livetimeMCTot)
