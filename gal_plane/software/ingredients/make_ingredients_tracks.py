#!/usr/bin/python -i
import os, sys, pickle,glob
import popen2
import os.path
from array import array
from math import *
from ROOT import *
#import root_util_py

gStyle.SetOptStat("")
gROOT.ForceStyle()

deg2rad = pi / 180

endings = ["png", "root", "tex", "pdf"]

def permute( inlist, axes ="xyz" ):
    return [ inlist[ axes.find("x")],
             inlist[ axes.find("y")],
             inlist[ axes.find("z")] ]


def rotate_3dhist( H, axes = "xyz" , name = "rothist"):

    V= [None]*3
    V[0] = [ H.GetXaxis().GetNbins(), H.GetXaxis().GetXmin(), H.GetXaxis().GetXmax()]
    V[1] = [ H.GetYaxis().GetNbins(), H.GetYaxis().GetXmin(), H.GetYaxis().GetXmax()]
    V[2] = [ H.GetZaxis().GetNbins(), H.GetZaxis().GetXmin(), H.GetZaxis().GetXmax()]

    print " src axis description = " , V

    W = permute( V , axes )

    print " dst axis description = " , W

    dst = TH3F( name, name,
                W[0][0], W[0][1], W[0][2],
                W[1][0], W[1][1], W[1][2],
                W[2][0], W[2][1], W[2][2] )

    for ix in range ( 1, H.GetXaxis().GetNbins() ) :
        for iy in range ( 1, H.GetYaxis().GetNbins() ) :
            for iz in range ( 1, H.GetZaxis().GetNbins() ) :
                bin = H.GetBin(ix,iy,iz)
                con = H.GetBinContent( bin )

                dst_bins = permute( [ ix,iy,iz ] , axes )
                dst_bin  = dst.GetBin( dst_bins[0], dst_bins[1], dst_bins[2] )
                dst.SetBinContent( dst_bin, con )

    return dst



cut   = "N52_10"
gamma = "2.5"

input_file = TFile("~/Desktop/PhD/antares/shower_hist/rerun/merge_all.root")
output_dir = "/home/ichanmich/Desktop/PhD/antares/ps_search/ingredient_files/"
output_dir = "/home/ichanmich/Desktop/PhD/antares/shower_hist/rerun/"

output_file = TFile(output_dir+"/ingredients_AaFit_Em"+gamma+".root","RECREATE")


LifeTime_data    = 1690.52488426
LifeTime_mupage  = 1584.57894676
LifeTime_nueCC   = (1594.37836806+ 1584.31895833+ 1594.5893287 + 1582.10821759)/4.
LifeTime_numuCC  = (1594.5893287 + 1594.77203704+ 1594.35225694+ 1593.9659375) /4.
LifeTime_NC      = (1592.51998843 +1594.37836806+1594.5893287+1594.21372685+1594.5893287+1594.08027778+1593.986875+1593.88028935) / 8.



deg2rad = pi / 180
#maxsindecl = sin((90-42.5) * deg2rad) + 0.01
maxsindecl = sin((90-35.5) * deg2rad) + 0.01
maxsindecl = .8

def integr( spline ):
    c =0
    dx = (1+maxsindecl)/10000.0
    for i in range (0, 10000):
        x = -1.0 + i*dx;
        c += spline.Eval(x)
    return c*dx

def get_scaled_spline ( spline , factor ):

    xx,yy = [],[]


    for i in range (spline.GetNp()):
        a,b = Double(), Double()
        spline.GetKnot(i,a,b)
        xx.append(a)
        yy.append(b* factor)
 #       print i,a,b , yy

    print "         xx = ",xx
    print "         yy = ",yy

    r = TSpline3( spline.GetTitle() , array( 'd' , xx ), array( 'd', yy ), len(xx ) ,"b1",0,0)
    return r


def set_spline_integral( spline, J ):
    for j in range (1,5) :
        I = integr( spline )
        print "I=",I
        f = J /I
        print "scale with ",f
        spline2 = get_scaled_spline( spline, f )
        del spline
        spline = spline2

    return spline




def get_spline ( hist , name , decl=1):

    xx,yy = [],[]

    for i in range( 1, hist.GetNbinsX() ):
        sindecl = hist.GetBinCenter(  i )
        if i==1 : sindecl = hist.GetBinLowEdge(  i )

        bgrate  = hist.GetBinContent( i )
        if decl and sindecl > maxsindecl : break
        xx.append( sindecl )
        yy.append( bgrate  )

    if decl:
        xx.append( maxsindecl )
        yy.append( 0 )

    print "X=",xx
    print "Y=",yy

    spline = TSpline3(name , array( 'd' , xx ), array( 'd', yy ), len(xx ) ,"b1",0,0)
    spline = set_spline_integral( spline, hist.Integral()+0.1 ) # the 0.1 is so that int(integral) will be correct .. changed from GetEntries to Integral.. due to the usage of mc here
    # print " spline integral =",ii

    return spline


#
#
#  ------------------------------------------ main --------------------------------------
#

TH1.AddDirectory(False);





c = TCanvas()
SetOwnership(c, False )
c.Divide(3,3)

c.cd(1) # -------------- acceptance for E-2 spectra vs sin(declination)-------------

logBeta_sinDeclination_NHits = input_file.Get( "numu_CC_Em"+gamma+"/Tracks_logBeta_sinDecl_NHits" )
logBeta_sinDeclination_NHits.Scale( LifeTime_data / LifeTime_numuCC )
logBeta_sinDeclination_NHits.Scale( 1 + .17 ) # Branching Ratio for tau -> mu


logBeta_sinDeclination_NHits.SetName("h3_logbeta_sindecl_nhits_"+cut)
logBeta_sinDeclination_NHits.SetTitle("3d histogram of logbeta, sindecl and nhits for E-2 shower neutrinos for cut "+cut)
logBeta_sinDeclination_NHits.Write()


h_trusindecl = logBeta_sinDeclination_NHits.ProjectionY("h_trusindecl")
h_trusindecl.Rebin(2)


h_trusindecl.Scale( 0.5 )           # 0.5 for average of nu/anti nu..
h_trusindecl.Scale( 1.0 / (2*pi) )  # diffuse to point source
h_trusindecl.Scale( 1.0 / h_trusindecl.GetBinWidth(1))


# At this point h_trusin decl contains the number of events detected
# in each declination bin for a uniform flux : dN/dE = 1.0e-8 x E^-2  ( GeV cm-2 s-1 sr-1)

h_trusindecl.Draw()


c3 = TCanvas()
h_trusindecl.SetXTitle("sin(#delta)")
h_trusindecl.SetYTitle("N_{sig}#times#Phi^{-1} / (10^{8} GeV^{-1} cm^{2} s)")
h_trusindecl.SetLineWidth(3)
h_trusindecl.Draw();

for ending in endings:
    c3.SaveAs(output_dir+"E"+gamma+"_"+cut+"_acceptance."+ending)



# write to output file
h_trusindecl.SetTitle("Acceptance vs sin(decl) for cut "+cut )
h_trusindecl.SetName("h_acceptance_"+cut)
h_trusindecl.Write()






c.cd(2) # -------------- background rate vs sin(declination)-------------

logBeta_sinDecl_NHits = input_file.Get("data_tr/Tracks_logBeta_sinDecl_NHits")
h_sindecl = logBeta_sinDecl_NHits.ProjectionY("h_sindecl")
h_sindecl.Rebin(5)
#h_sindecl.Rebin(10)

h_sindecl.SetXTitle("sin(#delta)")
h_sindecl.SetYTitle("dN/d sin(#delta)")
h_sindecl.SetMarkerSize(.8)
h_sindecl.SetLineWidth(2)
h_sindecl.SetFillColor(5)
for b in range (1, h_sindecl.GetNbinsX() +1 ) :
    h_sindecl.SetBinError( b , sqrt( h_sindecl.GetBinContent(b ) ))


h_sindecl1 = h_sindecl.Clone("h_sindecl1")
h_sindecl1.Scale ( 1.0 / h_sindecl1.GetBinWidth(1) );
spline1 = get_spline( h_sindecl1 , "spline1")

h_sindecl2 = h_sindecl.Clone("h_sindecl2")
h_sindecl2.Rebin(2)
h_sindecl2.Scale ( 1.0 / h_sindecl2.GetBinWidth(1) );
spline2 = get_spline( h_sindecl2 , "spline2")


spline1 = set_spline_integral( spline1, h_sindecl.Integral() )
spline2 = set_spline_integral( spline2, h_sindecl.Integral() )

spline1.SetLineWidth(2)
spline2.SetLineWidth(2)
spline1.SetLineColor(4)
spline2.SetLineColor(2)



h_sindecl.Scale ( 1.0 / h_sindecl.GetBinWidth(1) );
#h_sindecl.GetYaxis().SetRangeUser(0,200)
h_sindecl.Draw()

spline1.Draw("same")
spline2.Draw("same")

spline1.SetName("bgr_spline_default_"+cut)
spline2.SetName("bgr_spline_alternative_"+cut)
spline1.SetTitle("Background rate spline (default) for cut "+cut)
spline2.SetTitle("Background rate spline (alternative) for cut "+cut)
spline2.Write()
spline1.Write()

cbgr = TCanvas("cbgr_"+cut,'cbgr_'+cut,1)
h_sindecl.Draw()
spline1.Draw("same")
spline2.Draw("same")

for ending in endings:
    cbgr.SaveAs(output_dir+"E"+gamma+"_"+cut+'_backgroundSpline.'+ending)



c.cd(3) # -------------- background rate vs right ascension -------------

bkg_rightasc = input_file.Get("data_tr/Tracks_rightAscension")
bkg_rightasc.Rebin(2)

lineafit = TF1("lineafit", "[0]+x*[1]")
bkg_rightasc.Fit("lineafit")
print "chi2 of lineafit:", bkg_rightasc.GetFunction("lineafit").GetChisquare(), "/", bkg_rightasc.GetFunction("lineafit").GetNDF()
constfit = TF1("constfit", "[0]")
bkg_rightasc.Fit("constfit")
print "chi2 of constfit:", bkg_rightasc.GetFunction("constfit").GetChisquare(), "/", bkg_rightasc.GetFunction("constfit").GetNDF()

bkg_rightasc.Write("rightAscension_"+cut)

bkg_rightasc.SetMarkerSize(.8)
bkg_rightasc.Draw("e")
c3 = TCanvas("cbgr_RA", "cbgr_RA",1)
bkg_rightasc.SetMinimum(0)
bkg_rightasc.Draw("pe")


for ending in endings:
    c3.SaveAs(output_dir+"E"+gamma+"_"+cut+'_rightAscension.'+ending)


c.cd(4) # -------- nhits in data --------------

gPad.SetLogy()
h_nhits_dat = logBeta_sinDecl_NHits.ProjectionZ()
h_nhits_dat.Rebin(8)
h_nhits_dat.Draw()

h_nhits_dat.SetName("h_nhits_data_"+cut)
h_nhits_dat.SetTitle("nhits distribution in data for cut "+cut)
h_nhits_dat.Write()




c.cd(5) # -------- nhits in atm nu + mu mc ---------

gPad.SetLogy()

hmu_nhits = input_file.Get("mupage/Tracks_logBeta_sinDecl_NHits").ProjectionZ()
hmu_nhits.Scale( LifeTime_data / LifeTime_mupage )

hnu_nhits = input_file.Get("nue_CC_atm/Tracks_logBeta_sinDecl_NHits").ProjectionZ()
hnu_nhits.Scale( LifeTime_data / LifeTime_nueCC )
hnu_nhits.Add( input_file.Get("numu_CC_atm/Tracks_logBeta_sinDecl_NHits").ProjectionZ(), LifeTime_data / LifeTime_numuCC )
hnu_nhits.Add( input_file.Get("NC_atm/Tracks_logBeta_sinDecl_NHits").ProjectionZ(), LifeTime_data / LifeTime_NC )

hmu_nhits.Rebin(8)
hnu_nhits.Rebin(8)

hmu_nhits.SetLineColor(4)

hmu_nhits.Draw()
hnu_nhits.Draw("same")
hmu_nhits.GetYaxis().SetRangeUser(1e-1, 5e4)


hmc_nhits = hnu_nhits.Clone("hmc_nhits")
hmc_nhits.Add( hmu_nhits )
hmc_nhits.SetLineColor( 8 )
hmc_nhits.Draw("same")

hmc_nhits.SetName("h_nhits_atmmc_"+cut)
hmc_nhits.SetTitle("nhits distribution atm nu+mu mc for cut "+cut)
hmc_nhits.Write()


c.cd(6) # -------- PSF spline parameterization -------

hbeta = input_file.Get("numu_CC_Em"+gamma+"/Tracks_logXi")
hbeta.Scale( 1.0 / hbeta.Integral() )
hbeta.Rebin(4)


for x in range ( 1, hbeta.GetNbinsX()+1 ):
    con  = hbeta.GetBinContent( x )
    xx = 10 ** hbeta.GetBinCenter( x ) * deg2rad
    s  =  sin(xx) * log(10) * xx
    hbeta.SetBinContent( x, con /s  )

hbeta.Draw()

lowlogang = -1.3

bins = [ b for b in range (1,hbeta.GetNbinsX()+1) if hbeta.GetBinCenter( b ) < lowlogang ]
print "bins = " , bins
s = sum ([ hbeta.GetBinContent( b ) for b in bins[6:] ]) / len(bins[6:])
hbeta.Scale( 1.0 / s)
hbeta.SetFillColor(5)
hbeta.SetYTitle("dP / d #Omega")
hbeta2 = TH1D(hbeta)
hbeta2.GetXaxis().SetRangeUser(-2.5,2.5)
hbeta2.DrawCopy()
for b in bins :  hbeta.SetBinContent( b , 1 )

# prepare arrays for spline
vx,vy = [],[]
bbb = hbeta.FindBin( lowlogang )
for b in range (1, hbeta.GetNbinsX()+1, 2 ):
    if hbeta.GetBinCenter( b ) >  2.22 : continue
    vx.append( hbeta.GetBinCenter( b ))
    vy.append( hbeta.GetBinContent( b ))

vx.append( 2.22528 )
vy.append(0)
print vx , "\n", vy

tit = 'psf_spline_' + cut
spline = TSpline3(tit, array('d', vx), array('d', vy),
                    len(vx) , "e1b1", 0 , 0 )

spline.SetName(tit)
spline.SetLineColor(2)
spline.SetLineWidth(3)
spline.Draw("same")
SetOwnership( spline, False )

c_psf = TCanvas("c_psf_"+cut, "c_psf_"+cut)
hbeta2.Draw()
spline.Draw("same")

for ending in endings:
    c_psf.SaveAs(output_dir+"E"+gamma+"_"+cut+"_psf."+ending)


spline.SetName("psf_spline_"+cut)
spline.SetTitle("spline parametrization of dN/domega vs logbeta for cut "+cut)




c.cd(7)

h_ang = input_file.Get( "numu_CC_Em"+gamma+"/Tracks_logXi" )


h_ang.SetXTitle("log_{10}(#xi)")
h_ang.DrawCopy()

cang = TCanvas("cang_"+cut,"cang_"+cut)
cang.SetLogy()
h_ang.GetYaxis().SetRangeUser(1e-4, 5)
h_ang.DrawCopy()

#median = root_util_py.integrate( h_ang , normalize = True)
#median = GetQuintile( h_ang )

for ending in endings:
    cang.SaveAs(output_dir+"E"+gamma+"_"+cut+"_logbeta."+ending)

c2 = TCanvas("c2_"+cut,"c2_"+cut)
c2.SetGrid()
h_cum = h_ang.Clone()
h_cum.Scale(1./h_cum.Integral())
for a_bin in range(1, h_cum.GetNbinsX()+1):
    h_cum.SetBinContent(a_bin, h_cum.GetBinContent(a_bin-1)+h_cum.GetBinContent(a_bin))

h_cum.SetYTitle("cumulative distribution");
h_cum.SetXTitle("log_{10}(#xi/#circ)")
h_cum.SetLineWidth(2)
h_cum.GetXaxis().SetRangeUser(-2.3, 2.3)

h_cum.Draw("L")


for ending in endings:
    c2.SaveAs(output_dir+"E"+gamma+"_"+cut+"_angres_cum."+ending)
h_cum.SaveAs(output_dir+"E"+gamma+"_"+cut+"_angres_cum.root")


c.cd(8) # -------- nhits vs psf --------------

# on the x-axis should come beta, on y-axis should go sindecl
logBeta_sinDeclination_NHits.Project3D("zx").Draw("col")




c.cd(9)
spline.Draw()
spline.Write()

c.Update()
