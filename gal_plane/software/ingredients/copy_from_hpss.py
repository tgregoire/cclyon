#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Copy files from hpss to sps.
You have to get a file with all the name of the files you want to copy using something like `rfdir /hpss/in2p3.fr/group/antares/your/hpss/path/ | awk '{print $9}' > list_of_files_to_copy.txt`

Usage:
  copy_from_hpss.py [--help] <file_path> <hpss_path> <sps_path>

Options:
  -h --help     Show this help message and exit
  <file_path>   Path of the file containing the names of the hpss files
  <hpss_path>   The path of the hpss folder, starting by `/hpss/in2p3.fr/group/antares/`
  <sps_path>    The path where you want to put your files on sps.
"""

import os
from docopt import docopt # great package that change the documentation into parser

def main(file_path, hpss_path, sps_path):
    """
    Copy files of file_path from hpss_path to sps_path.
    """

    with open(file_path) as input_file:
        for current_file in input_file:
            if current_file == "." or current_file == "..":
                continue
            
            # Try 5 times as xrdcp can have difficulties
            for i in range(5):
                
                if hpss_path[-1] != "/":
                    hpss_path += "/"
                if sps_path[-1] != "/":
                    sps_path += "/"
                
                command = "xrdcp root://ccxroot.in2p3.fr:1999/" + hpss_path + current_file[:-1] + " " + sps_path
                print command
                check = os.system(command)
                if check == 0:
                    break
                if i == 4 and check != 0:
                    print "Error : xrdcp doesn't work !"

if __name__ == "__main__":
    arguments = docopt(__doc__)

    if arguments["<hpss_path>"][0:15] != '/hpss/in2p3.fr/':
        print 'hpss_path should start with /hpss/in2p3.fr/'
        exit(0)

    main(arguments["<file_path>"], arguments["<hpss_path>"] , arguments["<sps_path>"])
