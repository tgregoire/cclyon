#!/usr/bin/env python
"""
Produce declination distribution from the splines distributions of zenith and flat distribution of azimuth drawing a random time.
This is done to write the spline of dec corresponding to flat azimuth and the spline of flat azimuth itself into the
output file of sumHisto.py.
"""


import random
import math
from ROOT import *
from numpy import random, linspace, array


gSystem.Load("/sps/km3net/users/tgregoir/astro/lib/libastro.so")
antares = Astro.AntaresDetector()
local = Astro.LocalCoords()
local_reco = Astro.LocalCoords()
eq = Astro.EqCoords()


def date_to_mjd(year, month, day):
    """
    Convert a date to Julian Day.

    Algorithm from 'Practical Astronomy with your Calculator or Spreadsheet',
        4th ed., Duffet-Smith and Zwart, 2011.

    Parameters
    ----------
    year : int
        Year as integer. Years preceding 1 A.D. should be 0 or negative.
        The year before 1 A.D. is 0, 10 B.C. is year -9.

    month : int
        Month as integer, Jan = 1, Feb. = 2, etc.

    day : float
        Day, may contain fractional part.

    Returns
    -------
    jd : float
        Julian Day

    Examples
    --------
    Convert 6 a.m., February 17, 1985 to Julian Day

    >>> date_to_jd(1985,2,17.25)
    2446113.75

    """
    if month == 1 or month == 2:
        yearp = year - 1
        monthp = month + 12
    else:
        yearp = year
        monthp = month

    # this checks where we are in relation to October 15, 1582, the beginning
    # of the Gregorian calendar.
    if ((year < 1582) or
            (year == 1582 and month < 10) or
            (year == 1582 and month == 10 and day < 15)):
        # before start of Gregorian calendar
        B = 0
    else:
        # after start of Gregorian calendar
        A = math.trunc(yearp / 100.)
        B = 2 - A + math.trunc(A / 4.)

    if yearp < 0:
        C = math.trunc((365.25 * yearp) - 0.75)
    else:
        C = math.trunc(365.25 * yearp)

    D = math.trunc(30.6001 * (monthp + 1))

    jd = B + C + D + day + 1720994.5

    return jd - 2400000.5


def histo_to_spline(histo, n_points_spline):
    """Take a 1D histogram and return a Spline of it with a number of points close to n_points_spline"""
    n_bins = histo .GetNbinsX()
    for i in range(10):
        if n_bins % (n_points_spline + i) == 0:
            n_rebin = n_bins / (n_points_spline + i)
            break
        elif n_bins % (n_points_spline - i) == 0:
            n_rebin = n_bins / (n_points_spline - i)
            break
        # if i == 9 n_rebin not defined

    histo_rebinned = histo .Rebin(n_rebin, "histo_rebinned")
    histo_rebinned .Scale(1. / n_rebin)

    graph = TGraph(histo_rebinned .GetNbinsX() + 2)
    print 'making spline with', histo_rebinned .GetNbinsX(), '+ 2 points'

    # NOTE GetBinLowEdge has 0.02 too much so I remove it.
    graph .SetPoint(0, histo .GetBinLowEdge(0) - 0.02, (histo .GetBinContent(1) + histo .GetBinContent(2)) / 2.)
    for thisbin in range(1, histo_rebinned .GetNbinsX() + 1):
        graph .SetPoint(thisbin, histo_rebinned .GetBinCenter(thisbin), histo_rebinned .GetBinContent(thisbin))
    last_bin = histo .GetNbinsX() + 1
    graph .SetPoint(histo_rebinned .GetNbinsX() + 1, histo .GetBinLowEdge(last_bin) - 0.02 + histo .GetBinWidth(last_bin), (histo .GetBinContent(last_bin) + histo .GetBinContent(last_bin - 1)) / 2.)

    spline = TSpline3("histo_rebinned", graph)
    spline .SetName(histo .GetName() + "_spline" + str(n_points_spline) + "pts")

    return spline


infile = TFile("/sps/km3net/users/tgregoir/gal_plane/ingredient_files/copy_test.root", "update")
# infile = TFile("/sps/km3net/users/tgregoir/gal_plane/ingredient_files/temporary_file.root", "update")

PI = math.pi
EVT_TYPE = 'sh'

scaling = 208
if EVT_TYPE == 'tr':
    scaling = 7300

# histos
# pdf_bg_zen = infile .Get(EVT_TYPE+'_bgr_vs_coszen')
# pdf_bg_az = infile .Get(EVT_TYPE+'_bgr_vs_az')

# splines
pdf_bg_zen_spline = infile .Get(EVT_TYPE + '_bgr_vs_coszen_rebinned')
pdf_bg_az_spline = infile .Get(EVT_TYPE + '_bgr_vs_az_rebinned')

# output
pdf_bg_dec = TH1D(EVT_TYPE + "_bgr_vs_sindec_from_zen_az", "Shower Background vs sin(decl);sin(decl);# of events", 90, -1, 0.8)
pdf_bg_dec_ra = TH2D(EVT_TYPE + "_bgr_vs_sindec_ra_from_zen_az", "Shower Background vs sin(decl);ra;sin(decl);# of events", 90, -PI, PI, 90, -1, 0.8)

for coszen in linspace(-1, 0.1, 220):
    zen = math.acos(coszen)
    content_zen = pdf_bg_zen_spline .Eval(coszen)

    for az in linspace(0, 2 * PI, 720):
        # The colors in the following lines correspond to the shape in the plot colored_curves_demo.png
        # content_az = 40 if az < 2.5 else 22.5 # orange
        # content_az = 22.5 if az < PI or az > 2.5 + PI else 40 # green
        content_az = 29.463 # purple (kMagenta) (same normalization than green and orange)
        if EVT_TYPE == 'tr':
            content_az = pdf_bg_az_spline .Eval(az)  # spline


        # for i in range(50): # get more time per (zen, az) does not change significantly
        year = random.random_integers(2007, 2015)
        month = random.random_integers(1, 12)
        day = random.uniform(1, 28)  # I don't really care about the 29-31

        time = date_to_mjd(year, month, day)

        local.SetZenAzRad(zen, az)
        t = Astro.Time(time)
        eq = antares.LocalToEquatorial(local, t)
        dec = eq.GetDecRad()
        ra = eq.GetRaRad()
        if math.sin(dec) > 0.8 or math.sin(dec) < -1:
            print 'probleme, dec =', dec
        if ra > PI:
            ra -= 2. * PI

        pdf_bg_dec .Fill(math.sin(dec), content_zen * content_az)
        pdf_bg_dec_ra .Fill(ra, math.sin(dec), content_zen * content_az)

pdf_bg_dec .Scale(scaling * pdf_bg_dec .GetXaxis().GetNbins() / pdf_bg_dec .Integral() * (2/1.8)/2) # nbins is for 1.8 so...

pdf_bg_dec_spline2 = histo_to_spline(pdf_bg_dec, 30)
# pdf_bg_dec_spline2.SetLineColor(kGreen)
pdf_bg_dec_spline2.SetLineWidth(2)

pdf_bg_dec_spline = histo_to_spline(pdf_bg_dec, 15)
pdf_bg_dec_spline.SetName(EVT_TYPE+"_bgr_sindec_from_flat_az")

# pdf_bg_dec_spline2 .Draw()
# pdf_bg_dec_spline .Draw('same')

# Create a flat spline for the azimuth to write it in the file
azimuth_array = array([0., 2*math.pi])
Y_value = array([208/(2*math.pi), 208/(2*math.pi)])
sh_bgr_graph_az_flat = TGraph(2, azimuth_array, Y_value)
sh_bgr_spline_az_flat = TSpline3("sh_bgr_spline_az_flat", sh_bgr_graph_az_flat) # not needed for tracks
sh_bgr_spline_az_flat.SetName("sh_bgr_spline_az_flat")

pdf_bg_dec_spline.Draw()
raw_input("Press any key...")

# pdf_bg_dec_ra .Write()
# pdf_bg_dec .Write()
pdf_bg_dec_spline .Write()
# pdf_bg_dec_spline2 .Write()
if EVT_TYPE == 'sh':
    sh_bgr_spline_az_flat.Write()
infile .Close()

# Uncomment to plot the diff between az and az+pi
# sh_az = TH1D("sh_az", "Shower Background vs azimuth;azimuth (rad);N_{evts}", 300, 0, 2. * PI)
# for bin_az in range(1, pdf_bg_az .GetXaxis().GetNbins()+1):
#     az = pdf_bg_az .GetBinCenter(bin_az)
#     content_az = pdf_bg_az_spline .Eval(az)
#
#     az_shifted = az + PI
#     if az_shifted > 2*PI:
#         az_shifted -= 2. * PI
#     content_az_shifted = pdf_bg_az_spline .Eval(az_shifted)
#     sh_az.Fill(az, content_az - content_az_shifted)
#
# new TCanvas()
# sh_az.Draw()
