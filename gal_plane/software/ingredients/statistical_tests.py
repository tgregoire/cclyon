#!/usr/bin/env python
"""Make Chi2 and Kolmogorov tests.

Usage:
  statistical_tests.py [--help --plot --toy --shift]

Options:
  -h --help   Show this help message and exit
  -p --plot   Set to plot the histos
  -t --toy    Set to use the toy
  -s --shift  Set to test the effect of shifting the data for the data-flat comparison
"""

from docopt import docopt # great package that change the documentation into parser
import argparse
import random
import math
from math import pi
# pylint: disable=E0611, W0401
from numpy import linspace, array, ones
from ROOT import TFile, TSpline3, TH1F, TH1D, TGraph, TCanvas, kYellow, kRed, kGreen, kMagenta, kBlue, kOrange
# By Anne M. Archibald, 2007 and 2009
import numpy as np
from numpy import copy, sort, amax, arange, exp, sqrt, abs, floor, searchsorted
import scipy.misc as sc
import itertools
from scipy import stats

def kuiper_FPP(D,N):
    """Compute the false positive probability for the Kuiper statistic.
    Uses the set of four formulas described in Paltani 2004; they report 
    the resulting function never underestimates the false positive probability 
    but can be a bit high in the N=40..50 range. (They quote a factor 1.5 at 
    the 1e-7 level.
    Parameters
    ----------
    D : float
        The Kuiper test score.
    N : float
        The effective sample size.
    Returns
    -------
    fpp : float
        The probability of a score this large arising from the null hypothesis.
    Reference
    ---------
    Paltani, S., "Searching for periods in X-ray observations using 
    Kuiper's test. Application to the ROSAT PSPC archive", Astronomy and
    Astrophysics, v.240, p.789-790, 2004.
    """
    if D<0. or D>2.:
        raise ValueError("Must have 0<=D<=2 by definition of the Kuiper test")

    if D<2./N:
        return 1. - sc.factorial(N)*(D-1./N)**(N-1)
    elif D<3./N:
        k = -(N*D-1.)/2.
        r = sqrt(k**2 - (N*D-2.)/2.)
        a, b = -k+r, -k-r
        return 1. - sc.factorial(N-1)*(b**(N-1.)*(1.-a)-a**(N-1.)*(1.-b))/float(N)**(N-2)*(b-a)
    elif (D>0.5 and N%2==0) or (D>(N-1.)/(2.*N) and N%2==1):
        def T(t):
            y = D+t/float(N)
            return y**(t-3)*(y**3*N-y**2*t*(3.-2./N)/N-t*(t-1)*(t-2)/float(N)**2)
        s = 0.
        # NOTE: the upper limit of this sum is taken from Stephens 1965
        for t in xrange(int(floor(N*(1-D)))+1):
            term = T(t)*sc.comb(N,t)*(1-D-t/float(N))**(N-t-1)
            s += term
        return s
    else:
        z = D*sqrt(N) 
        S1 = 0.
        term_eps = 1e-12
        abs_eps = 1e-100
        for m in itertools.count(1):
            T1 = 2.*(4.*m**2*z**2-1.)*exp(-2.*m**2*z**2)
            so = S1
            S1 += T1
            if abs(S1-so)/(abs(S1)+abs(so))<term_eps or abs(S1-so)<abs_eps:
                break
        S2 = 0.
        for m in itertools.count(1):
            T2 = m**2*(4.*m**2*z**2-3.)*exp(-2*m**2*z**2)
            so = S2
            S2 += T2
            if abs(S2-so)/(abs(S2)+abs(so))<term_eps or abs(S1-so)<abs_eps:
                break
        return S1 - 8*D/(3.*sqrt(N))*S2

def kuiper(data, cdf=lambda x: x, args=()):
    """Compute the Kuiper statistic.
    
    Use the Kuiper statistic version of the Kolmogorov-Smirnov test to 
    find the probability that something like data was drawn from the 
    distribution whose CDF is given as cdf.
    
    Parameters
    ----------
    data : array-like
        The data values.
    cdf : callable
        A callable to evaluate the CDF of the distribution being tested
        against. Will be called with a vector of all values at once.
    args : list-like, optional
        Additional arguments to be supplied to cdf.
    Returns
    -------
    D : float
        The raw statistic.
    fpp : float
        The probability of a D this large arising with a sample drawn from
        the distribution whose CDF is cdf.
    Notes
    -----
    The Kuiper statistic resembles the Kolmogorov-Smirnov test in that 
    it is nonparametric and invariant under reparameterizations of the data. 
    The Kuiper statistic, in addition, is equally sensitive throughout 
    the domain, and it is also invariant under cyclic permutations (making 
    it particularly appropriate for analyzing circular data). 
    Returns (D, fpp), where D is the Kuiper D number and fpp is the 
    probability that a value as large as D would occur if data was 
    drawn from cdf.
    Warning: The fpp is calculated only approximately, and it can be 
    as much as 1.5 times the true value.
    Stephens 1970 claims this is more effective than the KS at detecting 
    changes in the variance of a distribution; the KS is (he claims) more 
    sensitive at detecting changes in the mean.
    If cdf was obtained from data by fitting, then fpp is not correct and 
    it will be necessary to do Monte Carlo simulations to interpret D. 
    D should normally be independent of the shape of CDF.
    """

    # FIXME: doesn't work for distributions that are actually discrete (for example Poisson).
    data = sort(data)
    cdfv = cdf(data,*args)
    N = len(data)
    D = amax(cdfv-arange(N)/float(N)) + amax((arange(N)+1)/float(N)-cdfv)
    print D
    
    return D, kuiper_FPP(D,N)

def kuiper_two(data1, data2):
    """Compute the Kuiper statistic to compare two samples.
    Parameters
    ----------
    data1 : array-like
        The first set of data values.
    data2 : array-like
        The second set of data values.
    
    Returns
    -------
    D : float
        The raw test statistic.
    fpp : float
        The probability of obtaining two samples this different from
        the same distribution.
    Notes
    -----
    Warning: the fpp is quite approximate, especially for small samples.
    """
    data1, data2 = sort(data1), sort(data2)

    if len(data2)<len(data1):
        data1, data2 = data2, data1

    cdfv1 = searchsorted(data2, data1)/float(len(data2)) # this could be more efficient
    cdfv2 = searchsorted(data1, data2)/float(len(data1)) # this could be more efficient
    D = (amax(cdfv1-arange(len(data1))/float(len(data1))) + 
            amax(cdfv2-arange(len(data2))/float(len(data2))))

    Ne = len(data1)*len(data2)/float(len(data1)+len(data2))
    return D, kuiper_FPP(D, Ne)

def fold_intervals(intervals):
    """Fold the weighted intervals to the interval (0,1).
    Convert a list of intervals (ai, bi, wi) to a list of non-overlapping
    intervals covering (0,1). Each output interval has a weight equal
    to the sum of the wis of all the intervals that include it. All intervals
    are interpreted modulo 1, and weights are accumulated counting 
    multiplicity.
    Parameters
    ----------
    intervals : list of three-element tuples (ai,bi,wi)
        The intervals to fold; ai and bi are the limits of the interval, and
        wi is the weight to apply to the interval.
    Returns
    -------
    breaks : array of floats length N
        The endpoints of a set of intervals covering [0,1]; breaks[0]=0 and
        breaks[-1] = 1
    weights : array of floats of length N-1
        The ith element is the sum of number of times the interval 
        breaks[i],breaks[i+1] is included in each interval times the weight
        associated with that interval.
    """
    r = []
    breaks = set([0,1])
    tot = 0
    for (a,b,wt) in intervals:
        tot += (np.ceil(b)-np.floor(a))*wt
        fa = a%1
        breaks.add(fa)
        r.append((0,fa,-wt))
        fb = b%1
        if fb!=0:
            # If fb==0, no need to trim - but this code would trim
            # [0,1] rather than [1,1]. So trap the special case.
            breaks.add(fb)
            r.append((fb,1,-wt))
        
    breaks = list(breaks)
    breaks.sort()
    breaks_map = dict([(f,i) for (i,f) in enumerate(breaks)])
    totals = np.zeros(len(breaks)-1)
    totals += tot
    for (a,b,wt) in r:
        if a!=b:
            totals[breaks_map[a]:breaks_map[b]]+=wt
    return np.array(breaks), totals

def cdf_from_intervals(breaks, totals):
    """Construct a callable piecewise-linear CDF from a pair of arrays.
    
    Take a pair of arrays in the format returned by fold_intervals and
    make a callable cumulative distribution function on the interval
    (0,1).
    Parameters
    ----------
    breaks : array of floats of length N
        The boundaries of successive intervals.
    weights : array of floats of length N-1
        The weight for each interval.
    Returns
    -------
    f : callable
        A cumulative distribution function corresponding to the 
        piecewise-constant probability distribution given by breaks, weights
    """
    if breaks[0]!=0 or breaks[-1]!=1:
        raise ValueError("Intervals must be restricted to [0,1]")
    if np.any(np.diff(breaks)<=0):
        raise ValueError("Breaks must be strictly increasing")
    if np.any(totals<0):
        raise ValueError("Total weights in each subinterval must be nonnegative")
    if np.all(totals==0):
        raise ValueError("At least one interval must have positive exposure")
    b = breaks.copy()
    c = np.concatenate(((0,), np.cumsum(totals*np.diff(b))))
    c /= c[-1]
    def cdf(x):
        ix = np.searchsorted(b[:-1],x)
        l, r = b[ix-1], b[ix] 
        return ((r-x)*c[ix-1]+(x-l)*c[ix])/(r-l)
    return cdf

def interval_overlap_length(i1,i2):
    """Compute the length of overlap of two intervals.
    
    Parameters
    ----------
    i1, i2 : pairs of two floats
        The two intervals.
    Returns
    -------
    l : float
        The length of the overlap between the two intervals.
    
    """
    (a,b) = i1
    (c,d) = i2
    if a<c:
        if b<c:
            return 0.
        elif b<d:
            return b-c
        else:
            return d-c
    elif a<d:
        if b<d:
            return b-a
        else:
            return d-a
    else:
        return 0

def histogram_intervals(n, breaks, totals):
    """Histogram of a piecewise-constant weight function.
    This function takes a piecewise-constant weight function and 
    computes the average weight in each histogram bin.
    Parameters
    ----------
    n : int
        The number of bins
    breaks : array of floats of length N
        Endpoints of the intervals in the PDF
    totals : array of floats of length N-1
        Probability densities in each bin
    
    Returns
    -------
    h : array of floats
        The average weight for each bin
    """
    h = np.zeros(n)
    start = breaks[0]
    for i in range(len(totals)):
        end = breaks[i+1]
        for j in range(n):
            ol = interval_overlap_length((float(j)/n,float(j+1)/n),(start,end))
            h[j] += ol/(1./n)*totals[i]
        start = end

        return h


def histo_to_array(histo):
    
    array = list()

    for evt in range(min(int(histo.GetEntries()), 100000)):
        array.append(histo.GetRandom())

    return np.array(array)

# def unweighted_histo_to_array(histo):
#     
#     array = list()
# 
#     for binX in range(1, histo.GetNbinsX()+1):
#         histo.GetBinContent
#         array.append(histo.GetRandom())
# 
#     return np.array(array)

def spline_to_histo(spline, nbins=100):
    name = spline.GetName()
    title = spline.GetTitle()
    x_min = spline.GetXmin()
    x_max = spline.GetXmax()
    histo = TH1D(name, title, nbins, x_min, x_max)

    for binX in range(1, histo.GetNbinsX()+2):
        Y_value = spline.Eval(histo.GetBinCenter(binX))
        histo.SetBinContent(binX, Y_value)

    return histo

def compute_chi2(histo1, histo2):
    """
    Compute the Chi2 between the two distributions.
    It takes as argument two TH1D.
    It returns the Chi2 and the number of points used.
    The number of points used will be determined by the number of bins in histo1.
    """

    chi2 = 0
    ndf = histo1.GetNbinsX()-1 # number of degrees of freedom

    for bin1 in range(1, histo1.GetNbinsX() + 1):
        Y1 = histo1.GetBinContent(bin1)
        bin2 = histo2.FindBin(histo1.GetBinCenter(bin1))
        Y2 = histo2.GetBinContent(bin2)
        if Y1 == 0 or Y2 == 0:
            ndf -= 1
            continue

        error1 = histo1.GetBinError(bin1)
        error2 = histo2.GetBinError(bin2)

        chi2 += (Y1-Y2)**2/(error1**2 + error2**2)

    return chi2, ndf

def print_results(histo0, histo1, chi2=False, kolmogorov=False, kuiper=False, plot=False, firstline=False):

    if chi2:
        if firstline:
            print "# Chi2 #"
            print "Comparison \t| p-value \t| chi2 \t| ndf \t| chi2/ndf"
            print "-----------\t|---------\t|------\t|-----\t|----------"
        histo1.Fit("pol0", "Q")
        scaling = histo1.GetFunction("pol0").GetParameter(0)

        histo0.Scale(scaling*histo0.GetNbinsX()/histo0.Integral())
        print "Change scaling line 386 to optimize chi2"

        chi2, deg_free = compute_chi2(histo0, histo1)
        print "\n*"+histo0.GetName()+"-"+histo1.GetName()+"*", "\t|", '{0:.3f}'.format(1 - stats.chi2.cdf(chi2, deg_free)), "\t|", '{0:.3f}'.format(chi2), "\t|", '{0:.3f}'.format(deg_free), "\t|", '{0:.3f}'.format(chi2/deg_free)
        if plot:
            raw_input()

def main(plot=False, toy=False, shift=False):
    """
    Get the curves and print the p-value of Chi2 and Kolmogorov tests.
    """

    # Get the splines and data
    infiles_folder = "/sps/km3net/users/tgregoir/gal_plane/ingredient_files/"

    data_file = TFile(infiles_folder+"Sum_ing_files_TheFinal_map3_test24juillet_bis.root")
    
    spline_azimuth = data_file.Get("sh_bgr_vs_az_rebinned")
    spline_azimuthH = spline_to_histo(spline_azimuth, 30)
    for binX in range(1, spline_azimuthH.GetNbinsX()+1):
        spline_azimuthH.SetBinError(binX, 0)
    spline_azimuthH.Scale(208./spline_azimuthH.Integral())

    histo_data = data_file.Get("sh_bgr_vs_az")
    histo_data.Scale(1.*4*pi/histo_data.GetNbinsX()/2.)

    nbins = histo_data.GetNbinsX()
    histo_data2 = TH1D("histo_data2", "Shower Background vs az;az;# of events", nbins, 0, 2*pi)
    
    # Redo data histo to have good weighting and create arrays for Kuiper
    array_data = list()
    array_flat = list()
    array_flat2 = list()

    shift_val = 0
    if shift:
        shift_val = nbins/2
    for binX in range(1, nbins+1):
        bin_content = histo_data.GetBinContent(binX)
        shifted_binX = binX + shift_val if binX  + shift_val <= nbins else binX - nbins + shift_val
        bin_center = histo_data.GetBinCenter(shifted_binX)
        for evt in range(int(round(bin_content))):
            histo_data2.Fill(bin_center, 1)
            array_data.append(bin_center)
    
    for evt in range(10000):
        array_flat.append(random.uniform(0, 2*pi))
        array_flat2.append(random.uniform(0, 2*pi))

    # Get MC histos
    MC_file = TFile(infiles_folder+"Sum_ing_files_MC_Kolmo_test.root")
    MCH = MC_file.Get("sh_az_atmMC")
    MC_nuH = MC_file.Get("sh_az_atmMC_nu")
    MC_muH = MC_file.Get("sh_az_atmMC_mu")

    # Get flat histos
    flatH = TH1D("flatH", "Shower Background vs az;az;# of events", nbins, 0, 2*pi)
    flatH.Sumw2()
    for evt in range(1000000):
        flatH.Fill(random.uniform(0, 2*pi)) #, 208./1000000.)
    
    flatH_chi2 = TH1D("flatH_chi2", "Shower Background vs az;az;# of events", nbins, 0, 2*pi) # for the chi2 by hands
    for binX in range(1, flatH_chi2.GetNbinsX()+1):
        flatH_chi2.SetBinContent(binX, 208./flatH_chi2.GetNbinsX())
        flatH_chi2.SetBinError(binX, 0)

    # Create a toy
    if toy:
        print "Toy:"
        toyH = TH1D("toyH", "Shower Background vs az;az;# of events", nbins, 0, 2*pi)
        toyH.Sumw2()
        for index in range(20):
            toyH.Reset()
            for evt in range(250000):
                azimuth = MC_nuH.GetRandom()
                # azimuth = random.uniform(0, 2*pi)
                toyH.Fill(azimuth)
            # print '{0:.3f}'.format(histo_data2.KolmogorovTest(toyH))
            flatH.Scale(toyH.Integral()/flatH.Integral())
            print "ROOT chi2:", '{0:.3f}'.format(toyH.Chi2Test(flatH, "UW"))
            deg_free = flatH.GetNbinsX()-1
            print "p-value by hand:", 1 - stats.chi2.cdf(compute_chi2(flatH, toyH), deg_free)

    # Plot
    if plot:
        histo_data2.SetFillColor(kYellow)
        histo_data2.Draw()
        flatH.SetLineWidth(3)
        flatH.SetLineColor(kRed)
        flatH.Draw("same")

        c2 = TCanvas("c2")
        MC_muH.SetLineWidth(3)
        MC_muH.SetLineColor(kOrange)
        MC_muH.Draw()
        MC_nuH.SetLineWidth(4)
        MC_nuH.SetLineColor(kGreen)
        MC_nuH.Draw("same")
        flatH.Scale(MCH.Integral()/flatH.Integral())
        flatH.SetLineWidth(1)
        flatH.SetLineColor(kRed)
        flatH.Draw("same")
        MCH.SetLineWidth(2)
        MCH.SetLineColor(kBlue)
        MCH.SetLineStyle(10)
        MCH.Draw("same")
        
        if toy:
            ctoy = TCanvas("ctoy")
            # flatH.SetLineWidth(1)
            # flatH.SetLineColor(kRed)
            # flatH.Draw("same")
            toyH.Draw("same")

    # Scale MC to data
    MC_muH.Scale(histo_data2.Integral()/MCH.Integral())
    MC_nuH.Scale(histo_data2.Integral()/MCH.Integral())
    MCH.Scale(histo_data2.Integral()/MCH.Integral())

    # Rebin all
    flatH.Rebin(10)
    flatH_chi2.Rebin(10)
    histo_data2.Rebin(10)
    MCH.Rebin(10)
    MC_nuH.Rebin(10)
    MC_muH.Rebin(10)
    
    # Chi2
    print_results(flatH_chi2, histo_data2, chi2=True, plot=plot, firstline=True)
    print_results(flatH_chi2, MCH, chi2=True, plot=plot)
    print_results(flatH_chi2, MC_nuH, chi2=True, plot=plot)
    print_results(flatH_chi2, MC_muH, chi2=True, plot=plot)
    
    print_results(MCH, histo_data2, chi2=True, plot=plot)
    print_results(MC_nuH, histo_data2, chi2=True, plot=plot)
    print_results(MC_muH, histo_data2, chi2=True, plot=plot)

    print_results(spline_azimuthH, histo_data2, chi2=True, plot=plot, firstline=True)
    # spline_azimuthH.Draw("same")
    # histo_data2.Draw("same")
    # raw_input()
    print_results(spline_azimuthH, MCH, chi2=True, plot=plot)
    print_results(spline_azimuthH, MC_nuH, chi2=True, plot=plot)
    print_results(spline_azimuthH, MC_muH, chi2=True, plot=plot)

    # print
    # print "**Kuiper**"
    # 
    # print "MC_nu-data", kuiper_two( histo_to_array(MC_nuH), np.array(array_data) )[1]
    # 
    # print "flat-data", kuiper_two( np.array(array_flat), np.array(array_data) )[1]
    # 
    # print "flat-MC_nu", kuiper_two( np.array(array_flat), histo_to_array(MC_nuH) )[1]
    # 
    # print "Buggy:"
    # print "flat-MC_mu **not valid**", kuiper_two( np.array(array_flat), histo_to_array(MC_muH) )[1]
    # print "Not valid as I generate an array from the histo without knowing the weight"
    # 
    # print "flat-MC_total **not valid**", kuiper_two( np.array(array_flat), histo_to_array(MCH) )[1]
    # print "Not valid as I generate an array from the histo without knowing the weight"
    # 
    # print "Tests:"
    # print "flat-flat_histo", kuiper_two( np.array(array_flat), histo_to_array(flatH) )[1]
    # 
    # print "flat-flat2", kuiper_two( np.array(array_flat), np.array(array_flat2))[1]
    # 
    # 
    # 
    # print
    # print "**Kolmogorov p-value**"
    # 
    # print "data", '{0:.3f}'.format(flatH.KolmogorovTest(histo_data2))
    # 
    # print "MC total", '{0:.3f}'.format(flatH.KolmogorovTest(MCH))
    # 
    # print "MC nu", '{0:.3f}'.format(flatH.KolmogorovTest(MC_nuH))
    # 
    # print "MC mu", '{0:.3f}'.format(flatH.KolmogorovTest(MC_muH))
    # 
    # print
    # print "data-MC total", '{0:.3f}'.format(histo_data2.KolmogorovTest(MCH))
    # print "data-MC_nu", '{0:.3f}'.format(histo_data2.KolmogorovTest(MC_nuH))
    # print "data-MC_nu", '{0:.3f}'.format(histo_data2.KolmogorovTest(MC_muH))
    # print "data-MC total", '{0:.3f}'.format(histo_data2.Chi2Test(MC_nuH, "WW"))


    # if plot:
    #     flatH.Scale(MC_nuH.Integral()/flatH.Integral()) # To have the good scaling on the plot
    #     raw_input()

if __name__ == "__main__":
    arguments = docopt(__doc__)

    main(arguments["--plot"], arguments["--toy"] , arguments["--shift"])
