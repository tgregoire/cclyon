#! /usr/bin/env python

"""insert docstring there"""

import os
import sys

TinoSkimmedFiles = False

official = True  # Put False here to be able to use something else
if "official" in sys.argv:
    official = True

opt = ""
if "no_old_plots" in sys.argv:
    opt += " no_old_plots"
if official:
    opt += " official"
print "opt is:", opt

n_jobs = range(100)
if "tau" in sys.argv:
    n_jobs = range(15)

for i in range(100):
    if ("Data" in sys.argv or "MC" in sys.argv) and str(i) in sys.argv:
        n_jobs = range(i, i + 1)

date = ""
if "date" in sys.argv:
    for i in range(len(sys.argv)):
        if sys.argv[i] == "date":
            date = sys.argv[i + 1]
    if "Data" not in sys.argv:
        os.system(
            "mkdir /sps/km3net/users/tgregoir/gal_plane/ingredient_files/MC_tim_" + date)
    if "MC" not in sys.argv:
        os.system(
            "mkdir /sps/km3net/users/tgregoir/gal_plane/ingredient_files/Data_tim_" + date)

if "Data" not in sys.argv:
    # directories = glob.glob("/sps/km3net/users/tgregoir/files/gal_plane/temporaryMC*/")
    # directories += glob.glob("/sps/km3net/users/tgregoir/files/gal_plane/temporaryMC*/*/")
    # for directory in directories:
    #     os.system("rm -f "+directory+"/*")
    #
    # os.system("rmdir /sps/km3net/users/tgregoir/files/gal_plane/temporaryMC*/*/")
    # os.system("rmdir /sps/km3net/users/tgregoir/files/gal_plane/temporaryMC*/")
    #
    # os.system("rmdir /sps/km3net/users/tgregoir/files/gal_plane/temporarytauNC*/*/")
    # os.system("rmdir /sps/km3net/users/tgregoir/files/gal_plane/temporarytauNC*/")
    #
    # os.system("rm -f /sps/km3net/users/tgregoir/gal_plane/ingredient_files/MC_tim/*.root")
    # os.system("rm -f /sps/km3net/users/tgregoir/gal_plane/software/ingredients/test_sindec.txt")

    # for n in range(100): #100
    #     os.system("qsub -P P_antares -q long -V -l hpss=1,sps=1,xrootd=1,ct=30:00:00,vmem=4G,fsize=20G -e /sps/km3net/users/tgregoir/gal_plane/software/ingredients/log_files/err/ -o /sps/km3net/users/tgregoir/gal_plane/software/ingredients/log_files/out/select_eventsMC"+str(n)+".log -j y -N MC"+str(n)+date+" /sps/km3net/users/tgregoir/gal_plane/software/ingredients/select_eventsMC.py "+str(n))

    # for n in range(80):
    #
    #     # shower_reco
    #     os.system("qsub -P P_antares -q long -V -l sps=1,ct=30:00:00,vmem=4G,fsize=20G -e /sps/km3net/users/tgregoir/gal_plane/software/ingredients/log_files/err/select_eventsMC"+str(n)+"shower_reco.leg -o /sps/km3net/users/tgregoir/gal_plane/software/ingredients/log_files/out/select_eventsMC"+str(n)+"shower_reco.log -j y -N MC"+str(n)+date+"shower_reco /sps/km3net/users/tgregoir/gal_plane/software/ingredients/select_eventsMC_bckp.py "+str(n)+" shower_reco")
    #
    #     #rQE
    #     os.system("qsub -P P_antares -q long -V -l sps=1,ct=30:00:00,vmem=4G,fsize=20G -e /sps/km3net/users/tgregoir/gal_plane/software/ingredients/log_files/err/select_eventsMC"+str(n)+"rQE.leg -o /sps/km3net/users/tgregoir/gal_plane/software/ingredients/log_files/out/select_eventsMC"+str(n)+"rQE.log -j y -N MC"+str(n)+date+"rQE /sps/km3net/users/tgregoir/gal_plane/software/ingredients/select_eventsMC_bckp.py "+str(n)+" rQE")

    if "sps" in sys.argv:
        for n in range(80):
            # shower_reco
            os.system("qsub -P P_antares -q long -V -l sps=1,ct=30:00:00,vmem=4G,fsize=20G -e /sps/km3net/users/tgregoir/gal_plane/software/ingredients/log_files/err/select_eventsMC" + str(n) + "shower_reco.log -o /sps/km3net/users/tgregoir/gal_plane/software/ingredients/log_files/out/select_eventsMC" +
                      str(n) + "shower_reco.log -j y -N MC" + str(n) + date + "shower_reco /sps/km3net/users/tgregoir/gal_plane/software/ingredients/select_eventsMC.py " + str(n) + opt + " shower_reco sps")

            # rQE
            os.system("qsub -P P_antares -q long -V -l sps=1,ct=30:00:00,vmem=4G,fsize=20G -e /sps/km3net/users/tgregoir/gal_plane/software/ingredients/log_files/err/select_eventsMC" + str(n) +
                      "rQE.log -o /sps/km3net/users/tgregoir/gal_plane/software/ingredients/log_files/out/select_eventsMC" + str(n) + "rQE.log -j y -N MC" + str(n) + date + "rQE /sps/km3net/users/tgregoir/gal_plane/software/ingredients/select_eventsMC.py " + str(n) + opt + " rQE sps")

    else:
        for n in n_jobs:  # 100
            if "tau" in sys.argv:
                os.system("qsub -P P_antares -q long -V -l hpss=1,sps=1,xrootd=1,ct=30:00:00,vmem=4G,fsize=20G -e /sps/km3net/users/tgregoir/gal_plane/software/ingredients/log_files/err/select_eventsMC" + str(n) +
                          ".log -o /sps/km3net/users/tgregoir/gal_plane/ingredient_files/MC_tim_" + date + "/select_eventsMC" + str(n) + ".log -j y -N MC" + str(n) + date + " /sps/km3net/users/tgregoir/gal_plane/software/ingredients/select_eventsMC.py tau " + str(n) + opt + " date " + date)
            else:
                os.system("qsub -P P_antares -q long -V -l hpss=1,sps=1,xrootd=1,ct=30:00:00,vmem=4G,fsize=20G -e /sps/km3net/users/tgregoir/gal_plane/software/ingredients/log_files/err/select_eventsMC" + str(n) +
                          ".log -o /sps/km3net/users/tgregoir/gal_plane/ingredient_files/MC_tim_" + date + "/select_eventsMC" + str(n) + ".log -j y -N MC" + str(n) + date + " /sps/km3net/users/tgregoir/gal_plane/software/ingredients/select_eventsMC.py " + str(n) + opt + " date " + date)


if "MC" not in sys.argv and "tau" not in sys.argv:
    # directories = glob.glob("/sps/km3net/users/tgregoir/files/gal_plane/temporaryData0*/")
    # for directory in directories:
    #     os.system("rm -f "+directory+"/*")
    #
    # os.system("rmdir /sps/km3net/users/tgregoir/files/gal_plane/temporaryData0*/")
    #
    # os.system("rm -f /sps/km3net/users/tgregoir/gal_plane/ingredient_files/Data_tim/*.root")
    if official and len(n_jobs) > 2:
        n_jobs = range(50)

    if TinoSkimmedFiles:
        print "./select_eventsData.py date " + date
        os.system("./select_eventsData.py date " + date)

    else:
        for n in n_jobs:  # 100
            # Tino's Data
            os.system("qsub -P P_antares -q long -V -l hpss=1,sps=1,xrootd=1,ct=30:00:00,vmem=4G,fsize=20G -e /sps/km3net/users/tgregoir/gal_plane/software/ingredients/log_files/err/select_eventsData" + str(n) +
                      ".log -o /sps/km3net/users/tgregoir/gal_plane/ingredient_files/Data_tim_" + date + "/select_eventsData" + str(n) + ".log -j y -N Data" + str(n) + date + " /sps/km3net/users/tgregoir/gal_plane/software/ingredients/select_eventsData.py " + str(n) + opt + " date " + date)
