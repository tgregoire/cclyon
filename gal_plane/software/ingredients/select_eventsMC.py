#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
from ROOT import *
from math import *
import datetime
import glob
import ctypes
import array
import sys
# from rereweightQEweightQE import ReweighterBetaTantra
from antares_db_utils import *
sys.path.insert(0, '/sps/km3net/users/tgregoir/gal_plane/software/ingredients')
from reweightQE import Reweighter
from reweightQE import ReweighterMestTantra

# NOTE: not used (but used in gwhen branch)
def angular_separation(lambda1, phi1, lambda2, phi2):
    """
    Get the angular distance between two points on a sphere.
    Is faster than using astropy (in a loop).
    """
    lambda1 += 2 * pi * (lambda1 < 0)
    lambda2 += 2 * pi * (lambda2 < 0)
    if phi1 > pi/2.:
        print "Warning phi =", phi1, phi2
        exit(0)

    sep = acos(sin(phi1)*sin(phi2) + cos(phi1)*cos(phi2)*cos(lambda1-lambda2))
    if sep > pi:
        sep = 2. * pi - sep
    return sep

#NOTE: not used (but used in gwhen branch)
def get_2D_cumulative(histo_2D, overflow_bins=False, anti_cumulative=False):
    """
    Make a cumulative along the X axis.
    """
    overflow = 0
    if overflow_bins:
        overflow = 1

    range_binX = range(1 - overflow, histo_2D.GetNbinsX()+1 + overflow)
    if anti_cumulative:
        range_binX = list(reversed(range_binX))

    for binY in range(1, histo_2D.GetNbinsY()+1):
        new_bin_content = 0
        for binX in range_binX:
            new_bin_content += histo_2D.GetBinContent(binX, binY)
            histo_2D.SetBinContent(binX, binY, new_bin_content)

    histo_2D.SetName(histo_2D.GetName()+"_"+"anti"*anti_cumulative+"cumulative")
    return histo_2D

start = datetime.datetime.now()

w_correctionTr = Reweighter()
w_correctionShMest = ReweighterMestTantra()
# w_correctionShBeta = ReweighterBetaTantra()

# Tino's scripts :
# /sps/km3net/users/tinom/searec/tino_project/private/standalone (plots_antDST.cxx and include.hh) ou make_ingredients_shower.py dans la presente directory

# load astro library for astronomical conversions
# galata/opt/astro/lib/libastro.so")
gSystem.Load("/sps/km3net/users/tgregoir/astro/lib/libastro.so")
antares = Astro.AntaresDetector()
local = Astro.LocalCoords()
local_reco = Astro.LocalCoords()
eq = Astro.EqCoords()

# Load AntDst library
gSystem.Load(os.path.expandvars("$ANTDSTROOT/lib/libAntDST.so"))
# Load selection library which content GetNontime, GetChargeRatio,
# GetCoincidentHits and GetLikelihood
gSystem.Load("/afs/in2p3.fr/home/t/tgregoir/sps/gal_plane/software/ingredients/selection.so")

# Strategies.h, these are not loaded with the library
eBBFit            = 1
eAart             = 2
eScanFit          = 3
eAAFit            = 4
eBBFitMest        = 5
eSeaPDF           = 6
eBBFitBrightPoint = 7
eOSFFit                     = 8
eKrakeFit                   = 9
eGridFit                    = 10
eShowerQFit                 = 11
eShowerDusjFit              = 12
eShowerTantraPositionPreFit = 13
eShowerTantraFit  = 14
eShowerAaFit      = 15
eNew              = 16

# TriggerTypes.h
e3D_SCAN = 6
eT3      = 14
eTQ      = 16

# ParticleType.h
eElectron             = 11
eAntiElectron         = ctypes.c_ulong(-11).value
eElectronNeutrino     = 12
eAntiElectronNeutrino = ctypes.c_ulong(-12).value
eMuon                 = 13
eAntiMuon             = ctypes.c_ulong(-13).value
eMuonNeutrino         = 14
eAntiMuonNeutrino     = ctypes.c_ulong(-14).value
eTau                  = 15
eAntiTau              = ctypes.c_ulong(-15).value
eTauNeutrino          = 16
eAntiTauNeutrino      = ctypes.c_ulong(-16).value
eBrightPoint          = 999

# Hit Type
eTriggered = 1

# EnergyRecoStrategy.h
eUnknownEnergyStrategy = 0
edEdX_CEA              = 1
eANN_ECAP              = 2
eML_NIKHEF             = 3
eR_Bologna             = 4
eTantraShowerEnergy    = 5
eAaShowerEnergy        = 6
eDusjShowerEnergy      = 7
eNewEnergyReco         = 8
eMonteCarlo            = 99

##########  Put the sparking run IDs in SparkingRuns  ##########
SparkingRuns_file = open("/sps/km3net/users/tgregoir/files/SparkingRuns.log", "r")
SparkingRuns = set()
lines = SparkingRuns_file.readlines()
for i in lines:
    thisline = i.split("\n")
    SparkingRuns.add(thisline[0])

DataRuns_file = open("/sps/km3net/users/tgregoir/files/gal_plane/DataRuns_official.list", "r")
DataRuns = set()
lines = DataRuns_file.readlines()
for i in lines:
    thisline = i.split("\n")
    DataRuns.add(str(int(thisline[0])))

JavTauRuns = [26030, 26110, 26270, 26470, 26640, 26710, 26770, 26800, 26820, 26830, 26870, 27090, 27100, 27270, 27290, 27470, 27550, 27750, 27830, 27990, 28280, 28450, 28620, 28630, 28770, 29050, 29180, 29220, 29270, 29280, 29320, 29350, 29370, 29440, 29500, 29580, 29760, 29830, 29910, 29920, 29980, 30060, 30080, 30170, 30210, 30310, 30320, 30380, 30410, 30450, 30950, 30970, 31080, 31130, 31160, 31170, 31230, 31680, 31920, 31970, 32110, 32150, 32160, 32340, 32430, 32450, 32570, 32580, 32600, 32700, 32830, 32990, 33110, 33230, 33300, 33320, 33530, 33590, 33660, 33700, 34360, 34490, 34560, 34580, 34610, 34630, 34640, 34650, 34790, 34890, 34960, 35000, 35170, 35180, 35190, 35360, 35470, 35480, 35520, 35530, 35630, 35700, 35750, 35790, 35880, 35900, 35910, 36110, 36220, 36260, 36450, 36570, 36590, 36740, 36860, 36870, 36900, 36970, 37090, 37150, 37220, 37300, 37320, 37350, 37420, 37430, 37440, 37470, 37620, 37770, 38080, 38170, 38230, 38400, 38560, 38590, 38600, 38610, 38620, 39580, 39640, 39700, 40150, 40710, 40790, 40870, 40880, 40910, 40930, 41310, 41350, 41370, 41390, 41400, 41410, 41540, 41550, 41580, 41740, 41810, 41850, 41940, 42020, 42190, 42320, 42500, 42810, 43050, 43390, 43480, 43610, 43800, 45250, 45330, 45400, 45430, 45440, 45650, 45700, 45860, 45920, 45960, 46030, 46120, 46150, 46170, 46230, 46310, 46360, 46460, 46510, 46650, 46700, 46750, 46770, 46820, 46890, 46970, 47070, 47580, 47660, 47670, 47730, 47840, 47900, 48070, 48280, 48370, 48490, 48520, 48950, 48970, 49310, 49350, 49360, 49420, 49500, 49690, 49790, 49980, 50080, 50230, 50330, 50340, 50390, 50440, 50610, 50700, 50740, 50800, 50850, 50960, 51130, 51260, 51290, 51370, 51390, 51480, 51600, 51710, 51720, 51740, 51780, 51810, 51870, 52240, 52250, 52340, 52350, 52430, 52460, 52550, 52580, 52590, 52640, 52790, 52930, 52950, 53080, 53110, 53220, 53230, 53710, 53770, 53900, 53940, 54060, 54150, 54260, 54330, 54370, 54610, 54680, 54740, 54820, 54850, 55050, 55110, 55720, 55760, 55830, 55920, 56600, 56610, 56670, 56680, 56690, 56730, 57060, 57110, 57240, 57280, 57350, 57360, 57370, 57440, 57690, 57710, 57820, 58090, 58110, 58140, 58150, 58200, 58210, 58220, 58260, 58300, 58420, 58430, 58490, 58550, 58560, 58620, 58640, 58650, 58770, 58790, 58810, 58830, 58900, 58940, 58950, 58960, 59090, 59150, 59180, 59260, 59320, 59330, 59370, 59380, 59390, 59430, 59460, 59580, 59770, 59930, 59960, 59990, 60050, 60150, 60240, 60260, 60390, 60410, 60420, 60460, 60570, 60590, 60610, 60740, 60800, 60870, 60940, 61020, 61030, 61040, 61070, 61100, 61180, 61290, 61330, 61390, 61450]

##########  Define all the histograms  ##########
model = "2"  # "Fermi"

if model == "Fermi":
    file_model = "Fermi_model"
    file2_model = "Fermi_"
else:
    file_model = "KRA_morphologies"
    file2_model = ""

morphoFile = TFile("/sps/km3net/users/tgregoir/gal_plane/ingredient_files/" + file_model + "_32768_bins_final.root")
morphoFile2 = TFile("/sps/km3net/users/tgregoir/gal_plane/ingredient_files/Map_CorrectUnits_" + file2_model + "final.root")
# files = glob.glob("/sps/km3net/users/tgregoir/files/recofiles/oneRun/reco_*mupage.root")

pdf_show = TFile.Open("/sps/km3net/users/tgregoir/gal_plane/software/ingredients/pdf_murej/outfile2_sig.root")
pdf_mupa = TFile.Open("/sps/km3net/users/tgregoir/gal_plane/software/ingredients/pdf_murej/outfile2_mu.root")
pdf3show = pdf_show.Get("h43dsig")
pdf3mup = pdf_mupa.Get("h43dmu")
pdf3show .Scale(1. / pdf3show.Integral())
pdf3mup  .Scale(1. / pdf3mup.Integral())

pi = TMath.Pi()
nside_ = 128 * 2
decl_arr = array.array('f')
ra_arr = array.array('f')
zen_arr = array.array('f')
az_arr = array.array('f')
nhits_arr = array.array('f')
ener_arr = array.array('f')
true_ener_arr = array.array('f')
nhits_arr2 = array.array('f')
delta_ra = 2. * pi / (nside_)
decl_arr_aitoff = array.array('f')
ra_arr_aitoff   = array.array('f')
delta_decl_aitoff = 180. / (nside_)
delta_ra_aitoff = 360. / (nside_)

for i in range(nside_ + 1):
    if i % 2 == 0:
        decl_arr .append(asin(-1 + i * 1. / (nside_ / 2)))
    if i % 4 == 0:
        zen_arr .append(acos(1 - i * 1. / (nside_ / 2)))
    ra_arr .append(-pi + i * delta_ra)
    az_arr .append(i * delta_ra)

for i in range(nside_ + 1):
    if i % 2 == 0:
        decl_arr_aitoff .append(-90 + i * delta_decl_aitoff)
    ra_arr_aitoff .append(-180 + i * delta_ra_aitoff)

for i in range(0, 201):
    nhits_arr .append(4 * i)
for i in range(0, 101):
    nhits_arr2 .append(8 * i)

for i in range(0, 71):
    ener_arr .append(7. * i / 70.)

for i in range(0, 91):
    true_ener_arr .append(9. * i / 90.)

model_spectrum_3D = morphoFile .Get("map" + model)
# KRA_spectrum = morphoFile .Get("map3").Project3D("z")
# Initial source morphology (before being * by acceptance, etc)
morpho = morphoFile2 .Get("map" + model + "_2D")
# for i in range(0, 4): # Put 0, 1, 2, 3 in argument to use, respectively, KRA_cut_5e6, KRA_cut_5e7, KRAgamma_cut_5e6, KRAgamma_cut_5e7
#     if str(i) in sys.argv:
#         model_spectrum_3D = morphoFile .Get("map"+str(i))
#         # KRA_spectrum = morphoFile .Get("map"+str(i)).Project3D("z")
#         morpho = morphoFile2 .Get("map"+str(i)+"_2D")
# KRA_spectrum .Scale(1./(128*128*2))
# scaled to have the mean of the bins == 1
morpho .Scale(128 * 128 * 2 / (morpho .Integral()))

if "no_old_plots" not in sys.argv:
    ##### Phy_0 #####
    sh_spectr3d = TH3F("sh_spectrum3d", "E-2 spectrum of the number of hits vs ra vs decl for showers;ra;decl;N_{hits};Number of events", len(ra_arr) - 1, ra_arr, len(decl_arr) - 1, decl_arr, len(nhits_arr) - 1, nhits_arr)  # 50, 0, 2000)
    sh_spectr = TH1D("sh_spectrum", "E-2 spectrum of the number of hits for showers;N_{hits}", 200, 0, 800)
    tr_spectr3d = TH3F("tr_spectrum3d", "E-2 spectrum of the number of hits vs ra vs decl for tracks;ra;decl;N_{hits};Number of events", len(ra_arr) - 1, ra_arr, len(decl_arr) - 1, decl_arr, len(nhits_arr) - 1, nhits_arr)  # 50, 0, 2000)
    tr_spectr = TH1D("tr_spectrum", "E-2 spectrum of the number of hits for tracks;N_{hits}", 200, 0, 800)
    acc_sh = TH1D("acc_sh", "Acceptance;sin(true decl);Nsh", 50, -1, 1)  # -6.5, -4, 200, -0.8, 0,)#-pi, pi, 100, -pi/2., pi/2.)#-6.5, -4, 200, -0.8, 0,)
    acc_shbis = TH1D("acc_shbis", "Acceptance;sin(decl);Nsh", 50, -1, 1)  # -6.5, -4, 200, -0.8, 0,)#-pi, pi, 100, -pi/2., pi/2.)#-6.5, -4, 200, -0.8, 0,)
    acc_tr = TH1D("acc_tr", "Acceptance;sin(true decl);Ntr", 50, -1, 1)  # -6.5, -4, 200, -0.8, 0,)#-pi, pi, 100, -pi/2., pi/2.)#-6.5, -4, 200, -0.8, 0,)
    acc_trbis = TH1D("acc_trbis", "Acceptance;sin(decl);Ntr", 50, -1, 1)  # -6.5, -4, 200, -0.8, 0,)#-pi, pi, 100, -pi/2., pi/2.)#-6.5, -4, 200, -0.8, 0,)

    PSF_sh = TH1D("PSF_sh", "Distribution of the angular error for showers;log10(angular error (/deg));Nevents", 35, -1.0, 2.5)
    PSF_tr = TH1D("PSF_tr", "Distribution of the angular error for tracks;log10(angular error (/deg));Nevents", 42, -2.5, 2.5)

    sh_spectr3d_KRA = TH3F("sh_spectrum3d_KRA", "KRA#gamma spectrum of the number of hits vs ra vs decl for showers;true ra;true decl;N_{hits};Number of events", len(ra_arr) - 1, ra_arr, len(decl_arr) - 1, decl_arr, len(nhits_arr) - 1, nhits_arr)  # 50, 0, 2000)
    sh_spectr3d_KRA_bis = TH3F("sh_spectrum3d_KRA_bis", "KRA#gamma spectrum of the number of hits vs ra vs decl for showers;reco ra;reco decl;N_{hits};Number of events", len(ra_arr) - 1, ra_arr, len(decl_arr) - 1, decl_arr, len(nhits_arr) - 1, nhits_arr)  # 50, 0, 2000)
    sh_spectr3d_zen_KRA = TH3F("sh_spectrum3d_zen_KRA", "KRA#gamma spectrum of the number of hits vs azimuth vs zenith for showers;reco az;reco zen;N_{hits};Number of events", len(az_arr) - 1, az_arr, len(zen_arr) - 1, zen_arr, len(nhits_arr2) - 1, nhits_arr2)  # 50, 0, 2000)
    sh_spectr_KRA = TH1D("sh_spectrum_KRA", "KRA#gamma spectrum of the number of hits for showers;N_{hits}", 200, 0, 800)

    tr_Etrue_Nhits_dec_KRA = TH3D("tr_Etrue_Nhits_dec_KRA", "KRA#gamma spectrum of the true energy vs the number of hits for tracks;log_{10}(E_{true} [GeV]);N_{hits};reco decl", len(ener_arr) - 1, ener_arr, len(nhits_arr) - 1, nhits_arr, len(decl_arr) - 1, decl_arr)
    tr_Etrue_Nhits_zen_KRA = TH3D("tr_Etrue_Nhits_zen_KRA", "KRA#gamma spectrum of the true energy vs the number of hits for tracks;log_{10}(E_{true} [GeV]);N_{hits};reco zen [rad]", len(ener_arr) - 1, ener_arr, len(nhits_arr) - 1, nhits_arr, len(zen_arr) - 1, zen_arr)

    hist_begin_run = TH1D("hist_begin_runMC",
                          "starting date of the run;[days]", 33000, 0, 3300)
    hist_end_run = TH1D("hist_end_runMC", "ending date of the run;[days]", 33000, 0, 3300)


tr_spectr3d_KRA = TH3F("tr_spectrum3d_KRA", "KRA#gamma spectrum of the number of hits vs ra vs decl for tracks;true ra;true decl;N_{hits};Number of events", len(ra_arr) - 1, ra_arr, len(decl_arr) - 1, decl_arr, len(nhits_arr) - 1, nhits_arr)  # 50, 0, 2000)
tr_spectr3d_KRA_bis = TH3F("tr_spectrum3d_KRA_bis", "KRA#gamma spectrum of the number of hits vs ra vs decl for tracks;reco ra;reco decl;N_{hits};Number of events", len(ra_arr) - 1, ra_arr, len(decl_arr) - 1, decl_arr, len(nhits_arr) - 1, nhits_arr)  # 50, 0, 2000)
tr_spectr3d_zen_KRA = TH3F("tr_spectrum3d_zen_KRA", "KRA#gamma spectrum of the number of hits vs azimuth vs zenith for showers;reco az;reco zen;N_{hits};Number of events", len(az_arr) - 1, az_arr, len(zen_arr) - 1, zen_arr, len(nhits_arr2) - 1, nhits_arr2)  # 50, 0, 2000)
tr_spectr_KRA = TH1D("tr_spectrum_KRA", "KRA#gamma spectrum of the number of hits for tracks;N_{hits}", 200, 0, 800)

sh_spectr_atm = TH1D("sh_spectr_atmmc", "Atmospheric spectrum of the number of hits;N_{hits}", 250, 0, 1000)
tr_spectr_atm = TH1D("tr_spectr_atmmc", "Atmospheric spectrum of the number of hits;N_{hits}", 310, 0, 992)
tr_spectr2d_atm = TH2D("tr_spectr2d_atmmc", "Atmospheric spectrum of the reconstructed energy versus declination;N_{hits}; Declination [rad]", len(nhits_arr) - 1, nhits_arr, len(decl_arr) - 1, decl_arr)
tr_spectr2d_atm_zen = TH2D("tr_spectr2d_atmmc_zen", "Atmospheric spectrum of the reconstructed energy versus zenith;N_{hits}; Zenith [rad]", len(nhits_arr2) - 1, nhits_arr2, len(zen_arr) - 1, zen_arr)
tr_spectr2d_e_atm_zen = TH2D("tr_spectr2d_e_atmmc_zen", "Atmospheric spectrum of the reconstructed energy vs zen for Tracks;log_{10}(E_{reco} [GeV]);zenith [rad]", len(ener_arr) - 1, ener_arr, len(zen_arr) - 1, zen_arr)
tr_spectr2d_e_atm_zen_mup = TH2D("tr_spectr2d_e_atmmc_zen_mup", "Atmospheric spectrum of the reconstructed energy vs zen for Tracks;log_{10}(E_{reco} [GeV]);zenith [rad]", len(ener_arr) - 1, ener_arr, len(zen_arr) - 1, zen_arr)
# tr_spectr2d_e_muatm_zen_bef_cuts0 = TH2D("tr_spectr2d_e_muatmmc_zen_bef_cuts0", "Atmospheric spectrum of the reconstructed energy vs zen for mupage Showers;log_{10}(E_{reco} [GeV]);zenith [rad]", len(ener_arr)-1, ener_arr, len(zen_arr)-1, zen_arr)
# tr_spectr2d_e_muatm_zen_bef_cuts1 = TH2D("tr_spectr2d_e_muatmmc_zen_bef_cuts1", "Atmospheric spectrum of the reconstructed energy vs zen for mupage Showers;log_{10}(E_{reco} [GeV]);zenith [rad]", len(ener_arr)-1, ener_arr, len(zen_arr)-1, zen_arr)
tr_spectr2d_e_muatm_zen_bef_cuts = TH2D("tr_spectr2d_e_muatmmc_zen_bef_cuts", "Atmospheric spectrum of the reconstructed energy vs zen for mupage Showers;log_{10}(E_{reco} [GeV]);zenith [rad]", len(ener_arr) - 1, ener_arr, len(zen_arr) - 1, zen_arr)
# tr_spectr2d_e_muatm_zen_bef_cuts3 = TH2D("tr_spectr2d_e_muatmmc_zen_bef_cuts3", "Atmospheric spectrum of the reconstructed energy vs zen for mupage Showers;log_{10}(E_{reco} [GeV]);zenith [rad]", len(ener_arr)-1, ener_arr, len(zen_arr)-1, zen_arr)
# tr_spectr2d_e_muatm_zen_bef_cuts4 = TH2D("tr_spectr2d_e_muatmmc_zen_bef_cuts4", "Atmospheric spectrum of the reconstructed energy vs zen for mupage Showers;log_{10}(E_{reco} [GeV]);zenith [rad]", len(ener_arr)-1, ener_arr, len(zen_arr)-1, zen_arr)


# sh_spectr_nueNC      = TH1D("sh_spectrum_nueNC", "E-2 spectrum for nue NC;log10(True energy)", 200, 0, 10)
# sh_spectr_numuNC     = TH1D("sh_spectrum_numuNC", "E-2 spectrum for numu NC;log10(True energy)", 200, 0, 10)
# sh_spectr_nutauNC    = TH1D("sh_spectrum_nutauNC", "E-2 spectrum for nutau NC;log10(True energy)", 200, 0, 10)

##### KRA #####
morpho_reco_sh = TH2D("morpho_reco_sh", "Distribution of the reconstructed events for showers;ra;decl;N_{evts}/bin", len(ra_arr) - 1, ra_arr, len(decl_arr) - 1, decl_arr)
morpho_reco_tr = TH2D("morpho_reco_tr", "Distribution of the reconstructed events for tracks;ra;decl;N_{evts}/bin", len(ra_arr) - 1, ra_arr, len(decl_arr) - 1, decl_arr)
morpho_reco_sh_aitoff = TH2D("morpho_reco_sh_aitoff", "Distribution of the reconstructed events for showers;-ra;decl;N_{evts}/bin", len(ra_arr_aitoff) - 1, ra_arr_aitoff, len(decl_arr_aitoff) - 1, decl_arr_aitoff)
morpho_reco_tr_aitoff = TH2D("morpho_reco_tr_aitoff", "Distribution of the reconstructed events for tracks;-ra;decl;N_{evts}/bin", len(ra_arr_aitoff) - 1, ra_arr_aitoff, len(decl_arr_aitoff) - 1, decl_arr_aitoff)
# morpho_reco_sh_aitoff = TH2D("morpho_reco_sh_aitoff", "Distribution of the reconstructed events for showers;ra;decl;N_{evts}/bin", nside_, 180, -180, nside_ / 2, -90, 90)
# morpho_reco_tr_aitoff = TH2D("morpho_reco_tr_aitoff", "Distribution of the reconstructed events for tracks;ra;decl;N_{evts}/bin", nside_, 180, -180, nside_ / 2, -90, 90)

# sh_spectr3d_e_KRA     = TH3F("sh_spectrum3d_e_KRA", "KRA#gamma spectrum
# of the reconstructed energy vs ra vs decl for showers;true ra;true
# decl;log_{10}(E_{reco} [GeV]);Number of events", len(ra_arr)-1, ra_arr,
# len(decl_arr)-1, decl_arr, len(ener_arr)-1, ener_arr)#50, 0, 2000)
sh_spectr3d_e_KRA_bis = TH3F("sh_spectrum3d_e_KRA_bis", "KRA#gamma spectrum of the reconstructed energy vs ra vs decl for showers;reco ra;reco decl;log_{10}(E_{reco} [GeV]);Number of events", len(ra_arr) - 1, ra_arr, len(decl_arr) - 1, decl_arr, len(ener_arr) - 1, ener_arr)  # 50, 0, 2000)
# sh_spectr3d_e_zen_KRA = TH3F("sh_spectrum3d_e_zen_KRA", "KRA#gamma
# spectrum of the reconstructed energy vs azimuth vs zenith for
# showers;reco az;reco zen;log_{10}(E_{reco} [GeV]);Number of events",
# len(az_arr)-1, az_arr, len(zen_arr)-1, zen_arr, len(ener_arr)-1,
# ener_arr)#50, 0, 2000)
sh_spectr_e_KRA = TH1D("sh_spectrum_e_KRA", "KRA#gamma spectrum of the reconstructed energy for showers;log_{10}(E_{reco} [GeV])", 70, 0, 7)

tr_spectr3d_e_KRA = TH3F("tr_spectrum3d_e_KRA", "KRA#gamma spectrum of the reconstructed energy vs ra vs decl for tracks;true ra;true decl;log_{10}(E_{reco} [GeV]);Number of events", len(ra_arr) - 1, ra_arr, len(decl_arr) - 1, decl_arr, len(ener_arr) - 1, ener_arr)  # 50, 0, 2000)
tr_spectr3d_e_KRA_bis = TH3F("tr_spectrum3d_e_KRA_bis", "KRA#gamma spectrum of the reconstructed energy vs ra vs decl for tracks;reco ra;reco decl;log_{10}(E_{reco} [GeV]);Number of events", len(ra_arr) - 1, ra_arr, len(decl_arr) - 1, decl_arr, len(ener_arr) - 1, ener_arr)  # 50, 0, 2000)
# tr_spectr3d_e_zen_KRA = TH3F("tr_spectrum3d_e_zen_KRA", "KRA#gamma
# spectrum of the reconstructed energy vs azimuth vs zenith for
# showers;reco az;reco zen;log_{10}(E_{reco} [GeV]);Number of events",
# len(az_arr)-1, az_arr, len(zen_arr)-1, zen_arr, len(ener_arr)-1,
# ener_arr)
tr_spectr_e_KRA = TH1D("tr_spectrum_e_KRA", "KRA#gamma spectrum of the reconstructed energy for tracks;log_{10}(E_{reco} [GeV])", 70, 0, 7)


acc_sh_KRA                       = TH1D("acc_sh_KRA", "Shower acceptance in function of the declination;sin(true decl);Nsh", 50, -1, 1)
acc_tr_KRA                       = TH1D("acc_tr_KRA", "Track acceptance in function of the declination;sin(true decl);Ntr", 50, -1, 1)
acc_sh_KRA_bis                   = TH1D("acc_sh_KRA_bis", "Distribution of the showers in function of the reconstructed declination;sin(reco decl);Nsh", 50, -1, 1)
acc_tr_KRA_bis                   = TH1D("acc_tr_KRA_bis", "Distribution of the tracks in function of the reconstructed declination;sin(reco decl);Ntr", 50, -1, 1)
acc_sh_zen_KRA                   = TH1D("acc_sh_zen_KRA", "Shower acceptance in function of zenith;cos(true zen);Nsh", 50, -1, 1)
acc_tr_zen_KRA                   = TH1D("acc_tr_zen_KRA", "Track acceptance in function of zenith;cos(true zen);Ntr", 50, -1, 1)

sindec_bins                      = 360
sh_sindec_atmMC                  = TH1D("sh_sindec_atmMC", "Shower Background vs sin(decl) from MC;sin(decl);dN/dsin(dec)", sindec_bins, -1, 0.8)
tr_sindec_atmMC                  = TH1D("tr_sindec_atmMC", "Track Background vs sin(decl) from MC;sin(decl);dN/dsin(dec)", sindec_bins, -1, 0.8)

sh_ra_atmMC                      = TH1D("sh_ra_atmMC", "Shower Background vs right ascension from MC;right ascension [rad];N_{evts}", 300, -pi, pi)
tr_ra_atmMC                      = TH1D("tr_ra_atmMC", "Track Background vs right ascension from MC;right ascension [rad];N_{evts}", 300, -pi, pi)

coszen_bins                      = 220
sh_coszen_atmMC                  = TH1D("sh_coszen_atmMC", "Shower Background vs cos(zen) from MC;cos(zen);dN/dcos(zen)", coszen_bins, -1, 0.1)
tr_coszen_atmMC                  = TH1D("tr_coszen_atmMC", "Track Background vs cos(zen) from MC;cos(zen);dN/dcos(zen)", coszen_bins, -1, 0.1)

sh_az_atmMC                      = TH1D("sh_az_atmMC", "Shower Background vs azimuth from MC;azimuth (rad);N_{evts}", 300, 0, 2. * pi)
sh_az_atmMC.Sumw2()
sh_az_atmMC_nu                      = TH1D("sh_az_atmMC_nu", "Shower Background vs azimuth from MC;azimuth (rad);N_{evts}", 300, 0, 2. * pi)
sh_az_atmMC_nu.Sumw2()
sh_az_atmMC_mu                      = TH1D("sh_az_atmMC_mu", "Shower Background vs azimuth from MC;azimuth (rad);N_{evts}", 300, 0, 2. * pi)
sh_az_atmMC_mu.Sumw2()
tr_az_atmMC                      = TH1D("tr_az_atmMC", "Track Background vs azimuth from MC;azimuth (rad);N_{evts}", 300, 0, 2. * pi)

sh_spectr_e_atm                  = TH1D("sh_spectr_e_atmmc", "Atmospheric spectrum of the reconstructed energy;log_{10}(E_{reco} [GeV])", 70, 0, 7)
sh_spectr2d_e_atm_zen            = TH2D("sh_spectr2d_e_atmmc_zen", "Atmospheric spectrum of the reconstructed energy vs zen for Showers;log_{10}(E_{reco} [GeV]);zenith [rad]", len(ener_arr) - 1, ener_arr, len(zen_arr) - 1, zen_arr)
sh_spectr_e_nuatm                = TH1D("sh_spectr_e_nuatmmc", "Atmospheric spectrum of the reconstructed energy without mupage events;log_{10}(E_{reco} [GeV])", 70, 0, 7)
sh_spectr2d_e_nuatm_zen          = TH2D("sh_spectr2d_e_nuatmmc_zen", "Atmospheric spectrum of the reconstructed energy vs zen for nu Showers;log_{10}(E_{reco} [GeV]);zenith [rad]", len(ener_arr) - 1, ener_arr, len(zen_arr) - 1, zen_arr)
sh_spectr_e_muatm                = TH1D("sh_spectr_e_muatmmc", "Atmospheric spectrum of the reconstructed energy with only mupage events;log_{10}(E_{reco} [GeV])", 70, 0, 7)
sh_spectr_e_muatm .Sumw2()
sh_spectr2d_e_muatm_zen          = TH2D("sh_spectr2d_e_muatmmc_zen", "Atmospheric spectrum of the reconstructed energy vs zen for mupage Showers;log_{10}(E_{reco} [GeV]);zenith [rad]", len(ener_arr) - 1, ener_arr, len(zen_arr) - 1, zen_arr)
sh_spectr2d_e_muatm_zen_bef_cuts = TH2D("sh_spectr2d_e_muatmmc_zen_bef_cuts", "Atmospheric spectrum of the reconstructed energy vs zen for mupage Showers;log_{10}(E_{reco} [GeV]);zenith [rad]", len(ener_arr) - 1, ener_arr, len(zen_arr) - 1, zen_arr)
tr_spectr_e_atm                  = TH1D("tr_spectr_e_atmmc", "Atmospheric spectrum of the reconstructed energy;log_{10}(E_{reco} [GeV])", 70, 0, 7)

PSF_sh_KRA                       = TH1D("PSF_sh_KRA", "Distribution of the angular error for showers;log10(angular error (/deg));Nevents", 35, -1.0, 2.5)
PSF_tr_KRA                       = TH1D("PSF_tr_KRA", "Distribution of the angular error for tracks;log10(angular error (/deg));Nevents", 42, -2.5, 2.5)


# 1 = CC nue a, 2 = NC nue a, 3 = CC anue a, 4 = NC anue a, 5 = CC numu a,
# 6 = NC numu a, 7 = CC anumu a, 8 = NC anumu a, 10 = CC nue b, 11 = NC
# nue b, 12 = CC anue b, 13 = NC anue b, 14 = CC numu b, 15 = NC numu b,
# 16 = CC anumu b, 17 = NC anumu b, 19 = mupage
histo_run_dur = TH1D("histo_run_durMC", "1=CCnuea, 2=NCnuea, 3=CCanuea, 4=NCanuea, 5=CCnumua, 6=NCnumua, 7=CCanumua, 8=NCanumua, 10=CCnueb, 11=NCnueb, 12=CCanueb, 13=NCanueb, 14=CCnumub, 15=NCnumub, 16=CCanumub, 17=NCanumub, 19=mupage", 20, 0.5, 20.5)
histo_run_number = TH1D("histo_run_numberMC", "Run numbers", 90001, -0.5, 90000.5)
histo_run_mupage = TH1D("histo_run_mupageMC", "Run numbers", 90001, -0.5, 90000.5)
Nevts_hist = TH1D("NevtsMC", "0=xrdcp failure, N KRA#gamma events 1=tr, 3=sh, N#phy_{0} events 6=tr, 8=sh", 9, -0.5, 8.5)

hist_cuts_tr_KRA = TH1D("hist_cuts_tr_KRA", "Signal events after each cuts for Tracks;cuts;N_{evts}", 15, 0.5, 15.5)
hist_cuts_tr_nuatm = TH1D("hist_cuts_tr_nuatm", "nu atm events after each cuts for Tracks;cuts;N_{evts}", 15, 0.5, 15.5)
hist_cuts_tr_mupage = TH1D("hist_cuts_tr_mupage", "Mupage events after each cuts for Tracks;cuts;N_{evts}", 15, 0.5, 15.5)

hist_cuts_sh_KRA = TH1D("hist_cuts_sh_KRA", "Signal events after each cuts for Showers;cuts;N_{evts}", 19, 0.5, 19.5)
hist_cuts_sh_nuatm = TH1D("hist_cuts_sh_nuatm", "nu atm events after each cuts for Showers;cuts;N_{evts}", 15, 0.5, 15.5)
hist_cuts_sh_mupage = TH1D("hist_cuts_sh_mupage", "Mupage events after each cuts for Showers;cuts;N_{evts}", 15, 0.5, 15.5)

# hist_cuts_lik_sh_KRA    = TH1D("hist_cuts_lik_sh_KRA", "Signal events after each cuts for Showers;cuts;N_{evts}", 12, -2.5, 57.5)
# hist_cuts_lik_sh_nuatm  = TH1D("hist_cuts_lik_sh_nuatm", "nu atm events after each cuts for Showers;cuts;N_{evts}", 12, -2.5, 57.5)
# hist_cuts_lik_sh_mupage = TH1D("hist_cuts_lik_sh_mupage", "Mupage events after each cuts for Showers;cuts;N_{evts}", 12, -2.5, 57.5)
#
# hist_cuts_lik_sh_KRA2    = TH1D("hist_cuts_lik_sh_KRA2", "Signal events after each cuts for Showers;cuts;N_{evts}", 12, -2.5, 57.5)
# hist_cuts_lik_sh_nuatm2  = TH1D("hist_cuts_lik_sh_nuatm2", "nu atm events after each cuts for Showers;cuts;N_{evts}", 12, -2.5, 57.5)
# hist_cuts_lik_sh_mupage2 = TH1D("hist_cuts_lik_sh_mupage2", "Mupage events after each cuts for Showers;cuts;N_{evts}", 12, -2.5, 57.5)

hist_channels_tr = TH1D("hist_channels_tr", "Nevts for each channel after Track cuts", 15, 0.5, 15.5)
hist_channels_sh = TH1D("hist_channels_sh", "Nevts for each channel after Shower cuts", 15, 0.5, 15.5)

MC_tr_trueEnergy = TH1D("MC_tr_trueEnergy", "MC_tr_trueEnergy", len(true_ener_arr) - 1, true_ener_arr)
MC_sh_trueEnergy = TH1D("MC_sh_trueEnergy", "MC_sh_trueEnergy", len(true_ener_arr) - 1, true_ener_arr)

MC_tr_angRes = TH1D("MC_tr_angRes", "MC_tr_angRes", 500, 0, 0.5)
MC_sh_angRes = TH1D("MC_sh_angRes", "MC_sh_angRes", 500, 0, 0.6)

# MC_sh_cos_zen_cut = TH1D("MC_sh_cos_zen_cut","MC_sh_cos_zen_cut", 40, -1, 1)
# MC_sh_M_est       = TH1D("MC_sh_M_est","MC_sh_M_est", 50, 0, 5000)
# MC_sh_beta        = TH1D("MC_sh_beta","MC_sh_beta", 35, 0, 35)
# MC_sh_RDF         = TH1D("MC_sh_RDF","MC_sh_RDF", 20, 0, 1)
# MC_sh_lik_muVeto  = TH1D("MC_sh_lik_muVeto","MC_sh_lik_muVeto", 50 ,-100 , 400)
#
# MC_tr_cos_zen_cut = TH1D("MC_tr_cos_zen_cut","MC_tr_cos_zen_cut", 40, -1, 1)
# MC_tr_lambda      = TH1D("MC_tr_lambda","MC_tr_lambda", 200, -10, 10)
# MC_tr_beta        = TH1D("MC_tr_beta","MC_tr_beta", 50, 0, 5)

MC_sh_cos_zen_cut = TH1D("MC_sh_cos_zen_cut", "MC_sh_cos_zen_cut", 22, -1, 0.1)
MC_sh_M_est = TH1D("MC_sh_M_est", "MC_sh_M_est", 100, 0, 1000)
MC_sh_beta = TH1D("MC_sh_beta", "MC_sh_beta", 26, 0, 26)
MC_sh_RDF = TH1D("MC_sh_RDF", "MC_sh_RDF", 14, 0.3, 1)
MC_sh_lik_muVeto = TH1D("MC_sh_lik_muVeto", "MC_sh_lik_muVeto", 36, 40, 400)

MC_tr_cos_zen_cut = TH1D("MC_tr_cos_zen_cut", "MC_tr_cos_zen_cut", 22, -1, 0.1)
MC_tr_lambda = TH1D("MC_tr_lambda", "MC_tr_lambda", 23, -5.15, -3.2)
MC_tr_beta = TH1D("MC_tr_beta", "MC_tr_beta", 10, 0, 1)

N_evts_gal_ridge = TH1D("N_evts_gal_ridge", "1=sig sh, 3=sig tr, 6=bg sh, 8=bg tr", 8, 0.5, 8.5)

if "tau" in sys.argv:
    acc_sh_tauCC_had = TH1D("acc_sh_tauCC_had", "Acceptance;sin(true decl);Nsh", 50, -1, 1)
    acc_sh_tauCC_e = TH1D("acc_sh_tauCC_e", "Acceptance;sin(true decl);Nsh", 50, -1, 1)
    acc_sh_tauCC_mu = TH1D("acc_sh_tauCC_mu", "Acceptance;sin(true decl);Nsh", 50, -1, 1)
    acc_tr_tauCC_mu = TH1D("acc_tr_tauCC_mu", "Acceptance;sin(true decl);Nsh", 50, -1, 1)

# beta_anti_cumul      = TH1D("beta_anti_cumul", "#beta sh anti cumulative", 200, -2, 100)
# beta_distrib         = TH1D("beta", "#beta sh distrib", 200, -2, 100)
# beta_distrib_tr_cosm = TH1D("beta_tr_cosm", "#beta tr distrib", 40, -6, -2)
# beta_distrib_tr_atm  = TH1D("beta_tr_atm", "#beta tr distrib", 40, -6, -2)
# check_Nevts          = TH1D("check_Nevts", "atm, 1:numuNC, 2:nueCC, 3:numuCC, phy0, 5:numuNC, 6:nueCC, 7:numuCC, 9:mupage", 10, 0.5, 10.5) # 1 = atm_nu_mu_CC, 3 = atm nu_X_NC+nu_e_CC, 5 = astro nu_mu_CC, 7 = astro nu_X_NC+nu_e_CC, 9 = muons
#
#
# acc_shbis_numuNC           = TH1D("acc_shbis_numuNC", "Acceptance;sin(decl);Nsh", 50, -1, 1)
# acc_shbis_nueNC           = TH1D("acc_shbis_nueNC", "Acceptance;sin(decl);Nsh", 50, -1, 1)
# acc_shbis_numuCC           = TH1D("acc_shbis_numuCC", "Acceptance;sin(decl);Nsh", 50, -1, 1)
# acc_shbis_nueCC           = TH1D("acc_shbis_nueCC", "Acceptance;sin(decl);Nsh", 50, -1, 1)
#
#
# Javfile = TFile.Open("/sps/km3net/users/tgregoir/gal_plane/ingredient_files/JavierRuns_5july.root")
# Jav_run_number = Javfile .Get("jav_run_numberMC")
# Diff_histos_run_number = Javfile .Get("Diff_histos_run_number")
# compar_run_number   = TH1D("compar_run_numberMC", "Run numbers", 80001, -0.5, 80000.5)
#
# comparison_file = open("/sps/km3net/users/tgregoir/gal_plane/software/ingredients/comparison.txt", "w")

##########  Define an initialize the variables  ##########
c_water = 0.217288148
one_year = 31557600  # seconds/year

phi_Fermi = 6.532e-6 # From the fit made by Fit_PASS8_Antonio.py
Emin = 3.366e-3 # lower bin used for the fit
PHI_Fermi = phi_Fermi * Emin**-0.4

cp_failure = 0
sh_sum_Nevts_KRA = 0
tr_sum_Nevts_KRA = 0
sh_sum_Nevts_phy0 = 0
tr_sum_Nevts_phy0 = 0
n_files = 0
Nmupage_sh = 0
Nmupage_tr = 0
official = 1
before = False
sps = 0
tau = 0
job = False

if "official" in sys.argv:
    official = 1

if "tau" in sys.argv:
    tau = 1
    official = 0
    print "\nTAU !\n"

if "sps" in sys.argv:
    sps = 1

if official:
    sps = 0


range_files = 0
n_jobs = 100
if tau:
    n_jobs = 15
for nb in range(n_jobs):
    if str(nb) in sys.argv and range_files == 0:
        job = True
        range_files = nb
        break

if official:
    if range_files < 55:
        before = True  # Files before 2013 (before run 68689)


n_files_per_job = 5
if sps:
    n_files_per_job = 3
if tau:
    n_files_per_job = 300
if official:
    if before:
        n_files_per_job = 175
    else:
        n_files_per_job = 1050


if "not_job" in sys.argv:
    job = False

n_files_max = -1
if not job:
    n_files_max = 200  # Put -1 to not stop
    range_files = 0
    if tau:
        n_files_per_job = 100

date = ""
if "date" in sys.argv:
    for i in range(len(sys.argv)):
        if sys.argv[i] == "date":
            date = "_" + sys.argv[i + 1]


print date

##########  Get the files  ##########

if not job:
    outfile = TFile.Open("/sps/km3net/users/tgregoir/gal_plane/software/ingredients/test_ing_files_MC.root", "recreate")

else:
    outfile = TFile.Open("/sps/km3net/users/tgregoir/gal_plane/ingredient_files/MC_tim" + date + "/ing_files_" + str(range_files) + ".root", "recreate")

if sps:
    lines = glob.glob("/afs/in2p3.fr/home/t/tinom/sps/datafiles/antares/mc/rbr_v3/*_v4/skim/merge/*.root")

else:
    if tau:
        MCFiles_tau = open("/sps/km3net/users/tgregoir/files/gal_plane/MCFiles_tauCC_26august.txt", "r")
        # MCFiles_tau  =
        # open("/sps/km3net/users/tgregoir/files/gal_plane/MCFiles_tauNC.txt",
        # "r") #MCFiles_tauNC_40000-45000.txt
        lines = MCFiles_tau.readlines()
        # lines        = ["MC_036930_anutau_CCshow_a_reco_dst.root", "MC_031020_anutau_CCshow_a_reco_dst.root"]

    elif not official:
        MCFiles_tino = open("/sps/km3net/users/tgregoir/files/gal_plane/MCFiles_tino.txt", "r")
        lines = MCFiles_tino.readlines()

    # elif not official:
    #     MCFiles_tino = open("/sps/km3net/users/tgregoir/files/gal_plane/MCFiles_tino.txt", "r")
    #     lines        = MCFiles_tino.readlines()
    #     # lines = [line for line in lines if "MC_0695" in line]
    #     lines = [line for line in lines if "MC_0500" in line] # NOTE change here the runs you want
    #     print lines
    #     # exit(0)

    else:  # if official
        if before:
            MCFiles_official = open("/sps/km3net/users/tgregoir/files/gal_plane/MCFiles_official_29august.txt", "r")
            lines = MCFiles_official.readlines()
        else:
            lines = glob.glob("/sps/km3net/users/antprod/mc/rbr_v3_QE/complete/*.root")


if official and not before:
    range_files -= 55

needMoreLoop = True
while needMoreLoop:

    ##########  Take the file from hpss and untar it  ##########
    n_files += 1
    if n_files <= range_files * n_files_per_job:
        n_files = range_files * n_files_per_job + 1

    if n_files > range_files * n_files_per_job + n_files_per_job or n_files > len(lines):
        needMoreLoop = False
        continue

    thisline = lines[n_files - 1]  # for each file in the txt

    if not sps and not (official and not before):
        # elif tau and not job:
        if tau:
            check = 1
            current_file = thisline.split("\n")

            for i in range(5):
                print "xrdcp root://ccxroot.in2p3.fr:1999//hpss/in2p3.fr/group/antares/mc/rbr/v3/reco/tau/AntDST/" + current_file[0] + " ./"
                check = os.system("xrdcp root://ccxroot.in2p3.fr:1999//hpss/in2p3.fr/group/antares/mc/rbr/v3/reco/tau/AntDST/" + current_file[0] + " ./")
                # print "xrdcp root://ccxroot.in2p3.fr:1999//hpss/in2p3.fr/group/antares/mc/rbr/v3/reco/"+current_file[0]+" ./"
                # check = os.system("xrdcp root://ccxroot.in2p3.fr:1999//hpss/in2p3.fr/group/antares/mc/rbr/v3/reco/"+current_file[0]+" ./")
                if check == 0:
                    break
                if i == 5 and check != 0:
                    print "Error : xrdcp doesn't work !"
                    cp_failure += 1
            files = glob.glob("./*dst.root")

        elif not official:
            check = 1
            if job:
                current_file = thisline.split("\n")
                for i in range(5):
                    # "+Dir+"/"+current_file[0]
                    print "xrdcp root://ccxroot.in2p3.fr:1999//hpss/in2p3.fr/group/antares/user/tinom/mc/" + current_file[0] + " ."
                    # "+Dir+"/"+current_file[0])
                    check = os.system("xrdcp root://ccxroot.in2p3.fr:1999//hpss/in2p3.fr/group/antares/user/tinom/mc/" + current_file[0] + " .")
                    if check == 0:
                        break
                    if i == 5 and check != 0:
                        print "Error : xrdcp doesn't work !"
                        cp_failure += 1
                os.system("tar xvzf " + current_file[0] + " -C ./")
                files = glob.glob("./*_v4/*.root")
                # print files
            else:
                files = glob.glob("/sps/km3net/users/tgregoir/files/gal_plane/MC/*.root")
                if n_files > 1:
                    continue

        elif before:  # if official and before 2013
            check = 1
            if job:
                current_file = thisline.split("\n")
                # print "this has to be changed:", current_file[14], current_file
                # os.system("mkdir /sps/km3net/users/tgregoir/files/gal_plane/temporaryMC/")
                # os.system("rm -f /sps/km3net/users/tgregoir/files/gal_plane/temporaryMC/*")
                # os.chdir("/sps/km3net/users/tgregoir/files/gal_plane/temporaryMC/")

                for i in range(5):  # 10
                    print "xrdcp root://ccxroot.in2p3.fr:1999//hpss/in2p3.fr/group/antares/mc/rbr/v3/reco/reprocess/AntDST/" + current_file[0] + " ."
                    check = os.system("xrdcp root://ccxroot.in2p3.fr:1999//hpss/in2p3.fr/group/antares/mc/rbr/v3/reco/reprocess/AntDST/" + current_file[0] + " .")
                    if check == 0:
                        break
                    if i == 5 and check != 0:
                        print "Error : xrdcp doesn't work !"
                        cp_failure += 1
                print "tar xvf " + current_file[0] + " -C ./"
                os.system("tar xvf " + current_file[0] + " -C ./")
                files = glob.glob("./*.root")
            else:  # if not job
                # files = glob.glob("/sps/km3net/users/tgregoir/files/gal_plane/lala/MicroAntDST*.root")
                # files = glob.glob("/sps/km3net/users/tgregoir/files/gal_plane/*.root")
                files = glob.glob("/sps/km3net/users/tgregoir/gal_plane/software/ingredients/test/*.root")
                # files =
                # glob.glob("/sps/km3net/users/baret/data/MicroAntDSTv1r4p2_Antares_*.root")
                # # From Bruny
                if n_files > 1:
                    continue

    else:  # if sps or (official and after)
        files = [thisline]
        # print files, "lala"

    #########  Read each of the last copied files  #########
    for infile in files:

        # if file exists, loop in it to look for good events.
        if os.path.exists(infile):

            if not TFile.Open(infile):
                print "Cannot open file,", infile, "continue to next file"
                continue

            dst = AntDST()
            dstfile = AntDSTFile(infile)
            dstfile .SetBuffers(dst)
            tfile = TFile.Open(infile)
            antData = tfile.Get("AntData")
            data_quality = AntDataQuality()

            if antData is None:
                print "antData == None in file", infile, "continue to next file"
                continue

            isMupage = 0
            isNuMuon = 0  # if not NuElec => NuMuon
            isNC = 0  # if not CC => NC
            isHighE = 0  # if not lowE => highE file
            isAnti = 0  # if not Anti particle => particle

            if "_mupage" in infile:
                isMupage = 1
            if "_NC" in infile:
                isNC = 1
            if "_numu" in infile or "_anumu" in infile:
                isNuMuon = 1
            if "_b_" in infile:
                isHighE = 1
            if "_anue" in infile or "_anumu" in infile:
                isAnti = 1

            i = 0
            Starting = True
            if dstfile.HasDataQuality():
                # while dstfile.ReadEvent(i)==AntDSTFile.eSuccess and ((sps and
                # dstfile.GetDataQuality(dst.GetRunId(), data_quality)>= 1) or
                # (not sps and dstfile.GetDataQuality(data_quality)>= 1)) and
                # (data_quality .GetScanFlag() != 1): # == 0: if I put == 0 I
                # have 1% less events
                while dstfile.ReadEvent(i) == AntDSTFile.eSuccess:
                    if sps:
                        dstfile .GetDataQuality(dst.GetRunId(), data_quality)
                    else:
                        dstfile.GetDataQuality(data_quality)
                    # print data_quality .GetQualityBasic()
                    if data_quality .GetQualityBasic() < 1 or data_quality.GetScanFlag() == 1:
                        break

            #   while dstfile.ReadEvent(i)==AntDSTFile.eSuccess:
            #     print dstfile.GetDataQuality(data_quality)

            #   while dstfile.ReadEvent(i)==AntDSTFile.eSuccess:
            #    dstfile.GetDataQuality(data_quality)
            #    if data_quality .GetQualityBasic() >= 1 and (data_quality .GetScanFlag() == 0 or data_quality .GetScanFlag() == 10):
            #    if dstfile.GetDataQuality(data_quality)>= 1 and data_quality .GetQualityBasic() < 1:
            #         print "l552"
            #         exit(0)
                #
            #    if dstfile.GetDataQuality(data_quality)< 1 and data_quality .GetQualityBasic() >= 1:
            #         print "l554"
            #         exit(0)
                #
            # if  dstfile.GetDataQuality(data_quality)>= 1 and
            # data_quality.GetScanFlag() > -1: # == 0: if I put == 0 I have 1%
            # less events

            # while dstfile.ReadEvent(i)==AntDSTFile.eSuccess and
            # dstfile.GetDataQuality(data_quality)>= 1 and data_quality
            # .GetQualityBasic()>= 1 and data_quality.GetScanFlag() > -1: # ==
            # 0: if I put == 0 I have 1% less events

                    # if i%100000 != 0: #
                    #     continue

                    ##########  Define and initialize variables  ##########
                    AaFit_sel = False  # selected as muon by AaTrack

                    if Starting or sps:  # Do this only once per file
                        Starting = False

                        ##########  Check if it is a sparking run  ##########
                        run_number = dst.GetRunId()
                        if str(run_number) in SparkingRuns:
                            print "run", run_number, "is not selected; sparking"
                            if sps:
                                i += 1
                                continue
                            else:
                                break

                        if str(run_number) not in DataRuns:
                            print "run", run_number, "is not selected; not in data"
                            if sps:
                                i += 1
                                continue
                            else:
                                break

                        if tau and run_number not in JavTauRuns:
                            print "run", run_number, "is not selected; not in JavTauRuns"
                            break

                        run_dur = data_quality .GetFrameTime() * data_quality .GetNSlices() / \
                            1000.
                        if run_number < 31051:
                            run_dur *= 0.8
                        # print data_quality .GetFrameTime(), data_quality .GetNSlices()
                        # run_dur2 = data_quality.GetRunDuration() # Slices
                        # print "good run_dur =", run_dur, "wrong one =",
                        # run_dur2
                        if histo_run_number .GetBinContent(histo_run_number .FindBin(run_number)) == 0:
                            histo_run_number .Fill(run_number, run_dur)

                        # 1 = CC nue a, 2 = NC nue a, 3 = CC anue a, 4 = NC
                        # anue a, 5 = CC numu a, 6 = NC numu a, 7 = CC anumu a,
                        # 8 = NC anumu a, 10 = CC nue b, 11 = NC nue b, 12 = CC
                        # anue b, 13 = NC anue b, 14 = CC numu b, 15 = NC numu
                        # b, 16 = CC anumu b, 17 = NC anumu b, 19 = mupage
                        if isMupage and histo_run_mupage .GetBinContent(histo_run_mupage .FindBin(run_number)) == 0:
                            histo_run_mupage .Fill(run_number, run_dur)
                            histo_run_dur .Fill(20, run_dur)
                        if not isMupage:
                            histo_run_dur .Fill(1 + 1 * isNC + 2 * isAnti + 4 * isNuMuon + 10 * isHighE, run_dur)

                        if "no_old_plots" not in sys.argv:
                            hist_begin_run .Fill(dst .GetMJD() - 54101)
                            hist_end_run .Fill(dst .GetMJD() - 54101 + (run_dur / 3600 / 24))

                    ##########  Compute the weights  ##########

                        # Tau scale: to take in account the Tau like Tino
                        if isMupage:
                            tau_scale_Tino = 1.
                        elif isNC:
                            tau_scale_Tino = 3. / 2.  # NC
                        elif isNuMuon:
                            tau_scale_Tino = 1.17  # numu CC
                        else:
                            tau_scale_Tino = 1.81  # nue CC

                        # Tau scale: to take in account the Tau
                        if isMupage:
                            tau_scale = 1.
                        elif not isNC and isNuMuon:
                            tau_scale = 1.09  # numu CC
                        elif not isNC and not isNuMuon:
                            tau_scale = 1.12  # nue CC
                        elif isNC and not isNuMuon:
                            tau_scale = 4.74  # nue NC
                        else:
                            tau_scale = 1.  # numu NC

                        if tau:
                            tau_scale = 1.

                        # check list spark avec wiki
                        # prescale: In most of Tino's files, only 1/10 of
                        # events are written so we multiply by 10
                        prescale = 1
                        if sps and run_number < 69000 and not isMupage:
                            prescale = 10
                            # print "lala"
                        elif (not sps) and run_number < 69000 and "tino-reco.PS.10.root" in infile .split("_"):
                            prescale = 10
                        if official:
                            prescale = 1

                        if official:
                            if run_number < 31051:  # 2007
                                DataMC_runDurScale = 200.42 / 126.15
                            elif run_number < 38234:  # 2008
                                DataMC_runDurScale = 213.47 / 130.98
                            elif run_number < 45539:  # 2009
                                DataMC_runDurScale = 229.42 / 128.98
                            elif run_number < 54253:  # 2010
                                DataMC_runDurScale = 244.36 / 178.73
                            elif run_number < 61907:  # 2011
                                DataMC_runDurScale = 292.01 / 192.32
                            elif run_number < 68691:  # 2012
                                DataMC_runDurScale = 261.88 / 166.89
                            elif run_number < 74353:  # 2013
                                DataMC_runDurScale = 268.89 / 228.59
                            elif run_number < 79222:  # 2014
                                DataMC_runDurScale = 340.16 / 297.84
                            else:  # 2015
                                DataMC_runDurScale = 353.91 / 303.41

                        else:
                            # # Tino's run_dur
                            if isMupage:
                                DataMC_runDurScale = 1690.52488426 / 1584.57894676
                            elif isNC:
                                DataMC_runDurScale = 1690.52488426 / \
                                    ((1592.51998843 + 1594.37836806 + 1594.5893287 + 1594.21372685 +
                                      1594.5893287 + 1594.08027778 + 1593.986875 + 1593.88028935) / 8.)
                            elif isNuMuon:  # numu CC
                                DataMC_runDurScale = 1690.52488426 / \
                                    ((1594.5893287 + 1594.77203704 +
                                      1594.35225694 + 1593.9659375) / 4.)
                            else:  # nue CC
                                DataMC_runDurScale = 1690.52488426 / \
                                    ((1594.37836806 + 1584.31895833 +
                                      1594.5893287 + 1582.10821759) / 4.)

                    ##########  Check if event is reconstructed  ##########
                    if not ((dst.HasStrategy(eShowerTantraFit) and dst.GetStrategy(eShowerTantraFit).HasRecParticle(eBrightPoint)) or (dst.HasStrategy(eAAFit) and dst.GetStrategy(eAAFit).HasRecParticle(eMuon))):
                        i += 1
                        print "Missing TANTRA and AaFit"
                        continue

                    # Get true MC informations (all my mc_... variables are
                    # true values)
                    time = dst.GetMJD()  # time in Modified Julian Day unit
                    mc_W2 = dst.GetMCEvent().GetWeight_W2()
                    mc_W3 = dst.GetMCEvent().GetWeight_W3()
                    # mc_Bartol     = dst.GetMCEvent().GetWeight_Bartol() #
                    Ngen = dst.GetMCEvent().GetNGenEvents()
                    if Ngen == 0:
                        print "Ngen = 0, continue"
                        i += 1
                        continue
                    mc_energy = dst.GetMCEvent().GetPrimaryParticle().GetEnergy()
                    mc_az = dst.GetMCEvent().GetPrimaryParticle().GetAzimuth()
                    mc_zen = dst.GetMCEvent().GetPrimaryParticle().GetZenith()
                    prim_part = dst.GetMCEvent().GetPrimary()

                    time_stamp = dst.GetTimeStamp()  # time in TTimeStamp [UTC]
                    time_date = time_stamp .GetDate()
                    time_day_of_year = time_stamp .GetDayOfYear()
                    time_in_decimal_years = int(time_date / 10000) + time_day_of_year / 367.

                    # if not isNuMuon and not isMupage and prim_part != eElectronNeutrino and prim_part != eAntiElectronNeutrino:
                    #     histo_run_number .Fill(run_number, 1e18)
                    #     print "Error with", infile

                    # Get the true decl and ra (that are not in Tino's MC
                    # files)
                    local.SetZenAzRad(mc_zen, mc_az)
                    t = Astro.Time(time)
                    eq = antares.LocalToEquatorial(local, t)
                    mc_dec = eq.GetDecRad()
                    mc_ra = eq.GetRaRad()
                    if mc_ra > pi:
                        mc_ra -= 2. * pi
                    # print mc_dec, mc_ra, 1

                    # if official:
                    #     mc_dec = dst.GetMCEvent().GetPrimaryParticle().GetDeclination()
                    #     mc_ra  = dst.GetMCEvent().GetPrimaryParticle().GetRightAscension()
                    #     print mc_dec, mc_ra, "à corriger si c\'est 0"

                    proba_morpho = morpho .Interpolate(mc_ra, mc_dec)
                    # log10 of the energy in TeV
                    logE = log10(mc_energy / 1000.)
                    # print model_spectrum_3D
                    # .GetZaxis().GetBinCenter(model_spectrum_3D
                    # .GetZaxis().GetFirst())
                    if logE > 4.90:
                        logE = 4.90
                    if logE < -2:
                        logE = -1.98

                    # KRA_spectrum gives me GeV.cm-2.s-1.sr-1 so I need to * by E-2. I divide by 3 because it is a flux for all flavors.
                    if model == "Fermi":
                        Weight_model = proba_morpho * PHI_Fermi * (mc_energy**(-2.4))/ Emin**-0.4 * (1. / 3.)
                    else:
                        Weight_model = model_spectrum_3D .GetBinContent(model_spectrum_3D .FindBin(mc_dec, mc_ra, logE)) * (mc_energy**(-2)) * (1. / 3.)
                    # if Weight_model == 0: # Their is an empty bin in the center of the spectrum
                    #     Weight_model = model_spectrum_3D .GetBinContent(model_spectrum_3D .FindBin(mc_dec, mc_ra, logE-0.06))*(mc_energy**(-2))*(1./3.)
                    if Weight_model == 0:
                        print "Warning: Weight_model spectrum probably too short!\tlog10(mc_energy(/TeV)) =", logE, "Probability to have this energy:", Weight_model, mc_dec, mc_ra
                    # W_phy0 is in N*sr so that I divide by the size of the bin and it always gives the same number of events. This is because it is done for a point source.
                    W_phy0 = 0.5e-8 * mc_W2 * (mc_energy**(-2)) * run_dur / one_year * 1e4 / Ngen * prescale * tau_scale_Tino * DataMC_runDurScale
                    W_phy0_tau = 0.5e-8 * mc_W2 * (mc_energy**(-2)) * run_dur / one_year * 1e4 / Ngen
                    # W_model = 0.5*mc_W2*Weight_model*run_dur/ one_year*1e4/ Ngen*prescale*tau_scale* DataMC_runDurScale *proba_morpho
                    # W_model is a number of events. If I divide by the angular area of my bin, it gives me the number of events that should fall in this bin. If I sum all the W_model it gives me the total number of events (full sky).
                    W_model = 0.5 * mc_W2 * Weight_model * run_dur / one_year * 1e4 / Ngen * prescale * tau_scale * DataMC_runDurScale

                    if W_model == 0:
                        proba_morpho = 1  # to not divide by 0
                        # I /proba_morpho because I do not want it to to take in account the morphology of the source. I use this for make_morphology_galac.C
                    W_KRA_bis = 0.5 * mc_W2 * Weight_model * run_dur / one_year * 1e4 / Ngen * prescale * tau_scale * DataMC_runDurScale / proba_morpho
                    W_atm = mc_W3 * run_dur / one_year / Ngen * prescale * DataMC_runDurScale  # Weight_atmo do the same
                    if run_number < 30412:
                        W_atm *= 0.8  # see elog analysis/125 (only MC)

                    # if not isMupage: print mc_energy, "E-2",
                    # 0.5e-8*(mc_energy**(-2)),"\tKRA", 0.5*Weight_model, "\t ratio",
                    # (0.5*Weight_model)/(0.5e-8*(mc_energy**(-2))),
                    # 0.5e-8*(mc_energy**(-2)) < 0.5*Weight_model

                    if isMupage:
                        W_atm = 3 * DataMC_runDurScale  # Put 10 for new production for runID>69000
                        W_phy0 = 0
                        W_phy0_tau = 0
                        W_model = 0
                        W_KRA_bis = 0

                    ##########  If reco by Aafit  ##########
                    if dst.HasStrategy(eAAFit) and dst.GetStrategy(eAAFit).HasRecParticle(eMuon):
                        reco_part = dst.GetStrategy(eAAFit).GetRecParticle(eMuon)
                        beta = reco_part.GetAngularError()
                        lambda_ = dst.GetStrategy(eAAFit).GetRecQuality()
                        zenith = reco_part.GetZenith()
                        azimuth = reco_part.GetAzimuth()
                        # cos_zenith = cos( reco_part.GetZenith() )
                        cos_theta_AA = cos(reco_part.GetTheta())
                        N_hits = reco_part.GetNUsedHits()
                        decl = reco_part.GetDeclination()
                        # ra           = reco_part.GetRightAscension()

                        Wcorr = w_correctionTr.QEreweighting(time_in_decimal_years, mc_energy, lambda_)

                        energy = reco_part.GetEnergy(edEdX_CEA)
                        if energy < 0.1:
                            energy = 0.1

                        if not (isNuMuon and not isNC) or isMupage:  # not numuCC
                            Wcorr = 1.
                        # if lambda_ > -5.2:
                        # test5.Fill(cos_theta_AA, cos(mc_theta),
                        # mc_W3)#W_phy0)

                        if (eT3 in dst.GetTriggerType() or e3D_SCAN in dst.GetTriggerType()):
                            hist_cuts_tr_KRA .Fill(1, W_model * Wcorr)
                            hist_cuts_tr_KRA .Fill(6, W_model * Wcorr * (isNuMuon and not isNC))
                            hist_cuts_tr_nuatm .Fill(1, W_atm * abs(isMupage - 1) * Wcorr)
                            hist_cuts_tr_mupage .Fill(1, W_atm * isMupage)
                            # MC_tr_cos_zen_cut .Fill(cos(zenith), W_atm*Wcorr)
                            if cos_theta_AA > -0.1:
                                hist_cuts_tr_KRA .Fill(2, W_model * Wcorr)
                                hist_cuts_tr_KRA .Fill(7, W_model * Wcorr * (isNuMuon and not isNC))
                                hist_cuts_tr_nuatm .Fill(2, W_atm * abs(isMupage - 1) * Wcorr)
                                hist_cuts_tr_mupage .Fill(2, W_atm * isMupage)
                                # MC_tr_lambda    .Fill(lambda_, W_atm*Wcorr)

                                # if lambda_ > -5.25 and beta > 0:tr_spectr2d_e_muatm_zen_bef_cuts0 .Fill(log10(energy), zenith, W_atm*isMupage)
                                # if beta*TMath.RadToDeg() < 1. and beta > 0:tr_spectr2d_e_muatm_zen_bef_cuts1 .Fill(log10(energy), zenith, W_atm*isMupage)
                                if lambda_ > -5.5 and beta * TMath.RadToDeg() < 1. and beta > 0:
                                    tr_spectr2d_e_muatm_zen_bef_cuts .Fill(log10(energy), zenith, W_atm * isMupage)
                                # if lambda_ > -5.25 and beta*TMath.RadToDeg() < 5. and beta > 0:tr_spectr2d_e_muatm_zen_bef_cuts3 .Fill(log10(energy), zenith, W_atm*isMupage)
                                # if lambda_ > -5.5 and beta*TMath.RadToDeg() < 10. and beta > 0:tr_spectr2d_e_muatm_zen_bef_cuts4 .Fill(log10(energy), zenith, W_atm*isMupage)

                                if lambda_ > -5.15:
                                    hist_cuts_tr_KRA .Fill(3, W_model * Wcorr)
                                    hist_cuts_tr_KRA .Fill(8, W_model * Wcorr * (isNuMuon and not isNC))
                                    hist_cuts_tr_nuatm .Fill(3, W_atm * abs(isMupage - 1) * Wcorr)
                                    hist_cuts_tr_mupage .Fill(3, W_atm * isMupage)
                                    # MC_tr_beta      .Fill(beta*TMath.RadToDeg(), W_atm*Wcorr)
                                    if beta * TMath.RadToDeg() < 1. and beta > 0:
                                        hist_cuts_tr_KRA .Fill(4, W_model * Wcorr)
                                        hist_cuts_tr_KRA .Fill(9, W_model * Wcorr * (isNuMuon and not isNC))
                                        hist_cuts_tr_nuatm .Fill(4, W_atm * abs(isMupage - 1) * Wcorr)
                                        hist_cuts_tr_mupage .Fill(4, W_atm * isMupage)

                                        AaFit_sel = True

                                        if isNuMuon and isNC:  # numuNC
                                            hist_channels_tr .Fill(1, W_model * Wcorr)
                                            hist_channels_tr .Fill(2, W_atm * abs(isMupage - 1) * Wcorr)
                                        elif isNuMuon and not isNC:  # numuCC
                                            hist_channels_tr .Fill(4, W_model * Wcorr)
                                            hist_channels_tr .Fill(5, W_atm * abs(isMupage - 1) * Wcorr)
                                        elif not isNuMuon and isNC:  # nueNC
                                            hist_channels_tr .Fill(7, W_model * Wcorr)
                                            hist_channels_tr .Fill(8, W_atm * abs(isMupage - 1) * Wcorr)
                                        elif not isNuMuon and not isNC:  # nueCC
                                            hist_channels_tr .Fill(10, W_model * Wcorr)
                                            hist_channels_tr .Fill(11, W_atm * abs(isMupage - 1) * Wcorr)

                                        hist_channels_tr .Fill(13, W_atm * isMupage)

                                        # if isMupage:
                                        #     print beta, lambda_, zenith, azimuth, cos_theta_AA, N_hits, decl, W_phy0, W_atm
                                        # else:
                                        #     print W_phy0, W_atm

                                        # if isNuMuon and not isNC and not isMupage: # numu CC
                                        #     beta_distrib_tr_cosm .Fill(lambda_, W_phy0)
                                        # if not isMupage:
                                        #     beta_distrib_tr_atm .Fill(lambda_, W_atm)

                                        if tau:
                                            if dst.GetMCEvent().HasParticle(eMuon) or dst.GetMCEvent().HasParticle(eAntiMuon) or "CCmu" in infile:
                                                acc_tr_tauCC_mu .Fill(sin(mc_dec), W_phy0_tau / (acc_tr_tauCC_mu .GetBinWidth(1) * 2 * pi) * Wcorr)

                                        # isNuMuon and not isNC and not
                                        # isMupage: # (a)numu CC
                                        
                                        # Get the reco ra (that is not in
                                        # Tino's MC files)
                                        local_reco.SetZenAzRad(zenith, azimuth)
                                        eq = antares.LocalToEquatorial(local_reco, t)
                                        ra = eq.GetRaRad()
                                        if ra > pi:
                                            ra -= 2. * pi

                                        if not isMupage:
                                            tr_sum_Nevts_KRA += W_model * Wcorr
                                            tr_sum_Nevts_phy0 += W_phy0 / (4 * pi) * Wcorr

                                            # print angle_rad, angle_deg,
                                            # (decl-mc_dec)*TMath .RadToDeg(),
                                            # ra, mc_ra,
                                            # min(abs((ra-mc_ra)*TMath
                                            # .RadToDeg()),abs((ra+2*pi-mc_ra)*TMath
                                            # .RadToDeg()),abs((ra-2*pi-mc_ra)*TMath
                                            # .RadToDeg()) ), "make a test with
                                            # 90deg"

                                            ##### KRA #####
                                            acc_tr_KRA          .Fill(sin(mc_dec), W_model / (acc_tr_KRA .GetBinWidth(1) * 2 * pi) * Wcorr)

                                            # tr_spectr3d_e_zen_KRA .Fill(azimuth, zenith, log10(energy), W_model*Wcorr)
                                            tr_spectr_e_KRA       .Fill(log10(energy), W_model * Wcorr)
                                            tr_spectr3d_e_KRA_bis .Fill(ra, decl, log10(energy), W_model * Wcorr)

                                            tr_spectr_KRA       .Fill(N_hits, W_model * Wcorr)
                                            tr_spectr3d_KRA_bis .Fill(ra, decl, N_hits, W_model * Wcorr)

                                            morpho_reco_tr      .Fill(ra, decl, W_model * Wcorr)
                                            morpho_reco_tr_aitoff .Fill(-ra * 180 / pi, decl * 180 / pi, W_model * Wcorr)  # I put -ra to plot it from positiv to negativ

                                            MC_tr_trueEnergy    .Fill(log10(mc_energy), W_model * Wcorr)

                                            MC_tr_angRes      .Fill(sqrt((ra - mc_ra)**2 + (decl - mc_dec)**2), W_model * Wcorr)

                                            if "no_old_plots" not in sys.argv:

                                                TestDir = TVector3()
                                                TestDir .SetXYZ(1., 0., 0.)

                                                TestDir .SetTheta(zenith)
                                                TestDir .SetPhi(azimuth)

                                                ScanDir = TVector3()
                                                ScanDir .SetXYZ(1., 0., 0.)

                                                ScanDir .SetTheta(mc_zen)
                                                ScanDir .SetPhi(mc_az)

                                                angle_rad = TestDir .Angle(ScanDir)
                                                angle_deg = angle_rad * TMath .RadToDeg()
                                                logangle = log10(angle_deg)

                                                PSF_tr_KRA .Fill(logangle, W_model * Wcorr)

                                                ##### Phy_0 #####
                                                PSF_tr .Fill(logangle, W_phy0)
                                                acc_tr              .Fill(sin(mc_dec), W_phy0 / (acc_tr .GetBinWidth(1) * 2 * pi) * Wcorr)
                                                acc_trbis           .Fill(sin(decl), W_phy0 / (acc_trbis .GetBinWidth(1) * 2 * pi) * Wcorr)
                                                # acc_tr_zen          .Fill(cos(mc_zen),W_phy0 / (acc_tr_zen .GetBinWidth(1) * 2*pi)*Wcorr )
                                                tr_spectr           .Fill(N_hits, W_phy0 * Wcorr)
                                                tr_spectr3d         .Fill(mc_ra, mc_dec, N_hits, W_phy0 * 2. * Wcorr)

                                                tr_spectr3d_zen_KRA .Fill(azimuth, zenith, N_hits, W_model * Wcorr)
                                                tr_spectr3d_KRA     .Fill(mc_ra, mc_dec, N_hits, W_model * Wcorr)

                                                tr_spectr3d_e_KRA     .Fill(mc_ra, mc_dec, log10(energy), W_model * Wcorr)

                                                acc_tr_KRA_bis      .Fill(sin(decl), W_KRA_bis / (acc_tr_KRA_bis .GetBinWidth(1) * 2 * pi) * Wcorr)
                                                acc_tr_zen_KRA      .Fill(cos(mc_zen), W_model / (acc_tr_zen_KRA .GetBinWidth(1) * 2 * pi) * Wcorr)

                                                tr_Etrue_Nhits_dec_KRA  .Fill(log10(mc_energy), N_hits, decl, W_model * Wcorr)
                                                tr_Etrue_Nhits_zen_KRA  .Fill(log10(mc_energy), N_hits, zenith, W_model * Wcorr)


                                        l_lon = reco_part.GetGalacticLongitude()
                                        b_lat = reco_part.GetGalacticLatitude()

                                        if l_lon > pi:
                                            l_lon -= 2 * pi

                                        if abs(l_lon) < (40 * pi / 180) and abs(b_lat) < (3 * pi / 180):
                                            N_evts_gal_ridge .Fill(8, W_atm * Wcorr)
                                            N_evts_gal_ridge .Fill(3, W_model * Wcorr)

                                        tr_spectr_atm         .Fill(N_hits, W_atm * Wcorr)
                                        tr_spectr_e_atm       .Fill(log10(energy), W_atm * Wcorr)
                                        tr_spectr2d_e_atm_zen .Fill(log10(energy), zenith, W_atm * Wcorr)
                                        if isMupage:
                                            tr_spectr2d_e_atm_zen_mup .Fill(log10(energy), zenith, W_atm * Wcorr)
                                        tr_spectr2d_atm       .Fill(N_hits, decl, W_atm * Wcorr)
                                        tr_spectr2d_atm_zen   .Fill(N_hits, zenith, W_atm * Wcorr)
                                        tr_sindec_atmMC       .Fill(sin(decl), W_atm * sindec_bins * (2. / 1.8) / 2. * Wcorr)
                                        tr_ra_atmMC           .Fill(ra, W_atm * tr_ra_atmMC .GetNbinsX() / (2 * pi) * Wcorr)
                                        tr_coszen_atmMC       .Fill(cos(zenith), W_atm * coszen_bins * (2. / 1.1) / 2. * Wcorr)
                                        tr_az_atmMC           .Fill(azimuth, W_atm * tr_az_atmMC .GetNbinsX() / (2 * pi) * Wcorr)

                                        MC_tr_cos_zen_cut .Fill(cos(zenith), W_atm * Wcorr)
                                        MC_tr_lambda    .Fill(lambda_, W_atm * Wcorr)
                                        MC_tr_beta      .Fill(beta * TMath.RadToDeg(), W_atm * Wcorr)

                    ##########  If reco by TANTRA  #########
                    if dst.HasStrategy(eShowerTantraFit) and dst.GetStrategy(eShowerTantraFit).HasRecParticle(eBrightPoint):
                        reco_part = dst.GetStrategy(eShowerTantraFit).GetRecParticle(eBrightPoint)

                        beta = reco_part.GetAngularError()
                        if beta < 0.:
                            i += 1
                            continue
                        N_hits = reco_part.GetNUsedHits()
                        zenith = reco_part.GetZenith()
                        azimuth = reco_part.GetAzimuth()
                        # cos_zenith = cos( reco_part.GetZenith() )
                        # theta      = reco_part.GetTheta()
                        cos_theta = cos(reco_part.GetTheta())
                        rec_vertex = reco_part.GetPosition()
                        M_Est = dst.GetStrategy(eShowerTantraFit).GetAdditionalParameters().at(2)
                        decl = reco_part.GetDeclination()
                        # ra         = reco_part.GetRa()
                        # if ra > pi: ra -= 2.*pi

                        energy = reco_part.GetEnergy(eTantraShowerEnergy)
                        if energy < 0.1:
                            energy = 0.1
                        # print energy

                        GridFit_Ratio = -100  # The GridFit ratio is the ratio between the sum of the compatible hits of all up-going and all down-going test directions
                        if dst.HasStrategy(eGridFit):
                            GridFit_Ratio = dst.GetStrategy(eGridFit).GetAdditionalParameters().at(0)

                        if dst.HasStrategy(eShowerDusjFit):
                            if dst.GetStrategy(eShowerDusjFit).HasRecParticle(eBrightPoint):
                                L_dusj = dst.GetStrategy(eShowerDusjFit).GetRecQuality()
                            elif dst.GetStrategy(eShowerDusjFit).HasRecParticle(eMuon):
                                L_dusj = dst.GetStrategy(eShowerDusjFit).GetRecQuality()
                            else:
                                L_dusj = -999999

                        Wcorr = w_correctionShMest.QEreweighting(time_in_decimal_years, mc_energy, M_Est)

                        if (isNuMuon and not isNC) or isMupage:  # not shower
                            Wcorr = 1.

                        beta_cut = 10.
                        if official or tau:
                            beta_cut = 26.
                        ##########  Shower selection  ##########
                        if (eT3 in dst.GetTriggerType() or e3D_SCAN in dst.GetTriggerType()):
                            hist_cuts_sh_KRA .Fill(1, W_model * Wcorr)
                            hist_cuts_sh_KRA .Fill(10, W_model * Wcorr * (isNC or not isNuMuon))
                            hist_cuts_sh_nuatm .Fill(1, W_atm * abs(isMupage - 1) * Wcorr)
                            hist_cuts_sh_mupage .Fill(1, W_atm * isMupage * Wcorr)
                            if not AaFit_sel:
                                hist_cuts_sh_KRA .Fill(2, W_model * Wcorr)
                                hist_cuts_sh_KRA .Fill(11, W_model * Wcorr * (isNC or not isNuMuon))
                                hist_cuts_sh_nuatm .Fill(2, W_atm * abs(isMupage - 1) * Wcorr)
                                hist_cuts_sh_mupage .Fill(2, W_atm * isMupage * Wcorr)
                                if sqrt(rec_vertex[0]**2 + rec_vertex[1]**2) < 300. and abs(rec_vertex[2]) < 250.:
                                    hist_cuts_sh_KRA .Fill(3, W_model * Wcorr)
                                    hist_cuts_sh_KRA .Fill(12, W_model * Wcorr * (isNC or not isNuMuon))
                                    hist_cuts_sh_nuatm .Fill(3, W_atm * abs(isMupage - 1) * Wcorr)
                                    hist_cuts_sh_mupage .Fill(3, W_atm * isMupage * Wcorr)
                                    # MC_sh_cos_zen_cut  .Fill(cos(zenith), W_atm*Wcorr)
                                    if cos_theta > -0.1:
                                        hist_cuts_sh_KRA .Fill(4, W_model * Wcorr)
                                        hist_cuts_sh_KRA .Fill(13, W_model * Wcorr * (isNC or not isNuMuon))
                                        hist_cuts_sh_nuatm .Fill(4, W_atm * abs(isMupage - 1) * Wcorr)
                                        hist_cuts_sh_mupage .Fill(4, W_atm * isMupage * Wcorr)
                                        # MC_sh_M_est      .Fill(M_Est, W_atm*Wcorr)
                                        if M_Est < 1000.:
                                            hist_cuts_sh_KRA .Fill(5, W_model * Wcorr)
                                            hist_cuts_sh_KRA .Fill(14, W_model * Wcorr * (isNC or not isNuMuon))
                                            hist_cuts_sh_nuatm .Fill(5, W_atm * abs(isMupage - 1) * Wcorr)
                                            hist_cuts_sh_mupage .Fill(5, W_atm * isMupage * Wcorr)
                                            # MC_sh_beta       .Fill(beta, W_atm*Wcorr)

                                            Nontime = GetNontime(dst.GetHitsVector(), reco_part)
                                            lik = GetLikelihood(dst.GetHitsVector(), reco_part, Nontime, pdf3show, pdf3mup)
                                            if lik > 50.:
                                                sh_spectr2d_e_muatm_zen_bef_cuts .Fill(log10(energy), zenith, W_atm * isMupage * Wcorr)

                                            # if cos_theta > -0.1:
                                            #     for cut_lik in range(0,60, 5):
                                            #         if lik > cut_lik:
                                            #             hist_cuts_lik_sh_KRA2 .Fill(cut_lik, W_model*Wcorr)
                                            #             hist_cuts_lik_sh_nuatm2 .Fill(cut_lik, W_atm*abs(isMupage-1)*Wcorr)
                                            #             hist_cuts_lik_sh_mupage2 .Fill(cut_lik, W_atm*isMupage*Wcorr)

                                            if beta < beta_cut:
                                                # and (((GridFit_Ratio/1.3)**3
                                                # + (float(N_hits)/150.)**3) >
                                                # 1. or tau)
                                                hist_cuts_sh_KRA .Fill(6, W_model * Wcorr)
                                                hist_cuts_sh_KRA .Fill(15, W_model * Wcorr * (isNC or not isNuMuon))
                                                hist_cuts_sh_nuatm .Fill(6, W_atm * abs(isMupage - 1) * Wcorr)
                                                hist_cuts_sh_mupage .Fill(6, W_atm * isMupage * Wcorr)
                                                # MC_sh_RDF        .Fill(L_dusj, W_atm*Wcorr)
                                                if L_dusj > 0.3:
                                                    hist_cuts_sh_KRA .Fill(7, W_model * Wcorr)
                                                    hist_cuts_sh_KRA .Fill(16, W_model * Wcorr * (isNC or not isNuMuon))
                                                    hist_cuts_sh_nuatm .Fill(7, W_atm * abs(isMupage - 1) * Wcorr)
                                                    hist_cuts_sh_mupage .Fill(7, W_atm * isMupage * Wcorr)
                                                    # MC_sh_lik_muVeto .Fill(lik, W_atm*Wcorr)
                                                # if cos_theta > -0.1:
                                                #     hist_cuts_sh_KRA .Fill(7, W_model*Wcorr)
                                                #     hist_cuts_sh_KRA .Fill(16, W_model*Wcorr*(isNC or not isNuMuon))
                                                #     hist_cuts_sh_nuatm .Fill(7, W_atm*abs(isMupage-1)*Wcorr)
                                                #     hist_cuts_sh_mupage .Fill(7, W_atm*isMupage*Wcorr)
                                    # and cos_theta_AA > 0.1):

                                                    # Nontime = GetNontime(dst.GetHitsVector(), reco_part)
                                                    # # q_ratio = GetChargeRatio(dst.GetUsedHits(eShowerTantraFit), reco_part)
                                                    # # q_ratio = GetChargeRatio(dst.GetHitsVector(), reco_part)
                                                    # lik     = GetLikelihood(dst.GetHitsVector(), reco_part, Nontime, pdf3show, pdf3mup)

                                                    # for cut_lik in range(0,60, 5):
                                                    #     if lik > cut_lik:
                                                    #         hist_cuts_lik_sh_KRA .Fill(cut_lik, W_model*Wcorr)
                                                    #         hist_cuts_lik_sh_nuatm .Fill(cut_lik, W_atm*abs(isMupage-1)*Wcorr)
                                                    #         hist_cuts_lik_sh_mupage .Fill(cut_lik, W_atm*isMupage*Wcorr)

                                                    if lik > 40.:
                                                        hist_cuts_sh_KRA .Fill(8, W_model * Wcorr)
                                                        hist_cuts_sh_KRA .Fill(17, W_model * Wcorr * (isNC or not isNuMuon))
                                                        hist_cuts_sh_nuatm .Fill(8, W_atm * abs(isMupage - 1) * Wcorr)
                                                        hist_cuts_sh_mupage .Fill(8, W_atm * isMupage * Wcorr)

                                                        if isNuMuon and isNC:  # numuNC
                                                            hist_channels_sh .Fill(1, W_model * Wcorr)
                                                            hist_channels_sh .Fill(2, W_atm * abs(isMupage - 1) * Wcorr)
                                                        elif isNuMuon and not isNC:  # numuCC
                                                            hist_channels_sh .Fill(4, W_model * Wcorr)
                                                            hist_channels_sh .Fill(5, W_atm * abs(isMupage - 1) * Wcorr)
                                                        elif not isNuMuon and isNC:  # nueNC
                                                            hist_channels_sh .Fill(7, W_model * Wcorr)
                                                            hist_channels_sh .Fill(8, W_atm * abs(isMupage - 1) * Wcorr)
                                                        elif not isNuMuon and not isNC:  # nueCC
                                                            hist_channels_sh .Fill(10, W_model * Wcorr)
                                                            hist_channels_sh .Fill(11, W_atm * abs(isMupage - 1) * Wcorr)

                                                        hist_channels_sh .Fill(13, W_atm * isMupage * Wcorr)

                                                        if tau:
                                                            if dst.GetMCEvent().HasParticle(eMuon) or dst.GetMCEvent().HasParticle(eAntiMuon) or "CCmu" in infile:
                                                                acc_sh_tauCC_mu .Fill(sin(mc_dec), W_phy0_tau / (acc_sh_tauCC_mu .GetBinWidth(1) * 2 * pi) * Wcorr)
                                                            elif dst.GetMCEvent().HasParticle(eElectron) or dst.GetMCEvent().HasParticle(eAntiElectron):
                                                                acc_sh_tauCC_e .Fill(sin(mc_dec), W_phy0_tau / (acc_sh_tauCC_e .GetBinWidth(1) * 2 * pi) * Wcorr)
                                                            else:
                                                                acc_sh_tauCC_had .Fill(sin(mc_dec), W_phy0_tau / (acc_sh_tauCC_had .GetBinWidth(1) * 2 * pi) * Wcorr)

                                                        # Get the reco ra
                                                        # (that is not in
                                                        # Tino's MC files)
                                                        local_reco.SetZenAzRad(zenith, azimuth)
                                                        eq = antares.LocalToEquatorial(local_reco, t)
                                                        ra = eq.GetRaRad()
                                                        if ra > pi:
                                                            ra -= 2. * pi

                                                        # (isNC or not isNuMuon) and not isMupage: # NC + (a)nue CC
                                                        if not isMupage:
                                                            sh_sum_Nevts_KRA += W_model * Wcorr
                                                            sh_sum_Nevts_phy0 += W_phy0 / (4 * pi)

                                                            ##### KRA #####
                                                            acc_sh_KRA            .Fill(sin(mc_dec), W_model * Wcorr / (acc_sh_KRA .GetBinWidth(1) * 2 * pi))

                                                            sh_spectr_e_KRA       .Fill(log10(energy), W_model * Wcorr)
                                                            # sh_spectr3d_e_KRA     .Fill(mc_ra, mc_dec, log10(energy), W_model*Wcorr)
                                                            sh_spectr3d_e_KRA_bis .Fill(ra, decl, log10(energy), W_model * Wcorr)
                                                            # sh_spectr3d_e_zen_KRA .Fill(azimuth, zenith, log10(energy), W_model*Wcorr)

                                                            morpho_reco_sh        .Fill(ra, decl, W_model * Wcorr)
                                                            morpho_reco_sh_aitoff .Fill(-ra * 180 / pi, decl * 180 / pi, W_model * Wcorr)  # I put -ra to plot it from positiv to negativ

                                                            MC_sh_trueEnergy      .Fill(log10(mc_energy), W_model * Wcorr)

                                                            MC_sh_angRes      .Fill(sqrt((ra - mc_ra)**2 + (decl - mc_dec)**2), W_model * Wcorr)

                                                            if "no_old_plots" not in sys.argv:

                                                                TestDir = TVector3()
                                                                TestDir .SetXYZ(1., 0., 0.)

                                                                TestDir .SetTheta(zenith)
                                                                TestDir .SetPhi(azimuth)

                                                                ScanDir = TVector3()
                                                                ScanDir .SetXYZ(1., 0., 0.)

                                                                ScanDir .SetTheta(mc_zen)
                                                                ScanDir .SetPhi(mc_az)

                                                                angle_rad = TestDir .Angle(ScanDir)
                                                                angle_deg = angle_rad * TMath .RadToDeg()
                                                                logangle = log10(angle_deg)

                                                                PSF_sh_KRA .Fill(logangle, W_model * Wcorr)

                                                                ##### Phy_0 ###
                                                                PSF_sh .Fill(logangle, W_phy0)
                                                                acc_sh              .Fill(sin(mc_dec), W_phy0 / (acc_sh .GetBinWidth(1) * 2 * pi) * Wcorr)
                                                                acc_shbis           .Fill(sin(decl), W_phy0 / (acc_shbis .GetBinWidth(1) * 2 * pi) * Wcorr)
                                                                # acc_sh_zen          .Fill(cos(mc_zen), W_phy0 / (acc_sh_zen .GetBinWidth(1) * 2*pi) )
                                                                sh_spectr           .Fill(N_hits, W_phy0 * Wcorr)
                                                                # Tino's
                                                                # Spectrum are
                                                                # for 1.e-8
                                                                # flux for nu
                                                                # and anu so a
                                                                # total 2e-8
                                                                # flux
                                                                sh_spectr3d         .Fill(mc_ra, mc_dec, N_hits, W_phy0 * 2. * Wcorr)

                                                                sh_spectr_KRA         .Fill(N_hits, W_model * Wcorr)
                                                                sh_spectr3d_KRA       .Fill(mc_ra, mc_dec, N_hits, W_model * Wcorr)
                                                                sh_spectr3d_KRA_bis   .Fill(ra, decl, N_hits, W_model * Wcorr)
                                                                sh_spectr3d_zen_KRA   .Fill(azimuth, zenith, N_hits, W_model * Wcorr)

                                                                acc_sh_KRA_bis        .Fill(sin(decl), W_KRA_bis * Wcorr / (acc_sh_KRA_bis .GetBinWidth(1) * 2 * pi))
                                                                acc_sh_zen_KRA        .Fill(cos(mc_zen), W_model * Wcorr / (acc_sh_zen_KRA .GetBinWidth(1) * 2 * pi))

                                                        l_lon = reco_part.GetGalacticLongitude()
                                                        b_lat = reco_part.GetGalacticLatitude()

                                                        if l_lon > pi:
                                                            l_lon -= 2 * pi

                                                        if abs(l_lon) < (40 * pi / 180) and abs(b_lat) < (3 * pi / 180):
                                                            N_evts_gal_ridge .Fill(6, W_atm * Wcorr)
                                                            N_evts_gal_ridge .Fill(1, W_model * Wcorr)

                                                        sh_spectr_atm   .Fill(N_hits, W_atm * Wcorr)
                                                        sh_spectr_e_atm .Fill(log10(energy), W_atm * Wcorr)
                                                        sh_spectr2d_e_atm_zen .Fill(log10(energy), zenith, W_atm * Wcorr)
                                                        sh_sindec_atmMC .Fill(sin(decl), W_atm * sindec_bins * (2. / 1.8) / 2. * Wcorr)
                                                        sh_ra_atmMC     .Fill(ra, W_atm * sh_ra_atmMC .GetNbinsX() / (2 * pi) * Wcorr)
                                                        sh_coszen_atmMC .Fill(cos(zenith), W_atm * coszen_bins * (2. / 1.1) / 2. * Wcorr)
                                                        sh_az_atmMC     .Fill(azimuth, W_atm * sh_az_atmMC .GetNbinsX() / (2 * pi) * Wcorr)
                                                        if not isMupage:
                                                            sh_az_atmMC_nu     .Fill(azimuth, W_atm * sh_az_atmMC_nu .GetNbinsX() / (2 * pi) * Wcorr)
                                                        else:
                                                            sh_az_atmMC_mu     .Fill(azimuth, W_atm * sh_az_atmMC_mu .GetNbinsX() / (2 * pi) * Wcorr)


                                                        if not isMupage:
                                                            sh_spectr_e_nuatm .Fill(log10(energy), W_atm * Wcorr)
                                                            sh_spectr2d_e_nuatm_zen .Fill(log10(energy), zenith, W_atm * Wcorr)
                                                        if isMupage:
                                                            sh_spectr_e_muatm .Fill(log10(energy), W_atm * Wcorr)
                                                            sh_spectr2d_e_muatm_zen .Fill(log10(energy), zenith, W_atm * Wcorr)

                                                        MC_sh_cos_zen_cut .Fill(cos(zenith), W_atm * Wcorr)
                                                        MC_sh_M_est       .Fill(M_Est, W_atm * Wcorr)
                                                        MC_sh_beta        .Fill(beta, W_atm * Wcorr)
                                                        MC_sh_RDF         .Fill(L_dusj, W_atm * Wcorr)
                                                        MC_sh_lik_muVeto  .Fill(lik, W_atm * Wcorr)

                    i += 1

                print "\t", i, "evts read\tsh_sum_Nevt_phy0", sh_sum_Nevts_KRA, "\ttr_sum_Nevt_phy0", tr_sum_Nevts_KRA, "\t", n_files, "\n"
            # print "N mupage reco as sh =", Nmupage_sh, "reco as tr",
            # Nmupage_tr

            tfile.Close()
            del dstfile
            del dst

        if n_files >= n_files_max and n_files_max != -1:
            break

    if job and not sps and not (official and not before) or tau:
        all_files = glob.glob("./*_v4/*.root")
        if official:
            all_files = glob.glob("./*i3.root")
        if tau:
            all_files = glob.glob("./*dst.root")
        print "Checking before I remove anything:\n", all_files
        for infile in all_files:
            os.system("rm -f " + infile)

        all_files = glob.glob("./*_v4/*.root")
        if official:
            all_files = glob.glob("./*i3.root")
        if tau:
            all_files = glob.glob("./*dst.root")
        print "Checking that I removed everything:\n", all_files

print "sh_sum_Nevt_KRA:", sh_sum_Nevts_KRA, "tr_sum_Nevt_KRA:", tr_sum_Nevts_KRA, "files:", n_files
print "sh_sum_Nevt_phy0:", sh_sum_Nevts_phy0, "tr_sum_Nevt_phy0:", tr_sum_Nevts_phy0

Nevts_hist .Fill(1, tr_sum_Nevts_KRA)
Nevts_hist .Fill(3, sh_sum_Nevts_KRA)
Nevts_hist .Fill(6, tr_sum_Nevts_phy0)
Nevts_hist .Fill(8, sh_sum_Nevts_phy0)
Nevts_hist .Fill(0, cp_failure)


# Write all the histos in the file
outfile.cd()

if tau:
    acc_sh_tauCC_had .Write()
    acc_sh_tauCC_e   .Write()
    acc_sh_tauCC_mu  .Write()
    acc_tr_tauCC_mu  .Write()

# acc_sh_zen           .Write()
# acc_tr_zen           .Write()
# sh_spectr_ener       .Write()
# sh_spectr3d_ener     .Write()
# tr_spectr_ener       .Write()
# tr_spectr3d_ener     .Write()
# sh_spectr_nueNC      .Write()
# sh_spectr_numuNC     .Write()
# sh_spectr_nutauNC    .Write()
# beta_anti_cumul      .Write()
# beta_distrib         .Write()
# beta_distrib_tr_cosm .Write()
# beta_distrib_tr_atm  .Write()
# acc_shbis_numuNC     .Write()
# acc_shbis_nueNC      .Write()
# acc_shbis_numuCC     .Write()
# acc_shbis_nueCC      .Write()
# check_Nevts          .Write()

if "no_old_plots" not in sys.argv:
    ##### Phy_0 #####
    sh_spectr               .Write()
    sh_spectr3d             .Write()
    tr_spectr               .Write()
    tr_spectr3d             .Write()

    PSF_sh                  .Write()
    PSF_tr                  .Write()

    acc_sh                  .Write()
    acc_shbis               .Write()
    acc_tr                  .Write()
    acc_trbis               .Write()

    sh_spectr_KRA           .Write()
    # sh_spectr3d_KRA       .Write()
    sh_spectr3d_KRA_bis     .Write()
    sh_spectr3d_zen_KRA     .Write()

    # tr_spectr3d_KRA       .Write()
    tr_spectr3d_zen_KRA     .Write()
    # tr_spectr3d_e_KRA     .Write()
    # tr_spectr3d_e_zen_KRA .Write()

    PSF_sh_KRA              .Write()
    PSF_tr_KRA              .Write()

    acc_sh_KRA_bis          .Write()
    acc_tr_KRA_bis          .Write()
    # acc_sh_zen_KRA        .Write()
    # acc_tr_zen_KRA        .Write()

    tr_Etrue_Nhits_dec_KRA  .Write()
    tr_Etrue_Nhits_zen_KRA  .Write()

    hist_end_run .Write()
    hist_begin_run .Write()


MC_tr_angRes .Write()
MC_sh_angRes .Write()

N_evts_gal_ridge  .Write()

# hist_cuts_lik_sh_KRA    .Write()
# hist_cuts_lik_sh_nuatm  .Write()
# hist_cuts_lik_sh_mupage .Write()
#
# hist_cuts_lik_sh_KRA2    .Write()
# hist_cuts_lik_sh_nuatm2  .Write()
# hist_cuts_lik_sh_mupage2 .Write()

MC_sh_trueEnergy  .Write()
MC_tr_trueEnergy  .Write()

MC_sh_cos_zen_cut .Write()
MC_sh_M_est       .Write()
MC_sh_beta        .Write()
MC_sh_RDF         .Write()
MC_sh_lik_muVeto  .Write()

MC_tr_cos_zen_cut .Write()
MC_tr_lambda      .Write()
MC_tr_beta        .Write()

##### KRA #####
hist_channels_tr        .Write()
hist_channels_sh        .Write()

hist_cuts_tr_KRA        .Write()
hist_cuts_tr_nuatm      .Write()
hist_cuts_tr_mupage     .Write()

hist_cuts_sh_KRA        .Write()
hist_cuts_sh_nuatm      .Write()
hist_cuts_sh_mupage     .Write()

Nevts_hist              .Write()
histo_run_dur           .Write()
histo_run_number        .Write()

sh_spectr_e_KRA         .Write()
# sh_spectr3d_e_KRA       .Write()
sh_spectr3d_e_KRA_bis   .Write()
# sh_spectr3d_e_zen_KRA .Write()
sh_spectr_e_atm         .Write()
sh_spectr_e_nuatm       .Write()
sh_spectr_e_muatm       .Write()
sh_spectr2d_e_atm_zen   .Write()
sh_spectr2d_e_nuatm_zen .Write()
sh_spectr2d_e_muatm_zen .Write()
sh_spectr2d_e_muatm_zen_bef_cuts .Write()

sh_spectr_atm           .Write()

tr_spectr_e_KRA         .Write()
tr_spectr3d_e_KRA_bis   .Write()
tr_spectr_e_atm         .Write()
tr_spectr2d_e_atm_zen   .Write()
tr_spectr2d_e_atm_zen_mup .Write()
# tr_spectr2d_e_muatm_zen_bef_cuts0 .Write()
# tr_spectr2d_e_muatm_zen_bef_cuts1 .Write()
tr_spectr2d_e_muatm_zen_bef_cuts .Write()
# tr_spectr2d_e_muatm_zen_bef_cuts3 .Write()
# tr_spectr2d_e_muatm_zen_bef_cuts4 .Write()

tr_spectr_atm           .Write()
tr_spectr2d_atm         .Write()
tr_spectr2d_atm_zen     .Write()
tr_spectr3d_KRA_bis     .Write()
tr_spectr_KRA           .Write()

sh_sindec_atmMC         .Write()
tr_sindec_atmMC         .Write()
sh_ra_atmMC             .Write()
tr_ra_atmMC             .Write()
sh_coszen_atmMC         .Write()
tr_coszen_atmMC         .Write()
sh_az_atmMC             .Write()
sh_az_atmMC_nu             .Write()
sh_az_atmMC_mu             .Write()
tr_az_atmMC             .Write()

acc_sh_KRA              .Write()
acc_tr_KRA              .Write()

morpho_reco_sh          .Write()
morpho_reco_tr          .Write()
morpho_reco_sh_aitoff   .Write()
morpho_reco_tr_aitoff   .Write()

outfile.Close()

end = datetime.datetime.now()
elapsed = end - start

print "elapsed", elapsed.seconds, "seconds"
