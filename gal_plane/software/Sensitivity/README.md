`./plot_stuff.py option date option_date Kevts option_Kevts`

needed:
option_Kevts == number of expected events from the model, given by sumHistos.py
option_date  == the suffix of the input file, it can be the date or anything else, it usally start by 'merged_'

option can be:
ts == gives the pdf of the TS, the anti-cumulative, the UL, discovery proba...
nsig == gives the nsig_injected-nsig_fitted vs nsig_injected
nsig_ratio == ratio of showers over tracks

**to get the DISCO PROBA on a larger scale change the variable flux_scale !!**
