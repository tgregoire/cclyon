#!/usr/local/bin/python -i

import sys
import os
import glob
import array
import math
from math import *
from os.path import expandvars
import numpy as np
# from table import table
sys.path.append(os.environ['ROOTSYS'] + '/lib/root')
from ROOT import *

def Beautify_axis_title(histo):

    histo .GetXaxis().SetTitleFont(132)
    histo .GetYaxis().SetTitleFont(132)
    histo .GetXaxis().CenterTitle()
    histo .GetYaxis().CenterTitle()
    histo .GetZaxis().CenterTitle()

    return histo

if gSystem.Load('goofit.so') < 0:
    sys.exit()
print '  +--------------------------+'
print '  |  Goof-it library loaded  |'
print '  +--------------------------+'


nevts_KRAg = 1

if "Kevts" in sys.argv:
    for i in range(len(sys.argv)):
        if sys.argv[i] == "Kevts":
            nevts_KRAg = float(sys.argv[i + 1])
else:
    print "\nKevts needed !!\n" * 3

maxRange = 76  # 110
# maxRange = 11
step = 1
if maxRange / step < 6:
    maxRange = 6 * step
nhist = (maxRange - 1) / step
# if (maxRange)/5/step)%1 != 0: print ""


def histograms():
    """Get the requested histograms from the corresponding file in pe_dir
       Returns a std::vector of the histograms."""

    v = vector('TH1F*')()
    w = vector('TF1*')()

    tree_name = "T"
    if "dry" in sys.argv:
        pe_dir = '/afs/in2p3.fr/home/t/tgregoir/sps/gal_plane/software/PseudoExperiments/'
    if "extended" in sys.argv:
        pe_dir = '/afs/in2p3.fr/home/t/tgregoir/sps/gal_plane/CPEx_output_10jan_ext/'
    if "date" in sys.argv:
        for i in range(len(sys.argv)):
            if sys.argv[i] == "date":
                date = sys.argv[i + 1]
        pe_dir = '/afs/in2p3.fr/home/t/tgregoir/sps/gal_plane/CPEx_output_' + date + '/'
    else:
        pe_dir = '/afs/in2p3.fr/home/t/tgregoir/sps/gal_plane/CPEx_output/'

    if "ts" or "ts_anti_cumul"in sys.argv:
        bin_anti_cum = array.array('f')
        for i in range(0, 1000, 1):
            bin_anti_cum.append((log10(1000000) - log10(1000000 - i * i)) * 25)

    for n in range(0, maxRange, step):  # [0, 30, 100]:#
        if True:  # "galacplane" in sys.argv:
            if "dry" in sys.argv:
                file_name = pe_dir + "dry_CPEx_galacticPlane_E_10.root"
            elif "extended" in sys.argv:
                # A remettre en CPEx_galacticPlane_extended_E_"+str(n)+".root
                file_name = pe_dir + \
                    "CPEx_galacticPlane_extended_E_" + str(n) + ".root"
            else:
                file_name = pe_dir + "CPEx_galacticPlane_E_" + str(n) + ".root"

        f = TFile(file_name)
        T = f.Get(tree_name)

        print n, "events injected"

        if not T:
            print "failed to get TTree : ", tree_name
            continue

        if "nsig_ratio" in sys.argv:
            # if n == 0:
            #     h2D = TH2F("ht_"+str(n),str(n)+" Signal Events;ratio sh/(tr+sh);Number of fitted signal", 100,0, 1, 100, 0, 40)
            #     T.Draw("Pex.showerclus.fit_tnsig+Pex.showerclus.fit_snsig:Pex.showerclus.fit_snsig/(Pex.showerclus.fit_tnsig+Pex.showerclus.fit_snsig)>>"+h2D.GetName(),"","colz")
            what = "Pex.showerclus.fit_snsig/(Pex.showerclus.fit_tnsig+Pex.showerclus.fit_snsig)"
            h = TH1F("ht_" + str(n), str(n) +
                     " Signal Events;#mu_{fitted showers}/#mu_{fitted showers+tracks};rate of Pseudo Experiment/bin width", 100, 0, 1)
        elif "nsig" in sys.argv:
            if "sh" in sys.argv:
                what = "Pex.showerclus.fit_snsig-" + str(0.203 * n)
            elif "tr" in sys.argv:
                what = "Pex.showerclus.fit_tnsig-" + str(0.797 * n)
            else:
                what = "Pex.showerclus.fit_nsig-" + str(n)
            # int((maxRange-1)),int((maxRange-1)*2))
            h = TH1F("ht_" + str(n), str(n) +
                     " Signal Events;Nsig_{fitted}-Nsig;Rate of Pseudo Experiments", 101, -int(maxRange*2/3) - 0.5, int(maxRange*2/3) + 0.5) #-maxRange - 0.5, maxRange + 0.5)
        elif "ts" in sys.argv:
            what = "Pex.showerclus.fit_TS"
            # h = TH1F("ht_"+str(n),str(n)+" Signal Events;#it{Q};Pseudo
            # Experiments", 10000,0, 400)
            h = TH1F("ht_" + str(n), str(n) + " Signal Events;#it{Q};pdf(Q)", len(
                bin_anti_cum) - 1, bin_anti_cum)  # 10000,0, 6*int(maxRange-1))
            # h = TH1F("ht_"+str(n),str(n)+" Signal Events;#it{Q};Pseudo
            # Experiments", 100,0, 40)
        elif "ts_anti_cumul"in sys.argv:
            print "DO NOT USE THIS, USE 'ts' INSTEAD"
            # what="Pex.showerclus.fit_TS"
            # h = TH1F("ht_"+str(n),str(n)+" Signal Events;#it{Q};Pseudo
            # Experiments", len(bin_anti_cum)-1, bin_anti_cum) #10000,0,
            # 6*int(maxRange-1))
        elif "nsig_anti_cumul" in sys.argv:
            what = "Pex.showerclus.fit_nsig"
            h = TH1F("ht_" + str(n), str(n) +
                     " Signal Events;log_{10}(log_{10}(L_{S+B} / L_{B}));Pseudo Experiments", 1500, -3, log10(60))
        if "dec" in sys.argv or "decl" in sys.argv:
            what = "Pex.showerclus.pevts.decl"
            h = TH1F("ht_" + str(n), str(n) +
                     " Signal Events;#delta / #circ;Pseudo Experiments", 95, -95, 95)
        if "ra" in sys.argv:
            what = "Pex.showerclus.pevts.ra"
            h = TH1F("ht_" + str(n), str(n) +
                     " Signal Events;#alpha / #circ;Pseudo Experiments", 91, -182, 182)

        T.Draw(what + ">>" + h.GetName(), "", "goff")

        if "ts" in sys.argv and n == 0:
            # for i in range(1, int(h.GetNbinsX()+1)):
            #     h.SetBinContent(i, h.GetBinContent(i)/h.GetBinWidth(i))

            # fit the last 5 % of the distribution
            # x1 = get_quantile( h, .95 )
            # x2 = h.GetBinCenter( h.GetNbinsX() )
            # b1 = h.FindBin( x1 )
            # print " extrapolate " , h.GetName() ,"using bin",b1, "and onwards"
            #
            # nn = sum( h.GetBinContent(b) > 0 for b in range(b1,h.GetNbinsX()+1) )
            # print "have ",nn,"non-empty bins for the fit"
            #
            # fit = TF1("fit","expo(0)", x1, 100)
            # h.Fit("fit","LR")
            # w.push_back(fit)
            fit = NULL

        if "nsig" in sys.argv or "nsig_ratio" in sys.argv:
            h .Scale(1. / h .Integral())

        if "nsig" in sys.argv:
            fit = TF1("fit", "gaus(0)", 1, 100)
            fit.SetParameter(0, 0.1)
            fit.SetParameter(1, 0)
            fit.SetParameter(2, 20)

            h.Fit("fit", "", "", 5 - n, 55)

        elif "ts_anti_cumul" in sys.argv:
            h2 = TH1F("ht_" + str(n), str(n) + " Signal Events;#it{Q};Rate of Pseudo Experiments", len(
                bin_anti_cum) - 1, bin_anti_cum)  # 10000,0, 6*int(maxRange-1))
            h.Scale(1. / h.Integral())
            sum_ = 0
            h2.SetBinContent(1, 1 - sum_)  # to have the first bin full
            for i in range(1, int(h.GetNbinsX() + 1)):
                sum_ += h.GetBinContent(i)
                h2.SetBinContent(i + 1, 1 - sum_)
            h = h2

            # h.Draw()
            fit = NULL

        else:
            fit = NULL

        if n == 0:
            h = Beautify_axis_title(h)

        h.SetDirectory(0)
        if "nsig" not in sys.argv:
            if n == 0:
                h.SetFillColor(5)
            elif n == maxRange - 1 - maxRange / 5:
                h.SetLineColor(kOrange + 1)
            else:
                h.SetLineColor(int(n * 5 / (maxRange - 1)))
            if "nsig_ratio" in sys.argv and n == maxRange - 1:
                h.SetLineColor(kBlue + 1)

        else:  # if nsig
            if n == 0:
                h.SetFillColor(5)
            elif n == maxRange - 1:
                fit.SetLineColor(kOrange + 1)
                h.SetLineColor(kOrange + 1)
            else:
                fit.SetLineColor(int(n * 5 / (maxRange - 1)))
                h.SetLineColor(int(n * 5 / (maxRange - 1)))
            w.push_back(fit)

        h.SetLineWidth(2)

        SetOwnership(h, False)
        v.push_back(h)

        if "dry" in sys.argv:
            break

    print "\nFor", date
    return v, w


if __name__ == "__main__":

    gStyle.SetOptStat(0)

    histos, fit = histograms()
    
    if "date" in sys.argv:
        for i in range(len(sys.argv)):
            if sys.argv[i] == "date":
                date = sys.argv[i + 1]
    
    option = "nsig_"
    if "ts" in sys.argv:
        option = "ts_"
    elif "sh" in sys.argv:
        option += "sh_"
    elif "tr" in sys.argv:
        option += "tr_"

    outfile = TFile("outfile_"+option+date+".root", "RECREATE")

    c = TCanvas("c", "c", 1200, 800)
    if "nsig" in sys.argv or "ts" in sys.argv:
        SetOwnership(c, False)
        c.Divide(2, 2)
        c.cd(1)

    if "nsig_ratio" in sys.argv:
        leg = TLegend(.62, .60, .87, .90)
        leg.SetFillColor(kWhite)
    else:
        leg = TLegend(.65, .60, .90, .90)
        leg.SetFillColor(kWhite)

    maximum = histos[0].GetMaximum()

    leg.AddEntry(histos[0], histos[0].GetTitle(), "f")
    histos[0] .GetXaxis().SetTitleSize(0.05)
    histos[0] .GetYaxis().SetTitleSize(0.05)
    histos[0] .Draw()

    if "dry" not in sys.argv:
        # [1, 2]:
        for i in range((maxRange - 1) / 5 / step, nhist + 1, (maxRange - 1) / 5 / step):
            leg.AddEntry(histos[i], histos[i].GetTitle(), "l")
    # for i in range(10,0,-2):
            histos[i].Draw("same")
            maximum = max(maximum, histos[i].GetMaximum())
    else:
        leg.AddEntry(histos[0], histos[0].GetTitle(), "l")

    if "nsig_ratio" in sys.argv:
        # leg.AddEntry(histos[0], histos[0].GetTitle(), "l")
        leg.Draw()
        c.SetLogy()

        c_nsig_ratio = TCanvas("c_nsig_ratio", "c_nsig_ratio", 1200, 800)
        c_nsig_ratio.cd()
        legFlux = TLegend(.62, .60, .87, .90)
        legFlux.SetFillColor(kWhite)

        # legFlux.AddEntry(histos[0], histos[0].GetTitle(), "f")
        # for i in range((maxRange-1)/5/step,nhist+1,(maxRange-1)/5/step):
        #     legFlux.AddEntry(histos[i], histos[i].GetTitle(), "l")
        # legFlux.Draw()
        # c_nsig_ratio.SetLogy()

        nsig_ratio_correct = vector('TH1F*')()
        nsig_ratio_correct .push_back(histos[0])
        histos[0] .GetXaxis().SetTitleSize(0.05)
        histos[0] .GetYaxis().SetTitleSize(0.05)
        histos[0].Draw()

        # legFlux.AddEntry(histos[0], histos[0].GetTitle(), "f")
        # for i in range((maxRange-1)/5/step,nhist+1,(maxRange-1)/5/step):
        #     legFlux.AddEntry(histos[i], histos[i].GetTitle(), "l")
        # legFlux.Draw()
        c_nsig_ratio.SetLogy()

        flux_scale = nevts_KRAg / ((maxRange - 1) / 5 * 2)  # 1

        for phi in range(1, nhist + 1):
            h4 = TH1F("nsig_ratio_" + str(phi), str(phi) +
                      " Signal Events;nsig_ratio;pdf(Q)", 100, 0, 1)
            phi *= flux_scale
            gauss_scale = 0
            for n_phi_scale in range(1, nhist + 1):
                gauss_scale += TMath .Gaus(n_phi_scale, phi, 0.15 * phi)
            for bin_TS in range(1, int(histos[0].GetNbinsX() + 1)):
                sum_ = 0
                for n_phi in range(1, nhist + 1):
                    gauss = TMath .Gaus(
                        n_phi * step, phi * step, 0.15 * phi * step) / gauss_scale
                    for n_inj in range(0, nhist + 1):
                        sum_ += TMath .Poisson(n_inj * step, n_phi * step) * \
                            gauss * histos[n_inj] .GetBinContent(bin_TS)

                        # print histos[n_inj] .GetBinContent(bin_TS), phi,
                        # n_inj, bin_TS, "\n",TMath .Poisson(n_inj*step,
                        # phi*step), n_inj*step, phi*step, "\n", sum_, "\n",
                        # gauss, n_phi
                h4 .SetBinContent(bin_TS, sum_)
                # if i==0 : h4 .SetFillColor(5)
                # elif i==nhist-maxRange/5:
                #     h4 .SetLineColor(kOrange+1)
                # else :
                #     h4 .SetLineColor(int(i/10))
                # if i%int((maxRange-1)/5/step) ==0:
                #     print i
                #     h4.Draw("same")
                h4 = Beautify_axis_title(h4)
                nsig_ratio_correct .push_back(h4)

        legFlux.AddEntry(nsig_ratio_correct[0], "Bckg only", "f")
        # nsig_ratio_correct[0].Draw()

        # [1, 2]:
        for i in range((maxRange - 1) / 5 / step, nhist, (maxRange - 1) / 5 / step):
            legFlux.AddEntry(nsig_ratio_correct[i], str(
                int(i * flux_scale / nevts_KRAg * 100.) / 100.) + " * #Phi_{KRA#gamma}", "l")
        # for i in range(10,0,-2):
            # if i==0 : nsig_ratio_correct[i] .SetFillColor(5)
            nsig_ratio_correct[i] .SetLineWidth(2)
            if i == nhist - maxRange / 5:
                nsig_ratio_correct[i] .SetLineColor(kOrange + 1)
            else:
                nsig_ratio_correct[i] .SetLineColor(int(i / 10))
            nsig_ratio_correct[i].Draw("same")
        legFlux.Draw()
        # c2.SetLogy()
        c_nsig_ratio.Update()

    if "ts" in sys.argv:
        # leg.AddEntry(fit[0], "exp. Fit", "l")
        bin_anti_cum = array.array('f')

        flux_scale = nevts_KRAg / ((maxRange - 1) / 5 * 2)  # 1

        for i in range(0, 1000, 1):
            bin_anti_cum.append((log10(1000000) - log10(1000000 - i * i)) * 25)

        ts_correct = vector('TH1F*')()
        ts_correct .push_back(histos[0])

        for phi in range(1, nhist + 1):
            h3 = TH1F("TS_" + str(phi), str(phi) +
                      " Signal Events;#it{Q};pdf(Q)", len(bin_anti_cum) - 1, bin_anti_cum)
            gauss = [[n_phi, TMath .Gaus(n_phi * flux_scale * step, phi * flux_scale * step, 0.15 * phi * flux_scale * step)] for n_phi in range(
                0, min(nhist + 1, phi * 2)) if TMath .Gaus(n_phi * flux_scale * step, phi * flux_scale * step, 0.15 * phi * flux_scale * step) > 10e-5]
            gauss_scale = sum([gauss[n][1] for n in range(len(gauss))])
            gauss = [[x, y / gauss_scale] for x, y in gauss]
            # for n_phi_scale in range(0, nhist + 1):
            #     gauss_scale += TMath .Gaus(n_phi_scale * flux_scale * step, phi * flux_scale * step, 0.15 * phi * flux_scale * step)
            # print phi, len(gauss), gauss_scale, gauss
            for bin_TS in range(1, int(histos[0].GetNbinsX() + 1)):
                sum_ = 0
                for n_phi, gauss_val in gauss:
                    # gauss = TMath .Gaus(n_phi * flux_scale * step, phi * flux_scale * step, 0.15 * phi * flux_scale * step) / gauss_scale
                    for n_inj in range(0, nhist + 1):
                        # to have it for signal, replace by: 13.72, phi*step) *
                        # histos[n_inj] .GetBinContent(bin_TS)
                        sum_ += TMath .Poisson(n_inj * step, n_phi * flux_scale * step) * \
                            gauss_val * histos[n_inj] .GetBinContent(bin_TS)
                        # print histos[n_inj] .GetBinContent(bin_TS), phi, n_inj, bin_TS, "\n",TMath .Poisson(n_inj*step, phi*step), n_inj*step, phi*step, "\n", sum_, gauss, n_phi, "\n"
                        # print n_inj*step, phi*step, TMath
                        # .Poisson(n_inj*step, phi*step), 13.72, TMath
                        # .Poisson(n_inj*step,13.72)
                h3 .SetBinContent(bin_TS, sum_)
            if phi == 0:
                h3 .SetFillColor(5)
            elif phi == nhist - maxRange / 5:
                h3 .SetLineColor(kOrange + 1)
            else:
                h3 .SetLineColor(int(phi / 10))
            
            h3 = Beautify_axis_title(h3)
            ts_correct .push_back(h3)

        ts_anti_cumul = vector('TH1F*')()

        for i in range(0, nhist + 1):
            h2 = TH1F("ht_" + str(i), str(i) +
                      " Signal Events;#it{Q};probability(#it{Q}_{meas} > #it{Q})", len(bin_anti_cum) - 1, bin_anti_cum)
            # ts_correct[i].Scale(1./ts_correct[i].Integral())
            sum_ = 0
            h2.SetBinContent(1, 1 - sum_)  # to have the first bin full
            for j in range(1, int(ts_correct[i].GetNbinsX() + 1)):
                sum_ += ts_correct[i].GetBinContent(j) / \
                    ts_correct[i].Integral()
                h2.SetBinContent(j + 1, 1 - sum_)

            if i == 0:
                h2 .SetFillColor(5)
                h2 = Beautify_axis_title(h2)

            elif i == nhist - maxRange / 5:
                h2 .SetLineColor(kOrange + 1)
            else:
                h2 .SetLineColor(int(i / 10))
            
            h2 = Beautify_axis_title(h2)
            ts_anti_cumul .push_back(h2)

        c4 = TCanvas("c4", "c4", 1200, 800)
        c4.SetLogy()
        # c.cd(1)

        # leg = TLegend(.20,.60,.45,.90)
        leg = TLegend(.65, .60, .90, .90)
        leg.SetFillColor(kWhite)

        # legFlux = TLegend(.20,.60,.45,.90)
        legFlux = TLegend(.63, .60, .90, .90)
        legFlux.SetFillColor(kWhite)

        ts_anti_cumul[0].SetLineWidth(2)
        leg.AddEntry(ts_anti_cumul[0], ts_anti_cumul[0].GetTitle(), "f")
        legFlux.AddEntry(ts_anti_cumul[0], "Bckg only", "f")
        ts_anti_cumul[0] .GetXaxis().SetTitleSize(0.05)
        ts_anti_cumul[0] .GetYaxis().SetTitleSize(0.05)
        ts_anti_cumul[0] .GetXaxis().SetTitleOffset(0.95)
        ts_anti_cumul[0] .GetYaxis().SetTitleOffset(0.95)
        ts_anti_cumul[0].Draw()

        if "dry" not in sys.argv:
            # nhist+1, I remove the last curve because the TS is not correct (I
            # do not sum over n > mu_sig)
            for i in range((maxRange - 1) / 5 / step, nhist, (maxRange - 1) / 5 / step):
                ts_anti_cumul[i].SetLineWidth(2)
                leg.AddEntry(ts_anti_cumul[i],
                             ts_anti_cumul[i].GetTitle(), "l")
                legFlux.AddEntry(ts_anti_cumul[i], str(
                    int(i * flux_scale / nevts_KRAg * 100.) / 100.) + " * #Phi_{ref}", "l")
                ts_anti_cumul[i].Draw("same")

    # if not "ts_anti_cumul" in sys.argv:
    #     histos[0].SetTitle("")
    #     histos[0].SetMaximum(maximum*3)
    #     histos[0].SetMinimum(1)
        outfile.cd()
        c4.Write()
        c4.SaveAs("c4.pdf")

    gPad.RedrawAxis()
    if "nsig" in sys.argv:
        gPad.SetGridx()
        c.cd(1).SetLogy()
        leg.Draw("same")

    if "ts" in sys.argv:
        legFlux.Draw("same")

    if "nsig" in sys.argv:
        nsig2D = TH2F("nsig2D", "nsig2D;Nsig_{injected};Nsig_{fitted}-Nsig_{injected};Rate of Pseudo Experiment", nhist +
                      1, -0.5, nhist * step + 0.5, 101, -int(maxRange*2/3) - 0.5, int(maxRange*2/3) + 0.5)  # int((maxRange-1)),int((maxRange-1)*2))
        Mean = TH1F("Mean", "Mean", nhist + 1, -0.5, nhist * step + 0.5)
        Median = TH1F("Median", "Median", nhist + 1, -0.5, nhist * step + 0.5)
        Sigma = TH1F("Sigma", "Sigma;Nsig;Sigma",
                     nhist + 1, -0.5, nhist * step + 0.5)
        for i in range(0, nhist + 1):  # [1, 2]:
            for j in range(1, 101):
                nsig2D.Fill(
                    i * step, histos[i].GetXaxis().GetBinCenter(j), histos[i].GetBinContent(j))

            Median .Fill(i * step, get_quantile(histos[i], 0.5))
            Mean .Fill(i * step, fit[i].GetParameter(1))
            Sigma .Fill(i * step, abs(fit[i].GetParameter(2)))
        Mean .SetLineColor(1)
        Mean .SetLineWidth(2)
        Median .SetLineColor(2)
        Median .SetLineWidth(2)

        c.cd(2)
        c.cd(2).SetRightMargin(0.15)
        c.cd(2).SetGridy()
        nsig2D .Draw("colz")
        nsig2D.GetZaxis().SetTitleOffset(1.2)
        # Mean .Draw("same")
        Median .Draw("same")
        c.cd(2).SetLogz()

        c.cd(3)  # 3 = TCanvas("c3", "c3", 725, 550)
        Sigma .GetXaxis().SetTitleSize(0.05)
        Sigma .GetYaxis().SetTitleSize(0.05)
        Sigma = Beautify_axis_title(Sigma)
        Sigma .Draw()

        c.Update()

    if "ts" in sys.argv:
        Disco = TH1F(
            "Disco_", "Discovery probability at 3 sigma;Number of injected signal events;Discovery probability [%]", nhist + 1, -0.5, maxRange * flux_scale - 0.5)
        Disco_flux = TH1F("Disco_flux", "Discovery probability at 3 sigma;#Phi_{signal} / #Phi_{KRA#gamma};Discovery probability [%]", nhist + 1, float(-0.5) / float(
            nevts_KRAg), float(maxRange * flux_scale - 0.5) / float(nevts_KRAg))
        # print nhist+1,0,float(maxRange-1)/float(nevts_KRAg)*100.

        Nsteps = 50
        b3sig = 0  # bin 3 sigma
        bUL_med = 0
        UpLim_med = 0
        bUL = [0] * (2 * Nsteps + 1)  # bins
        TS = [0] * Nsteps
        UpLim = [56 * flux_scale] * Nsteps
        pdf_bg = [0] * Nsteps

        for i in range(1, ts_anti_cumul[0].GetNbinsX() + 1):
            # 4.55e-2: # <- Pour 2sigma
            if b3sig == 0 and ts_anti_cumul[0].GetBinContent(i) <= 2.7e-3:
                b3sig = i
                print "\nWe exclude the bckg hypothesis at 3 sigma for TS =", ts_anti_cumul[0].GetXaxis().GetBinCenter(b3sig), b3sig

            # est equivalent avec plus que 0.5
            if bUL_med == 0 and ts_anti_cumul[0].GetBinContent(i) <= 0.5:
                bUL_med = i
                print "\nThe median upper limit is at a TS =", ts_anti_cumul[0].GetXaxis().GetBinCenter(bUL_med), bUL_med

            for j in range(len(bUL)):
                # The 1e-10 is here for when bin content is should be == 0
                if bUL[j] == 0 and ts_anti_cumul[0].GetBinContent(i) <= j / (2. * Nsteps) + 1e-10:
                    bUL[j] = i

            for j in range(len(TS)):
                if TS[j] == 0 and ts_anti_cumul[0].GetBinContent(i) <= j / (1. * Nsteps) + 1. / (Nsteps * 2.):
                    TS[j] = ts_anti_cumul[0].GetBinCenter(i)

        # print TS
        # print bUL

        for i in range(1, nhist + 1):
            if ts_anti_cumul[i].GetBinContent(bUL_med) >= 0.9 and UpLim_med == 0:
                UpLim_med = i * flux_scale
                # UL : The number of sig evts that we are sure at 90%
                # confidence level to not have when we have a TS which is the
                # median of the TS distribution of the background, ie a TS of
                # background
                print "\nThe median upper limit is", UpLim_med * step, "+/-", step - 1, "=", UpLim_med * step / nevts_KRAg, "time the KRAgamma flux"

            for j in range(len(bUL) / 2):
                pdf_bg[j] = sum([ts_correct[0] .GetBinContent(bins) * ts_correct[0] .GetBinWidth(bins) for bins in range(
                    bUL[(j + 1) * 2], bUL[j * 2])]) / (ts_correct[0] .GetBinLowEdge(bUL[j * 2]) - ts_correct[0] .GetBinLowEdge(bUL[(j + 1) * 2]))
                if UpLim[j] == 56 * flux_scale and ts_anti_cumul[i].GetBinContent(bUL[j * 2 + 1]) >= 0.9:
                    UpLim[j] = i * flux_scale

            Proba3sig = ts_anti_cumul[i].GetBinContent(b3sig)
            Disco .Fill(i * flux_scale * step, Proba3sig * 100)
            Disco_flux .Fill(float(i * flux_scale * step) /
                             float(nevts_KRAg), Proba3sig * 100)
            # print float((i+1)*step)/float(nevts_KRAg)*100.,Proba3sig

        scaling = sum([pdf_bg_ for pdf_bg_ in pdf_bg])
        # print pdf_bg, '\n\n', scaling
        # print '\n', bUL, '\n'
        UpLim_mean = sum([UpLim_ * pdf_bg_ for (UpLim_, pdf_bg_)
                          in zip(UpLim, pdf_bg)]) / scaling
        # print [UpLim_ * pdf_bg_ / scaling for (UpLim_, pdf_bg_) in zip(UpLim, pdf_bg)]
        # print UpLim
        # UL : The number of sig evts that we are sure at 90% confidence level
        # to not have when we have a TS which is the median of the TS
        # distribution of the background, ie a TS of background
        print "The average upper limit is", UpLim_mean * step, "+/-", step - 1, "=", UpLim_mean * step / nevts_KRAg, "time the KRAgamma flux"

        UL_array = array.array('f')
        TS_array = array.array('f')
        for i in range(len(UpLim)):
            if UpLim[i] != 56 * flux_scale:
                UL_array.append(UpLim[i] * step / nevts_KRAg)
                TS_array.append(TS[i])
            else:
                print "\nWarning ! There is a UL bigger than flux giving a mean of", 55 * flux_scale, "events (" + str(UpLim[i]) + "). We put the flux giving a mean of", 56 * flux_scale, "events for computing the sum instead and we remove it from the plot."
                print "Change 'flux_scale' variable to 1 to correct this problem... but it will change the fluxes ploted"
                print "'flux_scale' should be different to 1 only to make plots"

        # print UL_array, '\n', TS_array

        UL_vs_TS = TGraph(len(TS_array), TS_array, UL_array)
        UL_vs_TS .SetTitle("values of the UL for different values of TS")
        UL_vs_TS .SetMarkerStyle(20)
        UL_vs_TS .SetMarkerColor(1)
        UL_vs_TS .SetLineWidth(2)
        UL_vs_TS .SetLineColor(1)
        UL_vs_TS .GetXaxis().SetTitle("#it{Q}")
        UL_vs_TS .GetYaxis().SetTitle(
            "#Phi_{Upper Limit (90% CL)} / #Phi_{KRA#gamma}")
        UL_vs_TS .GetXaxis().SetTitleSize(0.05)
        UL_vs_TS .GetYaxis().SetTitleSize(0.05)

        # c2 = TCanvas("c2", "c2", 725, 550)
        c.cd(2)
        c.cd(2).SetGridx()
        c.cd(2).SetGridy()
        UL_vs_TS .Draw('acp')

        Fit_file = TFile(
            "/sps/km3net/users/tgregoir/gal_plane/software/Sensitivity/Fit_UL_vs_TS_map3.root")
        Fit_UL = Fit_file .Get("PrevFitTMP")
        legFit = TLegend(.63, .1, .90, .25)
        legFit .SetFillColor(kWhite)
        legFit .AddEntry(Fit_UL, "Fit with (ax+b)(1-c#exp{dx})", "l")
        c.cd(2).Update()

        # Disco.Draw()
        # c3 = TCanvas("c3", "c3", 725, 550)
        c.cd(3)
        c.cd(3).SetGridx()
        c.cd(3).SetGridy()
        Disco_flux .SetLineWidth(2)
        # Disco_flux .GetXaxis().SetTitleOffset(0.9)
        Disco_flux .GetYaxis().SetRangeUser(0, 100)
        Disco_flux .GetXaxis().SetTitleSize(0.05)
        Disco_flux .GetYaxis().SetTitleSize(0.05)
        Disco_flux = Beautify_axis_title(Disco_flux)
        Disco_flux .Draw()

        # c5 = TCanvas("c5", "c5", 1200, 800)
        c.cd(4)
        c.cd(4).SetLogy()
        # leg = TLegend(.20,.60,.45,.90)
        # leg = TLegend(.65,.60,.90,.90)
        # leg.SetFillColor(kWhite)

        # leg.AddEntry(ts_correct[0], ts_correct[0].GetTitle(), "f")

        for i in range(0, nhist + 1):
            for j in range(1, int(ts_correct[i].GetNbinsX() + 1)):
                ts_correct[i].SetBinContent(
                    j, ts_correct[i].GetBinContent(j) / ts_correct[i].GetBinWidth(j))
                histos[i].SetBinContent(
                    j, histos[i].GetBinContent(j) / histos[i].GetBinWidth(j))

        print "\nThe discovery probability of the KRAgamma model is", Disco_flux .GetBinContent(Disco_flux .FindBin(1)), "%\n"

        ts_correct[0].GetYaxis().SetRangeUser(1, 1e11)
        ts_correct[0].SetTitle(
            "PDF of the TS for a flux with a mean of <#mu_{sig}> signal events")

        integ = 0
        for j in range(1, int(ts_correct[0].GetNbinsX() + 1)):
            integ += ts_correct[0].GetBinContent(j) * \
                ts_correct[0].GetBinWidth(j)
        ts_correct[0].Scale(1. / integ)

        # ts_correct[0].GetYaxis().SetTitleOffset(1)
        ts_correct[0].GetYaxis().SetRangeUser(5e-5, 3e4)
        ts_correct[0].GetXaxis().SetRangeUser(0, 25)
        ts_correct[0].SetLineWidth(2)
        ts_correct[0] .GetXaxis().SetTitleSize(0.05)
        ts_correct[0] .GetYaxis().SetTitleSize(0.05)
        ts_correct[0].Draw()

        # integ = 0
        # for j in range(1, int(ts_correct[0].GetNbinsX() + 1)):
        #     integ += ts_correct[0].GetBinContent(j) * ts_correct[0].GetBinWidth(j)
        # ts_correct[0].Scale(1. / integ)
        #
        # ts_correct[0].SetLineWidth(2)
        # ts_correct[0].Draw("same")
        # 14,nhist,(maxRange-1)/5/step)):#nhist+1, I remove the last curve
        # because the TS is not correct (I do not sum over n > mu_sig)
        for i in reversed(range((maxRange - 1) / 5 / step, nhist, (maxRange - 1) / 5 / step)):
            # leg.AddEntry(ts_correct[i], ts_correct[i].GetTitle(), "l")
            ts_correct[i].SetLineWidth(2)

            integ = 0
            for j in range(1, int(ts_correct[i].GetNbinsX() + 1)):
                integ += ts_correct[i].GetBinContent(j) * \
                    ts_correct[i].GetBinWidth(j)
            ts_correct[i].Scale(1. / integ)
            print i * flux_scale

            ts_correct[i].Draw("same")
        legFlux.Draw("same")

        c.cd(1)
        c.cd(1).SetLogy()
        c.cd(1).SetGridx()
        c.cd(1).SetGridy()
        ts_anti_cumul[0] .GetXaxis().SetTitleSize(0.05)
        ts_anti_cumul[0] .GetYaxis().SetTitleSize(0.05)
        
        ts_anti_cumul[0] .Draw()

        c2 = TCanvas("c2", "c2", 1200, 800)
        c2.SetLogy()
        ts_correct[0] .GetXaxis().SetTitleSize(0.05)
        ts_correct[0] .GetYaxis().SetTitleSize(0.05)
        
        ts_correct[0] .Draw()

        # histos[0].GetYaxis().SetRangeUser(1, 2e10)
        # histos[0].SetTitle("PDF of the TS for n signal events")
        # histos[0].Draw()
        # for i in reversed(range((maxRange-1)/5/step,nhist,(maxRange-1)/5/step)):#nhist+1, I remove the last curve because the TS is not correct (I do not sum over n > mu_sig)
        #     # leg.AddEntry(histos[i], histos[i].GetTitle(), "l")
        #     histos[i].Draw("same")
        # leg.Draw("same")
        c.Update()
        outfile.cd()
        c2.Write()
        c2.SaveAs("c2.pdf")
    # outfile.Write()
    c.Write()
    c.SaveAs("c.pdf")
