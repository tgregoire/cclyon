#!/usr/local/bin/python -i

import sys,os,glob
from math import *
from os.path import expandvars
sys.path.append(os.environ['ROOTSYS']+'/lib/root')
from ROOT import *

if gSystem.Load('goofit.so') < 0  : sys.exit()
print '  +--------------------------+'
print '  |  Goof-it library loaded  |'
print '  +--------------------------+'
transformation = None  # global so we have acces to it if we want
extent = "0"

comb = "solo" not in sys.argv
gStyle.SetOptStat(0)

# Specify inputs
# ==============

sh_cut = 'ChargeRatio'
gamma  = '2'
aeff_file_t = "/sps/km3net/users/tgregoir/ps_search/ingredient_files/ingredients_AaFit_Em" +gamma+".root"
aeff_file_s = "/sps/km3net/users/tgregoir/ps_search/ingredient_files/ingredients_TANTRA_Em"+gamma+".root"

# open the aeff file (only once for speed)
f_aeff_t =  TFile(aeff_file_t)
if not f_aeff_t :
    print " failed to open file",aeff_file_t
    sys.exit()
f_aeff_s =  TFile(aeff_file_s)
if not f_aeff_s :
    print " failed to open file",aeff_file_s
    sys.exit()

h_acc_t = f_aeff_t.Get( "h_acceptance_N52_10" )
h_acc_s = f_aeff_s.Get( "h_acceptance_"+sh_cut )


# a few globals for easy debugging
gmedian     = None
lambg_extra = None
lambg       = None

def extrapolate( h , tailsize = 0.05 , opt = "0"):

    # fit the last 5 % of the distribution
    x1 = get_quantile( h, 1 - tailsize )
    x2 = h.GetBinCenter( h.GetNbinsX() )
    b1 = h.FindBin( x1 )
    print " extrapolate " , h.GetName() ,"using bin",b1, "and onwards"

    nn = sum( h.GetBinContent(b) > 0 for b in range(b1,h.GetNbinsX()+1) )
    print "have ",nn,"non-empty bins for the fit"

    f = TF1("f","expo", x1, 100)
    h.Fit(f,"LR"+opt)

    r = h.Clone( h.GetName()+"extra")
    r.SetDirectory(0)
    for b in range ( b1, r.GetNbinsX()+1 ) :
        if r.GetBinContent(b) > 0 : continue
        r.SetBinContent( b, f.Eval( h.GetBinCenter(b) ))
    r.Scale( 1.0 / h.Integral() )
    return r


def p_value( number_of_sigmas ):
    "I think this convention is called 'double sided'"
    return 1 - 1*TMath.Erf( number_of_sigmas /sqrt(2.0))

def number_of_sigmas( p_value ):
    "I think this convention is called 'double sided'"
    return sqrt(2.0) * TMath.ErfInverse( 1 - p_value )


def get_discovery_hist( declination_deg, significance_nsigma , mu_max = 30):

    name = "h_discpot_"+str(declination_deg)+"_"+str(significance_nsigma)
    if "extended" in sys.argv:
        name += "_extended_"+extent

    r = TH1F( name, name, mu_max*100, 0, mu_max )

    v = get_lambda_histograms( declination_deg , transform = 0 )

    global lambg
    global lambg_extra

    lambg = v[0].Clone()
    lambg_extra = extrapolate ( lambg )
    lambg.SetFillColor(2)

    r.pval        = p_value ( significance_nsigma )
    r.lambda_crit = get_quantile( lambg_extra , 1-r.pval )
    print "lambda_crit = ",r.lambda_crit," for significance:",significance_nsigma," p-value =", r.pval

    for b in range( 1, r.GetNbinsX()+1) :
        mu = r.GetBinCenter( b )
        hh = mix_poisson( v, mu )
        P = 1.0 - get_fraction_below( hh, r.lambda_crit )
        r.SetBinContent( b, P )

    r.mu10 = findx( r, 0.10 )
    r.mu50 = findx( r, 0.50 )
    r.mu90 = findx( r, 0.90 )

    return r





def compute_flux( declination_deg, nsig ):

    global h_acc_t
    global h_acc_s

    if not h_acc_t or (comb and not h_acc_s):
        print "failed to find acceptance histogram"
        sys.exit()

    a = h_acc_t.GetBinContent( h_acc_t.FindBin(sin(declination_deg/TMath.RadToDeg())) )
    if comb:
        a += h_acc_s.GetBinContent( h_acc_s.FindBin(sin(declination_deg/TMath.RadToDeg())) )
    # a is the number of events for a flux of 1e-8, so for N events we need af flux:
    return nsig / a * 1e-8


def get_chain( pat ):

    files = glob.glob( pat )



def get_lambda_histograms( declination_deg ,
                           transform = 0 , what="Pex.showerclus.fit_TS" ) :
    if not comb:
        what="Pex.clus.fit_TS"

    """Get the requested histograms from the corresponding file in pe_dir
       Returns a std::vector of the histograms."""


    global pe_dir



    v = vector('TH1F*')()

    tree_name = "T"
    pe_dir = '/afs/in2p3.fr/home/t/tgregoir/sps/ps_search/CPEx_output/'
    if gamma != "2":
        pe_dir = '/afs/in2p3.fr/home/t/tgregoir/sps/ps_search/CPEx_'+gamma+'/'

    for n in range( 0, 31 ):

        if comb:
            file_name = pe_dir + "CPEx_fixed_TC_10_SC_15_E_"+str(n)+"_D_"+str(declination_deg)+".root"
            if "full" in sys.argv:
                file_name = pe_dir + "CPEx_full_TC_3_SC_10_E_"+str(n)+"_D_"+str(declination_deg)+".root"
            if "galactic" in sys.argv:
                file_name = pe_dir + "CPEx_galactic_TC_10_SC_15_E_"+str(n)+"_D_"+str(declination_deg)+".root"
            if "extended" in sys.argv:
                if "noExtFit" in sys.argv:
                    file_name = pe_dir + "CPEx_extended_noExtFit_TC_15_SC_20_Ex_"+extent+"_E_"+str(n)+".root"
                else:
                    file_name = pe_dir + "CPEx_extended_TC_15_SC_20_Ex_"+extent+"_E_"+str(n)+".root"
            if "galacplane" in sys.argv:
                file_name = pe_dir + "CPEx_galacticPlane_E_"+str(n)+".root"
        else:
            file_name = pe_dir + "CPEx_fixed_B_1_C_10_trE_"+str(n)+"_shE_0_D_"+str(declination_deg)+".root"


        #file_name = pe_dir + "/CPEx_fixed_shift_TC_10_SC_15_E_"+str(n)+"_D_"+str(declination_deg)+".root"

        f = TFile( file_name )
        T = f.Get( tree_name )

        if not T :
            print "failed to get TTree : ", tree_name, "in file:",file_name
            h = TH1F("h_"+str(n),"h"+str(n),400,-1,200)
            SetOwnership(h, False )
            v.push_back(h)
            continue



        if transform == 0 :
            h = TH1F("h_"+str(n),"h"+str(n),400,-1,200)
            T.Draw(what+">>"+h.GetName(),"","goff")
        else :
            h = TH1F("ht_"+str(n),"h"+str(n),64000,-4,3)
            T.Draw("log10("+what+"+1.5e-3)>>"+h.GetName(),"","goff")

        h.SetDirectory(0)
        if n==0 : h.SetFillColor(5)
        else    : h.SetLineColor(n)

        if h.Integral(): h.Scale ( 1.0 / h.Integral())
        SetOwnership(h, False )
        v.push_back(h)


    if transform == 2 :

        # To apply FC, we transform lambda into some function f(x) so that
        # f(v[0]+v[10]) is a flat distribution. The 0 and 10 are arbitrary, but
        # seem to work ~well.

        hhh = v[0].Clone("hhh")
        hhh.Add(v[10])
        hhh.Scale ( 1.0 / hhh.Integral() )

        global transformation
        transformation = Transform( hhh )
        vt = vector('TH1F*')()
        for h in v :
            vt.push_back (  transformation.apply( h, 400 ) )
        return vt
    else :
        return v

    # end


def get_limit_on_nsig ( lambda_obs, declination ):

    v = get_lambda_histograms( declination,
                               transform = 2 )

    return compute_limit_neyman(v,lambda_obs)


def get_median_limit_on_nsig ( declination ):
    v = get_lambda_histograms( declination,
                               transform = 2 )

    gmedian = get_quantile( v[0], 0.5 )
    return compute_limit_neyman(v,gmedian)






if __name__ == "__main__":

    # ================================================================================
    #   main part
    # ================================================================================


    if 'sys' in sys.argv:
        set_acceptance_uncertainty( 0.15 )
        fout_name = "sensitivity_fixed_sys.root"
    else :
        set_acceptance_uncertainty( 0. )
        fout_name = "sensitivity_fixed.root"
    if  'full' in sys.argv:
        fout_name = fout_name.replace("fixed", "full")
    elif 'galactic' in sys.argv:
        fout_name = fout_name.replace("fixed", "galactic")
    elif 'extended' in sys.argv:
        fout_name = fout_name.replace("fixed", "extended")
    elif 'galacplane' in sys.argv:
        fout_name = fout_name.replace("fixed", "galacplane")


    if "noExtFit" in sys.argv:
        fout_name = fout_name.replace("extended", "extended_noExtFit")
    if gamma != "2":
        fout_name = fout_name.replace("galactic", "galactic_Em"+gamma)

    #fout_name = "sensitivity_dummy.root"

    fout =  TFile(fout_name,"RECREATE")



    viewit = True



    clim = TCanvas("clim","clim",900,1000)
    clim.Divide(2,3)


    if "full" in sys.argv or "galacplane" in sys.argv:
        nbins, dec_min, dec_max = 14, -95, 45
    elif "galactic" in sys.argv:
        nbins, dec_min, dec_max = 12, -57.5, 2.5
    elif "extended" in sys.argv:
        nbins, dec_min, dec_max = 11, -.25, 5.25
    else:
        nbins, dec_min, dec_max = 27, -92.5, 42.5



    h_ney  = TH1F( 'h_lim_nsig' , 'h_lim_nsig_;#delta / #circ;N_{Signal}', nbins,dec_min, dec_max )
    hf_ney = TH1F( 'hf_lim_nsig', 'hf_lim_nsig;#delta / #circ;d#Phi/dE E^{2} / (GeV cm^{-2} s^{-1})', nbins,dec_min, dec_max )

    g_lim_events = TGraph()
    g_lim_flux   = TGraph()

    for g in [ g_lim_events, g_lim_flux ]:
        g.SetMarkerSize(1)
        g.SetLineWidth(2)
        g.SetLineColor( 4 )
        g.SetMarkerStyle(20)




    disc_events =  TH1F( 'disc_events', 'disc_events;#delta / #circ;N_{Signal}', nbins,dec_min, dec_max )
    disc_flux   =  TH1F( 'disc_flux',   'disc_flux;#delta / #circ;d#Phi/dE E^{2} / (GeV cm^{-2} s^{-1})',   nbins,dec_min, dec_max)

    g_disc_events = TGraph()
    g_disc_flux   = TGraph()
    g_disc_events.SetMarkerSize(1)
    g_disc_flux  .SetMarkerSize(1)


    g_IC = TGraph()
    g_IC.SetPoint( g_IC.GetN(), asin(-0.97525775 )*TMath.RadToDeg(),  1.4171741E-8 )
    g_IC.SetPoint( g_IC.GetN(), asin(-0.9257732  )*TMath.RadToDeg(),  1.735604E-8  )
    g_IC.SetPoint( g_IC.GetN(), asin(-0.87216496 )*TMath.RadToDeg(),  1.96005E-8   )
    g_IC.SetPoint( g_IC.GetN(), asin(-0.8268041  )*TMath.RadToDeg(),  1.7007791E-8 )
    g_IC.SetPoint( g_IC.GetN(), asin(-0.7814433  )*TMath.RadToDeg(),  1.5683281E-8 )
    g_IC.SetPoint( g_IC.GetN(), asin(-0.7237113  )*TMath.RadToDeg(),  1.4171741E-8 )
    g_IC.SetPoint( g_IC.GetN(), asin(-0.6618557  )*TMath.RadToDeg(),  1.3068093E-8 )
    g_IC.SetPoint( g_IC.GetN(), asin(-0.62474227 )*TMath.RadToDeg(),  1.2050394E-8 )
    g_IC.SetPoint( g_IC.GetN(), asin(-0.58350515 )*TMath.RadToDeg(),  1.1808602E-8 )
    g_IC.SetPoint( g_IC.GetN(), asin(-0.5463917  )*TMath.RadToDeg(),  1.004099E-8  )
    g_IC.SetPoint( g_IC.GetN(), asin(-0.5010309  )*TMath.RadToDeg(),  8.537968E-9  )
    g_IC.SetPoint( g_IC.GetN(), asin(-0.4556701  )*TMath.RadToDeg(),  7.408586E-9  )
    g_IC.SetPoint( g_IC.GetN(), asin(-0.40206185 )*TMath.RadToDeg(),  6.428595E-9  )
    g_IC.SetPoint( g_IC.GetN(), asin(-0.3360825  )*TMath.RadToDeg(),  5.0406103E-9 )
    g_IC.SetPoint( g_IC.GetN(), asin(-0.28659794 )*TMath.RadToDeg(),  4.554801E-9  )
    g_IC.SetPoint( g_IC.GetN(), asin(-0.24948454 )*TMath.RadToDeg(),  4.0332295E-9 )
    g_IC.SetPoint( g_IC.GetN(), asin(-0.22061856 )*TMath.RadToDeg(),  3.64451E-9   )
    g_IC.SetPoint( g_IC.GetN(), asin(-0.19587629 )*TMath.RadToDeg(),  3.0367878E-9 )
    g_IC.SetPoint( g_IC.GetN(), asin(-0.17938145 )*TMath.RadToDeg(),  2.5822155E-9 )
    g_IC.SetPoint( g_IC.GetN(), asin(-0.15463917 )*TMath.RadToDeg(),  2.1956874E-9 )
    g_IC.SetPoint( g_IC.GetN(), asin(-0.12989691 )*TMath.RadToDeg(),  1.7928463E-9 )
    g_IC.SetPoint( g_IC.GetN(), asin(-0.11752577 )*TMath.RadToDeg(),  1.493889E-9  )
    g_IC.SetPoint( g_IC.GetN(), asin(-0.10515464 )*TMath.RadToDeg(),  1.2198063E-9 )
    g_IC.SetPoint( g_IC.GetN(), asin(-0.08865979 )*TMath.RadToDeg(),  1.0584531E-9 )
    g_IC.SetPoint( g_IC.GetN(), asin(-0.08041237 )*TMath.RadToDeg(),  8.642595E-10 )
    g_IC.SetPoint( g_IC.GetN(), asin(-0.055670105)*TMath.RadToDeg(),  7.499373E-10 )
    g_IC.SetPoint( g_IC.GetN(), asin(0.006185567 )*TMath.RadToDeg(),  6.3768023E-10)
    g_IC.SetPoint( g_IC.GetN(), asin(0.04742268  )*TMath.RadToDeg(),  6.5073733E-10)
    g_IC.SetPoint( g_IC.GetN(), asin(0.08453608  )*TMath.RadToDeg(),  7.2014416E-10)
    g_IC.SetPoint( g_IC.GetN(), asin(0.12989691  )*TMath.RadToDeg(),  7.9695387E-10)
    g_IC.SetPoint( g_IC.GetN(), asin(0.1628866   )*TMath.RadToDeg(),  7.8096297E-10)
    g_IC.SetPoint( g_IC.GetN(), asin(0.20824742  )*TMath.RadToDeg(),  7.9695387E-10)
    g_IC.SetPoint( g_IC.GetN(), asin(0.25360826  )*TMath.RadToDeg(),  9.000148E-10 )
    g_IC.SetPoint( g_IC.GetN(), asin(0.3072165   )*TMath.RadToDeg(),  9.564403E-10 )
    g_IC.SetPoint( g_IC.GetN(), asin(0.36494845  )*TMath.RadToDeg(),  9.000148E-10 )
    g_IC.SetPoint( g_IC.GetN(), asin(0.43092784  )*TMath.RadToDeg(),  1.0164034E-9 )
    g_IC.SetPoint( g_IC.GetN(), asin(0.52577317  )*TMath.RadToDeg(),  1.2198063E-9 )
    g_IC.SetPoint( g_IC.GetN(), asin(0.58762884  )*TMath.RadToDeg(),  1.1953308E-9 )
    g_IC.SetPoint( g_IC.GetN(), asin(0.6371134   )*TMath.RadToDeg(),  1.2198063E-9 )
    g_IC.SetPoint( g_IC.GetN(), asin(0.6742268   )*TMath.RadToDeg(),  1.2198063E-9 )
    g_IC.SetPoint( g_IC.GetN(), asin(0.73195875  )*TMath.RadToDeg(),  1.3775499E-9 )
    g_IC.SetPoint( g_IC.GetN(), asin(0.785567    )*TMath.RadToDeg(),  1.4639141E-9 )
    g_IC.SetPoint( g_IC.GetN(), asin(0.83092785  )*TMath.RadToDeg(),  1.493889E-9  )
    g_IC.SetPoint( g_IC.GetN(), asin(0.8556701   )*TMath.RadToDeg(),  1.721621E-9  )
    g_IC.SetPoint( g_IC.GetN(), asin(0.8804124   )*TMath.RadToDeg(),  1.984069E-9  )
    g_IC.SetPoint( g_IC.GetN(), asin(0.9134021   )*TMath.RadToDeg(),  2.2865254E-9 )
    g_IC.SetPoint( g_IC.GetN(), asin(0.93402064  )*TMath.RadToDeg(),  2.6350885E-9 )
    g_IC.SetPoint( g_IC.GetN(), asin(0.9670103   )*TMath.RadToDeg(),  3.2932556E-9 )
    g_IC.SetLineColor(kRed)


    for h in (h_ney,hf_ney) :
        h.SetDirectory(fout)
        SetOwnership(h,False)
        h.SetLineWidth(2)
        h.SetLineColor( 4 )
        h.SetMarkerStyle(20)


    loop_range = range( 1, h_ney.GetNbinsX()+1)
    if "galactic" in sys.argv:
        loop_range = [ -48.9548, -45.2462, -41.3519, -37.315, -33.1681, -29.01, -24.6384, -20.29, -15.9034, -11.4886, -7.0546 ]
    elif "extended" in sys.argv:
        loop_range = [ "0","0.5", "1", "2", "3", "4", "5" ]

    for b in loop_range :
        if "extended" in sys.argv:
            decl = -29.01
            extent = b
        elif "galactic" in sys.argv:
            decl = b
        else:
            decl = int ( "%.0f" % h_ney.GetBinCenter(b) ) # prevents rounding badness

        #if decl != -70: continue
        # first get the discovery potentials
        hdisc_3sigma = get_discovery_hist(decl, 3 )
        hdisc_5sigma = get_discovery_hist(decl, 5 )
        fout.cd()
        hdisc_3sigma.Write()
        hdisc_5sigma.Write()
        print "got the discpot"
        clim.cd(5)
        hdisc_3sigma.Draw()
        clim.cd(6)
        hdisc_5sigma.Draw()


        # calculate discovery hits and flux with bisection method
        left, right = 0, 30
        middle = (left+right)/2.
        counter = 0
        while abs( hdisc_5sigma.Interpolate( middle ) - .5 ) > .001:
            if hdisc_5sigma.Interpolate( middle ) > .5:
                right = middle
            else:
                left = middle
            middle = (left+right)/2.
            counter += 1
            if counter > 1000:
                print "bisection method failed!"
                break

        flux = compute_flux(decl, middle )
        limit = get_median_limit_on_nsig ( decl )
        flux_limit = compute_flux(decl, limit )

        print "DISC : %4d %4.3f %g" % ( decl, middle, flux )
        print "LIM  : %4d %4.3f %g" % ( decl, limit ,flux_limit )


        if "extended" in sys.argv:
            g_disc_events.SetPoint(g_disc_events.GetN(), float(extent), middle)
            g_disc_flux  .SetPoint(g_disc_flux  .GetN(), float(extent), flux)
            g_lim_events .SetPoint(g_lim_events .GetN(), float(extent), limit)
            g_lim_flux   .SetPoint(g_lim_flux   .GetN(), float(extent), flux_limit)
        else :
            g_disc_events.SetPoint(g_disc_events.GetN(), decl, middle)
            g_disc_flux  .SetPoint(g_disc_flux  .GetN(), decl, flux)
            g_lim_events.SetPoint(g_lim_events.GetN(), decl,      limit)
            g_lim_flux  .SetPoint(g_lim_flux  .GetN(), decl, flux_limit)


        if viewit :
            clim.cd(1)
            if "full" in sys.argv or "galactic" in sys.argv:
                h_ney.SetMinimum(0)
                h_ney.SetMaximum(10)
            else:
                h_ney.SetMaximum(5)
                h_ney.SetMinimum(0)
            h_ney.Draw('L')
            g_lim_events.Draw("lp same")
            gPad.SetGridy()

            clim.cd(2)
            if "full" in sys.argv:
                hf_ney.SetMinimum(5e-9)
                hf_ney.SetMaximum(1e-6)
            elif "galactic" in sys.argv:
                hf_ney.SetMinimum(1e-9)
                hf_ney.SetMaximum(1e-7)
            else:
                hf_ney.SetMinimum(1e-9)
                hf_ney.SetMaximum(1e-7)
            hf_ney.Draw('L')
            g_lim_flux.Draw("lp same")
            gPad.SetLogy()
            gPad.SetGridy()
            if "full" not in sys.argv and "galactic" not in sys.argv and "extended" not in sys.argv:
                g_IC.Draw('L same')
                legend = TLegend(.65,.7,.95,.95)
                legend.SetFillColor(kWhite)
                legend.AddEntry(hf_ney, "Sensitivity ANTARES", "l")
                legend.AddEntry(g_IC,   "Sensitivity IceCube", "l")
                legend.Draw("same")

            clim.cd(3)
            if "full" in sys.argv or "galactic" in sys.argv or "extended" in sys.argv:
                disc_events.SetMinimum(0)
                disc_events.SetMaximum(20)
            else:
                disc_events.SetMinimum(0)
                disc_events.SetMaximum(10)
            disc_events.Draw("L")
            g_disc_events.Draw("LP same")

            gPad.SetGridy()

            clim.cd(4)
            if "full" in sys.argv:
                disc_flux.SetMinimum(1e-8)
                disc_flux.SetMaximum(1e-6)
            else:
                disc_flux.SetMinimum(1e-8)
                disc_flux.SetMaximum(1e-7)
            disc_flux.Draw("L")
            g_disc_flux.Draw("LP same")
            gPad.SetLogy()
            gPad.SetGridy()

            clim.Update()

            clim.cd(5)
            if decl == -70:

                hdisc_3sigma.SetLineColor(kRed)
                hdisc_5sigma.SetLineColor(kBlue)
                hdisc_3sigma.DrawCopy()
                hdisc_5sigma.DrawCopy("same")

                gPad.SetGrid()

                legend = TLegend(.55, .2, .9, .5, "#delta = "+str(decl)+"#circ")
                legend.SetFillColor(kWhite)
                legend.AddEntry(hdisc_3sigma.Clone(), "3 #sigma", "l")
                legend.AddEntry(hdisc_5sigma.Clone(), "5 #sigma", "l")
                legend.Draw("same")


    fout.Write()

    fout.cd()
    g_IC         .Write("g_IC")
    g_lim_events .Write("g_lim_events",5)
    g_lim_flux   .Write("g_lim_flux",5)
    g_disc_events.Write("g_disc_events",5)
    g_disc_flux  .Write("g_disc_flux",5)



    fout.Close()
