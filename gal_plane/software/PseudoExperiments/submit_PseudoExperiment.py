#!/usr/bin/env python

import os
import os.path
import sys
import glob
import popen2
import time
from os.path import expandvars


dry_run      = 'dry' in sys.argv  # run only first job, run interactively instead submitting to queue
test_job     = 'test' in sys.argv  # run only first job, submit to queue
fix_job      = 'fixed' in sys.argv
float_job    = 'float' in sys.argv
declscan     = 'scan' in sys.argv  # inject signal at different declinations
candidates   = 'candidates' in sys.argv  # inject signal at (declination, right-ascension) from candidate lists and keep highest TS
galactic     = 'galactic' in sys.argv
galacplane   = 'galacplane' in sys.argv  # inject signal in function of the galactic plane map (template of neutrino emission)
galacplaneData = 'galacplaneData' in sys.argv  # Use a ntuple with the unblinded data to compute the TS
extended     = 'extended' in sys.argv
lesstrials   = 'lesstrials' in sys.argv  # jobs with only 10 trials and put the result in CPEx_output_test
date         = 'date' in sys.argv  # to put the date in outdir. Write the date in option in third place
comb         = 'comb' in sys.argv  # to produce the curves for the combined analysis


for i in range(len(sys.argv)):
    if sys.argv[i] == "tnbg":
        tnbg = sys.argv[i + 1]
    if sys.argv[i] == "snbg":
        snbg = sys.argv[i + 1]


# galacplane = True
if candidates:
    if not (fix_job or float_job or galactic):
        print "candidates but neither fixed, float nor galactic flag are set"
        sys.exit(0)


ShCut = "ChargeRatio"

if 'BATCH_SYSTEM' not in os.environ or os.environ["BATCH_SYSTEM"] != "GE":
    print " it seems you did not run the ge_env script"
    sys.exit()

exe     = expandvars("/sps/km3net/users/tgregoir/gal_plane/software/PseudoExperiments/run_PseudoExperiment.py")
if lesstrials:
    outdir  = expandvars("/sps/km3net/users/tgregoir/gal_plane/CPEx_output_test/")
elif date:
    for i in range(len(sys.argv)):
        if sys.argv[i] == "date":
            date_ = sys.argv[i + 1]
    os.system("mkdir /sps/km3net/users/tgregoir/gal_plane/CPEx_output_" + date_ + "/")
    outdir  = expandvars("/sps/km3net/users/tgregoir/gal_plane/CPEx_output_" + date_ + "/")
else:
    outdir  = expandvars("/sps/km3net/users/tgregoir/gal_plane/CPEx_output/")

#  outdir  = expandvars("~/sps/gal_plane/CPEx_shift/")


gammas = ["2"]
if "gamma" in sys.argv:
    gammas = ["2", "2.5", "3"]


tconesize = ['3']  # , '15', '5']
sconesize = ['10']  # , '15', '5']

extensions = ['0']

events  = [str(i) for i in range(0, 31)]

SourcePos = [['-70', '100']]

trials    = '1'  # allways gets overwritten, look below
ray       = ['10']

Nbit = 1

if fix_job:
    trials = '10000'
    tconesize = ['10']  # , '15', '5']
    sconesize = ['15']  # , '15', '5']
elif float_job:
    trials = '10000'
    SourcePos = []
    infile2 = open('../CandidateList_float.log', 'r')
    for line in infile2:
        if line[0] == '#':
            continue
        SourcePos.append([n for n in line.split()[1:4]])
elif galactic:
    trials    = '8000'
    tconesize = ['10']
    sconesize = ['15']
    if dry_run:
        SourcePos = [["-29.01", "-93.58"]]
    else:
        SourcePos = [['-48.9548', '-113.188'],
                     ['-45.2462', '-108.256'],
                     ['-41.3519', '-103.944'],
                     ['-37.315', '-100.127'],
                     ['-33.1681', '-96.7048'],
                     ['-29.01', '-93.58'],
                     ['-24.6384', '-90.7314'],
                     ['-20.29', '-88.0596'],
                     ['-15.9034', '-85.5345'],
                     ['-11.4886', '-83.1177'],
                     ['-7.0546', '-80.7757']]
elif galacplane:
    if lesstrials:
        trials    = '50'
        Nbit = 1
    else:
        trials = '10000' # number of PEx
        Nbit = 3

    # events  = ['0']
    maxRange = 76  # Max number of events
    events  = [str(i) for i in range(0, maxRange)]

elif extended:
    trials    = '5000'
    tconesize = ['15']
    sconesize = ['20']
    SourcePos  = [["-29.01", "-93.58"]]  # Galactic Centre
    extensions = ['0', '0.5', '1', '2', '3', '4', '5']
    if dry_run:
        extensions = ['1']

else:
    trials    = '5000'


# supposed to get the max-TS for all candidates for each PEx
# in fixed and float scenario
if candidates:
    events = ['0']
    SourcePos = [['-70', '100']]

if declscan:
    if fix_job:
        SourcePos = [[str(d), '100'] for d in range(-90, 50 + 1, 5)]
    else:
        SourcePos = [[str(d), '100'] for d in range(-90, 50 + 1, 10)]


if dry_run:
    trials = '500'
    Nbit = 1
    if not candidates:
        events  = ['14']
    if galacplaneData:
        trials = '3000'

if galacplaneData:
    Nbit = 1
    trials = '5000'
    events  = ['1']


if test_job:
    trials  = '50'
    events  = ['50']

trials = str(int(trials) / Nbit)
seed = 0


for ex in range(len(extensions)):
    for tc in range(len(tconesize)):
        for sc in range(len(sconesize)):
            for r in range(len(ray)):
                for d in range(len(SourcePos)):
                    for e in range(len(events)):
                        bckg = 0
                        if events[e] == '0':
                            bckg = 1
                        for g in range(len(gammas)):
                            for bit in range(Nbit * (1 + 9 * bckg)):  # to cut the jobs in Nbit

                                seed += 1

                                if galacplane:
                                    jobname = "Pex_" + str(e)
                                    jobname = "Pex_" + str(e) + "_" + str(bit)
                                else:
                                    jobname = "Pex_" + str(tc) + str(sc) + str(e) + str(d)

                                outfile = "CPEx"
                                outfile = "CPEx_" + str(bit)

                                if "gamma" in sys.argv:
                                    gamma = gammas[g]
                                    os.environ['PEX_TINGREDIENTS'] = "/sps/km3net/users/tgregoir/gal_plane/ingredient_files/ingredients_AaFit_Em" + gamma + ".root"
                                    os.environ['PEX_SINGREDIENTS'] = "/sps/km3net/users/tgregoir/gal_plane/ingredient_files/ingredients_TANTRA_Em" + gamma + ".root"
                                    outdir  = expandvars("/sps/km3net/users/tgregoir/gal_plane/CPEx_Em" + gamma + "/")
                                    jobname += str(g)

                                # outfile += "_candidates"

                                if candidates:
                                    outfile += "_candidates"
                                    os.environ['PEX_CANDID'] = "1"

                                if comb:
                                    outfile += "_comb"
                                    os.environ['PEX_COMB'] = "1"

                                if fix_job:
                                    os.environ['PEX_FIXED'] = "1"
                                    outfile += "_fixed"
                                    #  outfile += "_fixed_shift_0.5"
                                elif float_job:
                                    os.environ['PEX_FLOAT'] = "1"
                                    outfile += "_float"
                                elif galactic:
                                    os.environ['PEX_GALACT'] = "1"
                                    outfile += "_galactic"
                                elif galacplane:
                                    os.environ['PEX_GALACPLANE'] = "1"
                                    os.environ['PEX_EXTENSION'] = extensions[ex]
                                    outfile += "_galacticPlane"
                                    if extended:
                                        os.environ['PEX_EXTEND'] = "1"
                                        outfile += "_extended"
                                elif galacplaneData:
                                    os.environ['PEX_GALACPLANEDATA'] = "1"
                                    outfile += "_galacticPlaneData"
                                elif extended:
                                    os.environ['PEX_EXTEND'] = "1"
                                    os.environ['PEX_EXTENSION'] = extensions[ex]
                                    outfile += "_extended"
                                    # outfile += "_extended_noExtFit"
                                else:
                                    os.environ['PEX_FULL'] = "1"
                                    outfile += "_full"

                                if not galacplane and not galacplaneData:
                                    outfile += "_TC_" + tconesize[tc] + "_SC_" + sconesize[sc]
                                    if extended:
                                        outfile += "_Ex_" + extensions[ex]

                                outfile += "_E_" + events[e]

                                if not extended and not galacplane and not galacplaneData:
                                    outfile += '_D_' + SourcePos[d][0]
                                if float_job:
                                    outfile += '_RA_' + SourcePos[d][1]

                                outfile += '.root'

                                if dry_run:
                                    outfile = 'dry_' + outfile
                                if test_job:
                                    outfile = 'test_' + outfile

                                os.environ['PEX_SH_CUTNAME']     = ShCut
                                os.environ['PEX_TRIALS']         = trials
                                os.environ['PEX_NEVENTS']        = events[e]
                                os.environ['PEX_OUTFILE']        = outfile
                                os.environ['PEX_BKG']            = '1'
                                os.environ['PEX_TCONE']          = tconesize[tc]
                                os.environ['PEX_SCONE']          = sconesize[sc]
                                os.environ['PEX_SEED']           = str(seed)
                                os.environ['PEX_HITS']           = '1'
                                os.environ['PEX_DEC']            = SourcePos[d][0]
                                os.environ['PEX_RA']             = SourcePos[d][1]
                                os.environ['DRY']                = str(dry_run)
                                os.environ['PEX_TNBG']           = tnbg
                                os.environ['PEX_SNBG']           = snbg

                                cmd = ""

                                if dry_run:
                                    cmd = exe
                                else:
                                    if not test_job:
                                        outfile = outdir + outfile
                                    if test_job:
                                        outfile = expandvars("/sps/km3net/users/tgregoir/gal_plane/tests/") + outfile
                                    logfile = outfile.replace("root", "log")

                                    testfilelist = glob.glob(outfile)
                                    if len(testfilelist) > 0:
                                        print "file already there - skip"
                                        continue

                                    cmd = 'qsub -notify -P P_antares -V -l ct=24:00:00 -l vmem=4G -l fsize=25G -l sps=1'
                                    cmd += ' -N ' + jobname + ' '
                                    cmd += ' -o ' + logfile + ' -j y '
                                    cmd += exe

                                    os.environ['PEX_OUTFILE'] = outfile

                                print "outfile:", outfile
                                print "cmd:", cmd

                                os.system(cmd)

                                if dry_run:
                                    print "test done, bye"
                                    sys.exit(0)

                            # if test_job:
                            #     print "test done, bye"
                            #     sys.exit(0)

print "bye"
