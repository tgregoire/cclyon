This is the extended PseudoExperiment folder.

Two scripts are available in this folder:

run_PseudoExperiment.py      # actual run script
submit_PseudoExperiment.py   # submit script to grid engine including several options

To run the galacplane analysis :
change the input file line 422 of run_PseudoExperiment.py then test using `dry tnbg 7300 snbg 208` and or run
`make clean; make; ./submit_PseudoExperiment.py galacplane date option_date tnbg ntr snbg nsh`

needed:
ntr == number of tracks in data, given by sumHistos.py
nsh == number of showers in data, given by sumHistos.py

options:

option_date == the suffix of the output file, it can be the date or anything else
galacplane  == for the galactic plane analysis, the only one currently working
dry         == run it in interactive, better doing it before launching jobs
galacplaneData == to use once you have the unblinded data

For galacplaneData :
`make clean; make; ./submit_PseudoExperiment.py dry galacplaneData tnbg 7300 snbg 208`
Then get the median in the created ROOT file

then you should run `merge_files.py` in `/sps/km3net/users/tgregoir/gal_plane` by
doing
`./merge_files.py date option_date`

then plot_stuff.py in `/sps/km3net/users/tgregoir/gal_plane/software/Sensitivity`

COMBINATION
`make clean; make; ./submit_PseudoExperiment.py galacplane comb date the_date_comb tnbg 7300 snbg 208`

then 
```bash
cd /sps/km3net/users/tgregoir/gal_plane
./merge_files_comb.py date the_date_comb
cd /sps/km3net/users/tgregoir/gal_plane/software/ingredients/
./makeCombFile.py <infile> <topology> --model <3> --ntrials <1e3>
```
to create the txt files
