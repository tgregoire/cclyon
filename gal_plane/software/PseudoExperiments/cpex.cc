#include "cpex.hh"
#include "TRandom3.h"
#include "TStopwatch.h"
#include "TMinuit.h"
#include "TPad.h"
#include "TCanvas.h"
#include "TH2.h"
#include "TNtuple.h"
#include "TNtupleD.h"
#include "TFile.h"
#include <boost/iterator/iterator_concepts.hpp>
#include <sstream>

Clu* g_current_cluster;
Ana* g_current_ana;
Psf* g_current_psf;
Bgr* g_current_bgr;
Psf* g_current_psf_shower;
Bgr* g_current_bgr_shower;
int g_verbose_fitting;
bool g_current_FitNShower;
int g_fit_iter;
int GalacPlane;
double proba_zone;
double g_user[3];
double tnbg;
double snbg;
TH3*  g_current_psf_3d_tr;
TH1*  g_current_hist_E2;
TH2*  g_current_hist2d_atm;
TH1*  g_current_hist_atm;
TH3*  g_current_psf_3d_sh;
TH1*  g_current_hist_E2_shower;
TH2*  g_current_hist2d_atm_shower;
TH1*  g_acc_tracks;
TH1*  g_acc_shower;
TH1F* h_gen_ang = new TH1F("h_gen_ang","h_gen_ang",60,-3,3);
TH1F* test;

TF2
*gh_src_morphology;
TH2
*gh_galplane_morphology,
*gh_galplane_morphology_tr,
*gh_galplane_morphology_sh,
*gh_fit_morphology_tracks,
*gh_fit_morphology_shower;

TH3F *map_lik;


// global variable of the pex with get/set

bool g_ignore_hit_info_in_likelihood(false);




vector< vector< double > > minimize_path;

bool is_bogus ( double value )
{
        return std::isnan(value) || std::isinf(value);
}

void Pex::add_data()
{
        set_GalacPlane(1, 1);

        // TFile* data_file = TFile::Open("/sps/km3net/users/tgregoir/gal_plane/ingredient_files/data_ntuple.root");
        TFile* data_file = TFile::Open("/sps/km3net/users/tgregoir/gal_plane/ingredient_files/ntuple_Final_unblindedData.root");
        TNtupleD *data_ntuple = (TNtupleD*)data_file->Get("data_ntuple");

        double E_reco;
        double ra;
        double decl;
        double isShower;
        double time_date;
        double time_hour;
        double time_ns;

        data_ntuple->SetBranchAddress("E_reco",&E_reco);
        data_ntuple->SetBranchAddress("ra",&ra);
        data_ntuple->SetBranchAddress("decl",&decl);
        data_ntuple->SetBranchAddress("isShower",&isShower);
        data_ntuple->SetBranchAddress("time_date",&time_date);
        data_ntuple->SetBranchAddress("time_hour",&time_hour);
        data_ntuple->SetBranchAddress("time_ns",&time_ns);

        // int k = 0;
        // int l = 0;
        // for ( int j = 0; j < data_ntuple->GetEntries() ; j ++ )
        // {
        //     data_ntuple->GetEntry(j);
        //     if (isShower == 1) {l++;}
        //     else {k++;}
        //     cout << j << endl;
        // }
        //
        // cout << l << "  "<< k << endl;
        // sevts.resize(3);
        // showerevts.resize(l);

        int kk = 0;
        int ll = 0;

        for ( int j = 0; j < data_ntuple->GetEntries(); j++ )
        {
                data_ntuple->GetEntry(j);

                int isShower_ = (int) isShower;
                if (isShower_ == 1) {ll++; }
                else {kk++; }

                int year = (int) (time_date/10000);
                int month = (int) (time_date/100 - year*100);
                int day = (int) (time_date - year*10000 - month*100);

                int hour = (int) (time_hour/10000);
                int min = (int) (time_hour/100 - hour*100);
                int sec = (int) (time_hour - hour*10000 - min*100);

                int nsec = (int) time_ns;

                if (E_reco < 0.06) {E_reco = 0.06; } // if the energy is too small...
                // cout<<j<<"  ra "<<  ra<<"   dec "<< decl<<"  E_reco "<<  E_reco<<"  "<<  isShower<< "  time "<<TTimeStamp(year, month, day, hour, min, sec)<<endl;

                // if (j > 2)
                // {
                Sev t( j, decl, ra, E_reco, 1, isShower_);
                t.time = TTimeStamp(year, month, day, hour, min, sec, nsec);

                // cout << "time "<<time_date - 20000000. << "  "<< year<<"  "<< month<<"  "<<  day<<"  "<< t.time << endl;

                if ( isShower_ == 0) {
                        sevts.push_back( t );

                } else {
                        showerevts.push_back(t);
                }
                // }




                // else{
                //     TTimeStamp t = TTimeStamp(year, month, day, hour, min, sec);
                //
                //     if ( isShower_ == 0){
                //         sevts[j] = Sev( j,
                //                         decl,
                //                         ra,
                //                         nhits,
                //                         1,
                //                         isShower_) ;
                //         sevts[j].time = t;
                //     } else {
                //         showerevts[ll] = Sev( ll,
                //                         decl,
                //                         ra,
                //                         nhits,
                //                         1,
                //                         isShower_) ;
                //         showerevts[ll].time = t;
                //     }
                // }

                // r.id = j-1;
        }
        data_file->Close();

}


void Pex::add_background( Bgr& bgr,
                          Bgr& bgr3,
                          TH2* nhits_atm_histogram2d,
                          bool fluct_ntot /* = false */,
                          double scale_factor /* = 1 */,
                          int isShower_ /* = 0 */)
{
        // double sigma_relative_nhits = 0.1;
        // double sys_nhits_scale  = gRandom->Gaus( 1.0 , sigma_relative_nhits );
        int nbins = 200; // This gives me the number of bin in my histogram created from the TSpline
        bgr.f_per_coszen_vs_coszen->SetNpx(nbins); // We fix the number of bins we want to...
        TH1* hist_bgr_per_coszen = bgr.f_per_coszen_vs_coszen->GetHistogram(); // ...change the spline into an histogram
        // cout << hist_bgr_per_coszen->GetXaxis()->GetBinUpEdge(200) <<" should be 0.8 until now (using decl)"<<endl;
        for(int i = 0; i <= hist_bgr_per_coszen->GetXaxis()->GetNbins()+1; i++) { // Put the bins for coszen>0.1 to 0
                if(hist_bgr_per_coszen->GetBinCenter(i) < -1 || hist_bgr_per_coszen->GetBinCenter(i) > 0.1) {
                    // cout << hist_bgr_per_coszen->GetXaxis()->GetBinLowEdge(i) <<"  "<< hist_bgr_per_coszen->GetXaxis()->GetBinUpEdge(i) <<"  "<< hist_bgr_per_coszen->GetBinContent(i)  <<endl;
                    hist_bgr_per_coszen->SetBinContent(i, 0);
                }
                // else{
                //     cout << i<<"   "<<hist_bgr_per_coszen->GetBinContent(i)<<endl;
                // }
        }
        // double sum = 0;
        // for(int k= 0; k <= hist_bgr_per_coszen->GetXaxis()->GetNbins()+1; k++){
        //     sum += hist_bgr_per_coszen->GetBinContent(k);
        //     cout << k <<"  "<<hist_bgr_per_coszen->GetBinContent(k)<<" "<<sum/k*2.<<endl;
        //
        // }
        bgr3.f_per_az_vs_az->SetNpx(nbins); // We fix the number of bins we want to...
        TH1* hist_bgr_per_az = bgr3.f_per_az_vs_az->GetHistogram(); // ...change the spline into an histogram

        // cout << hist_bgr_per_coszen->Integral() <<"   "<<hist_bgr_per_az->Integral()<<  endl;
        // cout <<"bin content coszen "<<hist_bgr_per_coszen->GetBinCenter(1) <<"   "<<hist_bgr_per_coszen->GetBinCenter(100)<<  endl;
        // cout <<"bin content "<<hist_bgr_per_az->GetNbinsX() <<"   "<<hist_bgr_per_az->GetBinContent(1) <<"   "<<hist_bgr_per_az->GetBinContent(100)<<"   "<<hist_bgr_per_az->GetBinContent(5)<<"   "<<hist_bgr_per_az->GetBinContent(200)<<  endl;


        // int m = (int) ( (hist_bgr_per_coszen->Integral() )*2./nbins + 0.1);
        // cout<< m<<" m "<<endl;

        int size = 1;
        if (isShower_ == 0) {
                size = sevts.size();
        }
        if (isShower_ == 1) {
                size = showerevts.size();
        }

        // To reduce proba of an event where there is already some signal
        for (int i = 0; i< size; i++) {
                Vec dir;
                Sev& ev = sevts[i];
                if (isShower_ == 1) {
                        Sev& ev = showerevts[i];
                }
                convert_to_det( ev.ra, ev.decl, ev.time, dir ); // convert from equatorial to detector coordinates
                int bin_bgr = hist_bgr_per_coszen->FindBin(-dir.z); // -dir.z == cos(zenith)

                if (hist_bgr_per_coszen->GetBinContent(bin_bgr) < nbins/2.) {
                        hist_bgr_per_coszen->SetBinContent(bin_bgr, 0);
                        continue;
                }
                hist_bgr_per_coszen->SetBinContent(bin_bgr, hist_bgr_per_coszen->GetBinContent(bin_bgr)-nbins/2.); // Remove 1 event in full sky = remove nbins/2 per cos zen in one bin. To reduce proba of an event where there is already some signal

                int bin_az  = hist_bgr_per_az->FindBin(atan2( dir.y, dir.x )+TMath::Pi()); // + sys_offset_phi /*+ 0.5*TMath::DegToRad()*/;

                if (hist_bgr_per_az->GetBinContent(bin_az) < nbins/(2.*TMath::Pi())) {
                        hist_bgr_per_az->SetBinContent(bin_az, 0);
                        continue;
                }
                hist_bgr_per_az->SetBinContent(bin_az, hist_bgr_per_az->GetBinContent(bin_az)-nbins/(2.*TMath::Pi())); // To reduce proba of an event where there is already some signal // I multiply by 0.55 because I have only 55% not-empty bins
        }

        //if (isShower_ == 0) hist_bgr_per_coszen->Draw();

        // AH: added the 0.1, since I noticed we were generating 2039 events
        // in stead of 2040. (sept 16 2010)
        int n = (int) ( (hist_bgr_per_coszen->Integral() )*2./nbins + 0.1);

        // XXX
        // int n = 0;
        // if (isShower_ == 0) {
        //     n = (int) ( (hist_bgr_per_coszen->Integral() )*2./nbins + 0.1);
        // }
        // if (isShower_ == 1) {
        //     n = 208;
        // }

        if (bgr.bkg_scale != 1.0 ) {
                static int ii =0;
                if (ii<10 ) {
                        cout << " scale background integral = " << n << endl;
                        ii++;
                }
        }

        if ( fluct_ntot ) n = gRandom->Poisson( double(n) );  //110))
        // if ( fluct_ntot ) n = gRandom->Poisson(110);

        for (int i=0; i< n; i++) {

                // double azimuth = (gRandom->Rndm()) * 2 * M_PI;
                double azimuth = hist_bgr_per_az->GetRandom();
                // if (isShower_ == 1){
                //     cout << azimuth <<endl;
                // }
                double ra;
                double decl;

                const double coszen = hist_bgr_per_coszen->GetRandom();

                // check that the function is non-zero for the generated declination
                // can happen sometimes due to binnin in TF1::GetRandom()
                if ( bgr.f_per_coszen_vs_coszen->Eval( coszen ) <= 0 ) {
                        --i;
                        continue;
                }
                // cout << coszen << endl;

                // // generate the number of hits
                // double nhits = nhits_atm_histogram->GetRandom();
                // // if( isShower_ == 0 ) {nhits *= sys_nhits_scale;}

                static int t1 = TTimeStamp(2007,1,1, 1,1,1 ).GetSec(); // generate random time between these
                static int t2 = TTimeStamp(2015,1,1, 1,1,1 ).GetSec(); // ..two times

                TTimeStamp t = TTimeStamp( int ( t1 + (t2-t1) * gRandom->Rndm() ), 0 );
                // cout << t << endl;
                // cout << t <<"  " << coszen << "  " << azimuth << endl;

                Vec dir;
                dir.x = cos(azimuth);
                dir.y = sin(azimuth);
                dir.z = coszen;

                // convert from det to equatrial coordinates...
                // ...but also from evt direction to src direction so we correct this in the following lines
                convert_to_eq ( dir, t, ra, decl );
                ra -= M_PI;
                decl = -decl;

                if(ra > M_PI) {
                        ra -= 2*M_PI;
                }

                // cout << decl/TMath::Pi()*180 << "  " << ra/TMath::Pi()*180 <<"  " << t <<"  " << dir.z << endl;
                // convert_to_det( ra, decl, t, dir ); // convert from equatorial to detector coordinates
                // double zenith  = acos( -dir.z ); // + sys_offset_theta;
                // azimuth = atan2( dir.y, dir.x ) + M_PI; // + sys_offset_phi /*+ 0.5*TMath::DegToRad()*/;
                // if(azimuth < 0) {
                //         azimuth += 2*M_PI;
                // }
                // cout << decl/TMath::Pi()*180 << "  " << ra/TMath::Pi()*180 <<"  " << t <<"  " << cos(zenith) << "  " << azimuth << endl;


                TH1* hist_atm = 0;
                int bin = 0;
                int firstbin = 0;
                int lastbin = 0;
                double zenith = TMath::ACos(coszen);
                // cout << zenith << " zenith" << endl;
                int nbins_in_slice = 4;

                nhits_atm_histogram2d->GetYaxis()->SetRange(1,0); // reset
                double total_integral = nhits_atm_histogram2d->Integral();

                bin = nhits_atm_histogram2d->GetYaxis()->FindBin( zenith );
                // cout << bin << "  zenith "<< zenith << endl;
                // cout << bin << "  zenith "<< zenith <<   "bin center 2 "<< nhits_atm_histogram2d->GetYaxis()->GetBinCenter(2) << endl;

                if ( bin < 2 || bin >  nhits_atm_histogram2d->GetNbinsY() ) {
                        cout << " bogus bin for zenith = " << zenith <<"  "<< bin << endl;
                        exit(1);
                }

                firstbin = bin - (nbins_in_slice-1) / 2;
                lastbin  = bin + (nbins_in_slice-1) / 2;

                if (firstbin<2) firstbin = 2;
                if (lastbin> nhits_atm_histogram2d->GetNbinsY() ) lastbin = nhits_atm_histogram2d->GetNbinsY();
                // cout<<"firstbin  "<<firstbin<<"   lastbin  "<<lastbin<<endl;

                nhits_atm_histogram2d->GetYaxis()->SetRange(firstbin,lastbin);
                hist_atm = (TH1D*) nhits_atm_histogram2d->ProjectionX();

                if ( hist_atm->Integral() /  total_integral < 1e-4  ) {
                        cout << " error : too few events in slice: zenith " <<  hist_atm->Integral() <<" "<< bin <<" "<< decl <<" "<< firstbin <<" "<< lastbin << endl;
//             exit(0);
                }

                double nhits = hist_atm->GetRandom();
                // cout << nhits << endl;

                int size = sevts.size();
                if (isShower_ ==1) {
                        size = showerevts.size();
                }

                Sev s( size, decl, ra, nhits, 0, isShower_ ); // add this event in s
                s.time = t;

                if (isShower_ == 0) {
                        sevts.push_back( s );
                } else {
                        showerevts.push_back(s);
                }

        }
}




void Pex::add_signal_generic_2d( int n,
                                 Psf* psf,            // one of these two must be null, the other
                                 TH2* psf_vs_nhits,   // will be used -> 2d histo psf_vs_nhits
                                 double decl,         // in radians
                                 double ra,
                                 double sigma_relative_resolution,     // eg 0.15 for 15% effect
                                 double sigma_radians_pointing_theta,  // eg 0.1/57.2 for 0.1 degree
                                 double sigma_radians_pointing_phi,
                                 double sigma_relative_nhits,
                                 int isShower)                         // is 0 for muon tracks and 1 for showers

{
        if (psf) psf->update();  // in case user changed the pars

        if (mc_source.mu != 0 && mc_source.isTrack == 1 && mc_source.isShower == 1) { // allow only one mc_source
                cout << " attempting to add second source to a Pex -> bail!" << endl;
                exit(0);
        }
        static TH1D* hpsf;
        static TH1D* hnhits;
        static bool init = false;

        // throw random numbers for angular resolution systematics
        double sys_res_scale    = gRandom->Gaus( 1.0, sigma_relative_resolution    );
        double sys_offset_theta = gRandom->Gaus( 0, sigma_radians_pointing_theta );
        double sys_offset_phi   = gRandom->Gaus( 0, sigma_radians_pointing_phi   );
        double sys_nhits_scale  = gRandom->Gaus( 1.0, sigma_relative_nhits         );

        double nhit_E2;


        if ( sys_res_scale < 0.1 ) sys_res_scale = 0.1;



        for (int i(0); i < n; ++i) {
                double a =-1;
                cout << i <<endl;
                if (psf && !psf_vs_nhits ) {
                        cout << " using psf is not good " << endl;
                        exit(1);

                        a = psf->f_per_a_vs_a->GetRandom();
                } else if (psf_vs_nhits && !psf ) {
                        double loga; // log(angle/degree)

                        psf_vs_nhits->GetRandom2(loga, nhit_E2 ); // a is the angular error (distance between reco and true direction)


                        nhit_E2 *= sys_nhits_scale;
                        // smearing the number of hits can cause the event to fall below the nhits cut for showers
                        // drop this event and reroll
                        // if ( isShower && nhit_E2 < 40 ){
                        //     --i;
                        //     continue;
                        //     std::cout << "cheesus, not again" << std::endl;
                        // }


                        if ( loga < -3.5 || loga > 2.5) {
                                cout << " in Pex::add_signal_generic : bogus log(angle) " << loga << endl;
                                cout << " make sure input histogram is log(a/degree)! " << endl;
                                exit(0);
                        }
                        if (loga > log10(180) ) loga = log10(180);  // can happen due to binning.

                        a = pow(10,loga) / 180.0 * M_PI;

                        // apply systematic scaling of resolution
                        if (a *  sys_res_scale < M_PI)
                                a *= sys_res_scale;

                        if (verbose > 1 )
                                cout << " added signal event at " << a * 180 / M_PI << " degrees " << endl;
                } else {
                        cout << "  in Pex::add_signal_generic need either a psf or a psf_vs_nhits (and not both)" << endl;
                        exit(0);
                }

                double phi = (gRandom->Rndm()-0.5) * 2 * M_PI; // Produce an uniformly generated phi between [-pi, pi]
                Vec v(  sin(a) * cos(phi), sin(a) * sin(phi), cos( a ) ); // the reco signal will be at a distance a from where it was emitted (ra, decl) and at the angle phi

                if ( gh_src_morphology )
                        gh_src_morphology->GetRandom2( ra, decl );
                // if gh_galplane_morphology, the ra and dec have already been set
                v.rotate_y ( M_PI/2. - decl ); // turn the error to be around our signal and not around zero
                v.rotate_z ( ra );

                double meas_decl = asin( v.z );
                double meas_ra   = atan2( v.y, v.x );


                // It might be that the event ends up above the horizon
                // -- not really clear how to deal with that... for now just require the
                // declination to be within range --

                const double maxdecl = ( 90. - 43. ) * M_PI / 180.;
                if (meas_decl > maxdecl ) continue;

                int size = sevts.size();
                if (isShower ==1) {
                        size = showerevts.size();
                }

                Sev s( size, meas_decl, meas_ra, nhit_E2, 1, isShower ); // add this event in s

                // apply absolute pointing systematic
                static int t1 = TTimeStamp(2007,1,1, 1,1,1 ).GetSec(); // generate random time between these
                static int t2 = TTimeStamp(2015,1,1, 1,1,1 ).GetSec(); // ..two times
                static const int nmax = 1000;

                int i; Vec dir; TTimeStamp t;

                for (i=0; i<= nmax; i++) { // try until we have an upgoing event

                        t = TTimeStamp( int ( t1 + (t2-t1) * gRandom->Rndm() ), 0 );
                        convert_to_det( s.ra,s.decl, t, dir ); // convert from equatorial to detector coordinates
                        if (dir.z > 0 ) break;
                }
                if ( i == nmax ) {
                        cout << " warning: smear_phi failed to find an upgoing direction belonging to " << decl << "," << ra <<" or precisely " << s.decl << "," << s.ra << endl;
                }

                double phi_   = atan2( dir.y, dir.x ) + sys_offset_phi /*+ 0.5*TMath::DegToRad()*/;
                double theta_ = acos( dir.z ) + sys_offset_theta;
                dir.set_angles( theta_, phi_ );
                convert_to_eq ( dir, t, s.ra, s.decl ); // re-convert from det to equatrial coordinates but with the systematics
                // done with absolute pointing systematic
                //cout << "signal added at (ra,decl): " << s.ra-2*M_PI << "\t" << s.decl << endl;
                //cout << "signal added at (ra,decl): " << s.ra << "\t" << s.decl << endl;

                if (s.ra > M_PI) s.ra = s.ra-2*M_PI;

                if (isShower == 0) {
                        sevts.push_back( s );
                }
                else {
                        showerevts.push_back(s);
                }

        }


        // remember what we did.
        if (isShower == 0) mc_source.isTrack  =1.;
        if (isShower == 1) mc_source.isShower =1.;

        mc_source.nsig  = n;
        mc_source.mu    = -999;
        mc_source.decl  = decl;
        mc_source.ra    = ra;

}




/* the following is for mc studies only. It uses the Psf itself, but allows for a
   source morphology to be included */

void Pex::add_signal_simple( int n,
                             Psf* psf,
                             double decl, // in radians - ignored if we have morphology
                             double ra,
                             TH2* morphology /*=0*/ )
{
        if (psf) psf->update();  // in case user changed the pars


        for (int i = 0; i< n; i++) {

                const double a = psf->f_per_a_vs_a->GetRandom();

                //#cout << " generated measurment error angle = " << a * 57.2 << " deg " << endl;
                h_gen_ang->Fill( log10(a*57.2));


                double phi = (gRandom->Rndm()-0.5) * 2 * M_PI;
                Vec v(  sin(a) * cos(phi), sin(a) * sin(phi), cos( a ) );

                // at this point, we should add the morphology

                if ( morphology ) {
                        morphology->GetRandom2( ra, decl );
                        //cout << " true neutrino direction : " << ra << " " << decl << endl;
                }

                // move to the right coordinates

                v.rotate_y ( M_PI/2. - decl ); // turn the error to be around our signal and not around zero
                v.rotate_z ( ra );

                double meas_decl = asin( v.z );
                double meas_ra   = atan2( v.y, v.x );

                // It might be that the event ends up above the horizon
                // -- not really clear how to deal with that... for now just require the
                // declination to be within range --

                const double maxdecl = ( 90. - 42. ) * M_PI / 180.;
                if (meas_decl > maxdecl ) continue;

                Sev s(  sevts.size(), meas_decl, meas_ra, 0, 1 );
                sevts.push_back( s );
        }

        mc_source.nsig  = n;
        mc_source.mu    = -999;
        mc_source.decl  = decl;
        mc_source.ra    = ra;

}





void Pex::add_signal_psfhistogram3d( int n,
                                     TH3& psf_3d_histogram, //x = log(beta), y = sin(decl), z = nhits
                                     double decl, // in radians
                                     double ra,
                                     double sigma_relative_resolution,
                                     double sigma_radians_pointing_theta,
                                     double sigma_radians_pointing_phi,
                                     double sigma_relative_nhits,
                                     int isShower /*=0*/,      // 0 means muon track, 1 means shower
                                     int nbins_in_slice /*=5*/)
{

        static int cache_bin   =-99;
        static TH2D* psf_vs_nhits = 0;

        if (decl < -M_PI / 2. || decl > M_PI / 2. ) {
                cout << " error in add_signal_psfhistogram3d: decl must be -pi/2 < decl < pi/2 " << endl;
                cout << " decl = " << decl << endl;
                exit(0);
        }

        psf_3d_histogram.GetYaxis()->SetRange(1,0); // reset
        double total_integral = psf_3d_histogram.Integral();


        ostringstream oss;
        oss << decl;
        string res = oss.str();
        string zx_;
        string name = string("zx_") + res;

        if (decl<=-TMath::Pi()/2.) {
                cout << "decl<=-TMath::Pi()/2"<<endl;
                decl = -TMath::Pi()/2.;
        }

        int bin = psf_3d_histogram.GetYaxis()->FindBin( decl ); //sin(decl) );

        if ( bin < 1 || bin >  psf_3d_histogram.GetNbinsY() ) {
                cout << " bogus bin for declination = " << decl << endl;
                exit(1);
        }

        if (bin != cache_bin) {
                int firstbin = bin - (nbins_in_slice-1) / 2;
                int lastbin  = bin + (nbins_in_slice-1) / 2;

                if (firstbin<1) firstbin = 1;
                if (lastbin> psf_3d_histogram.GetNbinsY() ) lastbin = psf_3d_histogram.GetNbinsY();


                if ( verbose > 1 ) {
                        cout << "=========================================================" << endl;
                        cout << " projecting new slice to use for signal event generation " << endl;
                        cout << " decl = " << decl << endl;
                        cout << " bin range = " << firstbin << " " << lastbin << endl;
                }

                psf_3d_histogram.GetYaxis()->SetRange(firstbin,lastbin);
                psf_vs_nhits = (TH2D*) psf_3d_histogram.Project3D(name.c_str());

                if ( verbose > 1 ) {
                        cout << "integral of the 2d proj           = "<< psf_vs_nhits->Integral() << endl;
                        cout << "integral of the full 3d histogram = "<< total_integral << endl;
                        cout << "=================================================="<<endl;

                        psf_vs_nhits->Draw("goff");
                        gPad->Update();
                }


                if ( psf_vs_nhits->Integral() /  total_integral < 1e-3  ) {
                        cout << " error : too few events in slice " <<  psf_vs_nhits->Integral() << endl;
                }
                cache_bin = bin;
        }


        add_signal_psfhistogram_2d( n, *psf_vs_nhits, decl, ra,
                                    sigma_relative_resolution,
                                    sigma_radians_pointing_theta,
                                    sigma_radians_pointing_phi,
                                    sigma_relative_nhits,
                                    isShower);

}



// // Project a slice of sin(dec) (around the declination of the source) in psf vs nhits and call add_signal_psfhistogram_2d (which will call add_signal_generic_2d)
// void Pex::add_signal_galacplane( int n,
//                                  TH3& psf_3d_histogram, //x = ra, y = decl, z = nhits
//                                  double sigma_relative_nhits,
//                                  int isFullSky,
//                                  int isShower /*=0*/,      // 0 means muon track, 1 means shower
//                                  int nbins_in_slice /*=5*/)
// {
//     set_GalacPlane(isFullSky, 1);
//
//     if (isShower == 0){
//         sevts.resize(n);
//     } else {
//         showerevts.resize(n);
//     }
//
//     if (mc_source.mu != 0 && mc_source.isTrack == 1 && mc_source.isShower == 1){ // allow only one mc_source
//         cout << " attempting to add second source to a Pex -> bail!" << endl;
//         exit(0);
//     }
//
//     double ra;
//     double decl;
//     double prob_morph;
//
//     // double sys_nhits_scale  = gRandom->Gaus( 1.0 , sigma_relative_nhits );
//
//     TH2D* psf_2d_histogram = 0;
//     psf_2d_histogram = (TH2D*) psf_3d_histogram.Project3D("yz");
//
//     for (int i(0); i < n; i++){ // If GalacPlane I loop to put now the ra and dec
//
// // If GalacPlane I generate now the ra and dec (to be able to find nhits which depend on the decl)
//         do{
//               if (isShower)  gh_fit_morphology_shower->GetRandom2( ra, decl );
//               else           gh_fit_morphology_tracks->GetRandom2( ra, decl );
//               prob_morph = gh_galplane_morphology_tr->GetBinContent(gh_galplane_morphology_tr->FindBin(ra, decl));
//         }while (prob_morph < proba_zone);
//
//         static int cache_bin   =-99;
//
//         if (decl < -M_PI / 2. || decl > M_PI / 2. ){
//             cout << " error in add_signal_galacplane: decl must be -pi/2 < decl < pi/2 " << endl;
//             cout << " decl = "<<decl << endl;
//             // exit(0);
//         }
//
//         psf_2d_histogram->GetYaxis()->SetRange(1,0); // reset
//         double total_integral = psf_2d_histogram->Integral();
//
//         if (decl<=-TMath::Pi()/2.){
//             cout << "decl<=-TMath::Pi()/2"<<endl;
//             decl = -TMath::Pi()/2.;
//         }
//
//         int bin = psf_2d_histogram->GetYaxis()->FindBin( decl );
//
//         if ( bin < 1 || bin >  psf_2d_histogram->GetNbinsY() ){
//             cout << " bogus bin for declination = " << decl << endl;
//             exit(1);
//         }
//         static TH1D* hist_nhits = 0;
//
//         if (bin != cache_bin){
//             int firstbin = bin - (nbins_in_slice-1) / 2;
//             int lastbin  = bin + (nbins_in_slice-1) / 2;
//
//             if (firstbin<1) firstbin = 1;
//             if (lastbin> psf_2d_histogram->GetNbinsY() ) lastbin = psf_2d_histogram->GetNbinsY();
//             // cout<<"firstbin  "<<firstbin<<"   lastbin  "<<lastbin<<endl;
//
//             if ( verbose > 1 ){
//                 cout << "=========================================================" << endl;
//                 cout << " projecting new slice to use for signal event generation " << endl;
//                 cout << " decl = " << decl << endl;
//                 cout << " bin range = " << firstbin << " " << lastbin << endl;
//             }
//
//             psf_2d_histogram->GetYaxis()->SetRange(firstbin,lastbin);
//             hist_nhits = (TH1D*) psf_2d_histogram->ProjectionX();
//
//
//             if ( verbose > 1 ){
//                 cout << "integral of the 2d proj           = "<< hist_nhits->Integral() << endl;
//                 cout << "integral of the full 3d histogram = "<< total_integral << endl;
//                 cout << "=================================================="<<endl;
//
//                 hist_nhits->Draw("goff");
//                 gPad->Update();
//             }
//
//             if ( hist_nhits->Integral() /  total_integral < 1e-3  ){
//                 cout << " error : too few events in slice " <<  hist_nhits->Integral() <<" "<< bin <<" "<< decl <<" "<< firstbin <<" "<< lastbin << endl;
//     //             exit(0);
//             }
//             cache_bin = bin;
//         }
//
//
//         double nhit_E2;
//         nhit_E2 = hist_nhits -> GetRandom();
//
//
//         // if (isShower != 1) cout << nhit_E2 <<endl;
//
//
//         // nhit_E2 = gRandom->Poisson( double(170));
//         // cout<<"nhit_E2    "<<nhit_E2<<endl;
//         // if( isShower == 0 ) {nhit_E2 *= sys_nhits_scale;}
//         // cout<<"nhit_E2 2  "<<nhit_E2<<endl;
//
//         // smearing the number of hits can cause the event to fall below the nhits cut for showers
//         // drop this event and reroll
//         // if ( isShower && nhit_E2 < 40 ){
//         //     --i;
//         //     continue;
//         //     std::cout << "cheesus, not again" << std::endl;
//         // }
//
//         // It might be that the event ends up above the horizon
//         // -- not really clear how to deal with that... for now just require the
//         // declination to be within range --
//
//         const double maxdecl = ( 90. - 43. ) * M_PI / 180.;
//         if (decl > maxdecl ){
//             --i;
//             continue;
//         }
//
//
//         static int t1 = TTimeStamp(2007,1,1, 1,1,1 ).GetSec(); // generate random time between these
//         static int t2 = TTimeStamp(2009,1,1, 1,1,1 ).GetSec(); // ..two times
//         static const int nmax = 1000;
//
//         int j; Vec dir; TTimeStamp t;
//         // double zenith;
//
//         for (j=0; j<= nmax ; j++){ // try until we have an upgoing event
//
//             t = TTimeStamp( int ( t1 + (t2-t1) * gRandom->Rndm() ) , 0 );
//             convert_to_det( ra, decl, t , dir ); // convert from equatorial to detector coordinates
//             // zenith = acos(-dir.z);
//             if (dir.z > -0.1) break;
//         }
//
//         // cout << i <<"   "<< isShower <<"   "<< decl <<"   "<< ra <<"   "<< t <<"   "<< zenith << endl;
//
//         if ( j == nmax ){
//             cout << " warning: smear_phi failed to find an upgoing direction belonging to " << decl << "," << ra << endl;
//         }
//
//         if (ra > M_PI)  ra = ra-2*M_PI;
//
//         if ( isShower == 0){
//             sevts[i] = Sev( i,
//                             decl,
//                             ra,
//                             nhit_E2,
//                             1,
//                             isShower) ;
//             sevts[i].time = t;
//         } else {
//             showerevts[i] = Sev( i,
//                             decl,
//                             ra,
//                             nhit_E2,
//                             1,
//                             isShower) ;
//             showerevts[i].time = t;
//         }
//     }
//
//     // remember what we did.
//     if (isShower == 0) mc_source.isTrack  =1.;
//     if (isShower == 1) mc_source.isShower =1.;
//
//     mc_source.nsig  = n ;
//     mc_source.mu    = -999;
//     mc_source.decl  = decl;
//     mc_source.ra    = ra;
//
// }


// Project a slice of sin(dec) (around the declination of the source) in psf vs nhits and call add_signal_psfhistogram_2d (which will call add_signal_generic_2d)
void Pex::add_signal_galacplane( int n,
                                 TH3& psf_3d_histogram, //x = ra, y = decl, z = nhits
                                 double sigma_relative_nhits,
                                 int isFullSky,
                                 int isShower /*=0*/,      // 0 means muon track, 1 means shower
                                 int nbins_in_slice /*=5*/)
{
        // if(isShower != 42){
        set_GalacPlane(isFullSky, 1);

        if (isShower == 0) {
                sevts.resize(n);
        } else {
                showerevts.resize(n);
        }

        if (mc_source.mu != 0 && mc_source.isTrack == 1 && mc_source.isShower == 1) { // allow only one mc_source
                cout << " attempting to add second source to a Pex -> bail!" << endl;
                exit(0);
        }

        double ra;
        double decl;
        double prob_morph;

        // double sys_nhits_scale  = gRandom->Gaus( 1.0 , sigma_relative_nhits );

        for (int i(0); i < n; i++) { // If GalacPlane I loop to put now the ra and dec

                // If GalacPlane I generate now the ra and dec (to be able to find nhits which depend on the decl)
                do {
                        if (isShower) gh_fit_morphology_shower->GetRandom2( ra, decl );
                        else gh_fit_morphology_tracks->GetRandom2( ra, decl );
                        prob_morph = gh_galplane_morphology_tr->GetBinContent(gh_galplane_morphology_tr->FindBin(ra, decl));
                } while (prob_morph < proba_zone);

                static int cache_binD   =-99;
                static int cache_binR   =-99;

                if (decl < -M_PI / 2. || decl > M_PI / 2. ) {
                        cout << " error in add_signal_galacplane: decl must be -pi/2 < decl < pi/2 " << endl;
                        cout << " decl = "<<decl << endl;
                        // exit(0);
                }

                psf_3d_histogram.GetYaxis()->SetRange(1,0); // reset
                psf_3d_histogram.GetXaxis()->SetRange(1,0); // reset
                double total_integral = psf_3d_histogram.Integral();

                if (decl<=-TMath::Pi()/2.) {
                        cout << "decl<=-TMath::Pi()/2"<<endl;
                        decl = -TMath::Pi()/2.;
                }

                int binD = psf_3d_histogram.GetYaxis()->FindBin( decl );

                if ( binD < 1 || binD >  psf_3d_histogram.GetNbinsY() ) {
                        cout << " bogus bin for declination = " << decl <<"  "<< binD << endl;
                        exit(1);
                }

                int binR = psf_3d_histogram.GetXaxis()->FindBin( ra );

                if ( binR < 1 || binR >  psf_3d_histogram.GetNbinsX() ) {
                        cout << " bogus bin for right ascension = " << ra << endl;
                        exit(1);
                }
                // cout << binR << "  ra "<< ra << "   "<<binD <<"   decl "<< decl << endl;
                static TH1D* hist_nhits = 0;

                int firstbinD = binD - (nbins_in_slice-1) / 2;
                int lastbinD  = binD + (nbins_in_slice-1) / 2;

                if (firstbinD<1) firstbinD = 1;
                if (lastbinD> psf_3d_histogram.GetNbinsY() ) lastbinD = psf_3d_histogram.GetNbinsY();
                // cout<<"firstbinD  "<<firstbinD<<"   lastbinD  "<<lastbinD<<endl;

                int firstbinR = binR - (nbins_in_slice-1) / 2;
                int lastbinR  = binR + (nbins_in_slice-1) / 2;

                if (firstbinR<1) firstbinR = 1;
                if (lastbinR> psf_3d_histogram.GetNbinsX() ) lastbinD = psf_3d_histogram.GetNbinsX();

                if ( verbose > 1 ) {
                        cout << "=========================================================" << endl;
                        cout << " projecting new slice to use for signal event generation " << endl;
                        cout << " ra = " << ra << endl;
                        cout << " bin range = " << firstbinR << " " << lastbinR << endl;
                        cout << " decl = " << decl << endl;
                        cout << " bin range = " << firstbinD << " " << lastbinD << endl;
                }

                psf_3d_histogram.GetYaxis()->SetRange(firstbinD,lastbinD);
                psf_3d_histogram.GetXaxis()->SetRange(firstbinR,lastbinR);
                hist_nhits = (TH1D*) psf_3d_histogram.Project3D("z");


                if ( verbose > 1 ) {
                        cout << "integral of the 2d proj           = "<< hist_nhits->Integral() << endl;
                        cout << "integral of the full 3d histogram = "<< total_integral << endl;
                        cout << "=================================================="<<endl;

                        hist_nhits->Draw("goff");
                        gPad->Update();
                }

                if ( hist_nhits->Integral() /  total_integral < 1e-5  ) {
                        cout << " error : too few events in slice: decl " <<  hist_nhits->Integral() <<" "<< binD <<" "<< decl <<" "<< firstbinD <<" "<< lastbinD << endl;
                        cout << " and ra" <<  hist_nhits->Integral() <<" "<< binR <<" "<< ra <<" "<< firstbinR <<" "<< lastbinR << endl;
                        //             exit(0);
                }

                psf_3d_histogram.GetYaxis()->SetRange(1,0); // reset
                psf_3d_histogram.GetXaxis()->SetRange(1,0); // reset

                double nhit_E2;
                nhit_E2 = hist_nhits->GetRandom();


                // if (isShower != 1) cout << nhit_E2 <<endl;


                // nhit_E2 = gRandom->Poisson( double(170));
                // cout<<"nhit_E2    "<<nhit_E2<<endl;
                // if( isShower == 0 ) {nhit_E2 *= sys_nhits_scale;}
                // cout<<"nhit_E2 2  "<<nhit_E2<<endl;

                // smearing the number of hits can cause the event to fall below the nhits cut for showers
                // drop this event and reroll
                // if ( isShower && nhit_E2 < 40 ){
                //     --i;
                //     continue;
                //     std::cout << "cheesus, not again" << std::endl;
                // }

                // It might be that the event ends up above the horizon
                // -- not really clear how to deal with that... for now just require the
                // declination to be within range --

                const double maxdecl = ( 90. - 43. ) * M_PI / 180.;
                if (decl > maxdecl ) {
                        --i;
                        continue;
                }


                static int t1 = TTimeStamp(2007,1,1, 1,1,1 ).GetSec(); // generate random time between these
                static int t2 = TTimeStamp(2015,1,1, 1,1,1 ).GetSec(); // ..two times
                static const int nmax = 1000;

                int j; Vec dir; TTimeStamp t;
                // double zenith;

                for (j=0; j<= nmax; j++) { // try until we have an upgoing event

                        t = TTimeStamp( int ( t1 + (t2-t1) * gRandom->Rndm() ), 0 );
                        convert_to_det( ra, decl, t, dir ); // convert from equatorial to detector coordinates
                        // zenith = acos(-dir.z);
                        if (-dir.z < 0.1){
                            // cout << -dir.z << "  " << ra << "  " << decl << "   " << t << endl;
                            break;
                        }
                }

                // cout << i <<"   "<< isShower <<"   "<< decl <<"   "<< ra <<"   "<< t <<"   "<< zenith << endl;

                if ( j == nmax ) {
                        cout << " warning: smear_phi failed to find an upgoing direction belonging to " << decl << "," << ra << endl;
                }

                if (ra > M_PI) ra = ra-2*M_PI;

                if ( isShower == 0) {
                        sevts[i] = Sev( i,
                                        decl,
                                        ra,
                                        nhit_E2,
                                        1,
                                        isShower);
                        sevts[i].time = t;
                } else {
                        showerevts[i] = Sev( i,
                                             decl,
                                             ra,
                                             nhit_E2,
                                             1,
                                             isShower);
                        showerevts[i].time = t;
                }
        }

        // remember what we did.
        if (isShower == 0) mc_source.isTrack  =1.;
        if (isShower == 1) mc_source.isShower =1.;

        mc_source.nsig  = n;
        mc_source.mu    = -999;
        mc_source.decl  = decl;
        mc_source.ra    = ra;
        // }
        // else{
        //     // TFile* data_file = TFile::Open("/sps/km3net/users/tgregoir/gal_plane/ingredient_files/data_ntuple.root");
        //     TFile* data_file = TFile::Open("/sps/km3net/users/tgregoir/gal_plane/ingredient_files/Data_tim/Data_ing_files.root");
        //     TNtupleD *data_ntuple = (TNtupleD*)data_file->Get("data_ntuple");
        //
        //     double E_reco;
        //     double ra;
        //     double decl;
        //     double isShower;
        //     double time_date;
        //     double time_hour;
        //
        //     data_ntuple->SetBranchAddress("E_reco",&E_reco);
        //     data_ntuple->SetBranchAddress("ra",&ra);
        //     data_ntuple->SetBranchAddress("decl",&decl);
        //     data_ntuple->SetBranchAddress("isShower",&isShower);
        //     data_ntuple->SetBranchAddress("time_date",&time_date);
        //     data_ntuple->SetBranchAddress("time_hour",&time_hour);
        //
        //
        //     // int k = 0;
        //     // int l = 0;
        //     // for ( int j = 0; j < data_ntuple->GetEntries() ; j ++ )
        //     // {
        //     //     data_ntuple->GetEntry(j);
        //     //     if (isShower == 1) {l++;}
        //     //     else {k++;}
        //     //     cout << j << endl;
        //     // }
        //     //
        //     // cout << l << "  "<< k << endl;
        //     // sevts.resize(3);
        //     // showerevts.resize(l);
        //
        //     int kk = 0;
        //     int ll = 0;
        //
        //     for ( int j = 0; j < data_ntuple->GetEntries() ; j ++ )
        //     {
        //         data_ntuple->GetEntry(j);
        //
        //         int isShower_ = (int) isShower;
        //         if (isShower_ == 1) {ll++;}
        //         else {kk++;}
        //
        //         int year = (int) (time_date/10000);
        //         int month = (int) (time_date/100 - year*100);
        //         int day = (int) (time_date - year*10000 - month*100);
        //
        //         int hour = (int) (time_hour/10000);
        //         int min = (int) (time_hour/100 - hour*100);
        //         int sec = (int) (time_hour - hour*10000 - min*100);
        //
        //         cout<<j<<"   ra "<<  ra<<"   dec "<< decl<<"  E_reco "<<  E_reco<<"  "<<  isShower<< "  time "<<TTimeStamp(year, month, day, hour, min, sec)<< endl;
        //
        //         // if (j > 2)
        //         // {
        //             Sev t( j, decl, ra, E_reco, 1, isShower_);
        //             t.time = TTimeStamp(year, month, day, hour, min, sec);
        //
        //             // cout << "time "<<time_date - 20000000. << "  "<< year<<"  "<< month<<"  "<<  day<<"  "<< t.time << endl;
        //             // cout<<j<<"  dec "<< t.decl<<"  ra "<<  t.ra<<"  nhits "<<  t.nhits<<"  "<<  t.isShower<< endl;
        //
        //             if ( isShower_ == 0){
        //                 sevts.push_back( t );
        //             } else {
        //                 showerevts.push_back(t);
        //             }
        //         // }
        //
        //
        //
        //
        //         // else{
        //         //     TTimeStamp t = TTimeStamp(year, month, day, hour, min, sec);
        //         //
        //         //     if ( isShower_ == 0){
        //         //         sevts[j] = Sev( j,
        //         //                         decl,
        //         //                         ra,
        //         //                         nhits,
        //         //                         1,
        //         //                         isShower_) ;
        //         //         sevts[j].time = t;
        //         //     } else {
        //         //         showerevts[ll] = Sev( ll,
        //         //                         decl,
        //         //                         ra,
        //         //                         nhits,
        //         //                         1,
        //         //                         isShower_) ;
        //         //         showerevts[ll].time = t;
        //         //     }
        //         // }
        //
        //     // r.id = j-1;
        //     }
        //
        // data_file->Close();
        //
        // }

}


// // Project a slice of sin(dec) (around the declination of the source) in psf vs nhits and call add_signal_psfhistogram_2d (which will call add_signal_generic_2d)
// void Pex::add_signal_galacplane_tr( int n,
//                                  TH3& Etrue_ra_dec_param, //x = ra, y = decl, z = logEtrue
//                                  TH2& Etrue_Nhits,
//                                  double sigma_relative_nhits,
//                                  int isFullSky,
//                                  int isShower /*=0*/,      // 0 means muon track, 1 means shower
//                                  int nbins_in_slice /*=5*/)
// {
//     set_GalacPlane(isFullSky, 1);
//
//     if (isShower == 0){
//         sevts.resize(n);
//     } else {
//         showerevts.resize(n);
//     }
//
//     if (mc_source.mu != 0 && mc_source.isTrack == 1 && mc_source.isShower == 1){ // allow only one mc_source
//         cout << " attempting to add second source to a Pex -> bail!" << endl;
//         exit(0);
//     }
//
//     double ra;
//     double decl;
//     double prob_morph;
//
//     // double sys_nhits_scale  = gRandom->Gaus( 1.0 , sigma_relative_nhits );
//
//     for (int i(0); i < n; i++){ // If GalacPlane I loop to put now the ra and dec
//
// // If GalacPlane I generate now the ra and dec (to be able to find nhits which depend on the decl)
//         do{
//               if (isShower)  gh_fit_morphology_shower->GetRandom2( ra, decl );
//               else           gh_fit_morphology_tracks->GetRandom2( ra, decl );
//               prob_morph = gh_galplane_morphology_tr->GetBinContent(gh_galplane_morphology_tr->FindBin(ra, decl));
//         }while (prob_morph < proba_zone);
//
//         static int cache_bin   =-99;
//
//         if (decl < -M_PI / 2. || decl > M_PI / 2. ){
//             cout << " error in add_signal_galacplane: decl must be -pi/2 < decl < pi/2 " << endl;
//             cout << " decl = "<<decl << endl;
//             // exit(0);
//         }
//
//         Etrue_ra_dec_param.GetYaxis()->SetRange(1,0); // reset
//         Etrue_ra_dec_param.GetXaxis()->SetRange(1,0); // reset
//
//         if (decl<=-TMath::Pi()/2.){
//             cout << "decl<=-TMath::Pi()/2"<<endl;
//             decl = -TMath::Pi()/2.;
//         }
//
//         int binDecl = Etrue_ra_dec_param.GetXaxis()->FindBin( decl );
//
//         if ( binDecl < 1 || binDecl >  Etrue_ra_dec_param.GetNbinsX() ){
//             cout << " bogus bin for declination = " << decl << endl;
//             exit(1);
//         }
//
//         int binRa = Etrue_ra_dec_param.GetYaxis()->FindBin( ra );
//
//         if ( binRa < 1 || binRa >  Etrue_ra_dec_param.GetNbinsY() ){
//             cout << " bogus bin for ra = " << ra << endl;
//             exit(1);
//         }
//
//         static TH1D* hist_logE_true = 0;
//
//         if (binDecl != cache_bin){
//
//             Etrue_ra_dec_param.GetXaxis()->SetRange(binDecl,binDecl);
//             Etrue_ra_dec_param.GetYaxis()->SetRange(binRa,binRa);
//
//
//             hist_logE_true = (TH1D*) Etrue_ra_dec_param.Project3D("z");
//
//
//             if ( verbose > 1 ){
//                 cout << "integral of the 2d proj           = "<< hist_logE_true->Integral() << endl;
//                 // cout << "integral of the full 3d histogram = "<< total_integral << endl;
//                 cout << "=================================================="<<endl;
//
//                 hist_logE_true->Draw("goff");
//                 gPad->Update();
//             }
//
//             cache_bin = binDecl;
//         }
//
//
//         double LogE_true;
//         LogE_true = hist_logE_true -> GetRandom();
//
//         int binE = Etrue_Nhits.GetXaxis()->FindBin( LogE_true+3. ); // +3. to convert Tev in GeV
//
//         if ( binE < 1 || binE >  Etrue_Nhits.GetNbinsX() ){
//             cout << " bogus bin for Etrue = " << LogE_true << endl;
//             exit(1);
//         }
//         static TH1D* hist_nhits = 0;
//
//         if (binE != cache_bin){
//             int firstbinE = binE - (nbins_in_slice-1) / 2;
//             int lastbinE  = binE + (nbins_in_slice-1) / 2;
//
//             if (firstbinE<1) firstbinE = 1;
//             if (lastbinE> Etrue_Nhits.GetNbinsX() ) lastbinE = Etrue_Nhits.GetNbinsX();
//             // cout<<"firstbin  "<<firstbinE<<"   lastbin  "<<lastbinE<<endl;
//
//             if ( verbose > 1 ){
//                 cout << "=========================================================" << endl;
//                 cout << " projecting new slice to use for signal event generation " << endl;
//                 cout << " logEtrue = " << LogE_true << endl;
//                 cout << " bin range = " << firstbinE << " " << lastbinE << endl;
//             }
//
//             Etrue_Nhits.GetXaxis()->SetRange(firstbinE,lastbinE);
//             hist_nhits = (TH1D*) Etrue_Nhits.ProjectionY();
//
//
//             if ( verbose > 1 ){
//                 cout << "integral of the 2d proj           = "<< hist_nhits->Integral() << endl;
//                 // cout << "integral of the full 3d histogram = "<< total_integral << endl;
//                 cout << "=================================================="<<endl;
//
//                 hist_nhits->Draw("goff");
//                 gPad->Update();
//             }
//         }
//
//         double nhit_E2;
//
//         nhit_E2 = hist_nhits -> GetRandom();
//
//         cout << nhit_E2 <<endl;
//
//         // nhit_E2 = gRandom->Poisson( double(170));
//         // cout<<"nhit_E2    "<<nhit_E2<<endl;
//         // if( isShower == 0 ) {nhit_E2 *= sys_nhits_scale;}
//         // cout<<"nhit_E2 2  "<<nhit_E2<<endl;
//
//         // smearing the number of hits can cause the event to fall below the nhits cut for showers
//         // drop this event and reroll
//         // if ( isShower && nhit_E2 < 40 ){
//         //     --i;
//         //     continue;
//         //     std::cout << "cheesus, not again" << std::endl;
//         // }
//
//         // It might be that the event ends up above the horizon
//         // -- not really clear how to deal with that... for now just require the
//         // declination to be within range --
//
//         const double maxdecl = ( 90. - 43. ) * M_PI / 180.;
//         if (decl > maxdecl ){
//             --i;
//             continue;
//         }
//
//
//         static int t1 = TTimeStamp(2007,1,1, 1,1,1 ).GetSec(); // generate random time between these
//         static int t2 = TTimeStamp(2009,1,1, 1,1,1 ).GetSec(); // ..two times
//         static const int nmax = 1000;
//
//         int j; Vec dir; TTimeStamp t;
//         // double zenith;
//
//         for (j=0; j<= nmax ; j++){ // try until we have an upgoing event
//
//             t = TTimeStamp( int ( t1 + (t2-t1) * gRandom->Rndm() ) , 0 );
//             convert_to_det( ra, decl, t , dir ); // convert from equatorial to detector coordinates
//             // zenith = acos(-dir.z);
//             if (dir.z > -0.1) break;
//         }
//
//         // cout << i <<"   "<< isShower <<"   "<< decl <<"   "<< ra <<"   "<< t <<"   "<< zenith << endl;
//
//         if ( j == nmax ){
//             cout << " warning: smear_phi failed to find an upgoing direction belonging to " << decl << "," << ra << endl;
//         }
//
//         if (ra > M_PI)  ra = ra-2*M_PI;
//
//         if ( isShower == 0){
//             sevts[i] = Sev( i,
//                             decl,
//                             ra,
//                             nhit_E2,
//                             1,
//                             isShower) ;
//             sevts[i].time = t;
//         } else {
//             showerevts[i] = Sev( i,
//                             decl,
//                             ra,
//                             nhit_E2,
//                             1,
//                             isShower) ;
//             showerevts[i].time = t;
//         }
//     }
//
//     // remember what we did.
//     if (isShower == 0) mc_source.isTrack  =1.;
//     if (isShower == 1) mc_source.isShower =1.;
//
//     mc_source.nsig  = n ;
//     mc_source.mu    = -999;
//     mc_source.decl  = decl;
//     mc_source.ra    = ra;
//
// }


void minuit_func_with_hits(int & numpars, double *gin, double &f, double *x, int iflag)
{
        // record the minimizaiton path

        vector<double> vv;

        for (int i=0; i<3; i++)
        {
                vv.push_back( x[i] );

                if ( is_bogus(x[i]) )
                {
                        cout << " x = ";
                        for (int j =0; j< 3; j++ ) cout << x[j] << " ";
                        cout << endl;

                        print (" nan detected: quiting ");
                        print ("previous steps for this cluster : ");
                        foreach( v, minimize_path ) print ( v );

                        print ("current cluster :");
                        //    g_current_cluster -> printlots();
                        exit(1);
                }
        }



        bool verbose = g_verbose_fitting;

        double decl  = (numpars==1) ? g_user[1] : x[1];
        double ra    = (numpars==1) ? g_user[2] : x[2];

        /*std::cout << "Fitting Nsig: " << x[0] << std::endl;
           std::cout << "Fitting decl: " << decl << std::endl;
           std::cout << "Fitting ra: "   << ra   << std::endl;
         */

        f =  -g_current_cluster->lik_sb_with_hits(x[0], decl, ra,
                                                  *g_current_psf,
                                                  *g_current_bgr,
                                                  g_current_hist_E2,
                                                  g_current_hist_atm);

        //  std::cout << "Likelihood Value: " << f << " Nsig: "  << x[0] << std::endl;

        if (is_bogus(f))
        {
                cout << " likelihhood is bogus :" << f << endl;
                print ("for x = ", x[0], x[1], x[2] );
        }

        vv.push_back( f );
        minimize_path.push_back( vv );


        //print ("minuit_func called with",x[0],x[1],x[2] ," --> " , f );

        if ( verbose )
        {
                if (g_fit_iter==0)
                {
                        cout << " iter   decl        ra      funval " << endl;
                }

                cout << form("%5d   %g  %g  NLL = %10.7f",
                             g_fit_iter, decl*rad2deg, ra*rad2deg, f ) << endl;
        }


        // fill the fit history sequence
        g_current_cluster->fitdecl.push_back( x[1] );
        g_current_cluster->fitra.push_back( x[2] );
        g_fit_iter++;

}



void Clu::fit_with_nhits( Psf& psf, Bgr& bgr,
                          TH1* hist_E2,
                          TH1* hist_atm,
                          int verbose,
                          double fixed_decl /*= -999*/,
                          double fixed_ra /*= -999*/ )
{
        fit_ifail = fit_npars = 0;
        fit_decl  = fit_ra = fit_time = fit_tnsig = 0;

        if (pevts.size() == 0 )
        {
                cout << " fit tried of cluster with zero hits." << endl;
                fit_ifail = 1000;
                fit_nllsb = fit_nllb = 1;
                return;
        }

        TStopwatch stopwatch;

        double in_decl; // in_ is for initial
        double in_ra;
        double in_nsig;

        // compute center of gravity of the cluster
        get_starting_values ( in_nsig, in_decl, in_ra );
        if (in_nsig > 10 ) in_nsig = 10;

        //cout << "Starting Values (nsig, decl, ra): " << in_nsig << " " << in_decl << " " << in_ra << endl;

        int numpars = 3; // unless fixed_decl and ra are within good range
        if (fixed_decl >= -M_PI/2.  && fixed_decl <= M_PI/2. &&
            fixed_ra   >= -2*M_PI  && fixed_ra   <= 2*M_PI ) {

                in_decl = fixed_decl;
                in_ra   = fixed_ra;
                numpars = 1;
        }

        // in case, of fixed search, determine starting value for nsig by
        // events in a 3 degree cone

        if (numpars == 1) { // fixed

                in_nsig = 0;
                foreach ( ev, pevts ){
                        if ( dist( fixed_decl, fixed_ra,ev.decl, ev.ra) * 180 / M_PI < 3.0 ) in_nsig++;
                }
                if (in_nsig < 1) in_nsig =1;
        }

        fit_clu = new Clu();

        //cout << "Before rotation: " << pevts[0].ra << "\t" << pevts[0].decl << endl;

        rotated_to_zero( in_decl, in_ra, (*fit_clu) );


        //  cout << "After rotation: " << fit_clu->pevts[0].ra << "\t" << fit_clu->pevts[0].decl << endl;


        // the (start) coords are now zero:
        double x[3] = { in_nsig, 0, 0 };

        g_current_cluster = fit_clu;
        g_current_psf     = &psf;
        g_current_bgr     = &bgr;
        g_current_hist_E2 = hist_E2;
        g_current_hist_atm = hist_atm;
        g_fit_iter        = 0;
        g_verbose_fitting = verbose;

        // fill g_user with starting point values - usefull when fixing coordinates (see minuit_func)
        for (int i=0; i< 3; i++ ) {

                g_user[i] = x[i];
        }

        static TMinuit *gMinuit = new TMinuit( 3 );

        gMinuit->SetPrintLevel(-1);
        gMinuit->SetFCN( minuit_func_with_hits );

        int ierflg;

        //                 name    start step  down      up
        gMinuit->mnparm(0, "norm", x[0], 0.1, 1e-3,      1000,     ierflg);
        gMinuit->mnparm(1, "decl", x[1], 0.001, x[1]-0.1, x[1]+0.1,ierflg); // 0.001 radians = 0.05 deg
        gMinuit->mnparm(2, "ra",   x[2], 0.001, x[2]-0.1, x[2]+0.1,ierflg);

        if (numpars==1) { // decl and ra are fixed, tell that to minuit.

                gMinuit->FixParameter(1);
                gMinuit->FixParameter(2);
        }


        // Now ready for minimization step
        minimize_path.clear();
        double arglist[2];
        arglist[0] = 500;
        arglist[1] = 1.;
        gMinuit->mnexcm("MIGRAD", arglist,2,ierflg);

        double norm, err_norm;
        double ra, err_ra;
        double decl, err_decl;
        gMinuit->GetParameter( 0, norm, err_norm);
        gMinuit->GetParameter( 1, decl, err_decl);
        gMinuit->GetParameter( 2, ra,   err_ra);

        x[0] = norm;
        x[1] = decl;
        x[2] = ra;

        //  std::cout << "Fitted Nsig: " << norm << " Fitted Declination: " << decl << " Fitted RA: " << ra << std::endl;


        fit_nllsb  = -g_current_cluster->lik_sb_with_hits(x[0], x[1], x[2],
                                                          *g_current_psf,
                                                          *g_current_bgr,
                                                          g_current_hist_E2,
                                                          g_current_hist_atm,
                                                          &fit_nllb );

        Sev dum( 0, x[1], x[2], 0, 0 );
        Vec v = dum.as_vec();

        v.rotate_y ( -in_decl );
        v.rotate_z ( in_ra ); // sep 1 2010, aart en claudio: fix bug, remove minus sign

        dum.set(v);

        fit_npars = numpars;
        fit_decl  = dum.decl;
        fit_ra    = dum.ra;

        fit_tnsig  = x[0];
        fit_ifail = 0;
        fit_time  = stopwatch.RealTime();

        g_current_cluster = 0;
        g_verbose_fitting = 0;

        //if (!keep_fitcluster)
        delete fit_clu;
}




double Clu::lik_sb_with_hits (double nsig,
                              double decl,
                              double ra,
                              Psf& psf,
                              Bgr& bgr,
                              TH1* hist_E2,
                              TH1* hist_atm,
                              double* lik_b /* =0 */ )
{
        static int ncalled = 0;
        if (ncalled < 10 ) {
                ncalled++;
                cout << "Clu::lik_sb_with_hits... ignore-hits=" << g_ignore_hit_info_in_likelihood << endl;
        }

        double r = 0;
        bool verbose = g_verbose_fitting > 2;

        if (lik_b) *lik_b = 0;

        // Allow for different binning in the histograms.
        // by correcting for normalization and bin-siz
        const double scale_hist_E2  = hist_E2->Integral()  / hist_E2->GetBinWidth(1);
        const double scale_hist_atm = hist_atm->Integral() / hist_atm->GetBinWidth(1);


        for (int i = 0; i< pevts.size(); i++) {
                Sev& ev   = (pevts[i]);

                // if (ev.isShower == 0 && ev.nhits < 13 ){
                //     //             cout << " warning : nhist = " << ev.nhits << ".. setting to 13 to avoid log(0)..." << endl;
                //     ev.nhits = 13;
                // }


                int b = hist_E2->FindBin( ev.nhits );

                double prob_hit_E2  = 1;
                double prob_hit_atm = 1;


                if ( !g_ignore_hit_info_in_likelihood ) {
                        prob_hit_E2 = hist_E2->GetBinContent(b) * scale_hist_E2;

                        if ( b < 1 ) {
                                coolprint::print ("warning : nhits out of hist_E2 bounds", ev.nhits, prob_hit_E2);
                                b=1;
                        } else if ( b > hist_E2->GetNbinsX() ) {
                                coolprint::print ("warning : nhits out of hist_E2 bounds", ev.nhits, prob_hit_E2 );
                                b= hist_E2->GetNbinsX();
                        }

                        int bb= hist_atm->FindBin( ev.nhits );
                        prob_hit_atm = hist_atm->GetBinContent(bb) * scale_hist_atm;

                        if ( bb < 1 ) {
                                coolprint::print ("warning : nhits out of hist_atm bounds", ev.nhits, prob_hit_atm);
                                bb=1;
                        } else if ( bb > hist_E2->GetNbinsX() ) {
                                coolprint::print ("warning : nhits out of hist_atm bounds", ev.nhits, prob_hit_atm );
                                bb= hist_atm->GetNbinsX();
                        }
                }

                // --------------------------------------------------------------------
                //  set the spatial part, using either psd or (pre-smeared) morphology
                // --------------------------------------------------------------------

                double p_sig(0), d(-1);
                // if ( gh_fit_morphology ){
                //     int b = gh_fit_morphology->FindBin( ev.ra, ev.decl );
                //     p_sig =gh_fit_morphology->GetBinContent( b );
                //
                //     if ( ncalled < 10 ){
                //         cout << " using mophology for spatial part of signal likelihood fit " << endl;
                //         coolprint::print( ev.ra, ev.decl, p_sig );
                //     }
                // }
                // else { // regular point source : use psf object
                d  = dist( ev, decl, ra );
                // cout << "Angular Distance (pure Track): " << d*180/M_PI << endl;
                p_sig  = psf.per_sr_vs_a( &d );
                // }


                // -----------------
                // signal likelihood
                // -----------------


                //      std::cout << "PSF value: " << p_sig << std::endl;
                double ls = nsig * p_sig * prob_hit_E2;


                // ---------------------
                // background likelihood
                // ---------------------

                // NB: ev.decl is rotated to zero, so we cannot use it to know the bg here
                // .. as a hack: I save the real decl in lamb --- which I don't use anyway
                //double lb = bgr.per_sr_vs_decl( &ev.lamb );

                double lb = bgr.per_sr_vs_decl( &ev.lamb ) *  prob_hit_atm;

                //      cout <<< prob_hit_atm <<< endl;

                //      std::cout << "ls: " << ls << std::endl;
                //std::cout << "lb: " << lb << "\t lb/bkg_scale: " << lb/bgr.bkg_scale << std::endl;

                if (verbose) {
                        cout << id << " " << decl << " decl,ra = " << ev.lamb << " "
                             << ev.ra << " lb = " << lb << " nhits = "<< ev.nhits << endl;
                }

                r += log ( ls + lb );

                //      cout << "Ingredients: " << ev.decl << "\t"<<  decl << "\t" << ev.ra << "\t" <<  ra << endl;

                //cout << "Angular Distance: " << d << " PSF-Probability: " << p_sig <<" current fit for nsig: " << nsig << endl;

                if ( is_bogus( r ) ) {
                        cout << "Clu::lik_sb_with_hits: " << std::endl;
                        cout << " bogus value detected in likelihood lb r " << r << endl;
                        cout << r << " " << lb << " " << ls << endl;
                        cout << " declination (ev.lamb)          = " << ev.lamb << endl;
                        cout << " bgr.per_sr_vs_decl( &ev.lamb ) = " << bgr.per_sr_vs_decl( &ev.lamb ) << endl;
                        cout << " ls  = " << ls << endl;
                        cout << " psf = " << p_sig << endl;
                        cout << " nsig = " << nsig << endl;
                        cout << " hitprob_e2 = " << prob_hit_E2 << endl;
                        cout << " nhits = " <<  ev.nhits << endl;

                        cout << " PP " << nsig << " " << i << "/" << pevts.size()
                             << " \t " << d  << " \t " << p_sig << "              \t";
                }

                if (lik_b) *lik_b -= log ( lb );
        }

        if (lik_b !=  lik_b ) {
                cout << " Lb is equal to zero!! " << endl;
        }

        r -= nsig;

        //  cout << "R-value: " << r << endl;

        if (r!=r) {
                cout << " nan detected before return of lik_sb_with_hits" << endl;
                cout << " nsig = " << nsig << endl;
        }

        return r;
}


double Clu::combined_lik_sb_with_hits (double tnsig,
                                       double snsig,
                                       double decl,
                                       double ra,
                                       Psf& psf,
                                       Bgr& bgr,
                                       Psf& psf_shower,
                                       Bgr& bgr_shower,
                                       TH1* hist_E2,
                                       TH1* hist_atm,
                                       TH1* hist_E2_shower,
                                       TH1* hist_atm_shower,
                                       double* lik_b)
{

        static int scalled = 0;
        if (scalled < 10 ) {
                ++scalled;
                cout << "Clu::lik_sb_with_hits... ignore-hits=" << g_ignore_hit_info_in_likelihood << endl;
        }

        double r(0);

        bool verbose = g_verbose_fitting > 2;

        if (lik_b) *lik_b = 0;

        // Allow for different binning in the histograms.
        // by correcting for normalization and bin-size
        const double scale_hist_E2  = hist_E2->Integral()  / hist_E2->GetBinWidth(1);
        const double scale_hist_atm = hist_atm->Integral() / hist_atm->GetBinWidth(1);

        const double scale_hist_E2_shower  = hist_E2_shower->Integral()  / hist_E2_shower->GetBinWidth(1);
        const double scale_hist_atm_shower = hist_atm_shower->Integral() / hist_atm_shower->GetBinWidth(1);


        if (snsig > 500) {
                std::cout << "snsig atrociously large: " << snsig << "\n"
                          << "returning -1e99 for llhood" << std::endl;
                return -1e99;
        }



        for (int i = 0; i< pevts.size(); i++) {
                Sev& ev   = (pevts[i]);

                // if (ev.nhits < 13 && ev.isShower == 0){
                //     // cout << " warning : nhits = " << ev.nhits << ".. setting to 13 to avoid log(0)..." << endl;
                //     ev.nhits = 13;
                // }

                if (ev.nhits < 1.8 && ev.isShower ==1) {
                        // cout << " warning : nhits = " << ev.nhits << ".. setting to 21 to avoid log(0)...(for showers)" << endl;
                        ev.nhits = 1.8;
                }

                int b;
                if (ev.isShower == 1) // if event is a shower use the shower objects instead
                        b = hist_E2_shower->FindBin( ev.nhits );
                else
                        b = hist_E2->FindBin( ev.nhits );

                double prob_hit_E2  = 1;
                double prob_hit_atm = 1;


                if ( !g_ignore_hit_info_in_likelihood ) {

                        if ( ev.isShower == 1 )
                                prob_hit_E2 = hist_E2_shower->GetBinContent(b) * scale_hist_E2_shower;
                        else
                                prob_hit_E2 = hist_E2->GetBinContent(b) * scale_hist_E2;



                        if ( b < 1 ) {
                                coolprint::print ("warning : nhits out of hist_E2 bounds", ev.nhits, prob_hit_E2);
                                b=1;
                        }
                        if ( b > hist_E2->GetNbinsX() ) {
                                coolprint::print ("warning : nhits out of hist_E2 bounds", ev.nhits, prob_hit_E2 );
                                b= hist_E2->GetNbinsX();
                        }

                        int bb;
                        if ( ev.isShower == 1 )
                                bb= hist_atm_shower->FindBin( ev.nhits );
                        else
                                bb = hist_atm->FindBin( ev.nhits );


                        if ( ev.isShower == 1 )
                                prob_hit_atm = hist_atm_shower->GetBinContent(bb) * scale_hist_atm_shower;
                        else
                                prob_hit_atm = hist_atm->GetBinContent(bb) * scale_hist_atm;



                        if ( bb < 1 ) {
                                coolprint::print ("warning : nhits out of hist_atm bounds", ev.nhits, prob_hit_atm);
                                bb=1;
                        }
                        if ( bb > hist_E2->GetNbinsX() ) {
                                coolprint::print ("warning : nhits out of hist_atm bounds", ev.nhits, prob_hit_atm );
                                bb= hist_atm->GetNbinsX();
                        }
                }

                // --------------------------------------------------------------------
                //  set the spatial part, using either psf or (pre-smeared) mophrology
                // --------------------------------------------------------------------

                double p_sig(0), d(-1);
                // if ( gh_fit_morphology ){
                //     int b = gh_fit_morphology->FindBin( ev.ra, ev.decl );
                //     p_sig =gh_fit_morphology->GetBinContent( b );
                //
                //     if ( scalled < 10 ){
                //         cout << " using mophology for spatial part of signal likelihood fit " << endl;
                //         coolprint::print( ev.ra, ev.decl, p_sig );
                //     }
                // } else { // regular point source : use psf object

                d  = dist( ev, decl, ra );
                if ( ev.isShower == 1 )
                        p_sig = psf_shower.per_sr_vs_a( &d );
                else
                        p_sig = psf.per_sr_vs_a( &d );
                //
                // }

                prob_hit_E2  = std::max(prob_hit_E2, 1e-19);
                prob_hit_atm = std::max(prob_hit_atm, 1e-19);


                // -----------------
                // signal likelihood
                // -----------------

                double ls;
                if (ev.isShower == 0) {
                        ls = tnsig * p_sig * prob_hit_E2;
                } else {
                        ls = snsig * p_sig * prob_hit_E2;
                }


                // ---------------------
                // background likelihood
                // ---------------------

                // NB: ev.decl is rotated to zero, so we cannot use it to know the bg here
                // .. as a hack: I save the real decl in lamb --- which I don't use anyway
                //double lb = bgr.per_sr_vs_decl( &ev.lamb );

                double lb;
                if ( ev.isShower == 1 )
                        lb = bgr_shower.per_sr_vs_decl( &ev.lamb ) * prob_hit_atm;
                else
                        lb = bgr.per_sr_vs_decl( &ev.lamb ) *  prob_hit_atm;


                if (verbose) {
                        cout << id << " " << decl << " decl,ra = " << ev.lamb << " "
                             << ev.ra << " lb = " << lb << " nhits = "<< ev.nhits << endl;
                }


                // if (ls + lb == 0) continue;
                // else
                r += log( ls + lb );


                if ( is_bogus( r ) ) {
                        cout << "\n Clu::combined_lik_sb_with_hits: " << endl;
                        cout << " bogus value detected in likelihood: " << r << endl;
                        cout << " lb: " << lb << "\n ls: " << ls << endl;
                        cout << " declination (ev.lamb)          = " << ev.lamb << endl;
                        cout << " bgr.per_sr_vs_decl( &ev.lamb ) = " << bgr.per_sr_vs_decl( &ev.lamb ) << endl;
                        cout << " psf = " << p_sig << endl;
                        cout << " tnsig = " << tnsig << endl;
                        cout << " snsig = " << snsig << endl;
                        cout << " hitprob_e2  = " << prob_hit_E2  << endl;
                        cout << " hitprob_atm = " << prob_hit_atm << endl;
                        cout << " nhits = " <<  ev.nhits << endl;

                        cout << " event: "<< i << "/" << pevts.size()
                             << " \ndist: " << d  << "\n" << std::endl;
                }

                if (lik_b) *lik_b -= log ( lb );

        } // end of event loop



        r -= tnsig;
        r -= snsig;


        if (r!=r) {
                cout << " nan detected before return of lik_sb_with_hits" << endl;
                cout << " snsig = " << snsig << endl;
                cout << " tnsig = " << tnsig << endl;
        }

        return r;
}



int Pex::find_clusters( double conesize, int min_cluster_size)
{
        conesize /= (180/M_PI);

        clus.clear();
        set<string> strset; // set of stringreps of clusters to guarrantee uniquness

        for ( int i = 0; i < sevts.size(); i++ ) sevts[i].nclus = 0;

        for ( int i = 0; i < sevts.size(); i++ )
        {
                Sev& s = sevts[i];
                //      Clu c ( &s );
                Clu c ( i, s);
                for ( int j = 0; j < sevts.size(); j++ )
                {
                        if ( i == j ) continue;

                        Sev& t = sevts[j];
                        if ( fabs ( s.decl - t.decl ) > conesize ) continue;  // for speed

                        double d = dist( s, t );
                        if (d < conesize )
                        {
                                //      c.add( &t );
                                c.add( j, t );
                        }
                }

                if ( c.size() >= min_cluster_size )
                {
                        c.sort(); // also sets the stringrep

                        string s = c.stringrep;
                        if ( strset.find( s ) == strset.end() ) // keep only first
                        {
                                clus.push_back( c );
                                strset.insert( s );

                                // keep track of how many clusters an event belongs to
                                for (int k =0; k< c.pevts.size(); k++)
                                {
                                        c.pevts[k].nclus++;
                                }
                        }
                }
        }
        cout << " found " << clus.size() << " clusters " << endl;
        return clus.size();
}

int Pex::add_showers(double conesize, int min_cluster_size)
{
        conesize /= (180/M_PI);
        showerclus.clear();

        set<string> strset; // set of stringreps of clusters to guarrantee uniquness

        for ( int i = 0; i < showerevts.size(); ++i ) showerevts[i].nclus = 0;

        for ( int i = 0; i < clus.size(); ++i ) {
                Clu c;
                for ( int j = 0; j < clus[i].pevts.size(); ++j) {
                        c.add( clus[i].ievts[j], clus[i].pevts[j] );
                }

                int origsize = c.size();

                for ( int j = 0; j < showerevts.size(); ++j ) {

                        Sev& t = showerevts[j];
                        if ( fabs ( clus[i].fit_decl - t.decl ) > conesize ) continue;  // for speed

                        double d = dist( t, clus[i].fit_decl, clus[i].fit_ra );
                        if (d < conesize ) {
                                c.add( j, t );
                        }
                }



                if ( c.size() >= min_cluster_size+origsize ) {
                        c.sort(); // also sets the stringrep

                        string s = c.stringrep;
                        if ( strset.find( s ) == strset.end() ) { // keep only first

                                showerclus.push_back( c );
                                int s_size = showerclus.size()-1;
                                strset.insert( s );

                                // keep track of how many clusters an event belongs to
                                for (int k =0; k< c.pevts.size(); ++k) {
                                        c.pevts[k].nclus++;
                                }
                        }
                }
        }

        return showerclus.size();

}



int Pex::find_clusters_combined(double tr_conesize, double sh_conesize, int min_cluster_size, double sh_weight /*= 3*/)
{
        tr_conesize /= (180./M_PI);
        sh_conesize /= (180./M_PI);

        int N_tr(0), N_sh(0);

        clus.clear();
        set<string> strset; // set of stringreps of clusters to guarrantee uniquness

        for ( int i(0); i < sevts.size(); ++i ) sevts[i].nclus = 0;
        for ( int i(0); i < showerevts.size(); ++i ) showerevts[i].nclus = 0;


        // using tracks as seed and looking for other tracks and showers around
        for ( int i = 0; i < sevts.size(); i++ ) {
                Sev& s = sevts[i];
                Clu c ( i, s);

                // track loop
                for ( int j(0); j < sevts.size(); j++ ) {
                        if ( i == j ) continue;

                        Sev& t = sevts[j];
                        if ( fabs ( s.decl - t.decl ) > tr_conesize ) continue;  // for speed

                        double d = dist( s, t );
                        if (d < tr_conesize ) {
                                c.add( j, t );
                                ++N_tr;
                        }
                }

                // shower loop
                for ( int j(0); j < showerevts.size(); ++j ) {

                        Sev& t = showerevts[j];
                        if ( fabs ( s.decl - t.decl ) > sh_conesize ) continue;  // for speed

                        double d = dist( t, s );
                        if (d < sh_conesize ) {
                                c.add(  sevts.size()+j, t );
                                ++N_sh;
                        }
                }


                if ( N_tr+N_sh*sh_weight >= min_cluster_size ) {
                        c.sort(); // also sets the stringrep

                        string s = c.stringrep;
                        if ( strset.find( s ) == strset.end() ) { // keep only first
                                showerclus.push_back( c );
                                strset.insert( s );

                                // keep track of how many clusters an event belongs to
                                for (int k(0); k< c.pevts.size(); k++) {
                                        c.pevts[k].nclus++;
                                }
                        }
                }
        }





        // using showers as seed and looking for other tracks and showers around
        for ( int i = 0; i < showerevts.size(); i++ ) {
                Sev& s = showerevts[i];
                Clu c ( sevts.size()+i, s);

                // track loop
                for ( int j(0); j < sevts.size(); j++ ) {

                        Sev& t = sevts[j];
                        if ( fabs ( s.decl - t.decl ) > tr_conesize ) continue;  // for speed

                        double d = dist( s, t );
                        if (d < tr_conesize ) {
                                c.add( j, t );
                                ++N_tr;
                        }
                }

                // shower loop
                for ( int j(0); j < showerevts.size(); ++j ) {
                        if ( i == j ) continue;

                        Sev& t = showerevts[j];
                        if ( fabs ( s.decl - t.decl ) > sh_conesize ) continue;  // for speed

                        double d = dist( t, s );
                        if (d < sh_conesize ) {
                                c.add( sevts.size()+j, t );
                                ++N_sh;
                        }
                }


                if ( N_tr+N_sh*sh_weight >= min_cluster_size ) {
                        c.sort(); // also sets the stringrep

                        string s = c.stringrep;
                        if ( strset.find( s ) == strset.end() ) { // keep only first
                                showerclus.push_back( c );
                                strset.insert( s );

                                // keep track of how many clusters an event belongs to
                                for (int k(0); k< c.pevts.size(); k++) {
                                        c.pevts[k].nclus++;
                                }
                        }
                }
        }



        cout << " found " << showerclus.size() << " clusters " << endl;
        return clus.size();
}




int Pex::find_clusters_combined_galacticCentre(double tr_conesize, double sh_conesize, int min_cluster_size, double sh_weight /*= 3*/)
{
        tr_conesize /= (180./M_PI);
        sh_conesize /= (180./M_PI);

        int N_tr(0), N_sh(0);

        clus.clear();
        set<string> strset; // set of stringreps of clusters to guarrantee uniquness

        for ( int i(0); i < sevts.size(); ++i ) sevts[i].nclus = 0;
        for ( int i(0); i < showerevts.size(); ++i ) showerevts[i].nclus = 0;


        static double degtorad = 3.14159 / 180.;
        static double A(27.4 * degtorad);
        static double B(192.25*degtorad);
        static double C(33*degtorad);
        static double l_rad(30*degtorad);
        static double b_rad(15*degtorad);

        // using tracks as seed and looking for other tracks and showers around
        for ( int i = 0; i < sevts.size(); i++ ) {
                Sev& s = sevts[i];
                Clu c ( i, s);


                // only look at seeds close to the galactic centre
                double delt = s.decl;
                double alph = s.ra;
                double b = asin( cos(delt) * cos(A)*cos(alph-B) + sin(delt)*sin(A));
                double l = atan2( (sin(delt) - sin(b)*sin(A)),( cos(delt)*sin(alph-B)*cos(A) ) ) + C;
                if ( pow(l/l_rad,2) + pow(b/b_rad,2) > 1) continue;
                // std::cout << s.decl << " " << s.ra << std::endl;
                // std::cout << l << " " << b << std::endl << endl;


                // track loop
                for ( int j(0); j < sevts.size(); j++ ) {
                        if ( i == j ) continue;

                        Sev& t = sevts[j];
                        if ( fabs ( s.decl - t.decl ) > tr_conesize ) continue;  // for speed

                        double d = dist( s, t );
                        if (d < tr_conesize ) {
                                c.add( j, t );
                                ++N_tr;
                        }
                }

                // shower loop
                for ( int j(0); j < showerevts.size(); ++j ) {

                        Sev& t = showerevts[j];
                        if ( fabs ( s.decl - t.decl ) > sh_conesize ) continue;  // for speed

                        double d = dist( t, s );
                        if (d < sh_conesize ) {
                                c.add(  sevts.size()+j, t );
                                ++N_sh;
                        }
                }


                if ( N_tr+N_sh*sh_weight >= min_cluster_size ) {
                        c.sort(); // also sets the stringrep

                        string s = c.stringrep;
                        if ( strset.find( s ) == strset.end() ) { // keep only first
                                showerclus.push_back( c );
                                strset.insert( s );

                                // keep track of how many clusters an event belongs to
                                for (int k(0); k< c.pevts.size(); k++) {
                                        c.pevts[k].nclus++;
                                }
                        }
                }
        }





        // using showers as seed and looking for other tracks and showers around
        for ( int i = 0; i < showerevts.size(); i++ ) {
                Sev& s = showerevts[i];
                Clu c ( sevts.size()+i, s);


                // only look at seeds close to the galactic centre
                double delt = s.decl * degtorad;
                double alph = s.ra   * degtorad;
                double b = asin( cos(delt) * cos(A)*cos(alph-B) + sin(delt)*sin(A));
                double l = atan2( (sin(delt) - sin(b)*sin(A)),( cos(delt)*sin(alph-B)*cos(A) ) ) + C;
                if ( pow(l/l_rad,2) + pow(b/b_rad,2) > 1) continue;


                // track loop
                for ( int j(0); j < sevts.size(); j++ ) {

                        Sev& t = sevts[j];
                        if ( fabs ( s.decl - t.decl ) > tr_conesize ) continue;  // for speed

                        double d = dist( s, t );
                        if (d < tr_conesize ) {
                                c.add( j, t );
                                ++N_tr;
                        }
                }

                // shower loop
                for ( int j(0); j < showerevts.size(); ++j ) {
                        if ( i == j ) continue;

                        Sev& t = showerevts[j];
                        if ( fabs ( s.decl - t.decl ) > sh_conesize ) continue;  // for speed

                        double d = dist( t, s );
                        if (d < sh_conesize ) {
                                c.add( sevts.size()+j, t );
                                ++N_sh;
                        }
                }


                if ( N_tr+N_sh*sh_weight >= min_cluster_size ) {
                        c.sort(); // also sets the stringrep

                        string s = c.stringrep;
                        if ( strset.find( s ) == strset.end() ) { // keep only first
                                showerclus.push_back( c );
                                strset.insert( s );

                                // keep track of how many clusters an event belongs to
                                for (int k(0); k< c.pevts.size(); k++) {
                                        c.pevts[k].nclus++;
                                }
                        }
                }
        }



        cout << " found " << showerclus.size() << " clusters" << endl;
        return clus.size();
}












void combined_minuit_func_tino(int & numpars, double *gin, double &f, double *x, int iflag);
void Clu::combined_fit_tino(  Psf& psf_track,  Bgr& bgr_track,  TH3* psf_3d_tr, TH2* hist2d_atm_track,  TH1* hist_acc_tracks, double tnbg_,
                              Psf& psf_shower, Bgr& bgr_shower, TH3* psf_3d_sh, TH2* hist2d_atm_shower, TH1* hist_acc_shower, double snbg_,
                              double start_decl /*= -999 pour galplane analysis*/,
                              double start_ra /*= -999 pour galplane analysis*/,
                              double sigma_decl /*=  0.3*/,
                              double sigma_ra /*=  0.3*/,
                              bool comb /*=  false*/,
                              int model /*=  3*/)
//                               int    FitMode    /*=   0 */
{
        tnbg = tnbg_;
        snbg = snbg_;

        fit_ifail = fit_npars = 0;
        fit_decl  = fit_ra = fit_time = fit_tnsig = fit_snsig = fit_nsig = 0;

        static bool FitNShower = 0;

        if (pevts.size() == 0 ) {
                cout << " fit tried of cluster with zero hits." << endl;
                fit_ifail = 1000;
                fit_nllsb = fit_nllb = 1;
                return;
        }

        TStopwatch stopwatch;

        double in_decl; // in_ is for initial
        double in_ra;
        double in_tnsig(0);
        double in_snsig(0);
        double in_ratio;

        // if proper values for declination and right ascention are given,
        // run in fixed search mode
        // in this case, determine starting value for nsig by
        // track events in a 3 degree cone
        // shower events in a 10 degree cone
        if (start_decl >= -M_PI/2.  && start_decl <= M_PI/2. &&
            start_ra   >= -2*M_PI  && start_ra   <= 2*M_PI ) {
                in_decl = start_decl;
                in_ra   = start_ra;

                foreach ( ev, pevts ){
                        if ( dist( start_decl, start_ra,ev.decl, ev.ra) * 180 / M_PI <  3.0 && ev.isShower == 0) ++in_tnsig;
                        if ( dist( start_decl, start_ra,ev.decl, ev.ra) * 180 / M_PI < 10.0 && ev.isShower == 1) ++in_snsig;
                }

        } else {
                // compute center of gravity of the cluster // For galplane analysis, if get the number of tracks and showers
                get_combined_starting_values ( in_tnsig, in_snsig, in_decl, in_ra );
        }

        in_tnsig /= 70;
        in_snsig = in_tnsig/3.5;

        if (in_tnsig < 1) in_tnsig = 1;
        if (in_snsig < 1) in_snsig = 1;

        if (!GalacPlane && in_snsig > 10) in_snsig = 10;


        fit_clu = new Clu();

        if ( !GalacPlane ) rotated_to_zero( in_decl, in_ra, (*fit_clu) );
        else { // we don't do the rotation in this case
                rotated_to_zero( 0,0, (*fit_clu) );
        }

        double x[6];
        // the (start) coords are now zero:

        x[0] = in_tnsig;
        x[1] = in_snsig;
        x[2] = 0;
        x[3] = 0;

        g_current_cluster           = fit_clu;
        g_current_psf               = &psf_track; // Is unused
        g_current_bgr               = &bgr_track;
        g_current_psf_shower        = &psf_shower; // Is unused
        g_current_bgr_shower        = &bgr_shower;
        g_current_psf_3d_tr         = psf_3d_tr;
        g_current_hist2d_atm        = hist2d_atm_track;
        g_current_psf_3d_sh         = psf_3d_sh;
        g_current_hist2d_atm_shower = hist2d_atm_shower;
        g_acc_tracks                = hist_acc_tracks; // Is unused
        g_acc_shower                = hist_acc_shower; // Is unused
        g_fit_iter                  = 0;
        g_verbose_fitting           = 0;

        if (comb == 1){
            double n_sh_expect;
            double n_tr_expect;

            if (model == 2){
                n_sh_expect = 2.28990;
                n_tr_expect = 9.33347;
            }
            if (model == 3){
                n_sh_expect = 2.78362;
                n_tr_expect = 10.9365;
            }
            double ntracks = 0;
            for(double nshowers = 0; nshowers<= 3*n_sh_expect; nshowers+=n_sh_expect/10.) {
                fit_nllsb  = g_current_cluster->combined_lik_sb_with_hits_tino(ntracks, nshowers, x[2], x[3],
                                                                                *g_current_psf,
                                                                                *g_current_bgr,
                                                                                *g_current_psf_shower,
                                                                                *g_current_bgr_shower,
                                                                                g_current_psf_3d_tr,
                                                                                g_current_hist2d_atm,
                                                                                g_current_psf_3d_sh,
                                                                                g_current_hist2d_atm_shower,
                                                                                &fit_nllb,
                                                                                map_lik,
                                                                                comb);

                // cout<<"Showers "<<(fit_nllsb+fit_nllb)*100<<"  "<<ntracks<<"  "<<nshowers<<endl;

                llik.push_back(fit_nllsb+fit_nllb);
                fluxtr.push_back(ntracks);
                fluxsh.push_back(nshowers);
            }

            double nshowers = 0;
            for(double ntracks = 0; ntracks<= 3*n_tr_expect; ntracks+=n_tr_expect/10.) {
                fit_nllsb  = g_current_cluster->combined_lik_sb_with_hits_tino(ntracks, nshowers, x[2], x[3],
                                                                                *g_current_psf,
                                                                                *g_current_bgr,
                                                                                *g_current_psf_shower,
                                                                                *g_current_bgr_shower,
                                                                                g_current_psf_3d_tr,
                                                                                g_current_hist2d_atm,
                                                                                g_current_psf_3d_sh,
                                                                                g_current_hist2d_atm_shower,
                                                                                &fit_nllb,
                                                                                map_lik,
                                                                                comb);

                // cout<<fit_nllsb+fit_nllb<<"  "<<ntracks<<"  "<<nshowers<<endl;

                llik.push_back(fit_nllsb+fit_nllb);
                fluxtr.push_back(ntracks);
                fluxsh.push_back(nshowers);

            }
        }
        else{


            static TMinuit *sMinuit = new TMinuit( 4 );

            sMinuit->SetPrintLevel(-1);
            sMinuit->SetFCN( combined_minuit_func_tino ); // Set the function which will minimize lik_ratio

            int ierflg;
            float numpars(2);

            int tntot = 0;
            int sntot = 0;
            for (int i = 0; i< pevts.size(); i++) {
                    Sev& evt   = pevts[i];
                    if (evt.isShower == 1) sntot += 1;
            }
            tntot = pevts.size() - sntot;

            // Define parameters
            //                           name  start   step         down             up
            sMinuit->mnparm        (0, "tnorm", x[0],   0.1,        -tntot,           tntot,  ierflg);
            sMinuit->mnparm        (1, "snorm", x[1],   0.1,        -sntot,           sntot,  ierflg);

            /****** set range to fit direction, or fix if necessary *****/
            if (sigma_decl > 0) {
                    sMinuit->mnparm    (2, "decl",  x[2], 0.001, x[2]-sigma_decl, x[2]+sigma_decl,  ierflg);
                    ++numpars;
            } else {
                    sMinuit->FixParameter(2);
            }

            if (sigma_ra > 0) {
                    sMinuit->mnparm    (3, "ra",    x[3], 0.001, x[3]-sigma_ra, x[3]+sigma_ra,  ierflg);
                    ++numpars;
            } else {
                    sMinuit->FixParameter(3);
            }


            // Now ready for minimization step
            minimize_path.clear();
            double arglist[2];
            arglist[0] = 500;
            arglist[1] = 1.;

            // for (int i = 0; i< pevts.size(); i++){
            //     Sev& evt   = pevts[i];
            //     cout << evt.isShower << " lalalala ra " << evt.ra<< "  dec "<< evt.decl <<"  time "<< evt.time << endl;
            // }

            //  cout << "Starting Values (tnsig, snsig, decl, ra): " << in_tnsig << " "  << in_snsig << " " << in_decl << " " << in_ra << endl;
            sMinuit->mnexcm("MIGRAD", arglist,2,ierflg); // Call MIGRAD which minimize

            double tnorm, err_tnorm, snorm, err_snorm;
            double ra, err_ra;
            double decl, err_decl;
            sMinuit->GetParameter( 0, tnorm, err_tnorm);
            sMinuit->GetParameter( 1, snorm, err_snorm);
            sMinuit->GetParameter( 2, decl, err_decl);
            sMinuit->GetParameter( 3, ra, err_ra);

            //set the minimized parameters
            x[0] = tnorm;
            x[1] = snorm;
            x[2] = decl;
            x[3] = ra;

            // compute the lik ratio and put it in fit_nllsb
            fit_nllsb  = -g_current_cluster->combined_lik_sb_with_hits_tino(x[0], x[1], x[2], x[3],
                                                                            *g_current_psf,
                                                                            *g_current_bgr,
                                                                            *g_current_psf_shower,
                                                                            *g_current_bgr_shower,
                                                                            g_current_psf_3d_tr,
                                                                            g_current_hist2d_atm,
                                                                            g_current_psf_3d_sh,
                                                                            g_current_hist2d_atm_shower,
                                                                            &fit_nllb,
                                                                            map_lik); /*,
                                                                                      &fit_nllsb_track,
                                                                                      &fit_nllsb_shower,
                                                                                      &fit_nllb_track,
                                                                                      &fit_nllb_shower);*/



            if ( !GalacPlane ) {

                    Sev dum( 0, x[2], x[3], 0, 0 );
                    Vec v = dum.as_vec();

                    v.rotate_y ( -in_decl );
                    v.rotate_z ( in_ra ); // sep 1 2010, aart en claudio: fix bug, remove minus sign

                    dum.set(v);

                    fit_decl  = dum.decl;
                    fit_ra    = dum.ra;
            }

            fit_npars = numpars;

            fit_tnsig  = abs(x[0]);
            fit_snsig = abs(x[1]);
            fit_ifail = 0;
            fit_time  = stopwatch.RealTime();

            g_current_cluster = 0;
            g_verbose_fitting = 0;
        }
        //  cout << "FitResult (TrackE, ShowerE, Decl, Ra, LS, LB): " << fit_nsig << " " << fit_snsig << " " << fit_decl << " " << fit_ra << " " << fit_nllsb << " " << fit_nllb << endl;

        //if (!keep_fitcluster)
        delete fit_clu;
}


//minimize the lik ratio and put the result in g_current_cluster
void combined_minuit_func_tino(int & numpars, double *gin, double &f, double *x, int iflag){
        // record the minimizaiton path
        vector<double> vv;


        for (int i=0; i<4; i++) {
                vv.push_back( x[i] );

                if ( is_bogus(x[i]) ) {
                        cout << " x = ";
                        for (int j =0; j< 4; j++ ) cout << x[j] << " ";
                        cout << endl;

                        print (" nan detected: quiting ");
                        print ("previous steps for this cluster : ");
                        foreach( v, minimize_path ) print ( v );

//           print ("current cluster :");
//           g_current_cluster -> printlots();
                        exit(1);
                }
        }

        bool verbose = g_verbose_fitting;

        double x0   = x[0];
        double x1   = x[1];
        double decl = x[2];
        double ra   = x[3];

        //   cout << x0 << "\t" << x1 << endl;*
        double fit_nllb = 1;
        // compute the lik ratio and put it in f
        f = -g_current_cluster->combined_lik_sb_with_hits_tino(x0, x1, decl, ra,
                                                               *g_current_psf,
                                                               *g_current_bgr,
                                                               *g_current_psf_shower,
                                                               *g_current_bgr_shower,
                                                               g_current_psf_3d_tr,
                                                               g_current_hist2d_atm,
                                                               g_current_psf_3d_sh,
                                                               g_current_hist2d_atm_shower,
                                                               &fit_nllb);


        //  std::cout << "Likelihood Value: " << f << " Nsig: "  << x[0] << std::endl;

        if (is_bogus(f)) {
                cout << " likelihhood is bogus :" << f << endl;
                print ("for x = ", x[0], x[1], x[2], x[3] );
        }

        vv.push_back( f );
        minimize_path.push_back( vv ); //minimize le lik


        //print ("minuit_func called with",x[0],x[1],x[2] ," --> " , f );

        if ( verbose ) {
                if (g_fit_iter==0) {
                        cout << " iter   decl        ra      funval " << endl;
                }

                cout << form("%5d   %g  %g  NLL = %10.7f",
                             g_fit_iter, decl*rad2deg, ra*rad2deg, f ) << endl;
        }


        // fill the fit history sequence
        g_current_cluster->fitdecl.push_back( x[2] );
        g_current_cluster->fitra.push_back( x[3] );
        ++g_fit_iter;

}

// compute and return the lik ratio
double Clu::combined_lik_sb_with_hits_tino (double tnsig,
                                            double snsig,
                                            double decl,
                                            double ra,
                                            Psf& psf,
                                            Bgr& bgr,
                                            Psf& psf_shower,
                                            Bgr& bgr_shower,
                                            TH3* psf_3d_tr,
                                            TH2* hist2d_atm_track,
                                            TH3* psf_3d_sh,
                                            TH2* hist2d_atm_shower,
                                            double* lik_b,
                                            TH3F* map_lik_,
                                            bool comb,
                                            TH1F* test)
{
        double r(0);
//     bool verbose = g_verbose_fitting > 2;

        if (lik_b) *lik_b = 0;

        // Allow for different binning in the histograms.
        // by correcting for normalization and bin-size
        // const double scale_hist_atm = 1. / (hist_atm->Integral() * hist_atm->GetBinWidth(1));
        // const double scale_hist_atm_shower = 1. / (hist_atm_shower->Integral() * hist_atm_shower->GetBinWidth(1));

        // TH1F* F0 = new TH1F("F0", "F0", 1000,0,0.006);
        // TH1F* Phit_E2_0 = new TH1F("prob_hit_E2_0", "prob_hit_E2_0", 1000, 0, 0.1);
        // TH1F* B0 = new TH1F("B0", "B0", 1000,0,1300);
        // TH1F* Phit_atm0 = new TH1F("prob_hit_atm0", "prob_hit_atm0", 1000,0,0.1);
        //
        // TH1F* F1 = new TH1F("F1", "F1", 1000,0,0.006);
        // TH1F* Phit_E2_1 = new TH1F("prob_hit_E2_1", "prob_hit_E2_1", 1000, 0, 0.1);
        // TH1F* B1 = new TH1F("B1", "B1", 1000,0,1300);
        // TH1F* Phit_atm1 = new TH1F("prob_hit_atm1", "prob_hit_atm1", 1000,0,0.1);

//  Rescale gh_fit_morphology_shower and gh_fit_morphology_tracks
        double integral_sh(0);
        double integral_tr(0);

        double nbins = gh_galplane_morphology_tr->GetNbinsX()*gh_galplane_morphology_tr->GetNbinsY();
        if (proba_zone == 0) {
                integral_sh = gh_fit_morphology_shower->Integral();
                integral_tr = gh_fit_morphology_tracks->Integral();
        }
        else{
                for (int k(1); k <= nbins; k++) {
                        double prob_morph = gh_galplane_morphology_tr->GetBinContent(k);
                        if (prob_morph >= proba_zone) {
                                integral_sh += gh_fit_morphology_shower->GetBinContent(k);
                                integral_tr += gh_fit_morphology_tracks->GetBinContent(k);
                        }
                }
        }

        gh_fit_morphology_shower->Scale(1./integral_sh);
        gh_fit_morphology_tracks->Scale(1./integral_tr);


        TH2D* psf_2d_tr_Dec = 0;
        TH2D* psf_2d_tr_Ra  = 0;
        TH2D* psf_2d_sh_Dec = 0;
        TH2D* psf_2d_sh_Ra  = 0;
        // TH2D* psf_3d_tr = 0;
        // TH2D* psf_3d_sh = 0;
        // TH2D* morpho_local_tr = 0;
        // TH2D* morpho_local_sh = 0;
        double total_integral_tr  = 0;
        double total_integral_sh  = 0;
        double integral_morpho_tr = 0;
        double integral_morpho_sh = 0;

        int sntot = 0;
        int tntot = 0;
        if(pevts[0].P_pos_sig == -1 || (pevts[0].P_pos_sig <1e-30 && pevts[0].P_pos_sig > -1e-30)) { // if Proba to have this position is -1 or 0, ie if it has not been set, ie if it is the first time we enter this function
                for (int i = 0; i< pevts.size(); i++) {
                        Sev& evt   = pevts[i];
                        if (evt.isShower == 1) sntot += 1;
                }
                tntot = pevts.size() - sntot;

                /////////////////////////////////////////////////////
                // psf_3d_tr = (TH2D*) psf_3d_tr->Project3D("yz");

                psf_2d_tr_Dec = (TH2D*) psf_3d_tr->Project3D("yz");

                psf_2d_tr_Ra = (TH2D*) psf_3d_tr->Project3D("xz");

                // morpho_local_tr = (TH2D*) psf_3d_tr->Project3D("yx");
                // integral_morpho_tr = morpho_local_tr->Integral();
                // morpho_local_tr->Scale(1./(integral_morpho_tr)*128*128*2/(4.*M_PI)); // rate of events/sr in comparison to the mean rate (1 = mean rate, 2 = two times more probable than mean)

                // psf_3d_sh = (TH2D*) psf_3d_sh->Project3D("yz");

                psf_2d_sh_Dec = (TH2D*) psf_3d_sh->Project3D("yz");

                psf_2d_sh_Ra = (TH2D*) psf_3d_sh->Project3D("xz");

                // morpho_local_sh = (TH2D*) psf_3d_sh->Project3D("yx");
                // integral_morpho_sh = morpho_local_sh->Integral();
                // morpho_local_sh->Scale(1./(integral_morpho_sh)*128*128*2/(4.*M_PI));
                //////////////////////////////////////////////////////
        }
        else {
                sntot = pevts[0].sntot;
                tntot = pevts[0].tntot;
        }

//loop over all the events to compute the likelihood
        for (int i = 0; i< pevts.size(); i++) {
                // if (i ==  pevts.size() -1 ) {exit(0);}
                // cout << i <<endl;
                Sev& ev   = pevts[i];
                if (comb == 1){
                    if ((snsig == 0 && ev.isShower == 1) || (tnsig == 0 && ev.isShower == 0)){
                        continue;
                    }
                }
                // cout << i <<"  "<< ev.decl <<"  "<< ev.ra <<"  "<< ev.time << endl;

                if (ev.decl > 0.922) {
                        continue;
                }

                // if (ev.nhits < 13 && ev.isShower == 0){
                //     ev.nhits = 13;
                // }

                if (ev.nhits < 1.8 && ev.isShower ==1) {
                        ev.nhits = 1.8;
                }

                double lb;
                double ls;

                if(ev.P_pos_sig == -1 || (ev.P_pos_sig <1e-30 && ev.P_pos_sig > -1e-30)) {

                        TH1D* hist_nhits = 0;
                        TH1D* hist_nhits_Dec = 0;
                        TH1D* hist_nhits_Ra = 0;
                        int nbins_in_slice = 2;
                        int cache_bin = 0;
                        int binD = 0;
                        int firstbinD = 0;
                        int lastbinD = 0;
                        int binR = 0;
                        int firstbinR = 0;
                        int lastbinR = 0;
                        double zenith = 0;
                        double azimuth = 0;


                        if(ev.isShower == 1) {

                                psf_2d_sh_Dec->GetYaxis()->SetRange(1,0); // reset
                                psf_2d_sh_Ra->GetYaxis()->SetRange(1,0); // reset
                                double total_integral = psf_2d_sh_Dec->Integral();

                                double decl = ev.decl;
                                double ra = ev.ra;

                                if (decl<=-TMath::Pi()/2.) {
                                        cout << "decl<=-TMath::Pi()/2"<<endl;
                                        decl = -TMath::Pi()/2.;
                                }

                                binD = psf_2d_sh_Dec->GetYaxis()->FindBin( decl );
                                // cout << binD << "   decl " << decl << endl;

                                if ( binD < 1 || binD >  psf_2d_sh_Dec->GetNbinsY() ) {
                                        cout << " bogus bin for declination = " << decl <<"  "<< binD << endl;
                                        exit(1);
                                }

                                binR = psf_2d_sh_Ra->GetYaxis()->FindBin( ra );

                                if ( binR < 1 || binR >  psf_2d_sh_Ra->GetNbinsY() ) {
                                        cout << " bogus bin for right ascension = " << ra << endl;
                                        exit(1);
                                }

                                firstbinD = binD - (nbins_in_slice-1) / 2;
                                lastbinD  = binD + (nbins_in_slice-1) / 2;

                                if (firstbinD<1) firstbinD = 1;
                                if (lastbinD> psf_2d_sh_Dec->GetNbinsY() ) lastbinD = psf_2d_sh_Dec->GetNbinsY();
                                // cout<<"firstbinD  "<<firstbinD<<"   lastbinD  "<<lastbinD<<endl;

                                firstbinR = binR - (nbins_in_slice-1) / 2;
                                lastbinR  = binR + (nbins_in_slice-1) / 2;

                                if (firstbinR<1) firstbinR = 1;
                                if (lastbinR> psf_2d_sh_Ra->GetNbinsY() ) lastbinD = psf_2d_sh_Ra->GetNbinsY();

                                psf_2d_sh_Dec->GetYaxis()->SetRange(firstbinD,lastbinD);
                                hist_nhits_Dec = (TH1D*) psf_2d_sh_Dec->ProjectionX();
                                psf_2d_sh_Ra->GetYaxis()->SetRange(firstbinR,lastbinR);
                                hist_nhits_Ra = (TH1D*) psf_2d_sh_Ra->ProjectionX();

                                if ( hist_nhits_Dec->Integral() /  total_integral < 1e-5  ) {
                                        cout << " error : too few events in slice: decl " <<  hist_nhits_Dec->Integral() <<" "<< binD <<" "<< decl <<" "<< firstbinD <<" "<< lastbinD << endl;
                                        //             exit(0);
                                }

                                if ( hist_nhits_Ra->Integral() /  total_integral < 1e-5  ) {
                                        cout << " error : too few events in slice: ra " <<  hist_nhits_Ra->Integral() <<" "<< binR <<" "<< ra <<" "<< firstbinR <<" "<< lastbinR << endl;
                                        //             exit(0);
                                }

                        } else { // if Track
                                psf_2d_tr_Dec->GetYaxis()->SetRange(1,0); // reset
                                psf_2d_tr_Ra->GetYaxis()->SetRange(1,0); // reset
                                double total_integral = psf_2d_tr_Dec->Integral();

                                double decl = ev.decl;
                                double ra = ev.ra;

                                if (decl<=-TMath::Pi()/2.) {
                                        cout << "decl<=-TMath::Pi()/2"<<endl;
                                        decl = -TMath::Pi()/2.;
                                }

                                binD = psf_2d_tr_Dec->GetYaxis()->FindBin( decl );

                                if ( binD < 1 || binD >  psf_2d_tr_Dec->GetNbinsY() ) {
                                        cout << " bogus bin for declination = " << decl <<"  "<< binD << endl;
                                        exit(1);
                                }

                                binR = psf_2d_tr_Ra->GetYaxis()->FindBin( ra );

                                if ( binR < 1 || binR >  psf_2d_tr_Ra->GetNbinsY() ) {
                                        cout << " bogus bin for right ascension = " << ra << endl;
                                        exit(1);
                                }

                                firstbinD = binD - (nbins_in_slice-1) / 2;
                                lastbinD  = binD + (nbins_in_slice-1) / 2;

                                if (firstbinD<1) firstbinD = 1;
                                if (lastbinD> psf_2d_tr_Dec->GetNbinsY() ) lastbinD = psf_2d_tr_Dec->GetNbinsY();
                                // cout<<"firstbinD  "<<firstbinD<<"   lastbinD  "<<lastbinD<<endl;

                                firstbinR = binR - (nbins_in_slice-1) / 2;
                                lastbinR  = binR + (nbins_in_slice-1) / 2;

                                if (firstbinR<1) firstbinR = 1;
                                if (lastbinR> psf_2d_tr_Ra->GetNbinsY() ) lastbinD = psf_2d_tr_Ra->GetNbinsY();

                                psf_2d_tr_Dec->GetYaxis()->SetRange(firstbinD,lastbinD);
                                hist_nhits_Dec = (TH1D*) psf_2d_tr_Dec->ProjectionX();
                                psf_2d_tr_Ra->GetYaxis()->SetRange(firstbinR,lastbinR);
                                hist_nhits_Ra = (TH1D*) psf_2d_tr_Ra->ProjectionX();

                                if ( hist_nhits_Dec->Integral() /  total_integral < 1e-5 ) {
                                        cout << " error : too few events in slice: decl " <<  hist_nhits_Dec->Integral() <<" "<< binD <<" "<< decl <<" "<< firstbinD <<" "<< lastbinD << endl;
                                        //             exit(0);
                                }

                                if ( hist_nhits_Ra->Integral() /  total_integral < 1e-5  ) {
                                        cout << " error : too few events in slice: ra " <<  hist_nhits_Ra->Integral() <<" "<< binR <<" "<< ra <<" "<< firstbinR <<" "<< lastbinR << endl;
                                        //             exit(0);
                                }

                        }

                        Vec dir;

                        convert_to_det( ev.ra, ev.decl, ev.time,dir ); // convert from equatorial to detector coordinates
                        zenith  = acos( -dir.z ); // + sys_offset_theta;
                        azimuth = atan2( dir.y, dir.x ) + M_PI; // + sys_offset_phi /*+ 0.5*TMath::DegToRad()*/;
                        if(azimuth < 0) {
                                azimuth += 2*M_PI;
                        }


                        int bDec;
                        int bRa;
                        double prob_hit_atm    = 1;
                        double prob_hit_E2     = 1;
                        double prob_hit_E2_Dec = 1;
                        double prob_hit_E2_Ra  = 1;

                        if ( !g_ignore_hit_info_in_likelihood ) {

                                bDec = hist_nhits_Dec->FindBin( ev.nhits );

                                prob_hit_E2 = hist_nhits_Dec->GetBinContent(bDec) /(hist_nhits_Dec->Integral() * hist_nhits_Dec->GetBinWidth(1));
                                // prob_hit_E2 = TMath::Poisson(ev.nhits, 150);

                                if ( bDec < 1 ) {
                                        coolprint::print ("warning : nhits out of hist_nhits_Dec bounds", ev.isShower, hist_nhits_Dec->GetBinCenter(1), hist_nhits_Dec->GetBinCenter(hist_nhits_Dec->GetNbinsX()), ev.nhits, prob_hit_E2);
                                        bDec=1;
                                }

                                if ( bDec > hist_nhits_Dec->GetNbinsX() ) {
                                        coolprint::print ("warning : nhits out of hist_nhits_Dec bounds 2", ev.isShower, hist_nhits_Dec->GetBinCenter(1), hist_nhits_Dec->GetBinCenter(hist_nhits_Dec->GetNbinsX()), "nhits:", ev.nhits, prob_hit_E2);
                                        bDec= hist_nhits_Dec->GetNbinsX();
                                }
                                prob_hit_E2_Dec = hist_nhits_Dec->GetBinContent(bDec) /(hist_nhits_Dec->Integral() * hist_nhits_Dec->GetBinWidth(1));
                                // prob_hit_E2 = TMath::Poisson(ev.nhits, 170);


                                bRa = hist_nhits_Ra->FindBin( ev.nhits );

                                prob_hit_E2 = hist_nhits_Ra->GetBinContent(bRa) /(hist_nhits_Ra->Integral() * hist_nhits_Ra->GetBinWidth(1));
                                // prob_hit_E2 = TMath::Poisson(ev.nhits, 150);

                                if ( bRa < 1 ) {
                                        coolprint::print ("warning : nhits out of hist_nhits_Ra bounds", ev.isShower, hist_nhits_Ra->GetBinCenter(1), hist_nhits_Ra->GetBinCenter(hist_nhits_Ra->GetNbinsX()), ev.nhits, prob_hit_E2);
                                        bRa=1;
                                }

                                if ( bRa > hist_nhits_Ra->GetNbinsX() ) {
                                        coolprint::print ("warning : nhits out of hist_nhits_Ra bounds 2", ev.isShower, hist_nhits_Ra->GetBinCenter(1), hist_nhits_Ra->GetBinCenter(hist_nhits_Ra->GetNbinsX()), "nhits:", ev.nhits, prob_hit_E2);
                                        bRa= hist_nhits_Ra->GetNbinsX();
                                }
                                prob_hit_E2_Ra = hist_nhits_Ra->GetBinContent(bRa) /(hist_nhits_Ra->Integral() * hist_nhits_Ra->GetBinWidth(1));
                                // prob_hit_E2 = TMath::Poisson(ev.nhits, 170);


                                prob_hit_E2 = sqrt(prob_hit_E2_Dec) * sqrt(prob_hit_E2_Ra);
                                // prob_hit_E2 = prob_hit_E2_Dec;

                                TH1* hist_atm = 0;
                                int bin = 0;
                                int firstbin = 0;
                                int lastbin = 0;

                                // cout << ev.isShower << " lololo ra " << ev.ra<< "  dec "<< ev.decl <<"  time "<< ev.time <<"  zenith "<<zenith<< endl;

                                nbins_in_slice = 4;
                                if(ev.isShower == 1) {

                                        hist2d_atm_shower->GetYaxis()->SetRange(1,0); // reset
                                        double total_integral = hist2d_atm_shower->Integral();

                                        bin = hist2d_atm_shower->GetYaxis()->FindBin( zenith );

                                        if ( bin < 2 || bin >  hist2d_atm_shower->GetNbinsY() ) {
                                                cout << " bogus bin for zenith = " << zenith <<"  "<< bin << endl;
                                                cout << ev.isShower << "  ra " << ev.ra<< "  dec "<< ev.decl <<"  time "<< ev.time << endl;
                                                exit(1);
                                                zenith = 2.51;
                                                bin = 10;
                                        }

                                        firstbin = bin - (nbins_in_slice-1) / 2;
                                        lastbin  = bin + (nbins_in_slice-1) / 2;

                                        if (firstbin<2) firstbin = 2;
                                        if (lastbin> hist2d_atm_shower->GetNbinsY() ) lastbin = hist2d_atm_shower->GetNbinsY();
                                        // cout<<"firstbin  "<<firstbin<<"   lastbin  "<<lastbin<<endl;

                                        hist2d_atm_shower->GetYaxis()->SetRange(firstbin,lastbin);
                                        hist_atm = (TH1D*) hist2d_atm_shower->ProjectionX();

                                        if ( hist_atm->Integral() /  total_integral < 1e-4  ) {
                                                cout << " error : too few events in slice: zenith " <<  hist_atm->Integral() <<" "<< bin <<" "<< decl <<" "<< firstbin <<" "<< lastbin << endl;
                                                //             exit(0);
                                        }


                                        // hist_atm = (TH1D*) hist2d_atm_shower->ProjectionX();


                                }else{

                                        hist2d_atm_track->GetYaxis()->SetRange(1,0); // reset
                                        double total_integral = hist2d_atm_track->Integral();

                                        bin = hist2d_atm_track->GetYaxis()->FindBin( zenith );

                                        if ( bin < 2 || bin >  hist2d_atm_track->GetNbinsY() ) {
                                                cout << " bogus bin for zenith = " << zenith <<"  "<< bin << endl;
                                                cout << "  ra " << ev.ra<< "  dec "<< ev.decl <<"  time "<< ev.time << "  nhits "<< ev.nhits<< endl;
                                                exit(1);
                                                zenith = 2.51;
                                                bin = 10;
                                        }

                                        firstbin = bin - (nbins_in_slice-1) / 2;
                                        lastbin  = bin + (nbins_in_slice-1) / 2;

                                        if (firstbin<2) firstbin = 2;
                                        if (lastbin> hist2d_atm_track->GetNbinsY() ) lastbin = hist2d_atm_track->GetNbinsY();
                                        // cout<<"firstbin  "<<firstbin<<"   lastbin  "<<lastbin<<endl;

                                        hist2d_atm_track->GetYaxis()->SetRange(firstbin,lastbin);
                                        hist_atm = (TH1D*) hist2d_atm_track->ProjectionX();

                                        if ( hist_atm->Integral() /  total_integral < 1e-4  ) {
                                                cout << " error : too few events in slice: zenith " <<  hist_atm->Integral() <<" "<< bin <<" "<< decl <<" "<< firstbin <<" "<< lastbin << endl;
                                                //             exit(0);
                                        }


                                        // hist_atm = (TH1D*) hist2d_atm_track->ProjectionX();

                                }

                                double scale_hist_atm = 1. / (hist_atm->Integral() * hist_atm->GetBinWidth(1));

                                int bb;
                                bb = hist_atm->FindBin( ev.nhits );
                                prob_hit_atm = hist_atm->GetBinContent(bb) * scale_hist_atm;

                                if ( bb < 1 ) {
                                        coolprint::print ("warning : nhits out of hist_atm bounds", ev.nhits, prob_hit_atm);
                                        bb=1;
                                }
                                if ( bb > hist_atm->GetNbinsX() ) {
                                        coolprint::print ("warning : nhits out of hist_atm bounds", ev.nhits, prob_hit_atm );
                                        bb= hist_atm->GetNbinsX();
                                }
                                prob_hit_atm = hist_atm->GetBinContent(bb) * scale_hist_atm;
                                // cout << ev.isShower << "  proba " << prob_hit_atm << "    bin E "<< bb <<"   nhits "<<ev.nhits << endl;
                                // prob_hit_atm = TMath::Poisson(ev.nhits, 110);
                        }


                        ////////////////////////////////////////////////////////////
                        // --------------------------------------------------------------------
                        //  set the spatial part, using either psf or (pre-smeared) morphology
                        // --------------------------------------------------------------------
                        double ra_interp, decl_interp;
                        double p_sig(0), d(-1);
                        if ( gh_fit_morphology_tracks ) {
                                if ( GalacPlane ) {
                                        ra_interp = ev.ra;
                                        decl_interp = ev.decl;
                                }
                                else{
                                        ra_interp = -1.633279-ev.ra+ra;
                                        decl_interp = -.506145-ev.decl+decl;
                                }

                                if ( ev.isShower == 1 ) {
                                        p_sig = gh_fit_morphology_shower->Interpolate(ra_interp, decl_interp)*(128*128*2)/(4*M_PI);
                                        // p_sig = InterpolateTH2NoRestrain(gh_fit_morphology_shower, ra_interp, decl_interp)*(128*128*2)/(4*M_PI);
                                        // cout << p_sig << endl;
                                        // p_sig = gh_fit_morphology_shower->GetBinContent( gh_fit_morphology_shower->FindBin( ra_interp, decl_interp))*(128*128*2)/(4*M_PI); //InterpolateTH2NoRestrain(gh_fit_morphology_shower, ra_interp, decl_interp);

                                } else {
                                        p_sig = gh_fit_morphology_tracks->Interpolate(ra_interp, decl_interp)*(128*128*2)/(4*M_PI);
                                        // p_sig = InterpolateTH2NoRestrain(gh_fit_morphology_tracks, ra_interp, decl_interp)*(128*128*2)/(4*M_PI);
                                        // p_sig = gh_fit_morphology_tracks->GetBinContent( gh_fit_morphology_tracks->FindBin( ra_interp, decl_interp))*(128*128*2)/(4*M_PI);
                                }
                                if (p_sig < 1e-9) p_sig = 1e-9;



                        } else { // regular point source : use psf object

                                d  = dist( ev, decl, ra );
                                if ( ev.isShower == 1 )
                                        p_sig = psf_shower.per_sr_vs_a( &d );
                                else
                                        p_sig = psf.per_sr_vs_a( &d );
                        }
                        // cout << prob_hit_E2<<" c "<<prob_hit_atm<<"  "<< ev.ra<<"  "<<ev.decl<<"  "<<ev.isShower<<"  "<<ev.nhits<<endl;
                        prob_hit_E2  = std::max(prob_hit_E2, 1e-12);
                        prob_hit_atm = std::max(prob_hit_atm, 1e-12);


                        if(cos(zenith)>0.1) {
                            if (cos(zenith)<0.101) {
                                zenith = 1.4707;
                            }
                            else{
                                cout << "error cos(zen) > 0.1  "<< cos(zenith) << "  "<< ev.time << endl;
                                exit(0);
                            }
                        }
                        // double coszen = cos(zenith);
                        double p_bg;
                        if ( ev.isShower == 1 ) {

                                ///////// My spline decl
                                p_bg = bgr_shower.per_sr_vs_decl( &ev.decl )/(snbg); // rate of events/full_sky in comparison to the mean rate
                                // cout << "  sh  "<<sin(ev.decl) <<"  "<< p_bg << "  "<< p_sig <<endl;
                                // p_bg = bgr_shower.per_az_vs_az( &azimuth )/(snbg*2); // rate of events/full_sky in comparison to the mean rate
                                // p_bg = bgr_shower.per_coszen_vs_coszen( &coszen )/(snbg*4*pi) /1.1*1.78; // rate of events/full_sky in comparison to the mean rate
                                // cout << coszen <<"   sh   "<< p_bg*(snbg*2*pi) <<"  "<< p_bg*4*pi << endl;
                        } else {
                                p_bg = bgr.per_sr_vs_decl( &ev.decl )/(tnbg);
                                // cout << sin(ev.decl) <<"  "<< p_bg << "  "<< p_sig <<endl;
                                // p_bg = bgr.per_coszen_vs_coszen( &coszen )/(tnbg*4*pi) /1.1*1.78; // rate of events/full_sky in comparison to the mean rate
                                // cout << coszen <<" tr "<< p_bg*(tnbg*2*pi) <<"  "<< p_bg*4*pi << endl;
                        }

                        // if (i%101 == 0) cout << p_bg <<"  " <<zenith<<endl;

                        pevts[i].P_pos_sig = p_sig; // proba for signal event to be at this position
                        pevts[i].P_pos_bg  = p_bg;

                        pevts[i].P_nhits_sig = prob_hit_E2; //TMath::Gaus(prob_hit_E2, 0.0002); //proba for signal event to have this number of hits
                        pevts[i].P_nhits_bg  = prob_hit_atm; //TMath::Gaus(prob_hit_atm, 0.0002);


                        if(i == 0) {
                                pevts[0].sntot = sntot;
                                pevts[0].tntot = tntot;
                        }

                        // signal likelihood
                        if (pevts[i].isShower == 1) {
                                ls = abs(snsig) * pevts[i].P_pos_sig * pevts[i].P_nhits_sig;
                        } else {
                                ls = abs(tnsig) * pevts[i].P_pos_sig * pevts[i].P_nhits_sig;
                        }

                        // background likelihood
                        if ( pevts[i].isShower == 1 ) {
                                lb =  (sntot - abs(snsig)) * pevts[i].P_pos_bg * pevts[i].P_nhits_bg;
                        } else {
                                lb =  (tntot - abs(tnsig)) * pevts[i].P_pos_bg * pevts[i].P_nhits_bg;
                        }
                        // cout << pevts[i].P_nhits_sig/pevts[i].P_nhits_bg << endl;

                } else {
                        // cout<<"  lalala   "<<ev.P_nhits_sig<<;
                        // signal likelihood
                        if (ev.isShower == 1) {
                                ls = abs(snsig) * ev.P_pos_sig * ev.P_nhits_sig;
                        } else {
                                ls = abs(tnsig) * ev.P_pos_sig * ev.P_nhits_sig;
                        }


                        // background likelihood
                        if ( ev.isShower == 1 ) {
                                lb =  (sntot - abs(snsig)) * ev.P_pos_bg * ev.P_nhits_bg;
                        } else {
                                lb =  (tntot - abs(tnsig)) * ev.P_pos_bg * ev.P_nhits_bg;
                        }
                        // cout << ls <<"  "<< lb <<"  "<<ls/lb<<"  "<<ev.isShower<<"  "<<i<<"  "<<ev.time<<"  "<<ev.P_pos_bg<<"  "<<ev.decl<<"  "<<ev.ra<<"  "<<ev.P_pos_sig<<"  "<<  ev.P_nhits_bg <<"  "<< ev.P_nhits_sig <<endl;
                }


                r += log( ls  + lb );


                if ( is_bogus( r ) ) {
                        cout << "\n Clu::combined_lik_sb_with_hits: " << endl;
                        cout << " bogus value detected in likelihood: " << r << endl;
                        cout << " lb: " << lb << "\n ls: " << ls << endl;
                        cout << " declination (ev.decl)  = " << ev.decl << endl;
                        cout << " P_pos_bg = " << ev.P_pos_bg << endl;
                        cout << " P_pos_sig = " << ev.P_pos_sig << endl;
                        cout << " tnsig = " << tnsig << endl;
                        cout << " tntot = " << tntot << endl;
                        cout << " snsig = " << snsig << endl;
                        cout << " sntot = " << sntot << endl;
                        cout << " hitprob_e2  = " << ev.P_nhits_sig << endl;
                        cout << " hitprob_atm = " << ev.P_nhits_bg<< endl;
                        cout << " nhits = " <<  ev.nhits << endl;

                        cout << " event: "<< i << "/" << pevts.size()
                             << "\n" << std::endl;
                        exit(0);
                }

                double lbb; // lbb = lb(nsig=0), ie backgroud only
                if ( GalacPlane ) {
                        if ( ev.isShower == 1 ) {
                                lbb = sntot * ev.P_pos_bg * ev.P_nhits_bg;
                        } else {
                                lbb = tntot * ev.P_pos_bg * ev.P_nhits_bg;
                        }
                }

                if (lik_b) *lik_b -= log ( lbb );
                // if (map_lik_)  map_lik_->Fill(ev.ra, ev.decl, log(ls+lb)-log(lbb));

        } // end of event loop
          // r -= tnsig;
          // r -= snsig;
          // r += *lik_b;

        // r += log(TMath::Poisson(snsig, sexp));
        //if (!map_lik_)  cout << "p_sig = "<<TMath::Poisson(snsig, sexp)<<"    t = "<<tnsig<<"   s = "<<snsig<<"   r = "<<r<<endl;
        if (r!=r) {
                cout << " nan detected before return of lik_sb_with_hits" << endl;
                cout << " snsig = " << snsig << endl;
                cout << " tnsig = " << tnsig << endl;
        }
        if (test) test->Fill(tnsig, r+*lik_b);
        // cout << r+*lik_b <<"   "<<(sntot+tntot)/(snsig+tnsig)<<endl;
        // cout << r+*lik_b <<"   "<< tnsig<<"   "<<snsig<<endl;
        return r;
}

double InterpolateTH2NoRestrain(TH2* _Hist, double _x, double _y){

        if (_x < _Hist->GetXaxis()->GetBinCenter(1)) return 0;
        if (_x > _Hist->GetXaxis()->GetBinCenter( _Hist->GetNbinsX() ) ) return 0;

        if (_y < _Hist->GetYaxis()->GetBinCenter(1)) return 0;
        if (_y > _Hist->GetYaxis()->GetBinCenter( _Hist->GetNbinsY() ) ) return 0;

        return _Hist->Interpolate(_x, _y);
}
