#!/usr/bin/env python

import sys
import os
import random
import array
import datetime
from ROOT import *
from math import *
sys.path.append(os.environ['ROOTSYS'] + '/lib/')

# sys.argv.append( '-b-' )

# Load the pex library. This also pulls in the reco_... libary
ROOT.gSystem.Load("/afs/in2p3.fr/home/t/tgregoir/sps/gal_plane/software/bin/cpex.so")


print '  +--------------------------+'
print '  |    PEX library loaded    |'
print '  +--------------------------+'
rad2deg = 180. / TMath.Pi()
bg_syst = True

start = datetime.datetime.now()


# needed for cross-checks with the ific analysis
def read_from_file(pe, filename):
    i = 0
    r = Pex()
    s = Sev()
    ff = open(filename)
    for l in ff:
        v = l.split()
        if len(v) == 3 and "pseudo_" not in v[0]:
            s.ra = float(v[0])
            s.decl = float(v[1])
            s.nhits = float(v[2])
            s.id = i
            if s.ra == 0 and s.decl == 0:
                continue
            r.sevts.push_back(s)
            i += 1
    return r


# pseudo experiment generation
def genpex(pexdesc):
    """Generate a bunch of pseudo-experiments (Pex's), based on
       the settings described in pexdesc"""

    basename       = pexdesc['basename']  # random name chosen to describe the PEX
    cut            = pexdesc['cut']  # lambda cut applied to get the correct file of ingredients
    sconesize      = pexdesc['sconesize']  # search cone for showers
    tconesize      = pexdesc['tconesize']  # search cone for tracks
    true_src_pos   = pexdesc['true_src_pos'][:]  # true source position
    nsig           = pexdesc['nsig']  # number of signal events
    ntrials        = pexdesc['ntrials']  # number of repetitions
    seed           = pexdesc['seed']  # seed for simulating the directions
    systematics    = pexdesc['syst']  # array of systematics for the analysis, on pointing, positioning and nhits
    full_sky       = pexdesc['full_sky']  # bool to check if it is full sky search
    fixed          = pexdesc['fixed']  # bool to check if it is fixed search
    float_point    = pexdesc['float']  # bool to check if it is float-point
    candidates     = pexdesc['candid']  # if true, check the TS for each candidate source and take the max one
    galactic       = pexdesc['galact']
    comb           = pexdesc['comb']
    galacplane     = pexdesc['galacplane']
    galacplaneData = pexdesc['galacplaneData']
    extended       = pexdesc['extended']
    morph_src      = pexdesc['morph_src']
    morph_src_tr   = pexdesc['morph_src_tr']
    morph_src_sh   = pexdesc['morph_src_sh']
    morph_fit_tr   = pexdesc['morph_fit_tr']
    morph_fit_sh   = pexdesc['morph_fit_sh']

    filemode = pexdesc['filemode']  # standalone | read | write
    iofile = pexdesc['iofile']   # for read or write mode

    tnbg  = pexdesc['tnbg']
    snbg  = pexdesc['snbg']
    model = pexdesc['model']

    # sh3_sig_psf_ang_decl  = pexdesc['sh3_sig_psf_ang_decl']
    sh3_sig_ra_decl_Nhits = pexdesc['sh3_sig_ra_decl_Nhits']
    # sh3_sig_az_zen_Nhits  = pexdesc['sh3_sig_az_zen_Nhits']
    sh2d_nhits_atm = pexdesc['sh2d_nhits_atm']
    sh_nhits_e2 = pexdesc['sh_nhits_e2']
    # sh_nhits_data         = pexdesc['sh_nhits_data']

    # th3_sig_psf_ang_decl  = pexdesc['th3_sig_psf_ang_decl']
    th3_sig_ra_decl_Nhits = pexdesc['th3_sig_ra_decl_Nhits']
    # th3_sig_az_zen_Nhits  = pexdesc['th3_sig_az_zen_Nhits']
    th2d_nhits_atm = pexdesc['th2d_nhits_atm']
    th_nhits_e2 = pexdesc['th_nhits_e2']
    # th_nhits_data         = pexdesc['th_nhits_data']

    mincluster = pexdesc['min_clus']  # minimal size of the cluster.. is currently 3 could also be changed for showers due to the low background

    th3_sig_ra_decl_Nhits .RebinX(2)
    th3_sig_ra_decl_Nhits .RebinY(2)

    sh3_sig_ra_decl_Nhits .RebinX(2)
    sh3_sig_ra_decl_Nhits .RebinY(2)

    # th2d_nhits_atm .RebinY(2)
    # sh2d_nhits_atm .RebinY(2)

    th3_sig_ra_decl_Nhits_rebinned = th3_sig_ra_decl_Nhits
    th3_sig_ra_decl_Nhits_rebinned .RebinX(2)
    th3_sig_ra_decl_Nhits_rebinned .RebinY(2)
    # th3_sig_ra_decl_Nhits_rebinned .RebinZ(2)

    sh3_sig_ra_decl_Nhits_rebinned = sh3_sig_ra_decl_Nhits
    sh3_sig_ra_decl_Nhits_rebinned .RebinX(2)
    sh3_sig_ra_decl_Nhits_rebinned .RebinY(2)
    # sh3_sig_ra_decl_Nhits_rebinned .RebinZ(2) #I do not rebin this becaus it is only 70 bins

    # Print what we are about to do
    print
    print '  +--------------------------------------+'
    print '  | Going to run Pseudo Experiments      |'
    print '  +---+----------------------------------+'
    for k, v in locals().iteritems():
        if 'pexdesc' not in k:
            print "      | - ", k, '=', v
    print '  +--------------------------------------+'

    # load the detector parameterizations specified in pexdesc
    sbg = Bgr(pexdesc['sbgr'])
    sbg3 = Bgr(pexdesc['sbgr3'])
    sbg2 = Bgr(pexdesc['sbgr2'])
    sps = Psf(pexdesc['spsf'])
    tbg = Bgr(pexdesc['tbgr'])
    tbg3 = Bgr(pexdesc['tbgr3'])
    tbg2 = Bgr(pexdesc['tbgr2'])
    tps = Psf(pexdesc['tpsf'])

    sh_acc = D['sh_acceptance']
    th_acc = D['th_acceptance']

    treename = "_".join(["T",
                         # 'd'+str(true_src_pos[0]),
                         # 'a'+str(true_src_pos[1]),
                         ])

    pexdesc['treename'] = treename

    true_src_pos[0] /= rad2deg
    true_src_pos[1] /= rad2deg

    # make the output tree and the Pex object we will write to it

    pe = Pex()
    pe.verbose = 0

    f.cd()
    T = TTree(treename, treename)
    T.Branch("Pex", pe, 32000, 1)

    # if morphologies for extended sources are given, set them now
    if not galacplane:
        pe.set_src_morphology(morph_src)

    pe.set_fit_morphology(morph_fit_tr, morph_fit_sh)  # are morpho_reco_.. from Sum_ing_files_...

    if galacplane:
        pe.set_src_morphology(morph_src_tr, morph_src_sh)
        nside_ = 128 * 2
        decl_arr = array.array('f')
        ra_arr = array.array('f')
        lik_arr = array.array('f')
        delta_ra = 2. * pi / (nside_)

        for i in range(nside_ + 1):
            if i % 2 == 0:
                decl_arr.append(asin(-1 + i * 1. / (nside_ / 2)))
            ra_arr.append(-pi + i * delta_ra)
        if dry_run:
            for i in range(7):
                lik_arr.append(float(i) / 5 - 0.1)
        else:
            for i in range(1201):
                lik_arr.append(float(i) / 1000 - 0.1)

        map_lik = TH3F("map_lik", "Likelihood of each event", len(ra_arr) - 1, ra_arr, len(decl_arr) - 1, decl_arr, len(lik_arr) - 1, lik_arr)
        test = TH1F("test", "test", 100, 0, 10)

        pe.set_map_lik(map_lik, test)

        isFullSky = 1
        if extended:
            isFullSky = 0

        # Calculation of the mean acceptance ratio for signal in all sky
        morphology_src_tr = morph_src_tr
        morphology_src_sh = morph_src_sh

        if not isFullSky:

            nbins_src = morph_src_tr.GetXaxis().GetNbins() * morph_src_tr.GetYaxis().GetNbins()
            for i in range(1, nbins_src + 1):
                if morph_src_tr.GetBinContent(i) < 2e-4:
                    morphology_src_tr.SetBinContent(i, 0)
                    morphology_src_sh.SetBinContent(i, 0)

        ratio = sh_nhits_e2.Integral() / (th_nhits_e2.Integral() + sh_nhits_e2.Integral())
        print "ratio : s_acc / (t_acc + s_acc) =", ratio

    else:
        decl = true_src_pos[0]
        t_acc = th_acc.GetBinContent(th_acc.FindBin(sin(decl)))
        s_acc = sh_acc.GetBinContent(sh_acc.FindBin(sin(decl)))
        ratio = s_acc / (t_acc + s_acc)
        print "decl:", decl, "sin(decl):", sin(decl), "tr acc:", t_acc, "sh acc:", s_acc, "ratio:", ratio

    random2 = TRandom2(pexdesc['seed'])

    print 'running ', ntrials, ' pseudo-experiments '

    for i_trial in range(0, ntrials):
        print ""

        # generating track/shower signal ratio according to acceptance
        tnsig, snsig = 0, 0
        for i_sig in range(nsig):
            if random2.Uniform() > ratio:
                tnsig += 1
            else:
                snsig += 1
        # tnsig = nsig - 7
        # snsig = 7
        if not galacplaneData:
            print "generating N_Signal, N_Tracks, N_Showers: ", nsig, "=", tnsig, "+", snsig, "  for trial:", i_trial

        pe.id = i_trial

        # # systematic on background: mixes the two splines according to random.gauss
        # if bg_syst :
        #     tbg.rsyst = random.gauss( 0, 1 )
        #     sbg.rsyst = random.gauss( 0, 1 )
        #
        # pe.add_background( tbg , th2d_nhits_atm , True, 1.)
        # pe.add_background( sbg , sh2d_nhits_atm , True, 1., 1)

        # pe.comment=""
        # adding the muon signal
        if galacplane and not galacplaneData:
            pe.add_signal_galacplane(tnsig,
                                     th3_sig_ra_decl_Nhits,
                                     0.10,
                                     isFullSky,
                                     0  # isShower
                                     )

            pe.add_signal_galacplane(snsig,
                                     sh3_sig_ra_decl_Nhits,
                                     0.10,
                                     isFullSky,
                                     1  # isShower
                                     )

        # elif galacplaneData:
        #     pe.add_signal_galacplane(tnsig,
        #                              th3_sig_ra_decl_Nhits,
        #                              0.10,
        #                              isFullSky,
        #                              42 # isShower
        #                              )

        elif not galacplaneData:
            print "\nDid I forgot the 'galacplane' option ??\n"
            pe.add_signal_psfhistogram3d(tnsig,
                                         th3_sig_ra_decl_Nhits,
                                         th_acc,
                                         sh_acc,
                                         true_src_pos[0],
                                         true_src_pos[1],
                                         systematics[0],
                                         systematics[1],
                                         systematics[2],
                                         0.10,
                                         1  # isFullSky
                                         )

            pe.add_signal_psfhistogram3d(snsig,
                                         sh3_sig_ra_decl_Nhits,
                                         th_acc,
                                         sh_acc,
                                         true_src_pos[0],
                                         true_src_pos[1],
                                         systematics[0],
                                         systematics[1],
                                         systematics[2],
                                         0.10,
                                         1,  # isFullSky
                                         1  # isShower
                                         )

        pe.comment = ""

        # systematic on background: mixes the two splines according to random.gauss
        if bg_syst:

            if galacplaneData:
                tbg.rsyst = random.gauss(0, 1)
                sbg.rsyst = random.gauss(0, 1)

                tbg3.rsyst = random.gauss(0, 1)
                sbg3.rsyst = random.gauss(0, 1)

                tbg2.rsyst = random.gauss(0, 1)
                sbg2.rsyst = random.gauss(0, 1)

                # tbg.rsyst = tbg3.rsyst = tbg2.rsyst = random.gauss(0, 1)
                # sbg.rsyst = sbg3.rsyst = sbg2.rsyst = random.gauss(0, 1)

            else:

                # tbg.rsyst = random.gauss(0, 1)
                # sbg.rsyst = random.gauss(0, 1)
                #
                # tbg3.rsyst = random.gauss(0, 1)
                # sbg3.rsyst = random.gauss(0, 1)
                #
                # tbg2.rsyst = random.gauss(0, 1)
                # sbg2.rsyst = random.gauss(0, 1)

                tbg.rsyst = tbg3.rsyst = tbg2.rsyst = random.gauss(0, 1)
                sbg.rsyst = sbg3.rsyst = sbg2.rsyst = random.gauss(0, 1)

        if not galacplaneData:
            pe.add_background(tbg, tbg3, th2d_nhits_atm, True, 1.)
            pe.add_background(sbg, sbg3, sh2d_nhits_atm, True, 1., 1)

        verb = False

        # fit within a certain range around a given candidate
        # used for IceCube tracks since they have 1-2 degrees angular error estimate

        if galacplane and not galacplaneData:
            showerid = pe.select_all_events()  # puts all events (or selected one if extended source) in pe

            # map_lik.Reset()
            # fit the nsig and calculate the lik ratio and return it in cluster
            pe.showerclus[showerid].combined_fit_tino(tps, tbg2, th3_sig_ra_decl_Nhits_rebinned, th2d_nhits_atm, th_acc, tnbg,
                                                      sps, sbg2, sh3_sig_ra_decl_Nhits_rebinned, sh2d_nhits_atm, sh_acc, snbg, # XXX
                                                      -999,
                                                      -999,
                                                      # 1 # = fixed point
                                                      0, 0,
                                                      comb,
                                                      model)
            if comb:
                for j in range(31):
                    pe.showerclus[showerid].inj_tnsig .push_back(tnsig)
                    pe.showerclus[showerid].inj_snsig .push_back(snsig)

            # print list(pe.showerclus[showerid].llik_flux)

            # Compute the mean likelihood for each bin of the map
            if i_trial == ntrials - 1:
                map_lik_2D = map_lik.Project3D("yx")
                map_lik_2D.SetAxisRange(1e-3, 0.4, "Z")

                for xx in range(1, map_lik.GetXaxis().GetNbins() + 1):
                    for yy in range(1, map_lik.GetYaxis().GetNbins() + 1):
                        sum_ = 0
                        for ll in range(0, map_lik.GetZaxis().GetNbins() + 1):
                            sum_ += map_lik.GetBinContent(xx, yy, ll) * map_lik.GetZaxis().GetBinCenter(ll)
                            # if (ll == 0 or ll == map_lik.GetZaxis().GetNbins()+1) and map_lik.GetBinContent(xx,yy,ll) != 0: print map_lik.GetBinContent(xx,yy,ll)#sum_, xx,yy,ll, map_lik.GetBinContent(xx,yy,ll),map_lik.GetZaxis().GetBinCenter(ll), map_lik_2D.GetBinContent(xx,yy)
                        if map_lik_2D.GetBinContent(xx, yy) != 0:
                            map_lik_2D.SetBinContent(xx, yy, sum_ / map_lik_2D.GetBinContent(xx, yy))

        elif galacplaneData:
            pe.add_data()

            showerid = pe.select_all_events()  # puts all events (or selected one if extended source) in pe

            # print "  ra ", pe.showerclus[showerid].pevts.back().ra,"  lulu dec ", pe.showerclus[showerid].pevts.back().decl,"  E_reco ",  pe.showerclus[showerid].pevts.back().nhits,"  ",  pe.showerclus[showerid].pevts.back().isShower,"  time ", pe.showerclus[showerid].pevts.back().time
            # fit the nsig and calculate the lik ratio and return it in cluster
            pe.showerclus[showerid].combined_fit_tino(tps, tbg2, th3_sig_ra_decl_Nhits_rebinned, th2d_nhits_atm, th_acc, tnbg,
                                                      sps, sbg2, sh3_sig_ra_decl_Nhits_rebinned, sh2d_nhits_atm, sh_acc, snbg, # XXX
                                                      -999,
                                                      -999,
                                                      # 1 # = fixed point
                                                      0, 0,
                                                      comb,
                                                      model)

        else:
            print "search not set ... exiting"
            sys.exit()

        for clu in pe.clus:
            clu.fit_nsig = clu.fit_tnsig
        for clu in pe.showerclus:
            clu.id = i_trial
            clu.fit_nsig = clu.fit_tnsig + clu.fit_snsig
            clu.fit_TS = clu.fit_nllb - clu.fit_nllsb  # Put the TS value
            if nsig > 45:
                print "fitted total, tracks, shower:", "{0:.2f}".format(clu.fit_tnsig + clu.fit_snsig), "=", "{0:.2f}".format(clu.fit_tnsig), "+", "{0:.2f}".format(clu.fit_snsig), "TS:", clu.fit_TS
            else:
                print "fitted total, tracks, shower:", clu.fit_tnsig + clu.fit_snsig, "=", clu.fit_tnsig, "+", clu.fit_snsig, "TS:", clu.fit_TS
            print "events in cluster:", clu.pevts.size()
            if not galacplane:
                print "fitted position (ra,decl):", clu.fit_ra * 180 / pi, clu.fit_decl * 180 / pi

        if verb:
            pe.clus[0].print_fitinfo()

        pe.clear_sevts()  # test to save disc space

        if not dry_run:  # to save disc space
            map_lik.Reset()
            map_lik_2D.Reset()

        T.Fill()

        # print pe.showerclus[0].pevts[0].ra
        pe.clear()  # <-- important, since we reuse it.

    print 'done'
    f.Write()


# ============================================================================
# ============================================================================
#
#  Main part -- define default job parameters, then read envs settings
#
# ============================================================================
# ============================================================================

D = {}  # dict holding job options

ray     = 2  # sigma of the rayleigh distribution
nsig    = 10
decl    = -70
rias    = 100
ntrials = 3000

D['basename']             = "ALL"
D['cut']                  = "N52_10"
D['sh_cut']               = "ChargeRatio"
D['ting_file']            = "/sps/km3net/users/tgregoir/gal_plane/ingredient_files/ingredients_AaFit_Em2.root"
D['sing_file']            = "/sps/km3net/users/tgregoir/gal_plane/ingredient_files/ingredients_TANTRA_Em2.root"
D['ing_file2']            = "/sps/km3net/users/tgregoir/gal_plane/ingredient_files/Sum_ing_files_TheFinal_25July_map3.root" #Sum_ing_files_TheFinal_map3_4July.root"  # better splines XXX #Sum_ing_files_TheFinal_map3.root"
D['gal_plane_morph_file'] = "/sps/km3net/users/tgregoir/gal_plane/ingredient_files/KRA_morphologies_final.root"  # not really used
D['morph_file']           = "/sps/km3net/users/tgregoir/gal_plane/ingredient_files/morphologies.root"

D['seed']           = 15
D['sconesize']      = 6
D['tconesize']      = 3  # recommended conesize in the point source internal note
D['syst']           = [0.15, 0.0011, 0.0023, 0.1]  # [absolute resolution, pointing theta, pointing phi ]
D['full_sky']       = False
D['fixed']          = False
D['float']          = False
D['candid']         = False
D['galact']         = False
D['comb']           = False
D['extended']       = False
D['galacplane']     = False
D['galacplaneData'] = False
D['extension']      = "0"
D['tnbg']           = 0
D['snbg']           = 0
D['model']          = 3
D['morph_src']      = 0
D['morph_src_tr']   = 0
D['morph_src_sh']   = 0
D['morph_fit_tr']   = 0
D['morph_fit_sh']   = 0
D['filemode']       = 'standalone'
D['iofile']         = '/sps/km3net/users/tgregoir/gal_plane/test.txt'  # '/sps/km3net/users/schulte/aafitv1r0/pex/pex_output/test.txt'
D['min_clus']       = 3  # used in the Point source note for pure track analysis
D['outfile']        = "test_PEx.root"
hits_flag           = 1  # hits flag.. if 1 hits are considered


if D['ing_file2'].find('map') != -1:  # Get model from infile name
    D['model'] = int(D['ing_file2'][D['ing_file2'].find('map') + 3])

# set these environment variables to control what gets done.
if 'DRY' in os.environ.keys():
    dry_run             = bool(os.environ['DRY'])
if 'PEX_TRIALS' in os.environ.keys():
    ntrials             = int(os.environ['PEX_TRIALS'])
if 'PEX_TNBG' in os.environ.keys():
    D['tnbg']           = float(os.environ['PEX_TNBG'])
if 'PEX_SNBG' in os.environ.keys():
    D['snbg']           = float(os.environ['PEX_SNBG'])
if 'PEX_BASE' in os.environ.keys():
    D['basename']       = os.environ['PEX_BASE']
if 'PEX_CUTNAME' in os.environ.keys():
    D['cut']            = os.environ['PEX_CUTNAME']
if 'PEX_SH_CUTNAME' in os.environ.keys():
    D['sh_cut']         = os.environ['PEX_SH_CUTNAME']
if 'PEX_SEED' in os.environ.keys():
    D['seed']           = int(os.environ['PEX_SEED'])
if 'PEX_OUTFILE' in os.environ.keys():
    D['outfile']        = os.environ['PEX_OUTFILE']
if 'PEX_TINGREDIENTS' in os.environ.keys():
    D['ting_file']      = os.environ['PEX_TINGREDIENTS']
if 'PEX_SINGREDIENTS' in os.environ.keys():
    D['sing_file']      = os.environ['PEX_SINGREDIENTS']
if 'PEX_NEVENTS' in os.environ.keys():
    nsig                = int(os.environ['PEX_NEVENTS'])
if 'PEX_DEC' in os.environ.keys():
    decl                = float(os.environ['PEX_DEC'])
if 'PEX_RA' in os.environ.keys():
    rias                = float(os.environ['PEX_RA'])

if 'PEX_FULL' in os.environ.keys():
    D['full_sky']       = True
if 'PEX_FIXED' in os.environ.keys():
    D['fixed']          = True
if 'PEX_FLOAT' in os.environ.keys():
    D['float']          = True
if 'PEX_CANDID' in os.environ.keys():
    D['candid']         = True
if 'PEX_GALACT' in os.environ.keys():
    D['galact']         = True
if 'PEX_COMB' in os.environ.keys():
    D['comb']           = True
if 'PEX_GALACPLANEDATA' in os.environ.keys():
    D['galacplaneData'] = True
    D['galacplane']     = True
if 'PEX_GALACPLANE' in os.environ.keys():
    D['galacplane']     = True
if 'PEX_GALACPLANE' in os.environ.keys() and 'PEX_EXTEND' in os.environ.keys():
    D['extended']       = True

elif 'PEX_EXTEND' in os.environ.keys():
    D['extended']       = True
    D['extension']      = os.environ['PEX_EXTENSION']


if 'PEX_FILEMODE' in os.environ.keys():
    D['filemode']       = os.environ['PEX_FILEMODE']
if 'PEX_IOFILE' in os.environ.keys():
    D['iofile']         = os.environ['PEX_IOFILE']

if 'PEX_TCONE' in os.environ.keys():
    D['tconesize']      = float(os.environ['PEX_TCONE'])
if 'PEX_SCONE' in os.environ.keys():
    D['sconesize']      = float(os.environ['PEX_SCONE'])
if 'PEX_CLUSIZE' in os.environ.keys():
    D['min_clus']       = int(os.environ['PEX_CLUSIZE'])

if 'PEX_SEED' in os.environ.keys():
    D['seed']           = int(os.environ['PEX_SEED'])


if D['iofile']:
    D['iofile']         = open(D['iofile'], 'w' if D['filemode'] == 'write' else 'r')


if 'PEX_SYST' in os.environ.keys():
    D['syst']           = [float(x) for x in os.environ['PEX_SYST'].split()]
    syst                = "SYS"

if 'PEX_FULL_SKY' in os.environ.keys():
    D['full_sky']       = 'PEX_FULL_SKY' in os.environ.keys() and \
        os.environ['PEX_FULL_SKY'].lower() in "yes yup true 1 one affirmative correctemundo"


ROOT.gRandom.SetSeed(int(D['seed']))


# ============================================================
#  define the inputs - get the right ingredients from file
# ============================================================

cut = D['cut']
sh_cut = D['sh_cut']

singr_file = D['sing_file']
print "S-ingr. file = ", singr_file
sfingr_file = TFile(singr_file)

tingr_file = D['ting_file']
print "T-ingr. file = ", tingr_file
tfingr_file = TFile(tingr_file)

ingr_file2 = D['ing_file2']
print "ingr. file2 = ", ingr_file2
fingr_file2 = TFile(ingr_file2)


if not (sfingr_file and tfingr_file and fingr_file2):
    print "Failed to open files with ingredients : ", sfingr_file, tfingr_file
    sys.exit()


# D['spsf'] = "spline:"+singr_file+":psf_spline_CUT".replace('CUT',sh_cut) # This is unused by galacplane analysis
# D['tpsf'] = "spline:"+tingr_file+":psf_spline_CUT".replace('CUT',cut) # This is unused by galacplane analysis

D['spsf'] = "spline:" + ingr_file2 + ":PSF_spline_sh_KRA"  # This is unused by galacplane analysis
D['tpsf'] = "spline:" + ingr_file2 + ":PSF_spline_tr_KRA"  # This is unused by galacplane analysis

# D['tbgr2'] = "spline:"+tingr_file+":bgr_spline_default_CUT:bgr_spline_alternative_CUT".replace('CUT',cut)
# D['sbgr2'] = "spline:"+singr_file+":bgr_spline_default_CUT:bgr_spline_alternative_CUT".replace('CUT',sh_cut)
D['tbgr3'] = "spline:" + ingr_file2 + ":tr_bgr_vs_az_rebinned:tr_bgr_vs_az_rebinned_bis"
# D['sbgr3'] = "spline:" + ingr_file2 + ":sh_bgr_spline_az_flat:sh_bgr_vs_az_rebinned_bis" # using flat azimuth
D['sbgr3'] = "spline:" + ingr_file2 + ":sh_bgr_vs_az_rebinned:sh_bgr_vs_az_rebinned_bis"

D['tbgr2'] = "spline:" + ingr_file2 + ":tr_bgr_vs_sindec_rebinned:tr_bgr_vs_sindec_rebinned_bis"
# D['tbgr2'] = "spline:" + ingr_file2 + ":tr_bgr_sindec_from_flat_az:tr_bgr_vs_sindec_rebinned"
# D['sbgr2'] = "spline:" + ingr_file2 + ":sh_bgr_sindec_from_flat_az:sh_bgr_vs_sindec_rebinned" # using dec from flat azimuth
D['sbgr2'] = "spline:" + ingr_file2 + ":sh_bgr_vs_sindec_rebinned:sh_bgr_vs_sindec_rebinned_bis"

D['tbgr'] = "spline:" + ingr_file2 + ":tr_bgr_vs_coszen_rebinned:tr_bgr_vs_coszen_rebinned_bis"
D['sbgr'] = "spline:" + ingr_file2 + ":sh_bgr_vs_coszen_rebinned:sh_bgr_vs_coszen_rebinned_bis"

# D['tbgr2'] = "spline:"+ingr_file2+":tr_bgr_vs_coszen_rebinned:tr_bgr_vs_coszen_rebinned_bis"
# D['sbgr2'] = "spline:"+ingr_file2+":sh_bgr_vs_coszen_rebinned:sh_bgr_vs_coszen_rebinned_bis"


D['th3_sig_ra_decl_Nhits'] = fingr_file2 .Get("tr_spectrum3d_e_KRA_bis")
# D['th3_sig_az_zen_Nhits']  = fingr_file2 .Get("tr_spectrum3d_e_zen_KRA") # unused
D['th2d_nhits_atm'] = fingr_file2 .Get("tr_spectr2d_e_atmmc_zen_final_opt")
D['th_nhits_e2'] = fingr_file2 .Get("tr_spectrum_e_KRA")
# D['th_nhits_data']         = fingr_file2 .Get("tr_spectrum_e_bgr") # unused
D['th_acceptance'] = fingr_file2 .Get("acc_tr_KRA")  # unused by galplane analysis

D['sh3_sig_ra_decl_Nhits'] = fingr_file2 .Get("sh_spectrum3d_e_KRA_bis")
# D['sh3_sig_az_zen_Nhits']  = fingr_file2 .Get("sh_spectrum3d_e_zen_KRA") # unused
# D['sh2d_nhits_atm']        = fingr_file2 .Get("sh_spectr2d_e_atmmc_zen")
D['sh2d_nhits_atm'] = fingr_file2 .Get("sh_spectr2d_e_atmmc_zen_final_opt")
# D['sh_nhits_atm']          = fingr_file2 .Get("sh_spectr_atmmc")
D['sh_nhits_e2'] = fingr_file2 .Get("sh_spectrum_e_KRA")
# D['sh_nhits_data']         = fingr_file2 .Get("sh_spectrum_e_bgr") # unused
D['sh_acceptance'] = fingr_file2 .Get("acc_sh_KRA")  # unused by galplane analysis

D['morph_fit_tr'] = fingr_file2 .Get("morpho_reco_tr")
D['morph_fit_sh'] = fingr_file2 .Get("morpho_reco_sh")

if D['galacplane']:
    gal_plane_morph_file = TFile(D['gal_plane_morph_file'])
    if not gal_plane_morph_file:
        print "unable to open file with galactic plane morphologie: ", D['gal_plane_morph_file']
        sys.exit()
    D['morph_src_tr'] = gal_plane_morph_file .Get("map3_2D_tracks")
    D['morph_src_sh'] = gal_plane_morph_file .Get("map3_2D_showers")
    if not (D['morph_src_tr'] and D['morph_src_sh']):
        print "coud not find all morph files: ", D['morph_src_tr'], D['morph_src_sh']
        sys.exit()

if D['extended'] and not D['galacplane']:
    if D['extension'] == "0":
        D['morph_src'] = NULL
        D['morph_fit_tr'] = NULL
        D['morph_fit_sh'] = NULL
    else:
        morph_file = TFile(D['morph_file'])
        if not morph_file:
            print "unable to open file with morphologies: ", D['morph_file']
            sys.exit()
        D['morph_src'] = morph_file .Get("gaus_" + D['extension'] + "deg_atGC")
        D['morph_fit_tr'] = morph_file .Get("morph_" + D['extension'] + "deg_tracks")
        D['morph_fit_sh'] = morph_file .Get("morph_" + D['extension'] + "deg_shower")
        if not (D['morph_src'] and D['morph_fit_tr'] and D['morph_fit_sh']):
            print "coud not find morph files: ", D['morph_src'], D['morph_fit_tr'], D['morph_fit_sh']
            sys.exit()


for k, v in D.iteritems():
    try:
        if "morph_src_tr" in k and D['galacplane']:
            print k, v.Integral()
            continue
        if "morph_src_sh" in k and D['galacplane']:
            print k, v.Integral()
            continue
        if "morph_src" in k and D['extended']:
            print k, v.Integral(-pi, pi, -pi / 2., pi / 2.)
            continue
        I = v.Integral()
        print k, I,
        # if "h_nhits" in k :
        #     v.Scale( 1.0 / I )
        #     print " scaling to 1 "
        # else :
        print "."

    except AttributeError:
        pass


# ============================================================
#  construct the output file - if not set already in env.
# ============================================================


if not D['outfile']:

    if D['full_sky']:
        outdir = "/sps/km3net/users/tgregoir/gal_plane/CPEx_output/"

    D['outfile'] = outdir + "test_nhits.root"

f = TFile(D['outfile'], 'RECREATE')


# ============================================================
#  final prep and do some output of the settings
# ============================================================


print "  ================================="
for k, v in D.iteritems():
    print "  || %-12s" % (k), v
# print "  ||    trials : " , trials
print "  ================================="


if decl < -90:
    decl = 180 - decl
if decl > 90:
    decl = -180 + decl
D['true_src_pos'] = [decl, rias]


D['nsig'] = nsig
D['ntrials'] = ntrials

print "\ngenerating", ntrials, "pseudo experiments with", nsig, "signal events"
print ""

genpex(D)  # go, go, go!


f.Write()
f.Close()


end = datetime.datetime.now()
elapsed = end - start

print "elapsed ", elapsed.seconds, " seconds"
