#ifndef AHPEXINCED
#define AHPEXINCED

#include "aux.hh"
#include "detpar.hh"

#include "stringutil.hh"
#include "TF1.h"
#include "TF2.h"
#include "TH3.h"
#include "TH2.h"
#include "TNtuple.h"
#include "TFile.h"

#include "TGraph.h"
#include "TString.h"
#include "TMath.h"
#include "TRandom.h"

#include <algorithm>
#include "Table.hh"

// for fitting
struct Clu;
extern Clu* g_current_cluster;
extern TH1F* h_gen_ang;

extern TF2* gh_src_morphology;
extern TH2* gh_galplane_morphology_tr;
extern TH2* gh_galplane_morphology_sh;
extern TH2* gh_fit_morphology_tracks;
extern TH2* gh_fit_morphology_shower;
extern TH3F* map_lik;
extern TH1F* test;
extern double proba_zone;
extern int GalacPlane;



struct Sev : public TObject // Simple event / Search event
{
    int id;
    int id2;

    double ra;
    double decl;
    TTimeStamp time;

    double theta;
    double phi;

    double lamb; // abuse for saving original decl for bg computation - see rotated_to_zero
    int nlines;
    double nhits;
    int nclus;
    int run_id;


    int src; // 0 = mc_bg, 1 = mc_signal, 10 = data
    int isShower; // 0 = is a Track, 1 = is a shower
    double ls;

    double P_pos_sig; // Probability to have signal at this position
    double P_pos_bg; // Probability to have background at this position
    double P_nhits_sig;
    double P_nhits_bg;
    double sntot;
    double tntot;


    Sev(int id_,
        double decl_,
        double ra_,
        double nhits,
        int source,
        int isshower = 0,
        double ls_ = 0,
        double P_pos_sig_ = -1, // Probability to have signal at this position
        double P_pos_bg_ = -1, // Probability to have background at this position
        double P_nhits_sig_ = -1,
        double P_nhits_bg_ = -1,
        double sntot_ = -1,
        double tntot_ = -1,
        TTimeStamp time_ = TTimeStamp(2007,1,1, 1,1,1 )) : id(id_),
        decl(decl_), ra(ra_),
        nhits(nhits),
        src(source),
        nclus(0),
        lamb(0),
        isShower(isshower),
        ls(ls_),
        P_pos_sig(P_pos_sig_),
        P_pos_bg(P_pos_bg_),
        P_nhits_sig(P_nhits_sig_),
        P_nhits_bg(P_nhits_bg_),
        sntot(sntot_),
        tntot(tntot_),
        time(time_){}


        Sev(){}

        void print (ostream& out = cout ) const
        {
            out << form("det: %5.5f %5.5f   sky: %5.5f,%5.5f   lik: %5.5f   nhits: %5.5f    isShower: %5.5f",theta, phi , decl, ra , lamb , nhits, isShower);
        }

        string __str__() const
        {
            ostringstream s;
            print(s);
            return s.str();
        }

        Vec as_vec() const
        {
            return  Vec(  cos(decl) * cos(ra) , cos(decl) * sin(ra) , sin( decl ) );
        }

        void set( const Vec& v)
        {
            decl = asin( v.z / v.len() );
            ra   = atan2( v.y, v.x );
        }

        void read( Evt& e )
        {
            id     = e.run_id;
            id2    = e.frame_index;
            ra     = e.ra;
            decl   = e.decl;
            theta  = acos(e.trks[3].dir.z);
            phi    = atan2( e.trks[3].dir.y, e.trks[3].dir.x );
            lamb   = e.trks[3].lik;

            if (e.hitinfs.size() > 0)
            {
                nlines = e.hitinfs[0].nstrings;
                nhits  = e.hitinfs[0].nhits;
            }
        }

        ClassDef(Sev,1)
};


inline bool compare_sevptr( Sev* p1, Sev* p2 )
{
    return p1 -> id < p2 -> id;
}

inline bool compare_sev( Sev p1, Sev p2 )
{
    return p1.id < p2.id;
}


inline double dist( double decl1, double ra1, double decl2, double ra2)
{
    //http://mathworld.wolfram.com/GreatCircle.html

    const double dot = cos(decl1)*cos(decl2) * cos(ra1-ra2) + sin (decl1)*sin(decl2);
    return acos(dot);
}

inline double dist( const Sev& s1, double decl, double ra )
{
    return dist( s1.decl, s1.ra, decl,ra );
}

inline double dist( const Sev& s1, const Sev& s2 )
{
    return dist( s1.decl, s1.ra , s2.decl, s2.ra );
}

inline void set_GalacPlane( int isFullSky,int isGalacPlane = 0 ){
        GalacPlane = isGalacPlane;
        proba_zone = 0;
        if (! isFullSky){
            proba_zone = 2e-4;
        }
    }


struct Clu : public TObject // cluster of ptrs to Sev's
{
    int id;

    //vector< Sev* > pevts;  //
    vector< int > ievts;       // since there were some troubles with the pointers to the Sev we switched to integers identifying the events
    vector< Sev > pevts;       // here are the corresponding Sevs
    vector< double > llik;
    vector< double > fluxtr;
    vector< double > fluxsh;
    vector< int > inj_tnsig;
    vector< int > inj_snsig;
    bool own_evts;
    bool sorted;

    Sev seed_ev;             // event that's the seed for the cluster

    string stringrep;

    // the result of a fit
    float  fit_npars;     // one fixed point, 3 = free
    double fit_ra;
    double fit_decl;
    double fit_nsig;
    double fit_tnsig;
    double fit_snsig;     // necessary for combined track and shower pex
    double fit_nllsb;
    double fit_nllb;
    double fit_TS;



    int    fit_ifail;
    double fit_time;

    void print_fitinfo( ostream& out = cout )
    {
        out << "__cluster_fit_info__:" << endl;
        out << " # events     : " << pevts.size() << endl;
        out << " fit pars     : " << fit_npars << endl;
        out << " coords       : " << fit_decl << ", " << fit_ra << endl;
        out << " tnsig        : " << fit_tnsig << endl;
        out << " nllsb / nllb : " << fit_nllsb << " / " << fit_nllb << endl;
        out << " nag ifail    : " << fit_ifail << endl;
        out << " time         : " << fit_time << endl;
    }



    //  a copy of myself used for fitting away from boundaries/poles (no root)
    Clu* fit_clu; //!  <-- do not stream to root, cause root will crash like a little bith


    vector <double> fitdecl; // all the points tried by the minimizer
    vector <double> fitra;


    TGraph* fitgraph(int npoints = -1)  // if npoints > 0, plot the last npoints
    {
        int n=0;
        if (npoints > 0 )
        {
            n = fitdecl.size() - npoints -1;
        }
        else npoints = fitdecl.size();

        if (fitdecl.size() == 0 )
        {
            cout << " fitgraph() called with no fit. returning null." << endl;
            return 0;
        }
        TGraph* r = new TGraph( npoints , &fitra[n], &fitdecl[n] );
        return r;
    }

    Clu()
    {
        sorted = true;
        own_evts = false;
    }

    /*  Clu ( Sev* p )
     *  {
     *    add(p);
     *    sorted = true;
     *    own_evts = false;
}
*/
    Clu ( int i, Sev p)
    {
        seed_ev = p;
        add(i,p);
        sorted = true;
        own_evts = false;
    }

//     ~Clu(){
// //         if (own_evts){
//             cout << " deleting " << pevts.size() << " events owned by cluster " << endl;
//             for (int i=0; i< pevts.size(); i++){
//                 cout << "Deleting precious events" << endl;
//                 delete pevts[i];
//             }
// //         }
//     }


    /*  void add ( Sev* p )
     *  {
     *    sorted = false;
     *    pevts.push_back( p );
}
*/
    void add ( int i, Sev p )
    {
        sorted = false;
        pevts.push_back(p);
        ievts.push_back(i);
    }

    // compute the average direction
    /*  void get_starting_values( double& nsig,
     *			    double& decl_guess,
     *			    double& ra_guess )
     *  {
     *    nsig = pevts.size();
     *    Vec v;
     *    for (int i=0; i< pevts.size() ; i++) // add coords vectorialy
     *      {
     *	Sev& ev = *pevts[i];
     *	v += ev.as_vec();
}
v /= v.len();

Sev dum;
dum.set(v);
decl_guess = dum.decl;
ra_guess   = dum.ra;
}
*/
    // compute the average direction
    void get_starting_values( double& nsig,
                              double& decl_guess,
                              double& ra_guess )
    {
        nsig = pevts.size();
        Vec v;
        for (int i=0; i< pevts.size() ; i++) // add coords vectorialy
        {
            Sev& ev = pevts[i];
            v += ev.as_vec();
        }
        v /= v.len();

        Sev dum;
        dum.set(v);
        decl_guess = dum.decl;
        ra_guess   = dum.ra;
    }

    void get_combined_starting_values(double& tnsig,
                                      double& snsig,
                                      double& decl_guess,
                                      double& ra_guess)
    {
        Vec v;
        tnsig = 0.;
        snsig = 0.;
        for (int i = 0; i<pevts.size(); ++i){
            Sev& ev = pevts[i];
            v += ev.as_vec();
            if (pevts[i].isShower == 0)
                ++tnsig;
            else
                ++snsig;
        }
        v /= v.len();

        Sev dum;
        dum.set(v);
        decl_guess = dum.decl;
        ra_guess   = dum.ra;
    }


    // return a copy with rotated (copied)
    // events which are owned by the copy;

    void rotated_to_zero( double decl, double ra , Clu& r){
        r.pevts.resize( pevts.size() );
        r.ievts.resize( ievts.size() );

        for (int i=0; i< pevts.size() ; i++){
            Sev ev = Sev( pevts[i] );
            // this should be r.pevts[i]. ...
            r.pevts[i].lamb = ev.decl; // save true declination - need it to know the bg-rate while fitting
            Vec v = ev.as_vec();
            v.rotate_z ( - ra );
            v.rotate_y ( decl );
            ev.set(v);

            r.pevts[i] = ev;
            r.ievts[i] = ievts[i];
        }

        r.own_evts = true; // fuck should be true
    }


    void rotated_to_zero_shower( double decl, double ra , Clu& r)
    {
        r.pevts.resize( pevts.size() );
        r.ievts.resize( ievts.size() );

        for (int i=0; i< pevts.size() ; i++)
        {
            Sev& ev = *( new Sev( pevts[i] ));
            pevts[i].lamb = ev.decl; // save true declination - need it to know the bg-rate while fitting
            Vec v = ev.as_vec();
            v.rotate_z ( - ra   );
            v.rotate_y ( decl );
            ev.set(v);

            r.pevts[i] = ev;
            r.ievts[i] = ievts[i];
        }

        r.own_evts = true; // fuck should be true
    }


    void undo_rotate_to_zero( double decl, double ra ) // for testing
    {
        for (int i=0; i< pevts.size() ; i++)
        {
            Sev& ev = pevts[i];
            Vec v = ev.as_vec();
            v.rotate_y ( -decl );
            v.rotate_z ( ra );
            ev.set(v);
        }
    }

    int size() const        { return pevts.size(); };
    Sev seed() const        { return seed_ev; }

    void sort()
    {
        std::sort( pevts.begin(), pevts.end(), compare_sev );
        sorted = true;

        ostringstream ss;
        ss.width(4); ss.fill('0');
        ss << right << pevts.size();
        for (int i=0; i< pevts.size() ; i++)
        {
            ss << "." << pevts[i].id;
        }
        stringrep = ss.str();
    }

    void print(ostream& out = cout)
    {
        out << stringrep;
    }

    void printlots( ostream& out = cout )
    {
        out << endl;
        Table t1("evid", "decl", "ra","nclus","nhits","nlines","theta","phi","lambda","run number");
        for (int i=0; i< pevts.size() ; i++)
        {
            Sev& s= (pevts[i]);
            t1 << s.id << s.decl*180/M_PI << s.ra *180/M_PI << s.nclus << s.nhits << s.nlines << s.theta*180/M_PI << s.phi*180/M_PI << s.lamb << s.run_id;
        }
        t1.print_ascii( out );

        Table t2("x");
        for (int i=0; i< pevts.size() ; i++)
        {
            t2.add_column( form("%d", pevts[i].id) );
        }
        for (int i=0; i< pevts.size() ; i++)
        {
            t2 << pevts[i].id;
            for (int j=0; j< pevts.size() ; j++)
            {
                if (i==j) t2 << "-" ;
                else
                    t2 << dist( pevts[i], pevts[j] ) * 180/M_PI;
            }
        }

        t2.print_ascii( out );
    }


    TGraph* graph( bool use_degrees = false,  // shamefull duplication of Pex::graph
                   bool do_aitoff = true,
                   int how_many_clusters =0 )
    {
        vector<double> x;
        vector<double> y;

        for (int i=0; i< pevts.size() ; i++)
        {
            Sev& s = (pevts[i]);
            if (s.nclus < how_many_clusters ) continue;

            double xx,yy;
            if (do_aitoff)
            {
                aitoff( s.ra, s.decl, xx, yy, use_degrees);
            }
            else
            {
                xx = s.ra; yy = s.decl;
            }
            x.push_back( xx);
            y.push_back( yy);
        }

        TGraph* g = new TGraph( x.size(), &(x[0]) , &(y[0]) );
        g->SetMarkerStyle(25);
        g->SetMarkerColor(2 + how_many_clusters );
        g->SetTitle( Form ("cluster %d",id ) );
        g->GetXaxis()->SetTitle("ra");
        g->GetYaxis()->SetTitle("#delta");
        return g;
    }

    double lik_sb_with_hits (double nsig,
                             double decl,
                             double ra,
                             Psf& psf, Bgr& bgr,
                             TH1* hist_E2,
                             TH1* hist_atm,
                             double* lik_b = 0);

    double combined_lik_sb_with_hits (double tnsig,
                                      double snsig,
                                      double decl,
                                      double ra,
                                      Psf& psf, Bgr& bgr,
                                      Psf& psf_shower, Bgr& bgr_shower,
                                      TH1* hist_E2,
                                      TH1* hist_atm,
                                      TH1* hist_E2_shower,
                                      TH1* hist_atm_shower,
                                      double* lik_b = 0);/*,
                                      double* lik_track_s = 0,      // intoduced for cross-checks .. only signal likelihood for the track events
                                      double* lik_shower_s = 0,     // only signal likelihood for the shower events
                                      double* lik_track_b = 0,      // only bkg likelihood for the track events
                                      double* lik_shower_b = 0);    // only bkg likelihood for the shower events
                                      */
    //
    // double combined_lik_sb_with_hits_tino (double tnsig,
    //                                    double snsig,
    //                                    double decl,
    //                                    double ra,
    //                                    Psf& psf,         Bgr& bgr,
    //                                    Psf& psf_shower,  Bgr& bgr_shower,
    //                                    TH3* psf_3d_tr,
    //                                    TH1* hist_atm,
    //                                    TH3* psf_3d_sh,
    //                                    TH1* hist_atm_shower,
    //                                    double* lik_b = 0,
    //                                    TH3F* map_lik = 0);
    //

    double combined_lik_sb_with_hits_tino (double tnsig,
                                       double snsig,
                                       double decl,
                                       double ra,
                                       Psf& psf,         Bgr& bgr,
                                       Psf& psf_shower,  Bgr& bgr_shower,
                                       TH3* psf_3d_tr,
                                       TH2* hist2d_atm,
                                       TH3* psf_3d_sh,
                                       TH2* hist2d_atm_shower,
                                       double* lik_b,
                                       TH3F* map_lik = 0,
                                       bool comb = false,
                                       TH1F* test = 0);


//     double lik_sb_morphology( double nsig,
//                             Bgr& bgr,
//                             TH2* morphology,
//                             double* lik_b /* =0 */ );



    //   TH2F* hist_lik_sb (double nsig, Psf& psf, Bgr& bgr)
    //   {

    //     double m = 0.1;
    //     TH2F* r = new TH2F("hlik","hlik",
    // 		       100, -m , m ,
    // 		       100, -m , m );

    //     for (int x = 1 ; x < r->GetNbinsX(); x++)
    //       for (int y = 1 ; y < r->GetNbinsY(); y++)
    // 	{
    // 	  int b = r->GetBin(x,y);
    // 	  double xx = r->GetXaxis()->GetBinCenter( x );
    // 	  double yy = r->GetXaxis()->GetBinCenter( y );
    // 	  double c = lik_sb( nsig, yy, xx, psf, bgr );
    // 	  r->SetBinContent( b, c );
    // 	}
    //     return r;
    //   }


//     void fit( Psf& psf, Bgr& bgr,
//             int verbose = 0,
//             double fixed_decl = -999,
//             double fixed_ra   = -999);



    void fit_with_nhits( Psf& psf, Bgr& bgr,
                         TH1* hist_E2,
                         TH1* hist_atm,
                         int verbose = 0,
                         double fixed_decl = -999,
                         double fixed_ra   = -999 );

//     void combined_fit_with_nhits( Psf& psf_track, Bgr& bgr_track,
//                                   TH1* hist_E2_track,
//                                   TH1* hist_atm_track,
//                                   Psf& psf_shower, Bgr& bgr_shower,
//                                   TH1* hist_E2_shower,
//                                   TH1* hist_atm_shower,
//                                   int verbose = 0,
//                                   double fixed_decl = -999,
//                                   double fixed_ra   = -999 );

    void combined_fit_tino(  Psf& psf_track,  Bgr& bgr_track,  TH3* psf_3d_tr, TH2* histo2d_atm_track,  TH1* hist_acc_tracks, double tnbg_,
                             Psf& psf_shower, Bgr& bgr_shower, TH3* psf_3d_sh, TH2* histo2d_atm_shower, TH1* hist_acc_shower, double snbg_,
                             double start_decl = -999,
                             double start_ra   = -999,
                             double sigma_decl = 0.3,
                             double sigma_ra   = 0.3,
                             bool comb = false,
                             int model = 3
//                              int    fitMode    = 0 // 0: full sky, 1: fixed-point, 2: float-point
                          );


//     void combined_fit_tino(  Psf& psf_track,  Bgr& bgr_track,  TH3* psf_3d_tr, TSpline3* histo_atm_track,  TH1* hist_acc_tracks,
//                              Psf& psf_shower, Bgr& bgr_shower, TH3* psf_3d_sh, TSpline3* histo_atm_shower, TH1* hist_acc_shower,
//                              double start_decl = -999,
//                              double start_ra   = -999,
//                              double sigma_decl = 0.3,
//                              double sigma_ra   = 0.3
// //                              int    fitMode    = 0 // 0: full sky, 1: fixed-point, 2: float-point
//                           );


//     void fit_morphology( Bgr& bgr,
//                          TH2* morphology );




    ClassDef(Clu,1)
};

// comparison function for sorting... return the cluster with the largest lik. ratio.
inline bool greater_likrat( const Clu& a, const Clu& b )
{
    const double lr_a = -(a.fit_nllsb - a.fit_nllb);
    const double lr_b = -(b.fit_nllsb - b.fit_nllb);
    return lr_a > lr_b;
}

inline void sort_likrat( vector<Clu>& v )
{
    std::sort( v.begin(), v.end(), greater_likrat );
}


struct Source : public TObject
{
    double decl;
    double ra;
    double mu;   // the mean
    double nsig; // the actual thrown number of events
    double isTrack;
    double isShower;


    Source() : decl(0), ra(0), mu(0), nsig(0), isTrack(0), isShower(0) {}

    ClassDef(Source,1)
};


struct Pex : public TObject // Pseudo-experiment
{
    int verbose;
    int id;
    TString comment;
    vector< Sev > sevts;
    vector< Sev > showerevts;
    vector< Clu > clus;
    vector< Clu > showerclus;

    TH2F* h_morphology; // depricated

    static void set_map_lik( TH3F* _h){
        map_lik = _h;
    }
    static void set_map_lik( TH3F* _h, TH1F* _test){
        map_lik = _h;
        test = _test;
    }
    static void set_src_morphology( TF2* _h){
        gh_src_morphology = _h;
    }
    static void set_src_morphology( TH2* _h_tr, TH2* _h_sh){
        gh_galplane_morphology_tr = _h_tr;
        gh_galplane_morphology_sh = _h_sh;

    }
    static void set_fit_morphology( TH2* h_tr, TH2* h_sh){
        gh_fit_morphology_tracks = h_tr;
        gh_fit_morphology_shower = h_sh;
    }

    static TH1& get_gen_ang_hist() { return *h_gen_ang; }

    Source mc_source;

    Pex(): id(0), verbose(1), h_morphology(0) {}

    Pex(const Clu& c) : verbose(1),  h_morphology(0)
    {
        for (int i=0; i< c.pevts.size(); i++) sevts.push_back ( (c.pevts[i]) );
    }

    void clear_sevts()
    {
        for (int i=0; i< clus.size() ; i++)
        {
            //	cout << "Deleting precious sevts" << endl;
            clus[i].pevts.clear();
        }
        for (int i = 0; i<showerclus.size() ; i++){
            showerclus[i].pevts.clear();
        }
        sevts.clear();
        showerevts.clear();
    }


    void clear()
    {
        mc_source = Source();
        sevts.clear();
        showerevts.clear();
        clus.clear();
        showerclus.clear();
    }

    void add_data();

    void add_background( Bgr& bgr ,
                         Bgr& bgr_az ,
                         TH2* nhits_atm_histogram2d,
                         bool fluct_ntot = false,
                         double scale_factor = 1,
                         int isShower_ = 0); // throw an experiment


//     void add_background_shower( Bgr& bgr ,
//                                 TH1* nhits_atm_histogram,
//                                 bool fluct_ntot = false,
//                                 double scale_factor = 1,
//                                 int isShower_ = 1); // throw an experiment


//     void add_background_noene( Bgr& bgr ,
//                                bool fluct_ntot = false,
//                                double scale_factor = 1 ); // throw an experiment

    void add_signal_simple( int n,  // simplified version for mc studies
                            Psf* psf,
                            double decl,  // in radians
                            double ra,
                            TH2* morphology = 0 );


    void add_signal_generic_2d( int n,
                                Psf* psf,
                                TH2* psf_vs_nhits_histo,
                                double decl,
                                double ra,
                                double sigma_resolution,
                                double sigma_pointing_theta,
                                double sigma_pointing_phi,
                                double sigma_relative_nhits,
                                int isShower);




    void add_signal_psfhistogram_2d( int n,
                                     TH2& psf_vs_nhits,
                                     double decl,
                                     double ra,
                                     double sigma_relative_resolution,
                                     double sigma_radians_pointing_theta,
                                     double sigma_radians_pointing_phi,
                                     double sigma_relative_nhits = 0,
                                     int isShower = 0)
    {
        add_signal_generic_2d( n , 0, &psf_vs_nhits,
                               decl, ra,
                               sigma_relative_resolution,
                               sigma_radians_pointing_theta,
                               sigma_radians_pointing_phi,
                               sigma_relative_nhits,
                               isShower);
    }




    /*! the histogram should be a 2d histogram with x-axis = sin(decl)
     *     from -1 to +sin(42), and y-axis log10(angular_error/deg). */

    void add_signal_psfhistogram2d_noene( int n,
                                          TH2& h2_psf_vs_sindecl,
                                          double decl, // in radians
                                          double ra ,
                                          double sigma_relative_resolution,
                                          double sigma_radians_pointing_theta,
                                          double sigma_radians_pointing_phi,
                                          int nbins_in_slice = 5);


    // void add_signal_psfhistogram3d( int n,
    //                                 TH3& psf_3d_histogram,
    //                                 TH1* hist_acc_tracks,
    //                                 TH1* hist_acc_showers,
    //                                 double decl, // in radians
    //                                 double ra ,
    //                                 double sigma_relative_resolution,
    //                                 double sigma_radians_pointing_theta,
    //                                 double sigma_radians_pointing_phi,
    //                                 double sigma_relative_nhits =0,
    //                                 int isFullSky = 1,
    //                                 int isShower = 0,
    //                                 int nbins_in_slice = 5);

    void add_signal_psfhistogram3d( int n,
                                    TH3& psf_3d_histogram,
                                    double decl, // in radians
                                    double ra ,
                                    double sigma_relative_resolution,
                                    double sigma_radians_pointing_theta,
                                    double sigma_radians_pointing_phi,
                                    double sigma_relative_nhits =0,
                                    int isShower = 0,
                                    int nbins_in_slice = 5);


    void add_signal_galacplane( int n,
                                    TH3& psf_3d_histogram,
                                    double sigma_relative_nhits = 0,
                                    int isFullSky = 1,
                                    int isShower = 0,
                                    int nbins_in_slice = 6);

    // void add_signal_galacplane_tr( int n,
    //                                 TH3& Etrue_ra_dec_param,
    //                                 TH2& Etrue_Nhits,
    //                                 double sigma_relative_nhits = 0,
    //                                 int isFullSky = 1,
    //                                 int isShower = 0,
    //                                 int nbins_in_slice = 5);


    void add( Sev& s)
    {
        sevts.push_back( s );
    }

    void print(ostream& out = cout ) const
    {
        out << "Pex " << id << " " << sevts.size() << "  " << comment << endl;
        for (int i=0; i< sevts.size() ; i++)
        {
            out << form("%04d",i) << " / " << sevts.size() << "   " ;
            sevts[i].print( out );
            out << endl;
        }
    }

    string __str__() const
    {
        ostringstream s;
        print(s);
        return s.str();
    }

    string write_to_string() const
    {
        ostringstream s;
        s << "pseudo_experiment " << id << " " << sevts.size() << endl;
        foreach( e, sevts )
        {
            s <<  e.ra << " " << e.decl << " " << int(e.nhits) << endl;
        }
        return s.str();
    }

    bool read_from_string(const string& ss)
    {
        istringstream s(ss);
        string w="";
        while  (w!="pseudo_experiment")
        {
            s >> w;
            if (!s) return false;
        }
        int nevts;
        s >> id >> nevts;
        sevts.clear();
        sevts.resize(nevts);
        for( int i=0 ; i< nevts; i++)
        {
            s >> sevts[i].ra >> sevts[i].decl >> sevts[i].nhits;
        }

        if (!s) return false;
        else return true;
    }



    TGraph* graph( bool use_degrees = false,
                   bool do_aitoff = true,
                   int how_many_clusters =0 )
    {
        vector<double> x;
        vector<double> y;

        for (int i=0; i< sevts.size() ; i++)
        {
            Sev& s = sevts[i];
            if (s.nclus < how_many_clusters ) continue;

            double xx,yy;
            if (do_aitoff)
            {
                aitoff( s.ra, s.decl, xx, yy, use_degrees);
            }
            else
            {
                xx = s.ra;
                yy = s.decl;
            }
            x.push_back( xx);
            y.push_back( yy);
        }

        TGraph* g = new TGraph( x.size(), &(x[0]) , &(y[0]) );
        g->SetMarkerStyle(7);
        g->SetMarkerColor(4 + how_many_clusters );
        g->SetTitle( Form ("aitoff experminent %d %s",id, comment.Data() ) );
        g->GetXaxis()->SetTitle("ra");
        g->GetYaxis()->SetTitle("#delta");
        return g;
    }


    int select_events_in_box( double ra_min, double ra_max,
                              double de_min, double de_max )
    {
        clus.push_back( Clu() );
        Clu& r = *(clus.rbegin());

        for ( int j = 0; j < sevts.size() ; j ++ )
        {
            Sev& t = sevts[j];

            if ( t.ra < ra_min || t.ra > ra_max ) continue;
            if ( t.decl < de_min || t.decl > de_max ) continue;
            r.add(j, t );
        }
        return r.id;
    }

    int select_events_in_box( TH2* h )
    {
        select_events_in_box( h -> GetXaxis() -> GetXmin (),
                              h -> GetXaxis() -> GetXmax (),
                              h -> GetYaxis() -> GetXmin (),
                              h -> GetYaxis() -> GetXmax () );
    }


    int select_events_around( double decl, double ra, double conesize)
    {
        conesize /= (180/M_PI);

        clus.push_back( Clu() );
        Clu& r = *(clus.rbegin());
        r.id = clus.size()-1;

        for ( int j = 0; j < sevts.size() ; j ++ )
        {
            Sev& t = sevts[j];
            if ( fabs ( decl - t.decl ) > conesize ) continue; // for speed
            double d = dist( t, decl, ra );
            if (d < conesize )
            {
                r.add( j, t );
            }
        }
        return r.id;
    }

    int select_all_events_around( double decl, double ra, double conesize, double sconesize)
    {
        conesize /= (180/M_PI);
        sconesize /= (180/M_PI);

        showerclus.push_back( Clu() );

        Clu& r = *(showerclus.rbegin());
        r.id = showerclus.size()-1;

        for ( int j = 0; j < sevts.size() ; j ++ )
        {
            Sev& t = sevts[j];
            if ( fabs ( decl - t.decl ) > conesize ) continue; // for speed
            double d = dist( t, decl, ra );
            if (d < conesize )
            {
                r.add( j, t );
            }

        }

        for ( int j = 0; j < showerevts.size() ; j ++ )
        {
            Sev& t = showerevts[j];
            if ( fabs ( decl - t.decl ) > conesize ) continue; // for speed
            double d = dist( t, decl, ra );
            if (d < conesize )
            {
                r.add( j, t );
            }

        }

        return r.id;
    }

    int select_all_events()
    {

        showerclus.push_back( Clu() );

        Clu& r = *(showerclus.rbegin());
        r.id = showerclus.size()-1;

        for ( int j = 0; j < sevts.size() ; j ++ )
        {
            Sev& t = sevts[j];

            double prob_morph = gh_galplane_morphology_tr->GetBinContent(gh_galplane_morphology_tr->FindBin(t.ra, t.decl));
            if (prob_morph >= proba_zone){
                r.add( j, t );
                // cout << "  ra " << t.ra<< "  dec "<< t.decl <<"  time "<< t.time << "  nhits "<< t.nhits<< endl;
            }
        }

        for ( int j = 0; j < showerevts.size() ; j ++ )
        {
            Sev& t = showerevts[j];
            double prob_morph = gh_galplane_morphology_tr->GetBinContent(gh_galplane_morphology_tr->FindBin(t.ra, t.decl));
            if (prob_morph >= proba_zone){
                r.add( j, t );
                // cout << "  ra " << t.ra<< "  dec "<< t.decl <<"  time "<< t.time << "  nhits "<< t.nhits<< endl;
            }
        }
        // cout<<"  ra "<<  r.pevts.back().ra<<"  lulu dec "<< r.pevts.back().decl<<"  E_reco "<<  r.pevts.back().nhits<<"  "<<  r.pevts.back().isShower<<"  time "<< r.pevts.back().time << endl;

        return r.id;
    }


    // int add_data()
    // {
    //
    //     showerclus.push_back( Clu() );
    //
    //     Clu& r = *(showerclus.rbegin());
    //     r.id = showerclus.size()-1;
    //
    //     TFile* data_file = TFile::Open("/sps/km3net/users/tgregoir/gal_plane/ingredient_files/data_ntuple.root");
    //     TNtuple *data_ntuple = (TNtuple*)data_file->Get("data_ntuple");
    //
    //     float nhits;
    //     float ra;
    //     float dec;
    //     float isShower;
    //     // TTimeStamp time_;
    //
    //     data_ntuple->SetBranchAddress("nhits",&nhits);
    //     data_ntuple->SetBranchAddress("ra",&ra);
    //     data_ntuple->SetBranchAddress("dec",&dec);
    //     data_ntuple->SetBranchAddress("isShower",&isShower);
    //     // data_ntuple->SetBranchAddress("time_",&time_);
    //
    //     for ( int j = 0; j < data_ntuple->GetEntries() ; j ++ )
    //     {
    //         data_ntuple->GetEntry(j);
    //         // cout<<j<<"  "<< dec<<"  "<<  ra<<"  "<<  nhits<<"  "<<  isShower<< endl;
    //
    //         Sev t( j, dec, ra, nhits, 1, (int) isShower);
    //
    //         if ( isShower_ == 0){
    //             sevts.push_back( t );
    //         } else {
    //             showerevts.push_back(t);
    //         }
    //
    //     // r.id = j-1;
    //     }
    //     cout<< "nevent == "<<r.id << "??"<<endl;
    //
    //     data_file->Close();
    //     return r.id;
    // }


//     void fit_clusters( Psf& psf, Bgr& bgr,
//                        int verbose = 0,
//                        double fixed_decl = -999,  // this won't be sensible if you have >1
//                        double fixed_ra   = -999)
//     {
//         for (int i=0; i< clus.size(); i++)
//         {
//             Clu& c = clus[i];
//             c.fit( psf, bgr, verbose, fixed_decl, fixed_ra );
//         }
//     }

    void fit_clusters_nhits( Psf& psf, Bgr& bgr,
                             TH1* histo_E2,
                             TH1* histo_atm,
                             int verbose = 0,
                             double fixed_decl = -999,  // this won't be sensible if you have >1
                             double fixed_ra   = -999)

    {
        for (int i=0; i< clus.size(); i++)
        {
            Clu& c = clus[i];
            c.fit_with_nhits( psf, bgr, histo_E2, histo_atm ,verbose, fixed_decl, fixed_ra );
        }
    }


//     void combined_fit_clusters_nhits( Psf& psf_track, Bgr& bgr_track,
//                                       TH1* histo_E2_track,
//                                       TH1* histo_atm_track,
//                                       Psf& psf_shower, Bgr& bgr_shower,
//                                       TH1* histo_E2_shower,
//                                       TH1* histo_atm_shower,
//                                       int verbose = 0,
//                                       double fixed_decl = -999,  // this won't be sensible if you have >1
//                                       double fixed_ra   = -999)
//
//     {
//
//         for (int i=0; i< showerclus.size(); i++)
//         {
//             Clu& c = showerclus[i];
//             c.combined_fit_with_nhits( psf_track, bgr_track, histo_E2_track, histo_atm_track, psf_shower, bgr_shower, histo_E2_shower, histo_atm_shower, verbose, fixed_decl, fixed_ra );
//         }
//
//     }





    void sort_clusters_by_likrat()
    {
        std::sort( clus.begin(), clus.end(), greater_likrat);
        std::sort( showerclus.begin(), showerclus.end(), greater_likrat);
    }



    int find_clusters( double conesize , int min_cluster_size);

    // add_showers add shower events to already found clusters based on muon tracks
    int add_showers(double conesize , int min_cluster_size);

    // find clusters consisting of tracks AND showers
    int find_clusters_combined               ( double tr_conesize, double sh_conesize , int min_cluster_size, double sh_weight = 3);
    int find_clusters_combined_galacticCentre( double tr_conesize, double sh_conesize , int min_cluster_size, double sh_weight = 3);

    ClassDef( Pex, 2)
};



struct Ana : public TObject
{
    string desc;
    double labmda_cut;
    string psf_name;
    string bgr_name;

    ClassDef( Ana, 1)
};

void set_ignore_hit_info( bool v );
bool get_ignore_hit_info( );

void set_fixed_signal_ratio( double v);
double get_fixed_signal_ratio();



double InterpolateTH2NoRestrain(TH2* _Hist, double _x, double _y);

#endif
