The procedure to launch the reconstruction is the following:

```bash
cd ~/sps/rbr_tau/launcher/reco
source csh
./env-shell.sh
./submitter_reco_tau.sh
```

this will launch 10 jobs

Then check for errors 
```bash
cd ~/sps/rbr_tau/launcher/reco/mcProcessingLogs/tau
ll */MCP*.o* | grep "<Jul 18>" | awk 'system("cat " $9)' | grep -i "Error:"
ll */MCP*.o* | grep "<Jul 18>" | awk 'system("cat " $9)' | grep -i "Error:" | wc -l
ll */MCP*.o* | grep "<Jul 18>" | awk 'system("cat " $9" | grep Error | wc -l")' | grep 2
```
and also
```bash
cd ~/sps/rbr_tau/out/reco/mcTempOutputFolder/tau/
llg "<Jul 18>" | awk 'system("ls -lthr " $9 "/*")' | grep "root"
llg "<Jul 18>" | awk 'system("ls -lthr " $9 "/*")' | grep "total " | grep -v "total 0" | wc -l
```
to get the errors of the day

Check disk space also with `fs lq ${HOME}`
