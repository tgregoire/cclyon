#!/bin/zsh
# utility script for Sirene Monte Carlo.
version=1.0
script=${0##*/}

# ------------------------------------------------------------------------------------------
#
#                            Example script to run JSirene.
#
# ------------------------------------------------------------------------------------------


if [ -z $JPP_DIR ]; then
    echo "Variable JPP_DIR undefined."
    exit
fi


source $JPP_DIR/setenv.sh $JPP_DIR
source $JPP_DIR/zshlib.sh

set_variable  DEBUG          1
set_variable  WORKDIR        $JPP_DIR/workdir/
set_variable  INPUT_FILE     $JPP_DATA/genhen.km3net_wpd_V2_0.evt
set_variable  DETECTOR       $WORKDIR/antares_reference.detx

let RUN=1

while (( $RUN < 100 )); do

    set_variable  OUTPUT_FILE    $WORKDIR/sirene_${RUN}.root

    JSirene \
	-F $WORKDIR/I%p.dat  \
	-a $DETECTOR         \
	-f $INPUT_FILE       \
	-o $OUTPUT_FILE      \
	-T 0.015             \
	-d $DEBUG

    let RUN=$RUN+1
done
