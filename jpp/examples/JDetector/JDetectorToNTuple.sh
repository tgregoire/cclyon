#!/bin/zsh
version=1.0
script=${0##*/}

# ------------------------------------------------------------------------------------------
#
#                         Utility script to test JDetectorToNTuple.
#
# ------------------------------------------------------------------------------------------


if [ -z $JPP_DIR ]; then
    echo "Variable JPP_DIR undefined."
    exit
fi


source $JPP_DIR/setenv.sh $JPP_DIR
source $JPP_DIR/zshlib.sh


set_variable     DEBUG           2
set_variable     WORKDIR         ./
set_variable     DETECTOR        $JPP_DATA/km3net_reference.detx


if ( do_usage $* ); then
    usage "$script [detector file]"
fi

case $# in
    1) DETECTOR=$1;;
esac


set_variable     OUTPUT_FILE     $WORKDIR/ntuple.dat


if (( 1 )); then

    $JPP_DIR/examples/JDetector/JDetectorToNTuple \
	-a $DETECTOR     \
	-o $OUTPUT_FILE  \
	-d $DEBUG        \
	--!

fi

if (( 1 )); then
    
    set_variable     INPUT_FILE      $OUTPUT_FILE
    set_variable     OUTPUT_FILE     $WORKDIR/ntuple.detx

    $JPP_DIR/examples/JDetector/JNTupleToDetector \
	-a $DETECTOR     \
	-f $INPUT_FILE   \
	-o $OUTPUT_FILE  \
	-d $DEBUG        \
	--!

    JCompareDetector \
	-a $DETECTOR     \
	-b $OUTPUT_FILE   

fi

