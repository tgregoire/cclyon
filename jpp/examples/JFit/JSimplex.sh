#!/bin/zsh

source $JPP_DIR/setenv.sh $JPP_DIR
source $JPP_DIR/zshlib.sh


set_variable  DEBUG              1
set_variable  WORKDIR            $JPP_DIR/workdir/
set_variable  DETECTOR           $WORKDIR/antares_reference.detx
set_variable  GRID_DEG           5
set_variable  TMAX_NS            18
set_variable  ROAD_M             100
set_variable  SIGMA_NS           5
set_variable  OUTLIERS           3
set_variable  NUMBER_OF_PREFITS  96
set_variable  SIGMA_NS           3.0
set_variable  TMAX_NS            15


JSimplex \
    -a $DETECTOR             \
    -f $WORKDIR/prefit.root  \
    -o $WORKDIR/simplex.root \
    -N ${NUMBER_OF_PREFITS}  \
    -S ${SIGMA_NS}           \
    -T ${TMAX_NS}            \
    -U                       \
    -d ${DEBUG} --!
