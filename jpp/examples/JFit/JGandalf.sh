#!/bin/zsh

source $JPP_DIR/setenv.sh $JPP_DIR
source $JPP_DIR/zshlib.sh


set_variable  DEBUG              1
set_variable  WORKDIR            $JPP_DIR/workdir/
set_variable  DETECTOR           $WORKDIR/antares_reference.detx
set_variable  NUMBER_OF_PREFITS  96
set_variable  TTS_NS             2.0
set_variable  E_GEV              1e3
set_variable  R_HZ               50e3
set_variable  ROADWIDTH_M        100


JGandalf \
    -a $DETECTOR                 \
    -f $WORKDIR/simplex.root     \
    -P $WORKDIR/J%p.dat          \
    -o $WORKDIR/gandalf.root     \
    -N ${NUMBER_OF_PREFITS}      \
    -T ${TTS_NS}                 \
    -E ${E_GEV}                  \
    -B ${R_HZ}                   \
    -R ${ROADWIDTH_M}            \
    -d ${DEBUG} --!
