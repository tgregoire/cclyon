#!/bin/zsh
version=1.0
script=${0##*/}

# ------------------------------------------------------------------------------------------
#
#                         Utility script to test JEnergy.
#
# ------------------------------------------------------------------------------------------


if [ -z $JPP_DIR ]; then
    echo "Variable JPP_DIR undefined."
    exit
fi


source $JPP_DIR/setenv.sh $JPP_DIR
source $JPP_DIR/zshlib.sh

set_variable  DEBUG          1
set_variable  WORKDIR        $JPP_DIR/workdir/
set_variable  INPUT_FILE     $WORKDIR/gandalf.root
set_variable  OUTPUT_FILE    $WORKDIR/energy.root
set_variable  DETECTOR       $WORKDIR/antares_reference.detx
set_variable  JPP            $JPP_DIR/examples/JFit/
set_variable  ENERGY_CORRECTION   energy_correction.root

if ( do_usage $* ); then
    usage "$script [detector file [input file [output file]]]"
fi

case $# in
    3) OUTPUT_FILE=$3;&
    2) INPUT_FILE=$2;&
    1) DETECTOR=$1;;
esac

if (( 1 )) ; then

    print_variable     DETECTOR INPUT_FILE OUTPUT_FILE
    check_input_file   $DETECTOR $INPUT_FILE
    check_output_file  $OUTPUT_FILE

    set_variable       NUMBER_OF_PREFITS  1

    typeset -A WEIGHT

    WEIGHT[prefit]=""
    WEIGHT[simplex]=""
    WEIGHT[gandalf]=""

    OPTION=%

    if [[ $OPTION == % ]]; then
        for KEY in "${(k)WEIGHT[@]}"; do
            if [[ $INPUT_FILE == *${KEY}* ]]; then
                OPTION=$KEY
            fi
        done
    fi

    echo "Weight: $WEIGHT[$OPTION]."

    timer_start

    JEnergy \
        -a $DETECTOR                 \
	-f $INPUT_FILE               \
	-P $JPP_DATA/J%p.dat         \
	-E $ENERGY_CORRECTION        \
	-o $OUTPUT_FILE              \
	-N ${NUMBER_OF_PREFITS}      \
        $WEIGHT[$OPTION]             \
	-d $DEBUG --!

    timer_stop
    timer_print

fi
