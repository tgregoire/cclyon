#!/bin/zsh
version=1.0
script=${0##*/}

# ------------------------------------------------------------------------------------------
#
#                         Utility script to plot fit results.
#
# ------------------------------------------------------------------------------------------


if [ -z $JPP_DIR ]; then
    echo "Variable JPP_DIR undefined."
    exit
fi


source $JPP_DIR/setenv.sh $JPP_DIR
source $JPP_DIR/zshlib.sh

set_variable  DEBUG          2
set_variable  WORKDIR        $JPP_DIR/workdir/
set_variable  JPP            $JPP_DIR/examples/JFit/
set_variable  OPTION         N


if ( do_usage $* ); then
    usage "$script [working directory]"
fi

case $# in
    1) WORKDIR=$1;;
esac


typeset -A NUMBER_OF_FIT

NUMBER_OF_FIT[prefit]=12
NUMBER_OF_FIT[simplex]=12
NUMBER_OF_FIT[gandalf]=1
NUMBER_OF_FIT[energy]=1
NUMBER_OF_FIT[start]=1


for APPLICATION in prefit simplex gandalf energy start; do

    if (( 1 )); then

	set_variable  INPUT_FILE     $WORKDIR/${APPLICATION}.root
	set_variable  OUTPUT_FILE    postfit\[${APPLICATION}\].root

	if [[ ! -f $INPUT_FILE ]]; then
	    echo "Missing input file $INPUT_FILE"
	    echo "Run $JPP_DIR/examples/JFit/J${APPLICATION}.sh"
	    exit
	fi

	$JPP/JPostfit \
            -f $INPUT_FILE                        \
            -o $OUTPUT_FILE                       \
            -N ${NUMBER_OF_FIT[${APPLICATION}]}   \
	    -O ${OPTION}                          \
            -d ${DEBUG} --!

    fi
done


if (( 1 )); then

    JPlot1D \
	-f postfit\[prefit\].root:hx           \
	-f postfit\[simplex\].root:hx          \
	-f postfit\[gandalf\].root:hx          \
	-\> "angle [deg]"                      \
	-\^ "number of events [a.u]"           \
	-XX                                    \
	-L TR                                  \
	-o angle.gif

fi


if (( 1 )); then

    QUANTILES="0.33 0.5 0.66"

    for APPLICATION in prefit simplex gandalf; do

	JQuantiles2D \
            -f postfit\[${APPLICATION}\].root:h2 \
            -Q "$QUANTILES"                      \
            -o quantiles\[${APPLICATION}\].root
    done

    typeset -A LABEL
    typeset -A LOGX

    LABEL[E]="E [GeV]"
    LABEL[LINE]="E [GeV]"
    LABEL[LOGE]="E [GeV]"
    LABEL[N]="number of DOMs"

    LOGX[E]="-XX"
    LOGX[LINE]=""
    LOGX[LOGE]="-XX"
    LOGX[N]=""


    JPlot1D \
        -f quantiles\[prefit\].root:h21\.\*        \
        -f quantiles\[simplex\].root:h21\.\*       \
        -f quantiles\[gandalf\].root:h21\.\*       \
        -\> $LABEL[${OPTION}] $LOGX[${OPTION}]     \
        -\^ " [deg]"                               \
        -y "1e-2 1e1"                              \
	-Y                                         \
        -L TR                                      \
 	-o resolution.gif -d2

fi

if (( 0 )); then

    JPlot1D \
	-f postfit\[prefit].root:he                \
	-f postfit\[simplex].root:he               \
	-f postfit\[gandalf].root:he               \
	-XX                                        \
	-y "0 1.1"                                 \
	-\> "E [GeV]"                              \
	-\^"efficiency"                            \
	-L TR                                      \
	-o efficiency.gif

fi


if (( 0 )); then

    JPlot1D \
	-f postfit\[energy\].root:e0                    \
	-f postfit\[energy\].root:e1                   \
	-f postfit\[energy\].root:e2                    \
	-\^ "number of events [a.u.]"                   \
	-\> "log10(E_{fit}/GeV)"                        \
	-XX

fi

if (( 1 )); then

    JPlot2D \
	-f postfit\[energy\].root:ee                    \
	-O COLZ                                         \
	-XX -YY                                         \
	-\> "E_{true} [GeV]"                            \
	-\< "E_{fit} [GeV]"                             \
	-o ee.gif

fi

if (( 1 )); then

    JFit \
	-f postfit\[energy\].root:er                    \
        -F "[0]*exp(-0.5*(x-[1])*(x-[1])/([2]*[2]))"    \
        -@ "p0 = 0.8*GetMaximum"                        \
        -@ "p1 = 0.0"                                   \
        -@ "p2 = 0.4"                                   \
	-x "-0.8 +0.8"

    JPlot1D \
	-f fit.root:er                                  \
	-\> "log10(E_{fit}/E_{true})"                   \
	-o er.gif

fi


if (( 1 )); then

    JPlot2D \
	-f postfit\[start\].root:ha            \
	-OCOLZ                                 \
	-\> "R^{2} [m^{2}]"                    \
	-\< "z [m]"                            \

fi




if (( 0 )); then

    JPlot1D \
	-f postfit\[veto\].root:hv             \
	-\> "probability"                      \
	-\^ "number of events [a.u]"           \
	-L TR                                  \
	-o prob.gif

fi


if (( 1 )); then

    JPlot1D \
	-f postfit\[energy\].root:hz           \
	-\> "cos(#theta)"                      \
	-\^ "number of events [a.u]"           \
	-L TR                                  \
	-o zenith.gif

fi
