#!/bin/zsh

JPP_DIR=/sps/km3net/users/tgregoir/jpp/

source $JPP_DIR/setenv.sh $JPP_DIR

WORKDIR=$JPP_DIR/workdir/

if (( 1 )); then

    rm -f $WORKDIR/prefit.root
    rm -f $WORKDIR/simplex.root
    rm -f $WORKDIR/gandalf.root

fi

JPrefit.sh
JSimplex.sh
JGandalf.sh
