#!/bin/zsh

source $JPP_DIR/setenv.sh $JPP_DIR
source $JPP_DIR/zshlib.sh


set_variable  DEBUG              1
set_variable  WORKDIR            $JPP_DIR/workdir/
set_variable  DETECTOR           $WORKDIR/antares_reference.detx
set_variable  NUMBER_OF_PREFITS  1
set_variable  ROADWIDTH_M        100


JStart \
    -a $DETECTOR                 \
    -f $WORKDIR/gandalf.root     \
    -P $JPP_DATA/J%p.dat         \
    -f $WORKDIR/start.root       \
    -N ${NUMBER_OF_FIT}          \
    -R $ROADWIDTH_M              \
    -d $DEBUG --!
