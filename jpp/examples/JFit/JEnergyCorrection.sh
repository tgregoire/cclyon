#!/bin/zsh
version=1.0
script=${0##*/}

# ------------------------------------------------------------------------------------------
#
#                         Utility script to polt energy correction
#
# ------------------------------------------------------------------------------------------


if [ -z $JPP_DIR ]; then
    echo "Variable JPP_DIR undefined."
    exit
fi


source $JPP_DIR/setenv.sh $JPP_DIR
source $JPP_DIR/zshlib.sh


set_variable  DEBUG          3
set_variable  WORKDIR        $JPP_DIR/workdir/
set_variable  FCN_FILE


JEnergyCorrection \
    -E energy_correction_arca.txt \
    -o $WORKDIR/f1.root           \
    -d $DEBUG


JPlot1D \
    -f f1.root:h0        \
    -y "1e-3 5e1" -Y     \
    -XX                  \
    -\> "E [GeV]"        \
    -\^ "correction"     \
    -T "" -o e.gif
