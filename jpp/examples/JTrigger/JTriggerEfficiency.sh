#!/bin/zsh
version=1.0
script=${0##*/}

# ------------------------------------------------------------------------------------------
#
#                         Example script to run JTriggerEfficiency.
#
# ------------------------------------------------------------------------------------------


if [ -z $JPP_DIR ]; then
    echo "Variable JPP_DIR undefined."
    exit
fi


source $JPP_DIR/setenv.sh $JPP_DIR
source $JPP_DIR/zshlib.sh


set_variable  DEBUG          1
set_variable  WORKDIR        $JPP_DIR/workdir/
set_variable  INPUT_FILE     $WORKDIR/J*p.dat
set_variable  OUTPUT_FILE    $WORKDIR/trigger_efficiency+background.root
set_variable  DETECTOR       $WORKDIR/antares_reference.detx
set_variable  TRIGGER        $JPP_DATA/trigger_parameters_arca.txt
set_variable  BACKGROUND_HZ  50e3 25 2.5 0.25 0.0


if (( 1 )); then

    rm -f ${OUTPUT_FILE}
fi

JTriggerEfficiency \
    -a ${DETECTOR}                \
    -f ${INPUT_FILE}              \
    -o ${OUTPUT_FILE}             \
    -@"`cat $TRIGGER`"            \
    -@"ctMin=-1"                  \
    -O                            \
    -d ${DEBUG}                   \
    -B "$BACKGROUND_HZ"           \
    --!
