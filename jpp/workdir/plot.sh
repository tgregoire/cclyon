#!/bin/zsh
version=1.0
script=${0##*/}

# ------------------------------------------------------------------------------------------
#
#                         Utility script to plot fit results.
#
# ------------------------------------------------------------------------------------------


if [ -z $JPP_DIR ]; then
    echo "Variable JPP_DIR undefined."
    exit
fi


source $JPP_DIR/setenv.sh $JPP_DIR
source $JPP_DIR/zshlib.sh


set_variable  WORKDIR        $JPP_DIR/workdir/
set_variable  JPP            $JPP_DIR/examples/JFit/
set_variable  OPTION         N
set_variable  DEBUG          2

typeset -A NUMBER_OF_FIT

NUMBER_OF_FIT[prefit]=96
NUMBER_OF_FIT[simplex]=1
NUMBER_OF_FIT[gandalf]=1


for APPLICATION in prefit simplex gandalf; do

    if (( 1 )); then

	$JPP/JPostfit.sh \
	    -f $WORKDIR/${APPLICATION}.root       \
	    -o postfit\[${APPLICATION}\].root     \
	    -N ${NUMBER_OF_FIT[${APPLICATION}]}   \
	    -O ${OPTION}                          \
	    -d ${DEBUG}	--!
    fi

    if (( 1 )); then

	JSum1D \
	    -f postfit\[${APPLICATION}\].root:hx  \
	    -o sum\[${APPLICATION}\].root         \
	    -N
    fi

    if (( 1 )); then

	QUANTILES="0.33 0.5 0.66"

	JQuantiles2D \
	    -f postfit\[${APPLICATION}\].root:h2  \
	    -o quantiles\[${APPLICATION}\].root   \
	    -Q "$QUANTILES"
    fi
done


if (( 1 )); then

    JPlot1D \
	-f postfit\[prefit\].root:hx     \
	-f postfit\[simplex\].root:hx    \
	-f postfit\[gandalf\].root:hx    \
	-\> "angle [deg]"                \
	-\^ "number of events [a.u]"     \
	-XX                              \
	-L TR                            \
	-T "Antares"                     \
	-o angle.gif -B -y "0 1575"
fi


if (( 1 )); then

    JPlot1D \
	-f sum\[prefit\].root:hx         \
	-f sum\[simplex\].root:hx        \
	-f sum\[gandalf\].root:hx        \
	-\> "angle [deg]"                \
	-\^ "efficiency"                 \
	-XX                              \
	-L LT -G XY                      \
	-T "Antares"                     \
	-o sum.gif -B
fi


if (( 1 )); then

    typeset -A LABEL
    typeset -A LOGX

    LABEL[E]="E [GeV]"
    LABEL[LINE]="E [GeV]"
    LABEL[LOGE]="E [GeV]"
    LABEL[N]="number of DOMs"

    LOGX[E]="-XX"
    LOGX[LINE]=""
    LOGX[LOGE]="-XX"
    LOGX[N]=""

    JPlot1D \
        -f quantiles\[prefit\].root:h21\.\*     \
        -f quantiles\[simplex\].root:h21\.\*    \
        -f quantiles\[gandalf\].root:h21\.\*    \
        -\> $LABEL[${OPTION}] $LOGX[${OPTION}]  \
        -\^ " [deg]"                            \
	-x "0 100"                              \
        -y "1e-1 2e1"                           \
	-Y                                      \
        -L TR                                   \
	-T "Antares"                            \
 	-o resolution.gif -B
fi
