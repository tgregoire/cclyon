#! /usr/bin/env python

import os
from ROOT import *
from math import *
import datetime
import glob
import math
import ctypes
import array
from antares_db_utils import *
from argparse import ArgumentParser

start = datetime.datetime.now()

#load astro library for astronomical conversions
#gSystem.Load("/sps/km3net/users/galata/opt/astro/lib/libastro.so")
#antares = Astro.AntaresDetector()
#local = Astro.LocalCoords()
#eq = Astro.EqCoords()

#To submit :
#qsub -P P_antares -q long -V -l sps=1,ct=30:00:00,vmem=4G,fsize=20G -o /sps/km3net/users/tgregoir/log/selMuRej.log -j y -N selMuRej ./select_events_muonRej.py

Zcontainment = 200.
Rcontainment = 150.

#load AntDst library
gSystem.Load(os.path.expandvars("$ANTDSTROOT/lib/libAntDST.so"))
eBBFit = 1 ; eAart = 2 ; eScanFit = 3; eAAFit = 4; eBBFitMest = 5; eSeaPDF = 6; eBBFitBrightPoint = 7; 
eOSFFit = 8; eKrakeFit = 9; eGridFit = 10; eShowerQFit = 11; eShowerDusjFit = 12; eNew = 13; eShowerTinoFit = 14; 
eVertexTinoPreFit = 15; eShowerAaFit = 16; ##Strategies.h, these are not loaded with the library


e3D_SCAN = 6; eT3 = 14; #TriggerTypes.h
eElectron=11; eAntiElectron=ctypes.c_ulong(-11).value; 
eElectronNeutrino=12; eAntiElectronNeutrino=ctypes.c_ulong(-12).value; 
eMuon=13 ; eAntiMuon = ctypes.c_ulong(-13).value; 
eMuonNeutrino = 14; eAntiMuonNeutrino = ctypes.c_ulong(-14).value; 
eBrightPoint = 999 #ParticleType.h
eTriggered = 1; #Hit Type

eTinoShowerEnergy = 6; eAaShowerEnergy = 7 #EnergyRecoStrategy.h


parser = ArgumentParser(description='')
parser.add_argument('-n', dest='quantRuns', default=0, help='choice of the quantity of runs 0=one run, 1=few runs, 2=all runs')
options = parser.parse_args()

quantRuns = int(options.quantRuns)# Put 0 to have one run, 1 few runs, 2 lot of runs


n_runs_max = -1 #Put -1 to not stop
eff_vol_area = False #Put False by default and True if you want a file for effective volume or area

if quantRuns == 0:
  outfile = TFile.Open("/sps/km3net/users/tgregoir/data/ntuples/MuonRej/MuonRej_oneRun_29oct.root","recreate")
  files = glob.glob("/sps/km3net/users/tgregoir/data/recofiles/oneRun/reco_*.root")

if quantRuns == 1:
  outfile = TFile.Open("/sps/km3net/users/tgregoir/data/ntuples/MuonRej/MuonRej_fewRuns_29oct.root","recreate")
  files = glob.glob("/sps/km3net/users/tgregoir/data/recofiles/fewRuns/reco_*.root")

if quantRuns == 2:
  outfile = TFile.Open("/sps/km3net/users/tgregoir/data/ntuples/MuonRej/MuonRej_allRuns_29oct.root","recreate")
  files = glob.glob("/sps/km3net/users/tgregoir/data/recofiles/allRuns/reco_*.root")


#runID which are in /hpss/in2p3.fr/group/antares/mc/rbr/v3/gen/, used for effective_volume or area
runID = ['40225', '40227', '41789', '40220', '40223', '40229', '41780', '41543', '40098', '41540', '41546', '40793', '40553', '41548', '40799', '40768', '40944', '40762', '40763', '40765', '40766', '40233', '40231', '41781', '40238', '40786', '40784', '40783', '40780', '41711', '40788', '40795', '40790', '41839', '41836', '41835', '41830', '41226', '40926', '40606', '41828', '41822', '40161', '41731', '41826', '41352', '41696', '41737', '41231', '41233', '41234', '41237', '41699', '41161', '41692', '41691', '41690', '41697', '41353', '41166', '41769', '41847', '41845', '41222', '41367', '41224', '41228', '41584', '40884', '41369', '41173', '40177', '41171', '40171', '41177', '41365', '41175', '40880', '41567', '41560', '41563', '41569', '40545', '40741', '40962', '40189', '41217', '41214', '41212', '41597', '40169', '40167', '40160', '41164', '41578', '41684', '41444', '40777', '40775', '40772']

datapath= "MicroAntDST directory (check dict file name to get the path)"

curs = get_antares_db_cursor()

Sparkfile = open("/sps/km3net/users/tgregoir/data/SparkingRunIDs.txt", "r")
Spark_runID = set()
lines = Sparkfile.readlines()
for i in lines:
  thisline = i.split("\n")
  Spark_runID.add(thisline[0])


livetime=TVectorF(1); livetime[0]=0
apparent_livetime=TVectorF(1);apparent_livetime[0]=0

evt_nt_Tino = TNtuple("evt_nt_Tino","evt_nt_Tino","evt:lik_AA:qual_var:rec_vertexX:rec_vertexY:rec_vertexZ:rec_directionX:rec_directionY:rec_directionZ:rec_energy:mc_vertexX:mc_vertexY:mc_vertexZ:mc_directionX:mc_directionY:mc_directionZ:mc_energy:Ngen:mc_W2:mc_W3:mc_zenith:run_dur:mc_lepton_energy:declination:zenith_Shower:zenith_AA:beta_Shower:beta_AA:azimuth_err:HasAAreco:part_type:interaction")
evt_nt_Aart = TNtuple("evt_nt_Aart","evt_nt_Aart","evt:lik_AA:qual_var:rec_vertexX:rec_vertexY:rec_vertexZ:rec_directionX:rec_directionY:rec_directionZ:rec_energy:mc_vertexX:mc_vertexY:mc_vertexZ:mc_directionX:mc_directionY:mc_directionZ:mc_energy:Ngen:mc_W2:mc_W3:mc_zenith:run_dur:mc_lepton_energy:declination:zenith_Shower:zenith_AA:beta_Shower:beta_AA:azimuth_err:HasAAreco:part_type:interaction")

hit_Tino = TNtuple("hit_Tino","hit_Tino","evt:hitId:lastHit:N_hits_sel:N_ontime:dist_vertex:res_time:part_type:interaction:mc_W2:mc_W3:run_dur:rec_vertexX:rec_vertexY:rec_vertexZ:rec_energy:mc_energy:Ngen:zenith_Shower:Pos_MEst")
hit_Aart = TNtuple("hit_Aart","hit_Aart","evt:hitId:lastHit:N_hits_sel:N_ontime:dist_vertex:res_time:part_type:interaction:mc_W2:mc_W3:run_dur:rec_vertexX:rec_vertexY:rec_vertexZ:rec_energy:mc_energy:Ngen:zenith_Shower:Pos_MEst")


evt = 0
n_runs = 0
for infile in files:
  goodfiles = False
  if eff_vol_area:
    if infile.split("_")[2][1:6] in runID: #Used only for effective volume or area
      goodfile = True
  else:
    goodfile = True
  
  if os.path.exists(infile) and goodfile:### if file exists, loop in it to look for good events.

    dst     = AntDST()
    dstfile = AntDSTFile(infile)
    dstfile.SetBuffers(dst)
    tfile = TFile.Open(infile)
    antData = tfile.Get("AntData")
    data_quality = AntDataQuality()


    name = os.path.basename(infile).split("_")
    if name[1] == "MC": run_number = name[2]
    else: run_number = name[4]

    if run_number[1:5] in Spark_runID:
      continue


    info = get_run_infos(curs, run_number)
    run_dur = 10**-3*info['FrameTime']*info['NSlices'] #duree du run en secondes

    if antData == None:
      continue

    i=0
    if dstfile.HasDataQuality():
      while dstfile.ReadEvent(i)==AntDSTFile.eSuccess and dstfile.GetDataQuality(data_quality)>= 1:


	if not (dst.HasStrategy(eShowerTinoFit) and dst.GetStrategy(eShowerTinoFit).HasRecParticle(eBrightPoint)) and (dst.HasStrategy(eShowerAaFit) and dst.GetStrategy(eShowerAaFit).HasRecParticle(eBrightPoint)):
          i+=1
          continue

	has_part = 0
	part_type = 0
	interaction = -1
	mc_lepton_energy = -1
	evt += 1

	mc_W2 = dst.GetMCEvent().GetWeight_W2()
	mc_W3 = dst.GetMCEvent().GetWeight_W3()
	#print mc_W2, mc_W3
	Ngen = dst.GetMCEvent().GetNGenEvents()
	if infile.split("_")[5] == "CC.root": interaction = 1
	if infile.split("_")[5] == "NC.root": interaction = 2
	
	if infile.split("_")[3] == "nue" or infile.split("_")[3] == "anue": part_type = eElectronNeutrino
	if infile.split("_")[3] == "numu" or infile.split("_")[3] == "anumu": part_type = eMuonNeutrino
	if infile.split("_")[5] == "mupage.root": part_type = eMuon
	
	
	if dst.GetMCEvent().HasParticle(eElectronNeutrino):
          if dst.GetMCEvent().HasParticle(eElectron): mc_lepton_energy = dst.GetMCEvent().GetParticle(eElectron).GetEnergy()
#          mc_vertex = dst.GetMCEvent().GetParticle(eElectronNeutrino).GetPosition()
#          mc_direction = dst.GetMCEvent().GetParticle(eElectronNeutrino).GetAxis()
#          mc_energy = dst.GetMCEvent().GetParticle(eElectronNeutrino).GetEnergy()
#          mc_zenith = dst.GetMCEvent().GetParticle(eElectronNeutrino).GetZenith()
#          part_type = eElectronNeutrino
          has_part = 1

        if dst.GetMCEvent().HasParticle(eAntiElectronNeutrino):
          if dst.GetMCEvent().HasParticle(eAntiElectron): mc_lepton_energy = dst.GetMCEvent().GetParticle(eAntiElectron).GetEnergy()
#          mc_vertex = dst.GetMCEvent().GetParticle(eAntiElectronNeutrino).GetPosition()
#          mc_direction = dst.GetMCEvent().GetParticle(eAntiElectronNeutrino).GetAxis()
#          mc_energy = dst.GetMCEvent().GetParticle(eAntiElectronNeutrino).GetEnergy()
#          mc_zenith = dst.GetMCEvent().GetParticle(eAntiElectronNeutrino).GetZenith()
#          part_type = eAntiElectronNeutrino
          has_part = 1

	if dst.GetMCEvent().HasParticle(eMuonNeutrino):
          if dst.GetMCEvent().HasParticle(eMuon): mc_lepton_energy = dst.GetMCEvent().GetParticle(eMuon).GetEnergy()
#          mc_vertex = dst.GetMCEvent().GetParticle(eMuonNeutrino).GetPosition()
#          mc_direction = dst.GetMCEvent().GetParticle(eMuonNeutrino).GetAxis()
#          mc_energy = dst.GetMCEvent().GetParticle(eMuonNeutrino).GetEnergy()
#          mc_zenith = dst.GetMCEvent().GetParticle(eMuonNeutrino).GetZenith()
#          part_type = eMuonNeutrino
          has_part = 1

        if dst.GetMCEvent().HasParticle(eAntiMuonNeutrino):
          if dst.GetMCEvent().HasParticle(eAntiMuon): mc_lepton_energy = dst.GetMCEvent().GetParticle(eAntiMuon).GetEnergy()
#          mc_vertex = dst.GetMCEvent().GetParticle(eAntiMuonNeutrino).GetPosition()
#          mc_direction = dst.GetMCEvent().GetParticle(eAntiMuonNeutrino).GetAxis()
#          mc_energy = dst.GetMCEvent().GetParticle(eAntiMuonNeutrino).GetEnergy()
#          mc_zenith = dst.GetMCEvent().GetParticle(eAntiMuonNeutrino).GetZenith()
#          part_type = eAntiMuonNeutrino
          has_part = 1

	if dst.GetMCEvent().HasParticle(eMuon):
#          mc_lepton_energy = dst.GetMCEvent().GetParticle(eMuon).GetEnergy()
#          mc_vertex = dst.GetMCEvent().GetParticle(eMuon).GetPosition()
#          mc_direction = dst.GetMCEvent().GetParticle(eMuon).GetAxis()
#          mc_energy = dst.GetMCEvent().GetParticle(eMuon).GetEnergy()
#          mc_zenith = dst.GetMCEvent().GetParticle(eMuon).GetZenith()
#          part_type = eMuon
          has_part = 1

        if dst.GetMCEvent().HasParticle(eAntiMuon):
#          mc_lepton_energy = dst.GetMCEvent().GetParticle(eAntiMuon).GetEnergy()
#          mc_vertex = dst.GetMCEvent().GetParticle(eAntiMuon).GetPosition()
#          mc_direction = dst.GetMCEvent().GetParticle(eAntiMuon).GetAxis()
#          mc_energy = dst.GetMCEvent().GetParticle(eAntiMuon).GetEnergy()
#          mc_zenith = dst.GetMCEvent().GetParticle(eAntiMuon).GetZenith()
#          part_type = eAntiMuon
          has_part = 1

	if has_part == 1:

	  #run   = dst.GetRunId()
#	  if dst.HasStrategy(eAAFit) and dst.GetStrategy(eAAFit).HasRecParticle(eMuon):
#	    HasAAreco = True
#	    lik_AA = dst.GetStrategy(eAAFit).GetRecQuality()
#	    zenith_AA = dst.GetStrategy(eAAFit).GetRecParticle(eMuon).GetZenith()
#	    zenith_err  = dst.GetStrategy(eAAFit).GetRecParticle(eMuon).GetZenithError()
#	    azimuth_err = dst.GetStrategy(eAAFit).GetRecParticle(eMuon).GetAzimuthError()
#	    beta_AA = dst.GetStrategy(eAAFit).GetRecParticle(eMuon).GetAngularError()

#	  else:
#	    HasAAreco = False
#	    lik_AA = 0
#	    zenith_AA = 0
#	    zenith_err  = 0
#	    azimuth_err = 0
#	    beta_AA = 0

	  mcparticles = dst.GetMCEvent().GetParticles()

#	  if dst.HasStrategy(eShowerAaFit) and dst.GetStrategy(eShowerAaFit).HasRecParticle(eBrightPoint):
#	    zenith_Shower = dst.GetStrategy(eShowerAaFit).GetRecParticle(eBrightPoint).GetZenith()
#	    declination = dst.GetStrategy(eShowerAaFit).GetRecParticle(eBrightPoint).GetDeclination()
#	    beta_Shower = dst.GetStrategy(eShowerAaFit).GetRecParticle(eBrightPoint).GetAngularError()
#	    rec_vertex = dst.GetStrategy(eShowerAaFit).GetRecParticle(eBrightPoint).GetPosition()
#	    rec_direction = dst.GetStrategy(eShowerAaFit).GetRecParticle(eBrightPoint).GetAxis()
#	    rec_energy = dst.GetStrategy(eShowerAaFit).GetRecParticle(eBrightPoint).GetEnergy(eAaShowerEnergy)
#	    qual_var = dst.GetStrategy(eShowerAaFit).GetRecQuality()
#	    Pos_MEst = dst.GetStrategy(eShowerAaFit).GetAdditionalParameters().at(2) # Ne marche pas !
#
#	    if abs(rec_vertex[2])<Zcontainment and sqrt(rec_vertex[0]**2 + rec_vertex[1]**2)<Rcontainment and zenith_Shower > math.pi/2. and Pos_MEst<1000:
#	      N_ontime = 0
#	      lastHit = dst.GetNHits()
#	      for hitId in range(1,lastHit):
#		if dst.GetHit(hitId).isUsed(eShowerAaFit): # Donne toujours False !
#
#		  HitTime = dst.GetHit(hitId).GetTime()
#		  if HitTime>-20 and HitTime<60:
#		    N_ontime += 1
#		  PosHit = dst.GetHit(hitId).GetOMPosition()
#		  dist_vertex = sqrt((PosHit[0]-rec_vertex[0])**2 + (PosHit[1]-rec_vertex[1])**2 + (PosHit[2]-rec_vertex[2])**2)
#		  hit_Aart.Fill(array.array("f",[evt,hitId,lastHit,N_ontime,dist_vertex,HitTime,part_type,interaction,mc_W3,run_dur,rec_vertex[0],rec_vertex[1],rec_vertex[2],zenith_Shower,Pos_MEst]))

#            evt_nt_Aart.Fill(array.array("f",[evt,lik_AA,qual_var,rec_vertex[0],rec_vertex[1],rec_vertex[2],rec_direction[0],rec_direction[1],rec_direction[2],rec_energy,mc_vertex[0],mc_vertex[1],mc_vertex[2],mc_direction[0],mc_direction[1],mc_direction[2],mc_energy,Ngen,mc_W2,mc_W3,mc_zenith,run_dur,mc_lepton_energy,declination,zenith_Shower,zenith_AA,beta_Shower,beta_AA,azimuth_err,HasAAreco,part_type,interaction]))

	  if dst.HasStrategy(eShowerTinoFit) and dst.GetStrategy(eShowerTinoFit).HasRecParticle(eBrightPoint):
	    zenith_Shower = dst.GetStrategy(eShowerTinoFit).GetRecParticle(eBrightPoint).GetZenith()
#	    declination = dst.GetStrategy(eShowerTinoFit).GetRecParticle(eBrightPoint).GetDeclination()
#	    beta_Shower = dst.GetStrategy(eShowerTinoFit).GetRecParticle(eBrightPoint).GetAngularError()
	    rec_vertex = dst.GetStrategy(eShowerTinoFit).GetRecParticle(eBrightPoint).GetPosition()
	    rec_vertexTime = dst.GetStrategy(eShowerTinoFit).GetRecParticle(eBrightPoint).GetTime()
#	    rec_direction = dst.GetStrategy(eShowerTinoFit).GetRecParticle(eBrightPoint).GetAxis()
	    rec_energy = dst.GetStrategy(eShowerTinoFit).GetRecParticle(eBrightPoint).GetEnergy(eTinoShowerEnergy)
#	    qual_var = dst.GetStrategy(eShowerTinoFit).GetRecQuality()
	    Pos_MEst = dst.GetStrategy(eShowerTinoFit).GetAdditionalParameters().at(2)
	    
	    if abs(rec_vertex[2])<Zcontainment and sqrt(rec_vertex[0]**2 + rec_vertex[1]**2)<Rcontainment and zenith_Shower > math.pi/2. and Pos_MEst<1000:
	      N_ontime = 0
	      N_hits_sel = 0
	      
	      lastHit = dst.GetNHits()
	      for hitId in range(1,lastHit):
		if dst.GetHit(hitId).isUsed(eShowerTinoFit):

		  PosHit = dst.GetHit(hitId).GetOMPosition()
		  dist_vertex = sqrt((PosHit[0]-rec_vertex[0])**2 + (PosHit[1]-rec_vertex[1])**2 + (PosHit[2]-rec_vertex[2])**2)
		  res_time = dst.GetHit(hitId).GetTime() - dist_vertex/0.2173 - rec_vertexTime
		  if res_time>-20 and res_time<60:
		    N_ontime += 1
		  
		  if dst.GetHit(hitId).GetType() == eTriggered:
		    N_hits_sel += 1
		    hit_Tino.Fill(array.array("f",[evt,hitId,lastHit,N_hits_sel,N_ontime,dist_vertex,res_time,part_type,interaction,mc_W2,mc_W3,run_dur,rec_vertex[0],rec_vertex[1],rec_vertex[2],rec_energy,mc_lepton_energy,Ngen,zenith_Shower,Pos_MEst]))

#	    evt_nt_Tino.Fill(array.array("f",[evt,lik_AA,qual_var,rec_vertex[0],rec_vertex[1],rec_vertex[2],rec_direction[0],rec_direction[1],rec_direction[2],rec_energy,mc_vertex[0],mc_vertex[1],mc_vertex[2],mc_direction[0],mc_direction[1],mc_direction[2],mc_energy,Ngen,mc_W2,mc_W3,mc_zenith,run_dur,mc_lepton_energy,declination,zenith_Shower,zenith_AA,beta_Shower,beta_AA,azimuth_err,HasAAreco,part_type,interaction]))

	i+=1

      tfile.Close()
      del dstfile
      del dst


  n_runs+=1
  if n_runs>=n_runs_max and n_runs_max!= -1: break


#print "\nSelecting events ... "
#print "  ... creating TEntryList..."
#ch.Draw(">>sel_entry_list","(TMath::RadToDeg()*AafitFinalFit_Zenith>90)*(AafitLambdaFinalFit>"+str(MIN_LAMBDA_CUT)+")*(TMath::RadToDeg()*AafitErrorEstimateFinalFit<1.)")
#print " ok"



outfile.cd()

#evt_nt_Tino.Write()
#evt_nt_Aart.Write()
hit_Tino.Write()
hit_Aart.Write()
livetime.Write("livetime")
apparent_livetime.Write("apparent_livetime")
outfile.Close()

os.system("./MuonRej3D.py -n "+str(quantRuns)+"")


end = datetime.datetime.now()
elapsed = end-start

print "elapsed ",elapsed.seconds/60,"min", elapsed.seconds - (elapsed.seconds/60)*60, "sec"
